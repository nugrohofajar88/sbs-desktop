﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormSuratJalan : Form
    {
        public string id_admin;
        public string id_sj;
        public string tgl_sj;
        public string no_sj;
        public string ket_sj;
        public string id_spk;
        public string id_po;
        public string no_sp;
        public string customer;
        public string alamat_customer;
        public string nama_marketing;
        public string countSJ;
        public string countMonth;
        public string countYear;
        //public string telp_sj;
        //public string fax_sj;
        public string no_po_sj;

        private koneksi konek;

        private data_po selectedPO;
        
        public FormSuratJalan(string idSpk, string idPO, string idUser)
        {
            InitializeComponent();            
            konek = new koneksi();

            id_admin = idUser;
            id_spk = idSpk;
            id_po = idPO;
            no_sp = konek.getNoSP(id_po);

            //for creating no sj
            countSJ = konek.getCountSJ();
            string monthStr = DateTime.Now.ToString("MM");
            int monthInt = 1;
            int.TryParse(monthStr, out monthInt);
            countMonth = ToRoman(monthInt);
            countYear = DateTime.Now.ToString("yy");

            selectedPO = konek.getDataPO(id_po);

            reloadData();

            konek.fillAllPO(cbNoSP);
        }

        public void reloadData()
        {
            cbNoSP.Text = selectedPO.no_sp;
            konek.fillReadyPO(dataGridView1, selectedPO.id_po);
            tbCustomer.Text = selectedPO.customer;
            tbAlamat.Text = selectedPO.alamat_customer;
            tbNoPO.Text = selectedPO.no_po_customer;
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            int rowIndex = e.RowIndex;
            int columnIndex = e.ColumnIndex;
            if(columnIndex == 0 && rowIndex>=0)
            {
                dgv.Rows.RemoveAt(rowIndex);
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewColumn Mycolumn = dataGridView1.Columns[0];
            Mycolumn.Width = 30;
            
        }

        private void cbNoSP_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            selectedPO = (data_po)cb.SelectedItem;
            tbNoSJ.Text = "";
            reloadData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string no_sj = selectedPO.no_sp+"/"+ countSJ + "/SJ/SBS/" + countMonth + "/" + countYear;
            tbNoSJ.Text = no_sj;
        }

        private string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public void saveToDB()
        {
            //telp_sj = "Telp. " + tbTelp.Text;
            //fax_sj = "Fax. " + tbFax.Text;

            tgl_sj = dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm");
            no_sj = tbNoSJ.Text;
            ket_sj = tbKeterangan.Text;
            no_po_sj = tbNoPO.Text;

            id_sj = konek.addSJ(id_po,id_admin,no_sj,tgl_sj,"","",ket_sj,no_po_sj);

            foreach (DataGridViewRow Myrow in dataGridView1.Rows)
            {
                if (Myrow.Cells.Count > 1)
                {
                    string id_dt_po = Myrow.Cells["id_dt_po"].Value.ToString();
                    string qty_deliver = Myrow.Cells["QTY ORDER"].Value.ToString();
                    string kg_deliver = Myrow.Cells["KG ORDER"].Value.ToString();
                    konek.add_dt_sj(id_sj, id_dt_po, qty_deliver, kg_deliver);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveToDB();
            btnSave.Enabled = false;
            btnCetakPPN.Enabled = true;
            //btnCetakNonPPN.Enabled = true;
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int colIndex = e.ColumnIndex;

            if (dataGridView1.Columns[colIndex].Name == "QTY ORDER")
            {
                double lebar, panjang, tinggi = 0;
                decimal qtyVal = 0;
                string lStr, pStr, tStr, qtyStr = "";
                lStr = dataGridView1.Rows[rowIndex].Cells["lebar"].Value.ToString();
                pStr = dataGridView1.Rows[rowIndex].Cells["panjang"].Value.ToString();
                tStr = dataGridView1.Rows[rowIndex].Cells["tinggi"].Value.ToString();
                qtyStr = dataGridView1.Rows[rowIndex].Cells[colIndex].Value.ToString();
                double.TryParse(lStr, out lebar);
                double.TryParse(pStr, out panjang);
                double.TryParse(tStr, out tinggi);
                decimal.TryParse(qtyStr, out qtyVal);
                double kg = konek.countKg(panjang, lebar, tinggi, qtyVal);
                dataGridView1.Rows[rowIndex].Cells["KG ORDER"].Value = kg.ToString();

            }
        }

        private void btnCetakNonPPN_Click(object sender, EventArgs e)
        {
            SJReportNonPPN report = new SJReportNonPPN();

            report.Parameters["parameterIdSJ"].Value = id_sj;
            report.Parameters["parameterIdSJ"].Visible = false;

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        private void btnCetakPPN_Click(object sender, EventArgs e)
        {
            SJReportPPN report = new SJReportPPN();

            report.Parameters["parameterIdSJ"].Value = id_sj;
            report.Parameters["parameterIdSJ"].Visible = false;

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }
    }
}
