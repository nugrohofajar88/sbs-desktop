﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormPeriksaSPK : Form
    {
        string id_dt_spk = "6";
        string id_user = "";
        string id_spk = "";
        string id_po = "";
        koneksi konek;

        string mIdMesinOperation = "";
        detailSpk currentDetailSPK;
        addSpkNew currentSPKNew;

        public FormPeriksaSPK(string idSpk, string id_dtspk, string idPO)
        {
            InitializeComponent();

            konek = new koneksi();

            id_spk = idSpk;
            id_dt_spk = id_dtspk;
            id_po = idPO;

            //todo: find idmesinOperation dari id_dt_spk
            mIdMesinOperation = konek.getIdMesinOperation(id_dt_spk);
            String namaOperator = konek.getNamaOperator(mIdMesinOperation);
            label1.Text += " - (" + namaOperator + ")";

            currentDetailSPK = new detailSpk(id_dt_spk, mIdMesinOperation);
            currentDetailSPK.id_mesin_operation = mIdMesinOperation;

            lbox.Controls.Add(currentDetailSPK);
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {

            //todo : save tambahan dt_spk
            if (currentSPKNew != null)
            {
                currentSPKNew.saveToDB();
                currentSPKNew.disableEditing();
                currentSPKNew = null;                
            }

            //todo : set progress to finish
            konek.updateStatusPengerjaanDtSPK(id_dt_spk);

            this.Close();

        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            if (currentSPKNew != null)
            {
                currentSPKNew.saveToDB();
                currentSPKNew.disableEditing();
            }


            int idspk;
            int.TryParse(id_spk, out idspk);
            currentSPKNew = new addSpkNew(id_po, idspk);

            lbox.Controls.Add(currentSPKNew);
        }
    }
}
