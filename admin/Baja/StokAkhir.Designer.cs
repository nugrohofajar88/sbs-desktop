﻿namespace Baja
{
    partial class StokAkhir
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qtyA = new System.Windows.Forms.NumericUpDown();
            this.t = new System.Windows.Forms.TextBox();
            this.l = new System.Windows.Forms.TextBox();
            this.p = new System.Windows.Forms.TextBox();
            this.btnPlus = new System.Windows.Forms.Button();
            this.kgA = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.qtyA)).BeginInit();
            this.SuspendLayout();
            // 
            // qtyA
            // 
            this.qtyA.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyA.Location = new System.Drawing.Point(106, 4);
            this.qtyA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qtyA.Name = "qtyA";
            this.qtyA.Size = new System.Drawing.Size(44, 24);
            this.qtyA.TabIndex = 4;
            this.qtyA.TextChanged += new System.EventHandler(this.lptq_TextChanged);
            this.qtyA.Enter += new System.EventHandler(this.qtyO_Enter);
            // 
            // t
            // 
            this.t.Location = new System.Drawing.Point(69, 2);
            this.t.Multiline = true;
            this.t.Name = "t";
            this.t.Size = new System.Drawing.Size(35, 32);
            this.t.TabIndex = 3;
            this.t.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // l
            // 
            this.l.Location = new System.Drawing.Point(5, 2);
            this.l.Multiline = true;
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(30, 32);
            this.l.TabIndex = 1;
            this.l.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // p
            // 
            this.p.Location = new System.Drawing.Point(37, 2);
            this.p.Multiline = true;
            this.p.Name = "p";
            this.p.Size = new System.Drawing.Size(30, 32);
            this.p.TabIndex = 2;
            this.p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlus.Location = new System.Drawing.Point(207, 1);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(30, 30);
            this.btnPlus.TabIndex = 6;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            // 
            // kgA
            // 
            this.kgA.Location = new System.Drawing.Point(152, 2);
            this.kgA.Multiline = true;
            this.kgA.Name = "kgA";
            this.kgA.Size = new System.Drawing.Size(52, 32);
            this.kgA.TabIndex = 5;
            this.kgA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // StokAkhir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.kgA);
            this.Controls.Add(this.qtyA);
            this.Controls.Add(this.t);
            this.Controls.Add(this.l);
            this.Controls.Add(this.p);
            this.Controls.Add(this.btnPlus);
            this.Name = "StokAkhir";
            this.Size = new System.Drawing.Size(238, 36);
            ((System.ComponentModel.ISupportInitialize)(this.qtyA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.NumericUpDown qtyA;
        public System.Windows.Forms.TextBox t;
        public System.Windows.Forms.TextBox l;
        public System.Windows.Forms.TextBox p;
        public System.Windows.Forms.Button btnPlus;
        public System.Windows.Forms.TextBox kgA;
    }
}
