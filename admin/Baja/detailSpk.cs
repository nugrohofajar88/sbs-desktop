﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baja
{
    public partial class detailSpk : UserControl
    {
        public string id_dt_spk = "6";
        public string id_mesin_operation { get; set; }

        private dt_spk dataHeader;
        private koneksi konek;

        public List<string> listKesalahanSA = new List<string>();

        public detailSpk(string idDtSpk, string idMesinOperation)
        {
            InitializeComponent();
            konek = new koneksi();
            id_dt_spk = idDtSpk;
            id_mesin_operation = idMesinOperation;
            reloadData();

        }

        private void reloadData()
        {
            //load data header
            dataHeader = konek.getDataDtSpk(id_dt_spk);
            tbType.Text = dataHeader.type_stok_awal;
            tbUkuranAwal.Text = dataHeader.ukuran_awal;
            tbQtyAwal.Text = dataHeader.qty_awal;
            tbKgAwal.Text = dataHeader.kg_awal;
            tbUkuranOrder.Text = dataHeader.ukuran_order;
            tbQtyOrder.Text = dataHeader.qty_order;
            tbKgOrder.Text = dataHeader.kg_order;
            tbKet.Text = dataHeader.keterangan;

            //load langkah potong
            konek.getLangkahPotong(id_dt_spk, lbox, lboxWork);

            //load plan stok akhir
            konek.getPlanStokAkhir(id_dt_spk, lbox2);

            //todo load hasil operator
            konek.getRealStokAkhir(id_dt_spk, lboxWork2, CbSalahPotong_CheckStateChanged);
            
        }
        
        private void CbSalahPotong_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBox ch = (CheckBox)sender;
            string idSalah = ch.Tag.ToString();
            if(ch.Checked)
            {
                listKesalahanSA.Add(idSalah);
                if (!btnReport.Visible)
                    btnReport.Visible = true;
            }
            else if (listKesalahanSA.Contains(idSalah))
            {
                listKesalahanSA.Remove(idSalah);
                if (listKesalahanSA.Count == 0)
                    btnReport.Visible = false;
            }
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            //todo : report kesalahan
            foreach(string idStockAkhirSalah in listKesalahanSA)
            {
                konek.updateAsKesalahan(idStockAkhirSalah);
            }
            btnReport.Enabled = false;

        }
    }
}
