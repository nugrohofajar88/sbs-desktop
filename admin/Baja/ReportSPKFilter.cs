﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.XtraReports.UI;

namespace Baja
{
    public partial class ReportSPKFilter : Form
    {
        koneksi konek;

        public ReportSPKFilter()
        {
            InitializeComponent();
            InitializedFormat();
        }

        public void InitializedFormat()
        {
            konek = new koneksi();
            
            konek.selectAvailableSPKTahun(this.comboBox1);

            this.comboBox2.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string tahun = this.comboBox1.SelectedValue.ToString();
            int bulan = this.comboBox2.SelectedIndex;
            string bulanStr = this.comboBox2.Text;

            this.label2.Text = "Sedang menyiapkan data. Harap menunggu...";
            
            if (!tahun.Equals("") && !bulan.Equals(""))
            {
                ReportSPK report = new ReportSPK();
                report.Parameters["bulanStr"].Value = bulanStr + " " + tahun;
                report.Parameters["bulan"].Value = (bulan+1);
                report.Parameters["tahun"].Value = tahun;
                ReportPrintTool print = new ReportPrintTool(report);
                report.ShowPreviewDialog();

                this.label2.Text = "";
            }
            else
            {
                MessageBox.Show("Pilih periode laporan.");
            }
        }
    }
    
}
