﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormCustomer : Form
    {
        koneksi konek;
        ViewCustomer _viewCustomer;
        public string namaAdmin;
        public FormCustomer()
        {
            InitializeComponent();
            konek = new koneksi();
        }

        private void FormCustomer_Load(object sender, EventArgs e)
        {

        }

        private void btnViewSupplier_Click(object sender, EventArgs e)
        {
            if (!IsDisposed)
            {
                _viewCustomer = new ViewCustomer();
                _viewCustomer.FormClosed += _viewCustomer_FormClosed;
            }
            konek.viewCustomer(_viewCustomer.dataGridView1);
            _viewCustomer.Show();
        }

        private void _viewCustomer_FormClosed(object sender, FormClosedEventArgs e)
        {
            konek.closekoneksi();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int id_user = konek.getidAdmin(namaAdmin);
            konek.addCustomer(id_user,tbNama.Text,tbAlamat.Text,tbKet.Text,tbTelp.Text, tbCP.Text);
            reset();
        }

        public void reset()
        {
            tbNama.Text = "";
            tbAlamat.Text = "";
            tbTelp.Text = "";
            tbCP.Text = "";
            tbKet.Text = "";
        }


    }
}
