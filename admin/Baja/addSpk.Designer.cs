﻿namespace Baja
{
    partial class addSpk
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.qtyO = new System.Windows.Forms.NumericUpDown();
            this.cbO = new System.Windows.Forms.TextBox();
            this.to = new System.Windows.Forms.TextBox();
            this.lo = new System.Windows.Forms.TextBox();
            this.po = new System.Windows.Forms.TextBox();
            this.btnNewSave = new System.Windows.Forms.Button();
            this.kg = new System.Windows.Forms.Label();
            this.qty = new System.Windows.Forms.NumericUpDown();
            this.cbUk = new System.Windows.Forms.ComboBox();
            this.cb = new System.Windows.Forms.ComboBox();
            this.mesinCb = new System.Windows.Forms.ComboBox();
            this.kgO = new System.Windows.Forms.TextBox();
            this.kgA = new System.Windows.Forms.TextBox();
            this.qtyA = new System.Windows.Forms.TextBox();
            this.t = new System.Windows.Forms.TextBox();
            this.l = new System.Windows.Forms.TextBox();
            this.p = new System.Windows.Forms.TextBox();
            this.cbA = new System.Windows.Forms.TextBox();
            this.cbOUk = new System.Windows.Forms.ComboBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel2.Controls.Add(this.qtyO);
            this.panel2.Controls.Add(this.cbO);
            this.panel2.Controls.Add(this.to);
            this.panel2.Controls.Add(this.lo);
            this.panel2.Controls.Add(this.po);
            this.panel2.Controls.Add(this.btnNewSave);
            this.panel2.Controls.Add(this.kg);
            this.panel2.Controls.Add(this.qty);
            this.panel2.Controls.Add(this.cbUk);
            this.panel2.Controls.Add(this.cb);
            this.panel2.Controls.Add(this.mesinCb);
            this.panel2.Controls.Add(this.kgO);
            this.panel2.Controls.Add(this.kgA);
            this.panel2.Controls.Add(this.qtyA);
            this.panel2.Controls.Add(this.t);
            this.panel2.Controls.Add(this.l);
            this.panel2.Controls.Add(this.p);
            this.panel2.Controls.Add(this.cbA);
            this.panel2.Controls.Add(this.cbOUk);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1311, 50);
            this.panel2.TabIndex = 2;
            // 
            // qtyO
            // 
            this.qtyO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyO.Location = new System.Drawing.Point(751, 12);
            this.qtyO.Name = "qtyO";
            this.qtyO.Size = new System.Drawing.Size(44, 24);
            this.qtyO.TabIndex = 11;
            //this.qtyO.ValueChanged += new System.EventHandler(this.qtyO_ValueChanged);
            this.qtyO.TextChanged += new System.EventHandler(this.qtyO_ValueChanged);
            // 
            // cbO
            // 
            this.cbO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbO.Location = new System.Drawing.Point(427, 9);
            this.cbO.Multiline = true;
            this.cbO.Name = "cbO";
            this.cbO.Size = new System.Drawing.Size(123, 32);
            this.cbO.TabIndex = 5;
            this.cbO.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // to
            // 
            this.to.Location = new System.Drawing.Point(648, 9);
            this.to.Multiline = true;
            this.to.Name = "to";
            this.to.Size = new System.Drawing.Size(40, 32);
            this.to.TabIndex = 9;
            this.to.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.to.Visible = false;
            // 
            // lo
            // 
            this.lo.Location = new System.Drawing.Point(558, 9);
            this.lo.Multiline = true;
            this.lo.Name = "lo";
            this.lo.Size = new System.Drawing.Size(40, 32);
            this.lo.TabIndex = 7;
            this.lo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.lo.Visible = false;
            // 
            // po
            // 
            this.po.Location = new System.Drawing.Point(603, 9);
            this.po.Multiline = true;
            this.po.Name = "po";
            this.po.Size = new System.Drawing.Size(40, 32);
            this.po.TabIndex = 8;
            this.po.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.po.Visible = false;
            // 
            // btnNewSave
            // 
            this.btnNewSave.Location = new System.Drawing.Point(700, 10);
            this.btnNewSave.Name = "btnNewSave";
            this.btnNewSave.Size = new System.Drawing.Size(45, 30);
            this.btnNewSave.TabIndex = 10;
            this.btnNewSave.Text = "New";
            this.btnNewSave.UseVisualStyleBackColor = true;
            this.btnNewSave.Click += new System.EventHandler(this.btnNewSave_Click);
            // 
            // kg
            // 
            this.kg.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg.Location = new System.Drawing.Point(346, 10);
            this.kg.Name = "kg";
            this.kg.Size = new System.Drawing.Size(71, 32);
            this.kg.TabIndex = 4;
            // 
            // qty
            // 
            this.qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty.Location = new System.Drawing.Point(285, 12);
            this.qty.Name = "qty";
            this.qty.Size = new System.Drawing.Size(55, 24);
            this.qty.TabIndex = 3;
            //this.qty.ValueChanged += new System.EventHandler(this.qty_ValueChanged);
            this.qty.TextChanged += new System.EventHandler(this.qty_ValueChanged);
            // 
            // cbUk
            // 
            this.cbUk.FormattingEnabled = true;
            this.cbUk.ItemHeight = 13;
            this.cbUk.Location = new System.Drawing.Point(145, 14);
            this.cbUk.Name = "cbUk";
            this.cbUk.Size = new System.Drawing.Size(127, 21);
            this.cbUk.TabIndex = 2;
            this.cbUk.Text = "Pilih Ukuran";
            this.cbUk.SelectedIndexChanged += new System.EventHandler(this.cbUk_SelectedIndexChanged);
            // 
            // cb
            // 
            this.cb.FormattingEnabled = true;
            this.cb.ItemHeight = 13;
            this.cb.Items.AddRange(new object[] {
            ""});
            this.cb.Location = new System.Drawing.Point(12, 14);
            this.cb.Name = "cb";
            this.cb.Size = new System.Drawing.Size(127, 21);
            this.cb.TabIndex = 1;
            this.cb.Text = "Pilih Type";
            this.cb.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);
            // 
            // mesinCb
            // 
            this.mesinCb.FormattingEnabled = true;
            this.mesinCb.ItemHeight = 13;
            this.mesinCb.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.mesinCb.Location = new System.Drawing.Point(1230, 14);
            this.mesinCb.Name = "mesinCb";
            this.mesinCb.Size = new System.Drawing.Size(78, 21);
            this.mesinCb.TabIndex = 19;
            this.mesinCb.Text = "Pilih Mesin";
            this.mesinCb.SelectedIndexChanged += new System.EventHandler(this.mesinCb_SelectedIndexChanged);
            // 
            // kgO
            // 
            this.kgO.Location = new System.Drawing.Point(801, 9);
            this.kgO.Multiline = true;
            this.kgO.Name = "kgO";
            this.kgO.Size = new System.Drawing.Size(63, 30);
            this.kgO.TabIndex = 12;
            this.kgO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA
            // 
            this.kgA.Location = new System.Drawing.Point(1179, 7);
            this.kgA.Multiline = true;
            this.kgA.Name = "kgA";
            this.kgA.Size = new System.Drawing.Size(45, 32);
            this.kgA.TabIndex = 18;
            this.kgA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA
            // 
            this.qtyA.Location = new System.Drawing.Point(1133, 7);
            this.qtyA.Multiline = true;
            this.qtyA.Name = "qtyA";
            this.qtyA.Size = new System.Drawing.Size(40, 32);
            this.qtyA.TabIndex = 17;
            this.qtyA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA.TextChanged += new System.EventHandler(this.lptq_TextChanged);
            // 
            // t
            // 
            this.t.Location = new System.Drawing.Point(1087, 7);
            this.t.Multiline = true;
            this.t.Name = "t";
            this.t.Size = new System.Drawing.Size(40, 32);
            this.t.TabIndex = 16;
            this.t.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t.TextChanged += new System.EventHandler(this.lptq_TextChanged);
            // 
            // l
            // 
            this.l.Location = new System.Drawing.Point(986, 7);
            this.l.Multiline = true;
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(40, 32);
            this.l.TabIndex = 14;
            this.l.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l.TextChanged += new System.EventHandler(this.lptq_TextChanged);
            // 
            // p
            // 
            this.p.Location = new System.Drawing.Point(1031, 7);
            this.p.Multiline = true;
            this.p.Name = "p";
            this.p.Size = new System.Drawing.Size(50, 32);
            this.p.TabIndex = 15;
            this.p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p.TextChanged += new System.EventHandler(this.lptq_TextChanged);
            // 
            // cbA
            // 
            this.cbA.Location = new System.Drawing.Point(881, 7);
            this.cbA.Multiline = true;
            this.cbA.Name = "cbA";
            this.cbA.Size = new System.Drawing.Size(99, 32);
            this.cbA.TabIndex = 13;
            this.cbA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbOUk
            // 
            this.cbOUk.FormattingEnabled = true;
            this.cbOUk.ItemHeight = 13;
            this.cbOUk.Location = new System.Drawing.Point(556, 13);
            this.cbOUk.Name = "cbOUk";
            this.cbOUk.Size = new System.Drawing.Size(138, 21);
            this.cbOUk.TabIndex = 6;
            this.cbOUk.Text = "Pilih Ukuran";
            this.cbOUk.SelectedIndexChanged += new System.EventHandler(this.cbOUk_SelectedIndexChanged);
            // 
            // addSpk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Name = "addSpk";
            this.Size = new System.Drawing.Size(1311, 50);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.TextBox kgO;
        public System.Windows.Forms.TextBox cbA;
        public System.Windows.Forms.ComboBox cbOUk;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.TextBox kgA;
        public System.Windows.Forms.TextBox qtyA;
        public System.Windows.Forms.TextBox t;
        public System.Windows.Forms.TextBox l;
        public System.Windows.Forms.TextBox p;
        public System.Windows.Forms.ComboBox mesinCb;
        public System.Windows.Forms.Label kg;
        public System.Windows.Forms.NumericUpDown qty;
        public System.Windows.Forms.ComboBox cbUk;
        public System.Windows.Forms.ComboBox cb;
        private System.Windows.Forms.Button btnNewSave;
        public System.Windows.Forms.TextBox to;
        public System.Windows.Forms.TextBox lo;
        public System.Windows.Forms.TextBox po;
        public System.Windows.Forms.TextBox cbO;
        public System.Windows.Forms.NumericUpDown qtyO;
    }
}
