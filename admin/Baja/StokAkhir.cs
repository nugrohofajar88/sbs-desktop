﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class StokAkhir : UserControl
    {
        public StokAkhir()
        {
            InitializeComponent();
            
        }

        private void qtyO_Enter(object sender, EventArgs e)
        {
            NumericUpDown numUD = (NumericUpDown)sender;
            numUD.Select(0, numUD.Text.Length);
        }


        private void lptq_TextChanged(object sender, EventArgs e)
        {
            double pVal = 0, lVal = 0, tVal = 0, kgVal = 0;
            decimal qtyVal = 0;

            NumericUpDown currentTB = (NumericUpDown)sender;
            if (currentTB.Name != "qtyA" && (qtyA.Text == null || qtyA.Text.Length == 0))
                qtyA.Text = "0";

            if (p.Text != null && p.Text.Length > 0)
                pVal = int.Parse(p.Text);
            if (l.Text != null && l.Text.Length > 0)
                lVal = int.Parse(l.Text);
            if (t.Text != null && t.Text.Length > 0)
                tVal = int.Parse(t.Text);
            if (qtyA.Text != null && qtyA.Text.Length > 0)
                decimal.TryParse(qtyA.Text, out qtyVal);
            kgVal = countKg(pVal, lVal, tVal, qtyVal);
            kgA.Text = kgVal.ToString();
        }


        private double countKg(double p, double l, double t, decimal qtyVal)
        {
            int qty = (int)qtyVal;
            if (t > 0)
            {
                return (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;
            }
            else
            {
                return (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
            }
        }


    }
}
