﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormAddSpk : Form
    {
        public string no_po = "1/Y";
        public string id_po = "3";
        public int id_spk=0;
        public string userId = "1";

        koneksi konek;

        addSpk currentSPK;
        addSpkNew currentSPKNew;
        
        List<addSpk> listUC;

        //untuk menyimpan id induk, prequisite
        Dictionary<string, string> dicOrdered;
        Dictionary<string, string> dicStokAkhir;
        Dictionary<string, int> dicStokAwalQuotaUsed;


        Dictionary<string, List<ukuran>> orderedList;
        Dictionary<string, List<ukuran>> stokAkhirList;

        int locatX;
        int btnCount = 1;
        int counterDtSPK = 1;

        public FormAddSpk(string noPo, string admin)
        {
            InitializeComponent();
            konek = new koneksi();

            //-------------
            no_po = noPo;
            id_po = konek.getId_po(no_po).ToString();
            userId = konek.getidAdmin(admin).ToString();

            if (userId == "0")
                userId = konek.getidAdmin2(admin).ToString();
            //--------------------

            DateTime now = DateTime.Now;
            string formatForMySql = now.ToString("yyyy-MM-dd");
            tgl.Text = formatForMySql;

            string nama_cust = konek.getNameCustomer(no_po);
            cust.Text = nama_cust;

            konek.listOrder(dataGridView1, no_po);
            listUC = new List<addSpk>();

            dicOrdered = new Dictionary<string, string>();
            dicStokAkhir = new Dictionary<string, string>();
            orderedList = new Dictionary<string, List<ukuran>>();
            stokAkhirList = new Dictionary<string, List<ukuran>>();
            dicStokAwalQuotaUsed = new Dictionary<string, int>();
        }

        /*
        public FormAddSpk(string noPo, string admin)
        {
            InitializeComponent();

            konek = new koneksi();

            //-------------
            no_po = noPo;
            id_po = konek.getId_po(no_po).ToString();
            userId = konek.getidAdmin(admin).ToString();

            if (userId=="0")
                userId = konek.getidAdmin2(admin).ToString();
            //--------------------

            DateTime now = DateTime.Now;
            string formatForMySql = now.ToString("yyyy-MM-dd");
            tgl.Text = formatForMySql;

            string nama_cust = konek.getNameCustomer(no_po);
            cust.Text = nama_cust;

            locatX = 38;

            konek.listOrder(dataGridView1, no_po);
            listUC = new List<addSpk>();

            dicOrdered = new Dictionary<string, string>();
            dicStokAkhir = new Dictionary<string, string>();
            orderedList = new Dictionary<string, List<ukuran>>();
            stokAkhirList = new Dictionary<string, List<ukuran>>();
            dicStokAwalQuotaUsed = new Dictionary<string, int>();
        }
        */

        private void btnAddItemKosong_Click(object sender, EventArgs e)
        {
            if(id_spk == 0)
                id_spk = int.Parse(konek.addt_spk(userId, id_po, noSpk.Text, 0));

            if(currentSPKNew!=null)
            {
                currentSPKNew.saveToDB();
                currentSPKNew.disableEditing();
            }

            currentSPKNew = new addSpkNew(id_po);
            currentSPKNew.idspk = id_spk;

            lbox.Controls.Add(currentSPKNew);
            btnCount++;
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            if (currentSPK != null)
            {
                currentSPK.disableEditing();
                listUC.Add(currentSPK);

                insertStokAwalQuota();
                insertOrderedList();
                insertStokAkhirList();
            }

            currentSPK = new addSpk();
            currentSPK.Location = new Point(23, locatX);
            currentSPK.Name="btnAdd"+btnCount;
            currentSPK.cb.Name += btnCount;
            currentSPK.cbA.Name += btnCount;
            currentSPK.cbO.Name += btnCount;
            currentSPK.cbOUk.Name += btnCount;
            currentSPK.cbUk.Name += btnCount;
            currentSPK.p.Name += btnCount;
            currentSPK.l.Name += btnCount;
            currentSPK.t.Name += btnCount;
            currentSPK.kg.Name += btnCount;
            currentSPK.kgA.Name += btnCount;
            currentSPK.kgO.Name += btnCount;
            currentSPK.qty.Name += btnCount;
            currentSPK.qtyA.Name += btnCount;
            currentSPK.qtyO.Name += btnCount;
            currentSPK.idUserControl = counterDtSPK;
            currentSPK.id_po = id_po;
            currentSPK.no_po = no_po;
            counterDtSPK++;

            currentSPK.mapOrderedList = orderedList;
            currentSPK.mapStokAkhirList = stokAkhirList;
            currentSPK.mapStokAwalUsedQuota = dicStokAwalQuotaUsed;
         
            locatX = locatX + 40;

            lbox.Controls.Add(currentSPK);
            btnCount++;
        }

        private void insertStokAwalQuota()
        {
            if (!dicStokAwalQuotaUsed.ContainsKey(currentSPK.selectedType.id_type+"-"+currentSPK.selectedUkuranAwal.id_ukuran))
            {
                dicStokAwalQuotaUsed.Add(currentSPK.selectedType.id_type + "-" + currentSPK.selectedUkuranAwal.id_ukuran, (int)currentSPK.qty.Value);
            }
            else
            {

                dicStokAwalQuotaUsed[currentSPK.selectedType.id_type + "-" + currentSPK.selectedUkuranAwal.id_ukuran] += (int)currentSPK.qty.Value;
            }
        }

        private void insertOrderedList()
        {
            if (!orderedList.ContainsKey(currentSPK.selectedType.tipe))
            {
                List<ukuran> newUkuranList = new List<ukuran>();
                newUkuranList.Add(currentSPK.selectedUkuranOrder);
                orderedList.Add(currentSPK.selectedType.tipe, newUkuranList);
            }
            else
            {
                List<ukuran> newUkuranList = orderedList[currentSPK.selectedType.tipe];

                ukuran candidatUkuran = currentSPK.selectedUkuranOrder;
                int indexRemoved = -1;
                int i = 0;
                foreach (ukuran ukur in newUkuranList)
                {
                    if (ukur.nama_ukuran == candidatUkuran.nama_ukuran)
                    {
                        candidatUkuran.max_qty += ukur.max_qty;
                        indexRemoved = i;
                        break;
                    }
                    i++;
                }
                if (indexRemoved != -1)
                    newUkuranList.RemoveAt(indexRemoved);
                newUkuranList.Add(candidatUkuran);
                orderedList[currentSPK.selectedType.tipe] = newUkuranList;
            }
        }
        private void insertStokAkhirList()
        {
            if (currentSPK.selectedUkuranAkhir == null)
                return;

            if (!stokAkhirList.ContainsKey(currentSPK.selectedType.tipe))
            {
                List<ukuran> newUkuranList = new List<ukuran>();
                newUkuranList.Add(currentSPK.selectedUkuranAkhir);
                stokAkhirList.Add(currentSPK.selectedType.tipe, newUkuranList);
            }
            else
            {
                List<ukuran> newUkuranList = stokAkhirList[currentSPK.selectedType.tipe];

                ukuran candidatUkuran = currentSPK.selectedUkuranAkhir;
                int indexRemoved = -1;
                int i = 0;
                foreach (ukuran ukur in newUkuranList)
                {
                    if (ukur.nama_ukuran == candidatUkuran.nama_ukuran)
                    {
                        candidatUkuran.max_qty += ukur.max_qty;
                        indexRemoved = i;
                        break;
                    }
                    i++;
                }
                if (indexRemoved != -1)
                    newUkuranList.RemoveAt(indexRemoved);
                newUkuranList.Add(candidatUkuran);
                stokAkhirList[currentSPK.selectedType.tipe] = newUkuranList;
            }
        }


        private void btnOkBARU_Click(object sender, EventArgs e)
        {
            if (currentSPKNew != null)
            {
                if (id_spk == 0)
                    id_spk = int.Parse(konek.addt_spk(userId, id_po, noSpk.Text, 0));

                currentSPKNew.saveToDB();
                currentSPKNew.disableEditing();
                currentSPKNew = null;

                konek.updatePO(no_po, 1);
            }

            this.Close();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (currentSPK != null)
            {
                currentSPK.disableEditing();
                listUC.Add(currentSPK);

                id_spk = int.Parse(konek.addt_spk(userId, id_po, noSpk.Text,0));

                int i = 0;
                int jumStatus = 0;
                foreach (addSpk ucDtSpk in listUC)
                {
                    int status = 0;
                    bool isUseStokAkhir = false;
                    ucDtSpk.idspk = id_spk;
                    string idInserted = "NULL";
                    string prequisite = "NULL";
                    if (i==0)
                    {
                        if (ucDtSpk.selectedUkuranAwal.nama_ukuran == ucDtSpk.selectedUkuranOrder.nama_ukuran)
                            status = 1;
                        idInserted = ucDtSpk.doInsertToDB(prequisite, status, isUseStokAkhir);
                    }
                    else
                    {
                        string key = ucDtSpk.selectedType.tipe + "-" + ucDtSpk.selectedUkuranAwal.nama_ukuran;
                        if(dicOrdered.ContainsKey(key))
                        {
                            prequisite = dicOrdered[key];
                            status = -1;
                        }else if (dicStokAkhir.ContainsKey(key))
                        {
                            prequisite = dicStokAkhir[key];
                            status = -1;
                            isUseStokAkhir = true;
                        }else
                        {
                            if (ucDtSpk.selectedUkuranAwal.nama_ukuran == ucDtSpk.selectedUkuranOrder.nama_ukuran)
                                status = 1;
                        }

                        idInserted = ucDtSpk.doInsertToDB(prequisite, status, isUseStokAkhir);
                    }

                    //insert data induk stokOrder
                    string keyOrd = ucDtSpk.selectedType.tipe + "-" + ucDtSpk.selectedUkuranOrder.nama_ukuran;
                    if (!dicOrdered.ContainsKey(keyOrd))
                    {
                        dicOrdered.Add(keyOrd, idInserted);
                    }else
                    {
                        dicOrdered[keyOrd] = idInserted;
                    }
                    //insert data induk stokAkhir
                    if(ucDtSpk.selectedUkuranAkhir!=null)
                    {
                        string keyStokAkhir = ucDtSpk.selectedType.tipe + "-" + ucDtSpk.selectedUkuranAkhir.nama_ukuran;
                        if (!dicStokAkhir.ContainsKey(keyStokAkhir))
                        {
                            dicStokAkhir.Add(keyStokAkhir, idInserted);
                        }
                        else
                        {
                            dicStokAkhir[keyStokAkhir] = idInserted;
                        }

                    }


                    jumStatus += status;
                    i++;
                }

                currentSPK = null;


                if (jumStatus == listUC.Count)
                    konek.setFinishSPK(id_spk);


                konek.updatePO(no_po, 1);
            }

            this.Close();

        }
    }
}
