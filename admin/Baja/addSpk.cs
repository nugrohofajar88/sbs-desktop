﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class addSpk : UserControl
    {
        public string no_po = "PO/BAT/XI/3001/16";
        public string id_po = "6";

        koneksi konek;


        public int idspk { get; set; }
        public int idUserControl { get; set; }

        public type selectedType { get; set; }
        public ukuran selectedUkuranAwal { get; set; }
        public ukuran selectedUkuranOrder { get; set; }
        public ukuran selectedUkuranAkhir { get; set; }
        public string selectedMesin { get; set; }
        public string insertedId { get; set; }
        public string similiarIDPO { get; set; }

        public Dictionary<string, List<ukuran>> mapOrderedList;
        public Dictionary<string, List<ukuran>> mapStokAkhirList;

        public Dictionary<string, int> mapStokAwalUsedQuota;

        private string divider = "--------------";

        public addSpk()
        {
            InitializeComponent();
            konek = new koneksi();

            reloadData();
            selectedMesin = "NULL";
        }

        private void reloadData()
        {
            konek.selectAllAvailableBrgType(cb);
        }

        public void disableEditing()
        {
            //setelah add item baru, maka tidak bisa diedit
            mesinCb.Enabled = false;
            kgO.Enabled = false;
            qtyO.Enabled = false;
            kgA.Enabled = false;
            qtyA.Enabled = false;
            t.Enabled = false;
            l.Enabled = false;
            p.Enabled = false;
            cbA.Enabled = false;
            cbOUk.Enabled = false;
            cbO.Enabled = false;
            kg.Enabled = false;
            qty.Enabled = false;
            cbUk.Enabled = false;
            cb.Enabled = false;
            btnNewSave.Enabled = false;
            to.Enabled = false;
            lo.Enabled = false;
            po.Enabled = false;

            //set idStock Order and idStock Akhir
            int intIdStockOrder = konek.getIdStock(selectedType.id_type, selectedUkuranOrder.id_ukuran);
            selectedUkuranOrder.id_stock = intIdStockOrder.ToString();

            if(l.Text!=null && l.Text.Length>0 && p.Text!=null && p.Text.Length>0)
            {
                selectedUkuranAkhir = konek.getUkuran(l.Text, p.Text, t.Text);
                int intIdStockAkhir = konek.getIdStock(selectedType.id_type, selectedUkuranAkhir.id_ukuran);
                selectedUkuranAkhir.id_stock = intIdStockAkhir.ToString();
            }else
            {
                selectedUkuranAkhir = null;
            }

            int countPO_Same_name = konek.countPOWithSameSize(id_po, selectedUkuranOrder.id_ukuran.ToString());
            if(countPO_Same_name>0)
            {
                similiarIDPO = konek.findPOWithSameSize(id_po, selectedUkuranOrder.id_ukuran.ToString(), selectedType.tipe);
            }else
            {
                similiarIDPO = "NULL";
            }
            Console.WriteLine("similiar id_dt_po = " + similiarIDPO);


        }

        private void btnNewSave_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if(btn.Text.ToLower().Equals("new"))
            {
                btn.Text = "Save";
                lo.Visible = true;
                po.Visible = true;
                to.Visible = true;
                cbOUk.Visible = false;
            }
            else
            {
                btn.Text = "New";
                lo.Visible = false;
                po.Visible = false;
                to.Visible = false;
                cbOUk.Visible = true;

                //insert new ukuran 
                string newNamaUkuran = grupUkuran(po, lo, to);
                konek.getUkuran(lo.Text, po.Text, to.Text);


                //infalidate cbukuran with updated values
                konek.getAllUkuran(cbOUk, id_po);
                cbOUk.SelectedIndex = findUkuran(cbOUk, lo.Text, po.Text, to.Text);
            }
        }

        public string grupUkuran(TextBox p, TextBox l, TextBox t)
        {
            string gabung;
            if (t.Text.Length > 0)
                gabung = l.Text + " x " + p.Text + " x " + t.Text;
            else
                gabung = l.Text + " x " + p.Text;

            return gabung;
        }

        private double countKg(double p, double l, double t, decimal qtyVal)
        {
            int qty = (int)qtyVal;
            if (t > 0)
            {
                return (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;
            }
            else
            {
                return (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
            }
        }

        private int findUkuran(ComboBox combo, string lStr, string pStr, string tStr)
        {
            int hasil = 0;
            int l = int.Parse(lStr);
            int p = int.Parse(pStr);
            int t = 0;
            int tryT = 0;
            
            if (int.TryParse(tStr, out tryT))
                t = tryT;

            int i = 0;
            foreach(ukuran item in combo.Items)
            {
                if(item.lebar == l && item.panjang == p && item.tinggi == t )
                {
                    hasil = i;
                    break;
                }
                i++;
            }

            return hasil;
        }

        private void cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedType = (type)((ComboBox)sender).SelectedItem;
            cbO.Text = selectedType.ToString();
            cbA.Text = selectedType.ToString();

            //filling ukuran awal
            konek.getAvailableUkuran(cbUk, selectedType.id_type.ToString(), id_po, mapStokAwalUsedQuota);
            //filling from orderedList
            if(mapOrderedList!=null && mapOrderedList.Count>0)
            {
                if(mapOrderedList.ContainsKey(selectedType.tipe))
                {
                    cbUk.Items.Add(divider);
                    foreach (ukuran m_ukuran in mapOrderedList[selectedType.tipe])
                    {
                        cbUk.Items.Add(m_ukuran);
                    }
                }
                
            }

            //filling from stokAkhirList
            if (mapStokAkhirList != null && mapStokAkhirList.Count > 0)
            {
                if (mapStokAkhirList.ContainsKey(selectedType.tipe))
                {
                    if (mapStokAkhirList[selectedType.tipe].Count>0)
                    {
                        cbUk.Items.Add(divider);
                        foreach (ukuran m_ukuran in mapStokAkhirList[selectedType.tipe])
                        {
                            cbUk.Items.Add(m_ukuran);
                        }
                    }
                    
                }

            }


            //filling ukuran order with all kind of ukuran, but greater than minimum Ukuran from id_po
            konek.getAllUkuran(cbOUk, id_po);

        }

        private void cbUk_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.Text == divider)
                return;
            selectedUkuranAwal = (ukuran)combo.SelectedItem;
            qty.Maximum = selectedUkuranAwal.max_qty;

        }

        private void qty_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numQty = (NumericUpDown)sender;
            double kgVal = 0;
            if (selectedUkuranAwal != null)
                kgVal = countKg(selectedUkuranAwal.panjang, selectedUkuranAwal.lebar, selectedUkuranAwal.tinggi, numQty.Value);
            kg.Text = kgVal.ToString();
        }

        private void cbOUk_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedUkuranOrder = (ukuran)((ComboBox)sender).SelectedItem;
        }

        private void qtyO_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numQty = (NumericUpDown)sender;
            double kgVal = 0;
            if (selectedUkuranOrder != null)
            {
                kgVal = countKg(selectedUkuranOrder.panjang, selectedUkuranOrder.lebar, selectedUkuranOrder.tinggi, numQty.Value);
                selectedUkuranOrder.max_qty = (int)numQty.Value;
            }
                
            kgO.Text = kgVal.ToString();
        }

        private void lptq_TextChanged(object sender, EventArgs e)
        {
            double pVal = 0, lVal = 0, tVal = 0, kgVal = 0;
            decimal qtyVal = 0;

            TextBox currentTB = (TextBox)sender;
            if (currentTB.Name!="qtyA" && (qtyA.Text == null || qtyA.Text.Length == 0))
                qtyA.Text = "0";

            if (p.Text != null && p.Text.Length > 0)
                pVal = int.Parse(p.Text);
            if (l.Text != null && l.Text.Length > 0)
                lVal = int.Parse(l.Text);
            if (t.Text != null && t.Text.Length > 0)
                tVal = int.Parse(t.Text);
            if (qtyA.Text != null && qtyA.Text.Length > 0)
                decimal.TryParse(qtyA.Text, out qtyVal);
            kgVal = countKg(pVal, lVal, tVal, qtyVal); 
            kgA.Text = kgVal.ToString();
        }

        private void mesinCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            selectedMesin = combo.Text.ToString();
        }

        public string doInsertToDB(string prequisite, int status, bool isUsingStokAkhir)
        {
            string hasil = "";
            if (selectedUkuranAkhir != null)
                hasil = konek.adddt_spk( idspk, int.Parse(selectedUkuranAwal.id_stock), int.Parse(selectedUkuranOrder.id_stock), 
                    int.Parse(selectedUkuranAkhir.id_stock), qty.Value.ToString(), qtyO.Value.ToString(), qtyA.Text, status, kg.Text,
                    kgO.Text, kgA.Text, selectedMesin, prequisite);
            else
            {
                hasil = konek.adddt_spk(idspk, int.Parse(selectedUkuranAwal.id_stock), int.Parse(selectedUkuranOrder.id_stock),
                    -1, qty.Value.ToString(), qtyO.Value.ToString(), qtyA.Text, status, kg.Text,
                    kgO.Text, kgA.Text, selectedMesin, prequisite);
            }
                
            
            if(prequisite!="NULL")
                konek.decreseStock(int.Parse(selectedUkuranAwal.id_stock), qty.Value.ToString());

            insertedId = hasil;
            //filling id_dt_po for surat jalan
            konek.setDTPO(insertedId, similiarIDPO);

            if (isUsingStokAkhir)
                konek.setIsStokAkhirUsed(prequisite);

            return hasil;
        }
    }
}
