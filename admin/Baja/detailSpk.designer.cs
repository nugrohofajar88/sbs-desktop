﻿namespace Baja
{
    partial class detailSpk
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbKet = new System.Windows.Forms.TextBox();
            this.tbKgAwal = new System.Windows.Forms.TextBox();
            this.lbox2 = new System.Windows.Forms.FlowLayoutPanel();
            this.lbox = new System.Windows.Forms.FlowLayoutPanel();
            this.lboxWork = new System.Windows.Forms.FlowLayoutPanel();
            this.lboxWork2 = new System.Windows.Forms.FlowLayoutPanel();
            this.tbType = new System.Windows.Forms.TextBox();
            this.tbUkuranAwal = new System.Windows.Forms.TextBox();
            this.tbQtyAwal = new System.Windows.Forms.TextBox();
            this.tbUkuranOrder = new System.Windows.Forms.TextBox();
            this.tbQtyOrder = new System.Windows.Forms.TextBox();
            this.tbKgOrder = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbKet
            // 
            this.tbKet.Location = new System.Drawing.Point(1196, 4);
            this.tbKet.Multiline = true;
            this.tbKet.Name = "tbKet";
            this.tbKet.Size = new System.Drawing.Size(110, 116);
            this.tbKet.TabIndex = 24;
            // 
            // tbKgAwal
            // 
            this.tbKgAwal.Location = new System.Drawing.Point(330, 9);
            this.tbKgAwal.Multiline = true;
            this.tbKgAwal.Name = "tbKgAwal";
            this.tbKgAwal.ReadOnly = true;
            this.tbKgAwal.Size = new System.Drawing.Size(58, 30);
            this.tbKgAwal.TabIndex = 17;
            this.tbKgAwal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbox2
            // 
            this.lbox2.AutoScroll = true;
            this.lbox2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lbox2.Location = new System.Drawing.Point(916, 4);
            this.lbox2.Name = "lbox2";
            this.lbox2.Size = new System.Drawing.Size(262, 152);
            this.lbox2.TabIndex = 22;
            // 
            // lbox
            // 
            this.lbox.AutoScroll = true;
            this.lbox.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lbox.Location = new System.Drawing.Point(641, 3);
            this.lbox.Name = "lbox";
            this.lbox.Size = new System.Drawing.Size(262, 153);
            this.lbox.TabIndex = 21;
            // 
            // lboxWork
            // 
            this.lboxWork.AutoScroll = true;
            this.lboxWork.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lboxWork.Location = new System.Drawing.Point(640, 222);
            this.lboxWork.Name = "lboxWork";
            this.lboxWork.Size = new System.Drawing.Size(262, 155);
            this.lboxWork.TabIndex = 25;
            // 
            // lboxWork2
            // 
            this.lboxWork2.AutoScroll = true;
            this.lboxWork2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lboxWork2.Location = new System.Drawing.Point(916, 222);
            this.lboxWork2.Name = "lboxWork2";
            this.lboxWork2.Size = new System.Drawing.Size(357, 155);
            this.lboxWork2.TabIndex = 26;
            // 
            // tbType
            // 
            this.tbType.Location = new System.Drawing.Point(4, 9);
            this.tbType.Multiline = true;
            this.tbType.Name = "tbType";
            this.tbType.ReadOnly = true;
            this.tbType.Size = new System.Drawing.Size(147, 30);
            this.tbType.TabIndex = 1;
            this.tbType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUkuranAwal
            // 
            this.tbUkuranAwal.Location = new System.Drawing.Point(152, 9);
            this.tbUkuranAwal.Multiline = true;
            this.tbUkuranAwal.Name = "tbUkuranAwal";
            this.tbUkuranAwal.ReadOnly = true;
            this.tbUkuranAwal.Size = new System.Drawing.Size(137, 30);
            this.tbUkuranAwal.TabIndex = 2;
            this.tbUkuranAwal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbQtyAwal
            // 
            this.tbQtyAwal.Location = new System.Drawing.Point(290, 9);
            this.tbQtyAwal.Multiline = true;
            this.tbQtyAwal.Name = "tbQtyAwal";
            this.tbQtyAwal.ReadOnly = true;
            this.tbQtyAwal.Size = new System.Drawing.Size(39, 30);
            this.tbQtyAwal.TabIndex = 3;
            this.tbQtyAwal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbUkuranOrder
            // 
            this.tbUkuranOrder.Location = new System.Drawing.Point(397, 9);
            this.tbUkuranOrder.Multiline = true;
            this.tbUkuranOrder.Name = "tbUkuranOrder";
            this.tbUkuranOrder.ReadOnly = true;
            this.tbUkuranOrder.Size = new System.Drawing.Size(137, 30);
            this.tbUkuranOrder.TabIndex = 30;
            this.tbUkuranOrder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbQtyOrder
            // 
            this.tbQtyOrder.Location = new System.Drawing.Point(536, 9);
            this.tbQtyOrder.Multiline = true;
            this.tbQtyOrder.Name = "tbQtyOrder";
            this.tbQtyOrder.ReadOnly = true;
            this.tbQtyOrder.Size = new System.Drawing.Size(39, 30);
            this.tbQtyOrder.TabIndex = 31;
            this.tbQtyOrder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbKgOrder
            // 
            this.tbKgOrder.Location = new System.Drawing.Point(577, 9);
            this.tbKgOrder.Multiline = true;
            this.tbKgOrder.Name = "tbKgOrder";
            this.tbKgOrder.ReadOnly = true;
            this.tbKgOrder.Size = new System.Drawing.Size(54, 30);
            this.tbKgOrder.TabIndex = 32;
            this.tbKgOrder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Location = new System.Drawing.Point(1161, 193);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 23);
            this.label20.TabIndex = 124;
            this.label20.Text = "+";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label21.Location = new System.Drawing.Point(1103, 193);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 23);
            this.label21.TabIndex = 128;
            this.label21.Text = "KG";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Location = new System.Drawing.Point(1058, 193);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 23);
            this.label22.TabIndex = 125;
            this.label22.Text = "QTY";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.Location = new System.Drawing.Point(1012, 193);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 23);
            this.label23.TabIndex = 127;
            this.label23.Text = "T";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Location = new System.Drawing.Point(966, 193);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 23);
            this.label24.TabIndex = 126;
            this.label24.Text = "P";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Location = new System.Drawing.Point(916, 193);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 23);
            this.label25.TabIndex = 123;
            this.label25.Text = "L";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Location = new System.Drawing.Point(821, 193);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 23);
            this.label18.TabIndex = 122;
            this.label18.Text = "KG";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label17.Location = new System.Drawing.Point(781, 193);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 23);
            this.label17.TabIndex = 119;
            this.label17.Text = "QTY";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Location = new System.Drawing.Point(735, 193);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 23);
            this.label16.TabIndex = 121;
            this.label16.Text = "T";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Location = new System.Drawing.Point(696, 193);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 23);
            this.label15.TabIndex = 120;
            this.label15.Text = "P";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Location = new System.Drawing.Point(651, 193);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 23);
            this.label14.TabIndex = 117;
            this.label14.Text = "L";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(916, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(321, 23);
            this.label4.TabIndex = 116;
            this.label4.Text = "OPERATOR - STOK AKHIR";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(651, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 23);
            this.label3.TabIndex = 115;
            this.label3.Text = "OPERATOR - LANGKAH POTONG";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnReport
            // 
            this.btnReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.Location = new System.Drawing.Point(914, 378);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(361, 41);
            this.btnReport.TabIndex = 141;
            this.btnReport.Text = "SET KESALAHAN";
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Visible = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // detailSpk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbKgOrder);
            this.Controls.Add(this.tbQtyOrder);
            this.Controls.Add(this.tbUkuranOrder);
            this.Controls.Add(this.tbQtyAwal);
            this.Controls.Add(this.tbUkuranAwal);
            this.Controls.Add(this.tbType);
            this.Controls.Add(this.lboxWork2);
            this.Controls.Add(this.lboxWork);
            this.Controls.Add(this.tbKet);
            this.Controls.Add(this.tbKgAwal);
            this.Controls.Add(this.lbox2);
            this.Controls.Add(this.lbox);
            this.Name = "detailSpk";
            this.Size = new System.Drawing.Size(1325, 427);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox tbKet;
        public System.Windows.Forms.TextBox tbKgAwal;
        private System.Windows.Forms.FlowLayoutPanel lbox2;
        private System.Windows.Forms.FlowLayoutPanel lbox;
        private System.Windows.Forms.FlowLayoutPanel lboxWork;
        private System.Windows.Forms.FlowLayoutPanel lboxWork2;
        public System.Windows.Forms.TextBox tbType;
        public System.Windows.Forms.TextBox tbUkuranAwal;
        public System.Windows.Forms.TextBox tbQtyAwal;
        public System.Windows.Forms.TextBox tbUkuranOrder;
        public System.Windows.Forms.TextBox tbQtyOrder;
        public System.Windows.Forms.TextBox tbKgOrder;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnReport;
    }
}
