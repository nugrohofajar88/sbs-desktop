﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using System.Net.NetworkInformation;
using System.Net;

namespace Baja
{
    public partial class FormAdmin : Form
    {
        koneksi konek;
        FormSupplier _formSupplier;
        FormUser _formUser;
        FormStock _formStock;
        FormCustomer _formCustomer;
        FormCekReady _formCekReady;
        ViewSJ _viewSJ;
        ViewSPB _viewSPB;
        FormLoading _formLoading;
        FormSPKFinish _formSPKFinish;

        // simpan dta
        public string admin;
        public string id_admin;

        //close form
        public FormLogin _formlogin;

        bool pending = false;
        string no_Po_Pending = "";

        String selectedCetakNoSPK = "";
        String selected_IdSPK = "";
        String selected_IdDtSPK = "";
        String selected_IdPO = "";

        DataTable dTable;
        DataTable dt = null;
        int pageIndex, pageSize = 100;
        bool inProcessed = false;
        bool oninet = false;

        public FormAdmin(String username)
        {
            InitializeComponent();
            konek = new koneksi();

            //_formspk = new FormSPK();
            _formSupplier = new FormSupplier();
            _formUser = new FormUser();

            this.admin = username;
            id_admin = konek.getidAdmin2(admin).ToString();
            oninet = checkConnection();
        }

        private void FormAdmin_Load_1(object sender, EventArgs e)
        {
            tbnamalogin.Text = admin;

        }

        private void timernotif_Tick(object sender, EventArgs e)
        {
            _formLoading = new FormLoading();
            _formLoading.Show();
            fillNotif();

            //backgroundWorker.RunWorkerAsync();
            timernotif.Enabled = false;

        }

        private void fillNotif()
        {
            lbox.Controls.Clear();
            int locatX = 38;

            try
            {
                konek.openkoneksi();
                MySqlCommand dbcmd = konek.db.CreateCommand();
                string sql = "select no_po, keterangan from t_po where status_po ='0' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        Button btn = new Button();
                        btn.Location = new Point(23, locatX);
                        btn.Height = 30;
                        btn.Width = 300;
                        string no_po = reader.GetString(0).ToString().ToUpper();
                        string keterangan_po = (!reader.GetString(1).ToString().ToUpper().Equals("")) ? " - " + reader.GetString(1).ToString().ToUpper() : "";
                        btn.Text = "PO" + no_po + keterangan_po;
                        btn.Name = "btn" + reader.GetString(0).ToString().ToUpper();
                        btn.Tag = reader.GetString(0).ToString().ToUpper();
                        btn.BackColor = Color.Blue;
                        btn.ForeColor = Color.White;
                        btn.Click += Btn_Click;

                        locatX = locatX + 40;
                        lbox.Controls.Add(btn);
                    }

                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (konek.db != null)
                {
                    konek.closekoneksi();
                }
            }
        }

        public bool checkConnection()
        {
            string url = "http://api.developer.web.id/api/information/application_settings/id/1";
            try
            {
                System.Net.WebRequest myRequest = System.Net.WebRequest.Create(url);
                System.Net.WebResponse myResponse = myRequest.GetResponse();
            }
            catch (System.Net.WebException)
            {
                return false;
            }

            using (WebClient wc = new WebClient())
            {
                var json    = wc.DownloadString(url);
            }
            return true;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            Button newButton = sender as Button;
            string _tag = newButton.Tag.ToString();
            _formCekReady = new FormCekReady(_tag, tbnamalogin.Text);
            _formCekReady.FormClosed += _formCekReady_FormClosed;
            _formCekReady.btnPending.Click += BtnPending_Click;
            _formCekReady.Show();

            timernotif.Enabled = false;

        }

        private void BtnPending_Click(object sender, EventArgs e)
        {
            pending = true;
            no_Po_Pending = _formCekReady.no_po;
        }

        private void _formCekReady_FormClosed(object sender, FormClosedEventArgs e)
        {
            timernotif.Enabled = true;
        }

        private void btnAddSuplier_Click(object sender, EventArgs e)
        {
            _formSupplier = new FormSupplier();
            _formSupplier.namaAdmin = tbnamalogin.Text;
            _formSupplier.Show();
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            _formUser = new FormUser();
            _formUser.admin = tbnamalogin.Text;
            _formUser.Show();
        }

        private void btnAddStock_Click(object sender, EventArgs e)
        {
            _formStock = new FormStock();
            _formStock.Show();
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            _formCustomer = new FormCustomer();
            _formCustomer.namaAdmin = tbnamalogin.Text;
            _formCustomer.Show();
        }

        public void viewStatus()
        {
            //konek.viewReportNoInvoice(dgv);
            //konek.viewReport(dgv);
            konek.viewReportDtSPK(dgv);
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow Myrow in dgv.Rows)
            {
                if (Myrow.Cells.Count > 5)
                {
                    Object status = Myrow.Cells["status"].Value;
                    if (status != null)
                    {
                        string strStatus = status.ToString().ToLower();
                        if (strStatus.Equals("antrian"))
                        {
                            Myrow.Cells["status"].Style.BackColor = Color.Red;
                        }
                        else if (strStatus.Equals("progress"))
                        {
                            Myrow.Cells["status"].Style.BackColor = Color.Green;
                        }
                        else if (strStatus.Equals("finish"))
                        {
                            Myrow.Cells["status"].Style.BackColor = Color.Yellow;
                        }
                    }
                }
            }
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgv.Rows[rowIndex];
            if (row.Cells[0].Value == null)
                return;
            String statusSelected = row.Cells["status"].Value.ToString();
            //if (statusSelected.ToLower().Equals("finish"))
            //{
            //    selectedCetakNoSPK = row.Cells["spk"].Value.ToString();
            //    btnCetak.Enabled = true;
            //}
            //else
            //{
            //    btnCetak.Enabled = false;
            //}
            selectedCetakNoSPK = row.Cells["id_spk"].Value.ToString();
            selected_IdSPK = row.Cells["id_spk"].Value.ToString();
            selected_IdDtSPK = row.Cells["dt spk"].Value.ToString();
            selected_IdPO = row.Cells["id_po"].Value.ToString();

            btnCetak.Enabled = true;
            btnBuatSPB.Enabled = true;

        }

        private void btnCetak_Click(object sender, EventArgs e)
        {

            //InvoiceReport report = new InvoiceReport();

            //report.Parameters["parameterNoSPK"].Value = selectedCetakNoSPK;
            //report.Parameters["parameterNoSPK"].Visible = false;

            //ReportPrintTool printTool = new ReportPrintTool(report);
            //printTool.ShowPreviewDialog();
            FormSuratJalan _formSuratJalan = new FormSuratJalan(selected_IdSPK, selected_IdPO, id_admin);
            _formSuratJalan.FormClosed += _formSuratJalan_closed;
            _formSuratJalan.Show();

            timernotif.Enabled = false;

        }


        private void btnBuatSPB_Click(object sender, EventArgs e)
        {
            FormSuratPengirimanBarang _formSuratPengirimanBarang = new FormSuratPengirimanBarang(selected_IdSPK, selected_IdPO, id_admin);
            _formSuratPengirimanBarang.FormClosed += _formSuratJalan_closed;
            _formSuratPengirimanBarang.Show();

            timernotif.Enabled = false;
        }


        private void _formSuratJalan_closed(object sender, FormClosedEventArgs e)
        {
            timernotif.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Diagnostics.Process.Start(Application.ExecutablePath);

        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgv.Rows[rowIndex];
            if (row.Cells[0].Value == null)
                return;
            String statusSelected = row.Cells["status"].Value.ToString();
            String selectedIdSPK = row.Cells["id_spk"].Value.ToString();
            String selectedIdDtSPK = row.Cells["dt spk"].Value.ToString();
            String selectedIdPO = row.Cells["id_po"].Value.ToString();

            FormPeriksaSPK _formPeriksa = new FormPeriksaSPK(selectedIdSPK, selectedIdDtSPK, selectedIdPO);
            _formPeriksa.FormClosed += _formPeriksa_FormClosed;
            _formPeriksa.Show();

            timernotif.Enabled = false;

        }

        private void _formPeriksa_FormClosed(object sender, FormClosedEventArgs e)
        {
            timernotif.Enabled = true;
        }

        private void btnViewSJ_Click(object sender, EventArgs e)
        {
            _viewSJ = new ViewSJ();
            konek.viewSJ(_viewSJ.dgvSJ);
            _viewSJ.Show();
            _viewSJ.isFirstTime = false;
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            _formLoading = new FormLoading();
            _formLoading.Show();
            fillNotif();

            pageIndex = 0;
            dt = new DataTable();

            dTable = konek.dataReportDtSPK();
            backgroundWorker.RunWorkerAsync();

        }

        private void FormAdmin_Shown(object sender, EventArgs e)
        {
            _formLoading = new FormLoading();
            _formLoading.Show();
            fillNotif();

            if (dt == null)
            {
                pageIndex = 0;
                dt = new DataTable();
            }

            dTable = konek.dataReportDtSPK();
            backgroundWorker.RunWorkerAsync();
        }

        public void loadTable()
        {
            inProcessed = true;

            if (dt == null)
            {
                pageIndex = 0;

                dt = new DataTable();

            }

            DataTable temp = dTable.AsEnumerable().Select(x => x).Skip((pageIndex * pageSize)).Take(pageSize).CopyToDataTable();
            if (temp.Rows.Count > 0)
            {
                dt.Merge(temp);

                if (dt != null)
                {
                    DataTable resultTable = new DataTable();
                    DataColumn numberColumn = new DataColumn();
                    numberColumn.ColumnName = "No.";
                    numberColumn.DataType = typeof(int);
                    numberColumn.AutoIncrement = true;
                    numberColumn.AutoIncrementSeed = 1;
                    numberColumn.AutoIncrementStep = 1;
                    resultTable.Columns.Add(numberColumn);
                    resultTable.Merge(dt);

                    setDataSource(resultTable);
                }
            }
            else
                MessageBox.Show("Data is null");
        }

        internal delegate void SetDataSourceDelegate(DataTable table);
        private void setDataSource(DataTable table)
        {
            // Invoke method if required:
            if (this.InvokeRequired)
            {
                this.Invoke(new SetDataSourceDelegate(setDataSource), table);
            }
            else
            {
                dgv.DataSource = table;
                dgv.ClearSelection();

                inProcessed = false;
                if (_formLoading != null)
                    _formLoading.Dispose();
            }
        }

        public int GetDisplayedRowsCount()
        {
            int count = dgv.Rows[dgv.FirstDisplayedScrollingRowIndex].Height;
            count = dgv.Height / count;
            return count;
        }

        private void onDoWork(object sender, DoWorkEventArgs e)
        {
            loadTable();
        }

        private void onRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_formLoading != null)
                _formLoading.Dispose();

            if (pageIndex > 0)
            {
                int display = dgv.Rows.Count - pageSize;
                dgv.FirstDisplayedScrollingRowIndex = display;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _formSPKFinish = new FormSPKFinish();
            _formSPKFinish.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReportSPKFilter form = new ReportSPKFilter();
            form.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TandaTerima tt = new TandaTerima();
            tt.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormCompanyProfile cp = new FormCompanyProfile();
            cp.ShowDialog();
        }

        private void btnViewSPB_Click(object sender, EventArgs e)
        {
            _viewSPB = new ViewSPB();
            konek.viewSPB(_viewSPB.dgvSJ);
            _viewSPB.Show();
            _viewSPB.isFirstTime = false;
        }

        private void dgv_Scroll(object sender, ScrollEventArgs e)
        {
            int display = dgv.Rows.Count - dgv.DisplayedColumnCount(false);
            if (e.Type == ScrollEventType.SmallIncrement || e.Type == ScrollEventType.LargeIncrement)
            {
                if (e.NewValue >= dgv.Rows.Count - GetDisplayedRowsCount() && (e.NewValue + GetDisplayedRowsCount()) < dTable.Rows.Count && !inProcessed)
                {
                    if (backgroundWorker != null && backgroundWorker.IsBusy)
                        backgroundWorker.CancelAsync();
                    else if (backgroundWorker != null && !backgroundWorker.CancellationPending)
                    {
                        pageIndex++;
                        _formLoading = new FormLoading();
                        _formLoading.Show();

                        backgroundWorker.RunWorkerAsync();
                    }
                }
            }
        }
    }
}
