﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormCompanyProfile : Form
    {
        koneksi konek;

        public FormCompanyProfile()
        {
            InitializeComponent();
            konek = new koneksi();

            InitializeData();
        }

        public void InitializeData()
        {
            data_perusahaan dp = konek.getDataPerusahaan();

            if (dp != null)
            {
                this.tbNamaPerusahaan.Text = dp.nama_perusahaan;
                this.tbAlamat.Text = dp.alamat;
                this.tbKota.Text = dp.kota;
                this.tbTelp.Text = dp.telp;
                this.tbFax.Text = dp.fax;

                this.tbNorek.Text = dp.norek;
                this.tbNorekNama.Text = dp.norek_nama;
                this.tbBankCabang.Text = dp.norek_bank;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nama_perusahaan = this.tbNamaPerusahaan.Text;
            string alamat = this.tbAlamat.Text;
            string kota = this.tbKota.Text;
            string telp = this.tbTelp.Text;
            string fax = this.tbFax.Text;

            string norek = this.tbNorek.Text;
            string norek_nama = this.tbNorekNama.Text;
            string bank_cabang = this.tbBankCabang.Text;

            data_perusahaan perusahaan = new data_perusahaan();
            perusahaan.nama_perusahaan = nama_perusahaan;
            perusahaan.alamat = alamat;
            perusahaan.kota = kota;
            perusahaan.telp = telp;
            perusahaan.fax = fax;
            perusahaan.norek = norek;
            perusahaan.norek_nama = norek_nama;
            perusahaan.norek_bank = bank_cabang;

            konek.updateDataPerusahaan(perusahaan);

            MessageBox.Show("Data berhasil disimpan.");
        }
    }
}
