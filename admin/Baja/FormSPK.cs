﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;

namespace Baja
{
    public partial class FormSPK : Form
    {        
        koneksi konek;
        int mesin = 0;
        public string admin = "";
        public string no_po = "";

        public FormSPK(string noPO, string adm)
        {
            InitializeComponent();
            konek = new koneksi();

            no_po = noPO;
            admin = adm;
            string nama_cust = konek.getNameCustomer(no_po);
            cust.Text = nama_cust;
            //this.TopMost = true;
        }

        private void FormSPK_Load(object sender, EventArgs ae)
        {
            DateTime now = DateTime.Now;
            string formatForMySql = now.ToString("yyyy-MM-dd");

            tgl.Text = formatForMySql;
            selectType();
        }

        public void selectType()
        {             
            konek.selectBrgType(cb1, no_po);
            konek.selectBrgType(cb2, no_po);
            konek.selectBrgType(cb3, no_po);
            konek.selectBrgType(cb4, no_po);
            konek.selectBrgType(cb5, no_po);
            konek.selectBrgType(cb6, no_po);
            konek.selectBrgType(cb7, no_po);
            konek.selectBrgType(cb8, no_po);
            konek.selectBrgType(cb9, no_po);

            konek.selectBrgType(cbO1, no_po);
            konek.selectBrgType(cbO2, no_po);
            konek.selectBrgType(cbO3, no_po);
            konek.selectBrgType(cbO4, no_po);
            konek.selectBrgType(cbO5, no_po);
            konek.selectBrgType(cbO6, no_po);
            konek.selectBrgType(cbO7, no_po);
            konek.selectBrgType(cbO8, no_po);
            konek.selectBrgType(cbO9, no_po);
        }
        
        private void cb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA1.Text = cb1.Text;
            cbUk1.Items.Clear();
            cbUk1.Text = "Pilih Ukuran";
            type selectedType = (type)cb1.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbUk1, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk1.SelectedItem;
            type orderType = (type)cbO1.SelectedItem;
            konek.getnamaUkuran(cbUk1, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb2_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA2.Text = cb2.Text;
            cbOUk2.Items.Clear();
            cbOUk2.Text = "Pilih Ukuran";
            type selectedType = (type)cb2.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbOUk2, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk2.SelectedItem;
            type orderType = (type)cbO2.SelectedItem;
            konek.getnamaUkuran(cbUk2, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb3_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA3.Text = cb3.Text;
            cbOUk3.Items.Clear();
            cbOUk3.Text = "Pilih Ukuran";
            type selectedType = (type)cbO3.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbOUk3, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk3.SelectedItem;
            type orderType = (type)cbO3.SelectedItem;
            konek.getnamaUkuran(cbUk3, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb4_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA4.Text = cb4.Text;
            cbOUk4.Items.Clear();
            cbOUk4.Text = "Pilih Ukuran";
            type selectedType = (type)cbO4.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbOUk4, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk4.SelectedItem;
            type orderType = (type)cbO4.SelectedItem;
            konek.getnamaUkuran(cbUk4, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb5_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA5.Text = cb5.Text;
            cbOUk5.Items.Clear();
            cbOUk5.Text = "Pilih Ukuran";
            type selectedType = (type)cbO5.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbOUk5, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk5.SelectedItem;
            type orderType = (type)cbO5.SelectedItem;
            konek.getnamaUkuran(cbUk5, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb6_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA6.Text = cb6.Text;
            cbOUk6.Items.Clear();
            cbOUk6.Text = "Pilih Ukuran";
            type selectedType = (type)cbO6.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbOUk6, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk6.SelectedItem;
            type orderType = (type)cbO6.SelectedItem;
            konek.getnamaUkuran(cbUk6, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb7_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA7.Text = cb7.Text;
            cbOUk7.Items.Clear();
            cbOUk7.Text = "Pilih Ukuran";
            type selectedType = (type)cbO7.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbOUk7, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk7.SelectedItem;
            type orderType = (type)cbO7.SelectedItem;
            konek.getnamaUkuran(cbUk7, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb8_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA8.Text = cb8.Text;
            cbOUk8.Items.Clear();
            cbOUk8.Text = "Pilih Ukuran";
            type selectedType = (type)cbO8.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbOUk8, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk8.SelectedItem;
            type orderType = (type)cbO8.SelectedItem;
            konek.getnamaUkuran(cbUk8, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        private void cb9_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbA9.Text = cb9.Text;
            cbOUk9.Items.Clear();
            cbOUk9.Text = "Pilih Ukuran";
            type selectedType = (type)cbO9.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            //konek.getnamaUkuran(cbUk9, selectedType.id_type.ToString());
            ukuran selectedUkuran = (ukuran)cbOUk9.SelectedItem;
            type orderType = (type)cbO9.SelectedItem;
            konek.getnamaUkuran(cbUk9, orderType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString(), selectedType.id_type.ToString());
        }

        // start of cb ukuran //

        //private void cbUk1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk1.SelectedItem;
        //    type selectedType = (type)cb1.SelectedItem;
        //    konek.getQtyHarga(qty1, kg1, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk2_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk2.SelectedItem;
        //    type selectedType = (type)cb2.SelectedItem;
        //    konek.getQtyHarga(qty2, kg2, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk3_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk3.SelectedItem;
        //    type selectedType = (type)cb3.SelectedItem;
        //    konek.getQtyHarga(qty3, kg3, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk4_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk4.SelectedItem;
        //    type selectedType = (type)cb4.SelectedItem;
        //    konek.getQtyHarga(qty4, kg4, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk5_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk5.SelectedItem;
        //    type selectedType = (type)cb5.SelectedItem;
        //    konek.getQtyHarga(qty5, kg5, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk6_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk6.SelectedItem;
        //    type selectedType = (type)cb6.SelectedItem;
        //    konek.getQtyHarga(qty6, kg6, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk7_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk7.SelectedItem;
        //    type selectedType = (type)cb7.SelectedItem;
        //    konek.getQtyHarga(qty7, kg7, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk8_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk8.SelectedItem;
        //    type selectedType = (type)cb8.SelectedItem;
        //    konek.getQtyHarga(qty8, kg8, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        //private void cbUk9_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ukuran selectedUkuran = (ukuran)cbUk9.SelectedItem;
        //    type selectedType = (type)cb9.SelectedItem;
        //    konek.getQtyHarga(qty9, kg9, selectedUkuran.id_ukuran, selectedType.id_type);
        //}

        private void cbO1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbOUk1.Items.Clear();
            cbOUk1.Text = "Pilih Ukuran";
            type selectedType = (type)cbO1.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk1, selectedType.id_type.ToString(),no_po);

            //if (cb1.SelectedItem.ToString()==cbO1.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust1.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA1.Text = cbO1.SelectedItem.ToString();
            //}
          
        }

        private void cbO2_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk2.Items.Clear();
            cbUk2.Text = "Pilih Ukuran";
            type selectedType = (type)cbO2.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk2, selectedType.id_type.ToString(), no_po);

            //if (cb2.SelectedItem.ToString() == cbO2.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust2.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA2.Text = cbO2.SelectedItem.ToString();
            //}
        }

        private void cbO3_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk3.Items.Clear();
            cbUk3.Text = "Pilih Ukuran";
            type selectedType = (type)cbO3.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk3, selectedType.id_type.ToString(), no_po);

            //if (cbO3.SelectedItem.ToString() == cb3.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust3.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA3.Text = cb3.SelectedItem.ToString();
            //}
        }

        private void cbO4_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk4.Items.Clear();
            cbUk4.Text = "Pilih Ukuran";
            type selectedType = (type)cbO4.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk4, selectedType.id_type.ToString(), no_po);

            //if (cbO4.SelectedItem.ToString() == cb4.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust4.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA4.Text = cb4.SelectedItem.ToString();
            //}
        }

        private void cbO5_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk5.Items.Clear();
            cbUk5.Text = "Pilih Ukuran";
            type selectedType = (type)cbO5.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk5, selectedType.id_type.ToString(), no_po);

            //if (cbO5.SelectedItem.ToString() == cb5.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust5.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA5.Text = cb5.SelectedItem.ToString();
            //}
        }

        private void cbO6_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk6.Items.Clear();
            cbUk6.Text = "Pilih Ukuran";
            type selectedType = (type)cbO6.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk6, selectedType.id_type.ToString(), no_po);

            //if (cbO6.SelectedItem.ToString() == cb6.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust6.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA6.Text = cb6.SelectedItem.ToString();
            //}
        }

        private void cbO7_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk7.Items.Clear();
            cbUk7.Text = "Pilih Ukuran";
            type selectedType = (type)cbO7.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk7, selectedType.id_type.ToString(), no_po);

            //if (cbO7.SelectedItem.ToString() == cb7.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust7.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA7.Text = cb7.SelectedItem.ToString();
            //}
        }

        private void cbO8_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk8.Items.Clear();
            cbUk8.Text = "Pilih Ukuran";
            type selectedType = (type)cbO8.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk8, selectedType.id_type.ToString(), no_po);

            //if (cbO8.SelectedItem.ToString() == cb8.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust8.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA8.Text = cb8.SelectedItem.ToString();
            //}
        }

        private void cbO9_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbUk9.Items.Clear();
            cbUk9.Text = "Pilih Ukuran";
            type selectedType = (type)cbO9.SelectedItem;
            Console.WriteLine(selectedType.id_type.ToString());
            konek.getnamaUkuranOrder(cbOUk9, selectedType.id_type.ToString(), no_po);

            //if (cbO9.SelectedItem.ToString() == cb9.SelectedItem.ToString())
            //{
            //    //isi nama customer
            //    int id_cust = konek.getId_Customer(no_po);
            //    string nama_cust = konek.getNameCustomer(id_cust.ToString());
            //    Cust9.Text = nama_cust;

            //    //isi kolom type barang
            //    cbA9.Text = cb9.SelectedItem.ToString();
            //}
        }

        private void cbOUk1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk1.SelectedItem;
            type selectedType = (type)cbO1.SelectedItem;
            konek.getQtyKgOrder(qtyO1, kgO1, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb1, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk1, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

        private void cbOUk2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk2.SelectedItem;
            type selectedType = (type)cbO2.SelectedItem;
            konek.getQtyKgOrder(qtyO2, kgO2, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb2, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk2, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

        private void cbOUk3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk3.SelectedItem;
            type selectedType = (type)cbO3.SelectedItem;
            konek.getQtyKgOrder(qtyO3, kgO3, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb3, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk3, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());

        }

        private void cbOUk4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk4.SelectedItem;
            type selectedType = (type)cbO4.SelectedItem;
            konek.getQtyKgOrder(qtyO4, kgO4, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb4, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk4, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

        private void cbOUk5_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk5.SelectedItem;
            type selectedType = (type)cbO5.SelectedItem;
            konek.getQtyKgOrder(qtyO5, kgO5, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb5, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk5, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

        private void cbOUk6_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk6.SelectedItem;
            type selectedType = (type)cbO6.SelectedItem;
            konek.getQtyKgOrder(qtyO6, kgO6, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb6, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk6, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

        private void cbOUk7_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk7.SelectedItem;
            type selectedType = (type)cbO7.SelectedItem;
            konek.getQtyKgOrder(qtyO7, kgO7, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb7, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk7, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

        private void cbOUk8_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk8.SelectedItem;
            type selectedType = (type)cbO8.SelectedItem;
            konek.getQtyKgOrder(qtyO8, kgO8, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb8, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk8, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

        private void cbOUk9_SelectedIndexChanged(object sender, EventArgs e)
        {
            ukuran selectedUkuran = (ukuran)cbOUk9.SelectedItem;
            type selectedType = (type)cbO9.SelectedItem;
            konek.getQtyKgOrder(qtyO9, kgO9, selectedUkuran.id_ukuran, selectedType.id_type);
            konek.getTypeRekomendasi(cb9, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
            //konek.getnamaUkuran(cbUk9, selectedType.id_type.ToString(), no_po, selectedUkuran.id_ukuran.ToString());
        }

       

        private void msn1_Click(object sender, EventArgs e)
        {
            msn1.BackColor = Color.Blue;
            msn2.BackColor = SystemColors.Control;
            msn3.BackColor = SystemColors.Control;
            mesin = 1;
        }

        private void msn2_Click(object sender, EventArgs e)
        {
            msn2.BackColor = Color.Blue;
            msn1.BackColor = SystemColors.Control;
            msn3.BackColor = SystemColors.Control;
            mesin = 2;
        }

        private void msn3_Click(object sender, EventArgs e)
        {
            msn3.BackColor = Color.Blue;
            msn2.BackColor = SystemColors.Control;
            msn1.BackColor = SystemColors.Control;
            mesin = 3;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(noSpk.Text))
            {
                MessageBox.Show("Silahkan SPK di isi");
            }
            else
            {

                if (!string.IsNullOrEmpty(cbA1.Text) && !string.IsNullOrEmpty(p1.Text) && !string.IsNullOrEmpty(l1.Text)
                    /*&& !string.IsNullOrEmpty(t1.Text)*/ && !string.IsNullOrEmpty(qtyA1.Text) && !string.IsNullOrEmpty(kgA1.Text))
                {
                    int no_mesin = mesin;
                    int inputby = konek.getidAdmin(admin);
                    int id_po = konek.getId_po(no_po);
                    string spk = noSpk.Text;
                    int status = 0;

                    //add t_spk
                    konek.addt_spk(no_mesin, inputby, id_po, spk, status);
                    Console.WriteLine("nomesin: "+no_mesin+" inputby:"+inputby+" id_po"+id_po+" spk:"+spk);

                    //cek ukuran akhir
                    cekUkuran();
                    
                }

            }
        }

        public void cekUkuran()
        {

            int id_po = konek.getId_po(no_po);

            if (!string.IsNullOrEmpty(p1.Text) && !string.IsNullOrEmpty(l1.Text) /*&& !string.IsNullOrEmpty(t1.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb1, cbO1, cbUk1, cbOUk1, p1, l1, t1, qty1, qtyO1, qtyA1, kg1, kgO1, kgA1);
            }

            if (!string.IsNullOrEmpty(p2.Text) && !string.IsNullOrEmpty(l2.Text) /*&& !string.IsNullOrEmpty(t2.Text)*/)
            {
               cekUkuranAdd_DTSPK(id_po, cb2, cbO2, cbUk2, cbOUk2, p2, l2, t2, qty2, qtyO2, qtyA2, kg2, kgO2, kgA2);
            }

            if (!string.IsNullOrEmpty(p3.Text) && !string.IsNullOrEmpty(l3.Text) /*&& !string.IsNullOrEmpty(t3.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb3, cbO3, cbUk3, cbOUk3, p3, l3, t3, qty3, qtyO3, qtyA3, kg3, kgO3, kgA3);
            }

            if (!string.IsNullOrEmpty(p4.Text) && !string.IsNullOrEmpty(l4.Text) /*&& !string.IsNullOrEmpty(t4.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb4, cbO4, cbUk4, cbOUk4, p4, l4, t4, qty4, qtyO4, qtyA4, kg4, kgO4, kgA4);
            }

            if (!string.IsNullOrEmpty(p5.Text) && !string.IsNullOrEmpty(l5.Text) /*&& !string.IsNullOrEmpty(t5.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb5, cbO5, cbUk5, cbOUk5, p5, l5, t5, qty5, qtyO5, qtyA5, kg5, kgO5, kgA5);
            }

            if (!string.IsNullOrEmpty(p6.Text) && !string.IsNullOrEmpty(l6.Text) /*&& !string.IsNullOrEmpty(t6.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb6, cbO6, cbUk6, cbOUk6, p6, l6, t6, qty6, qtyO6, qtyA6, kg6, kgO6, kgA6);
            }

            if (!string.IsNullOrEmpty(p7.Text) && !string.IsNullOrEmpty(l7.Text) /*&& !string.IsNullOrEmpty(t7.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb7, cbO7, cbUk7, cbOUk7, p7, l7, t7, qty7, qtyO7, qtyA7, kg7, kgO7, kgA7);
            }

            if (!string.IsNullOrEmpty(p8.Text) && !string.IsNullOrEmpty(l8.Text) /*&& !string.IsNullOrEmpty(t8.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb8, cbO8, cbUk8, cbOUk8, p8, l8, t8, qty8, qtyO8, qtyA8, kg8, kgO8, kgA8);
            }

            if (!string.IsNullOrEmpty(p9.Text) && !string.IsNullOrEmpty(l9.Text) /*&& !string.IsNullOrEmpty(t9.Text)*/)
            {
                cekUkuranAdd_DTSPK(id_po, cb9, cbO9, cbUk9, cbOUk9, p9, l9, t9, qty9, qtyO9, qtyA9, kg9, kgO9, kgA9);
            }

            //jika sudah oke maka status po menjadi 1
            konek.updatePO(no_po,1);
            this.Close();

        }

        private void cekUkuranAdd_DTSPK(
                int id_po, ComboBox cb, ComboBox cbO, ComboBox cbUk, ComboBox cbOUk, TextBox p, TextBox l, TextBox t,
                 NumericUpDown qty, TextBox qtyO, TextBox qtyA, Label kg, TextBox kgO, TextBox kgA
            )
        {
            if (!string.IsNullOrEmpty(p.Text) && !string.IsNullOrEmpty(l.Text) /*&& !string.IsNullOrEmpty(t.Text)*/)
            {
                //1 type untuk order, awal, sama akhir
                type type_order = (type)cbO.SelectedItem;
                type type_awal = (type)cb.SelectedItem;
                int id_type_order = type_order.id_type;
                int id_type_awal = type_awal.id_type;
                string ukuranAkhir = grupUkuran(p, l, t);
                int id_ukuran_order = ((ukuran)cbOUk.SelectedItem).id_ukuran;


                int id_spk = konek.getId_Spk(noSpk.Text);
                int id_stockawal = konek.getIdStock(id_type_awal, ((ukuran)cbUk.SelectedItem).id_ukuran);

                int id_stockorder = konek.getId_dt_po(id_po, id_ukuran_order);
                int id_stockakhir = 0;
                
                string qty_awal = qty.Text;
                string qty_order = qtyO.Text;
                string qty_akhir = qtyA.Text;
                string kg_awal = kg.Text;
                string kg_order = kgO.Text;
                string kg_akhir = kgA.Text;

                if (konek.cekUkuran(ukuranAkhir) == false)//jk ukuran tdk ada di M_ukuran
                {

                    //add ke stok baru
                    //tambah ukuran dulu ke m_ukuran
                    konek.addUkuran(ukuranAkhir, p.Text, l.Text, t.Text);
                    //id ukuran baru di-insertkan ke stock
                    int id_new_ukuran = konek.getIndexUkuran(ukuranAkhir);
                    konek.addStockOnly(id_type_awal.ToString(), id_new_ukuran.ToString(), "0", kgA.Text);

                    //menentukan stok akhir dari id stock akhir baru
                    id_stockakhir = konek.getIdStock(id_type_awal, id_new_ukuran);
                }
                else
                {
                    //cek stok dengan typeStock dan ukuranAkhir ada atau tidak? 
                    int id_ukuran_akhir = konek.getIndexUkuran(ukuranAkhir);
                    //jika tidak ada, 
                    //maka add ke stok baru, jika ada, maka update qty stok tadi dilakukan jika operator melakukan finish                    
                    if (konek.cekStock(id_type_awal, id_ukuran_akhir) == false)
                        konek.addStockOnly(id_type_awal.ToString(), id_ukuran_akhir.ToString(), "0", kgA.Text);

                    //menentukan stock akhir
                    id_stockakhir = konek.getIdStock(id_type_awal, id_ukuran_akhir); 
                }

                //add dt_spk
                Console.WriteLine("iki spk: " + id_spk + id_stockawal + id_stockorder + id_stockakhir);
                konek.adddt_spk(id_spk, id_stockawal, id_stockorder, id_stockakhir, qty_awal, qty_order, qty_akhir, 0,
                    kg_awal, kg_order, kg_akhir);

                //proses pengurangan stok awal
                konek.decreseStock(id_stockawal, qty_awal);

                //proses penambahan quota stok akhir, dilakukan setelah operator melakukan finish, atau setelah diklik oleh admin
            }
        }

        public string grupUkuran(TextBox p,TextBox l, TextBox t)
        {
            string gabung;
            if(t.Text.Length>0)
                gabung = l.Text + " x " + p.Text + " x " + t.Text;
            else
                gabung = l.Text + " x " + p.Text;

            return gabung;
        }

        private void cbA1_MouseClick(object sender, MouseEventArgs e)
        {
            //if (cb1.SelectedItem.ToString() == cbO1.SelectedItem.ToString())
            //{
            //    cbA1.Text = cb1.SelectedItem.ToString();
            //}
            //else
            //{
            //    MessageBox.Show("Barang berbeda antara stock awal dna stock order");
            //}
        }

        private void cbUk1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbUk = (ComboBox)sender;
            ukuran selectedUkuran = (ukuran)cbUk.SelectedItem;
            if(cbUk.Name.EndsWith("1"))
                qty1.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("2"))
                qty2.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("3"))
                qty3.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("4"))
                qty4.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("5"))
                qty5.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("6"))
                qty6.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("7"))
                qty7.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("8"))
                qty8.Maximum = selectedUkuran.max_qty;
            else if (cbUk.Name.EndsWith("9"))
                qty9.Maximum = selectedUkuran.max_qty;
        }

        private void qty1_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numQty = (NumericUpDown)sender;
            if (numQty.Name.EndsWith("1"))
            {
                ukuran selectedUkuran = (ukuran)cbUk1.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg1.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("2"))
            {
                ukuran selectedUkuran = (ukuran)cbUk2.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg2.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("3"))
            {
                ukuran selectedUkuran = (ukuran)cbUk3.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg3.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("4"))
            {
                ukuran selectedUkuran = (ukuran)cbUk4.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg4.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("5"))
            {
                ukuran selectedUkuran = (ukuran)cbUk5.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg5.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("6"))
            {
                ukuran selectedUkuran = (ukuran)cbUk6.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg6.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("7"))
            {
                ukuran selectedUkuran = (ukuran)cbUk7.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg7.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("8"))
            {
                ukuran selectedUkuran = (ukuran)cbUk8.SelectedItem;
                double kg = 0;
                if (selectedUkuran != null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg8.Text = kg.ToString();
            }
            else if (numQty.Name.EndsWith("9"))
            {
                ukuran selectedUkuran = (ukuran)cbUk9.SelectedItem;
                double kg = 0;
                if(selectedUkuran!=null)
                    kg = countKg(selectedUkuran.panjang, selectedUkuran.lebar, selectedUkuran.tinggi, numQty.Value);
                kg9.Text = kg.ToString();
            }
        }

        private double countKg(double p, double l, double t,  decimal qtyVal)
        {
            int qty = (int)qtyVal;
            if (t > 0)
            {
                return (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;            }
            else
            {
                return (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
            }
        }

        private void p1_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Name.EndsWith("1"))
            {
                double p=0, l=0, t=0, kg=0;
                decimal qty = 0;
                if (p1.Text != null && p1.Text.Length>0)
                    p = int.Parse(p1.Text);
                if (l1.Text != null && l1.Text.Length > 0)
                    l = int.Parse(l1.Text);
                if (t1.Text != null && t1.Text.Length > 0)
                    t = int.Parse(t1.Text);
                if (qtyA1.Text != null && qtyA1.Text.Length > 0)
                    qty = decimal.Parse(qtyA1.Text);
                kg = countKg(p, l, t, qty);
                kgA1.Text = kg.ToString();
            }else if (tb.Name.EndsWith("2"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p2.Text != null && p2.Text.Length > 0)
                    p = int.Parse(p2.Text);
                if (l2.Text != null && l2.Text.Length > 0)
                    l = int.Parse(l2.Text);
                if (t2.Text != null && t2.Text.Length > 0)
                    t = int.Parse(t2.Text);
                if (qtyA2.Text != null && qtyA2.Text.Length > 0)
                    qty = decimal.Parse(qtyA2.Text);
                kg = countKg(p, l, t, qty);
                kgA2.Text = kg.ToString();
            }
            else if (tb.Name.EndsWith("3"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p3.Text != null && p3.Text.Length > 0)
                    p = int.Parse(p3.Text);
                if (l3.Text != null && l3.Text.Length > 0)
                    l = int.Parse(l3.Text);
                if (t3.Text != null && t3.Text.Length > 0)
                    t = int.Parse(t3.Text);
                if (qtyA3.Text != null && qty3.Text.Length > 0)
                    qty = decimal.Parse(qtyA3.Text);
                kg = countKg(p, l, t, qty);
                kgA3.Text = kg.ToString();
            }
            else if (tb.Name.EndsWith("4"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p4.Text != null && p4.Text.Length > 0)
                    p = int.Parse(p4.Text);
                if (l4.Text != null && l4.Text.Length > 0)
                    l = int.Parse(l4.Text);
                if (t4.Text != null && t4.Text.Length > 0)
                    t = int.Parse(t4.Text);
                if (qtyA4.Text != null && qtyA4.Text.Length > 0)
                    qty = decimal.Parse(qtyA4.Text);
                kg = countKg(p, l, t, qty);
                kgA4.Text = kg.ToString();
            }
            else if (tb.Name.EndsWith("5"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p5.Text != null && p5.Text.Length > 0)
                    p = int.Parse(p5.Text);
                if (l5.Text != null && l5.Text.Length > 0)
                    l = int.Parse(l5.Text);
                if (t5.Text != null && t5.Text.Length > 0)
                    t = int.Parse(t5.Text);
                if (qtyA5.Text != null && qtyA5.Text.Length > 0)
                    qty = decimal.Parse(qtyA5.Text);
                kg = countKg(p, l, t, qty);
                kgA5.Text = kg.ToString();
            }
            else if (tb.Name.EndsWith("6"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p6.Text != null && p6.Text.Length > 0)
                    p = int.Parse(p6.Text);
                if (l6.Text != null && l6.Text.Length > 0)
                    l = int.Parse(l6.Text);
                if (t6.Text != null && t6.Text.Length > 0)
                    t = int.Parse(t6.Text);
                if (qtyA6.Text != null && qtyA6.Text.Length > 0)
                    qty = decimal.Parse(qtyA6.Text);
                kg = countKg(p, l, t, qty);
                kgA6.Text = kg.ToString();
            }
            else if (tb.Name.EndsWith("7"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p7.Text != null && p7.Text.Length > 0)
                    p = int.Parse(p7.Text);
                if (l7.Text != null && l7.Text.Length > 0)
                    l = int.Parse(l7.Text);
                if (t7.Text != null && t7.Text.Length > 0)
                    t = int.Parse(t7.Text);
                if (qtyA7.Text != null && qtyA7.Text.Length > 0)
                    qty = decimal.Parse(qtyA7.Text);
                kg = countKg(p, l, t, qty);
                kgA7.Text = kg.ToString();
            }
            else if (tb.Name.EndsWith("8"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p8.Text != null && p8.Text.Length > 0)
                    p = int.Parse(p8.Text);
                if (l8.Text != null && l8.Text.Length > 0)
                    l = int.Parse(l8.Text);
                if (t8.Text != null && t8.Text.Length > 0)
                    t = int.Parse(t8.Text);
                if (qtyA8.Text != null && qtyA8.Text.Length > 0)
                    qty = decimal.Parse(qtyA8.Text);
                kg = countKg(p, l, t, qty);
                kgA8.Text = kg.ToString();
            }
            else if (tb.Name.EndsWith("9"))
            {
                double p = 0, l = 0, t = 0, kg = 0;
                decimal qty = 0;
                if (p9.Text != null && p9.Text.Length > 0)
                    p = int.Parse(p9.Text);
                if (l9.Text != null && l9.Text.Length > 0)
                    l = int.Parse(l9.Text);
                if (t9.Text != null && t9.Text.Length > 0)
                    t = int.Parse(t9.Text);
                if (qtyA9.Text != null && qtyA9.Text.Length > 0)
                    qty = decimal.Parse(qtyA9.Text);
                kg = countKg(p, l, t, qty);
                kgA9.Text = kg.ToString();
            }
        }
    }
}
