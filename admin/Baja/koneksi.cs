﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace Baja
{
    public class koneksi
    {
        public MySqlConnection db;
        public string connetionString = null;
        Dictionary<int, t_po> nePO;
        SettingManager _settingManager = new SettingManager("settings.xml");
        public string jabatanAkses = "";

        public koneksi()
        {
            string server = _settingManager.GetValue("server");
            string database = _settingManager.GetValue("database");
            string username = _settingManager.GetValue("username");
            string password = _settingManager.GetValue("password");
            jabatanAkses = _settingManager.GetValue("jabatan");
            connetionString = "server=" + server + ";database=" + database + ";uid=" + username + ";pwd=" + password + ";";
            db = new MySqlConnection(connetionString);

        }
        
        public void fillJabatan(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_jabatan from jabatan ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    cb.Items.Add(nama);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        internal DataTable dataReportDtSPK(bool finish = false)
        {
            DataTable dTable = new DataTable();            
            try
            {
                openkoneksi();
                string status = (finish) ? " and (dt.status_pengerjaan = 2)" : " and (dt.status_pengerjaan = 0 or dt.status_pengerjaan = 1)";
                string sql = "SELECT DATE_FORMAT(spk.input_time, '%d/%m/%Y') AS Tanggal, spk.no_spk as SPK, spk.id_spk, spk.id_po, " +
                            " dt.id_dt_spk as `dt spk`, dt.type, dt.nama_ukuran, " +
                            " dt.qty_order, dt.kg_order, dt.id_mesin, dt.keterangan, " +
                            " DATE_FORMAT(dt.start, '%H:%i') AS Start, DATE_FORMAT(dt.finish, '%H:%i') AS Finish, " +
                            " TIMESTAMPDIFF(MINUTE, dt.start, dt.finish) AS Waktu, " +
                            " case dt.status_pengerjaan " +
                            "     when 0 then 'Antrian' " +
                            "     when 1 then 'Progress' " +
                            "     else 'Finish' " +
                            " end AS Status " +
                            "FROM `t_spk` spk, " +
                            "( " +
                            "  SELECT qr . * , t_mesinoperation.start, t_mesinoperation.finish " +
                            "  FROM( " +
                            "    SELECT dt.id_dt_spk, dt.id_spk, m_type.type, m_ukuran.nama_ukuran, dt.qty_order, dt.kg_order, dt.id_mesin, dt.keterangan, dt.status_pengerjaan " +
                            "    FROM dt_spk dt, m_type, m_ukuran, m_stock " +
                            "    WHERE dt.stock_awal = m_stock.id_stock " +
                            "    AND m_stock.id_type = m_type.id_type " +
                            "    AND dt.stock_order = m_ukuran.id_ukuran " +
                            "  )qr " +
                            "  LEFT OUTER JOIN t_mesinoperation ON (qr.id_dt_spk = t_mesinoperation.id_dt_spk)  " +
                            ")dt " +
                            "where spk.id_spk = dt.id_spk " + status + " ; ";

                Console.WriteLine("QUERY DGV == " + sql);
                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                MyAdapter.Fill(dTable);
                Console.WriteLine(dTable);


            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return dTable;
        }

        public DataTable getReportSPK()
        {
            DataTable dTable = new DataTable();
            try
            {
                openkoneksi();
                string sql = "SELECT t_spk.id_spk, t_spk.id_po, " +
                            " DATE_FORMAT(t_spk.input_time, '%d') tgl_spk, " +
                            " DATE_FORMAT(t_spk.input_time, '%m') bln_spk, " +
                            " DATE_FORMAT(t_spk.input_time, '%Y') thn_spk, " +
                            " DATE_FORMAT(t_spk.input_time, '%Y-%m-%d') tgl_spk_full, " +
                            " t_spk.no_spk, m_sj.no_sj, " +
                            " dt_spk.keterangan nama_customer, " +
                            " m_type.type nama_barang, m_ukuran.nama_ukuran, dt_spk.qty_order, m_type.type type_terjual, " +
                            " IF(m_sj.no_sj IS NULL, 0, ROUND(dt_spk.kg_order, 2)) berat_terjual, " +
                            " IF(m_sj.no_sj IS NULL, 0, ROUND(dt_spk.kg_awal, 2)) berat_gudang,  " +
                            " IF(m_sj.no_sj IS NULL, 0, (ROUND(dt_spk.kg_order, 2) - ROUND(dt_spk.kg_awal, 2))) berat_selisih, " +
                            " m_stock.harga_stock harga_satuan, " +
                            " IF(m_sj.no_sj IS NULL, 0, (ROUND(dt_spk.kg_order, 2) * m_stock.harga_stock)) total, " +
                            " CASE WHEN t_po.ppn > 0 AND t_po.ppn IS NOT NULL THEN 1 ELSE 0 END is_ppn " +
                            " FROM t_spk " +
                            " INNER JOIN dt_spk ON dt_spk.id_spk = t_spk.id_spk " +
                            " INNER JOIN m_stock ON m_stock.id_stock = dt_spk.stock_awal " +
                            " INNER JOIN m_type ON m_type.id_type = m_stock.id_type " +
                            " INNER JOIN m_ukuran ON m_ukuran.id_ukuran = dt_spk.stock_order " +
                            " LEFT JOIN m_sj ON m_sj.id_po = t_spk.id_po " +
                            " INNER JOIN t_po ON t_po.id_po = t_spk.id_po ";

                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                MyAdapter.Fill(dTable);
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return dTable;
        }

        public string getNamaOperator(string idMesinOperation)
        {
            string nama = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_dpn, username from m_user, t_mesinoperation where t_mesinoperation.id_user = m_user.id_user " +
                    " and t_mesinoperation.id_mesin_operation='" + idMesinOperation + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0) && reader.GetString(0).Length>0)
                        nama = reader.GetString(0).ToString().ToUpper();
                    else
                        nama = reader.GetString(1).ToString().ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return nama;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }


        public string namaAdmin(string user)
        {
            string namasalah = "xxxxxxxx";
            string nama = user;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_dpn as status from m_user where username='" + user + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        nama = reader.GetString(0).ToString().ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }


        public string getNoSP(string id_po)
        {
            string noSP = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select no_po from t_po where id_po='" + id_po + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        noSP = reader.GetString(0).ToString().ToUpper();
                    return noSP;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return noSP;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return noSP;
        }
        
        public string getCountSJ()
        {
            string noSJ = "1";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT coalesce(max(id_sj)+1, 1) from m_sj; ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        noSJ = reader.GetString(0).ToString().ToUpper();
                    return noSJ;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return noSJ;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return noSJ;
        }

        public string getCountSPB()
        {
            string noSJ = "1";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT coalesce(max(id_spb)+1, 1) from m_spb; ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        noSJ = reader.GetString(0).ToString().ToUpper();
                    return noSJ;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return noSJ;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return noSJ;
        }

        public int getidAdmin(string user)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_user as status from m_user where upper(nama_dpn)='" + user.ToUpper() + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public int getidAdmin2(string user)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_user as status from m_user where upper(username) ='" + user.ToUpper() + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public Boolean LoginAdmin(string user, string password)
        {
            user = user.ToUpper();
            password = password.ToUpper();

            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select username, password, upper(nama_jabatan) as status " + 
                             " from m_user, Jabatan " + 
                             " where m_user.id_jabatan = Jabatan.id_jabatan " +
                             "   and upper(username) = '" + user + "' " +
                             "   and upper(password) = '" + password + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {                        
                        if (jabatanAkses.ToUpper().Contains(reader.GetString(2)))
                        {
                            return true;
                        }

                        MessageBox.Show("Maaf Jabatan Tidak Sesuai");
                    }else
                    {
                        MessageBox.Show("Maaf username dan password anda tidak ada di Database");
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                MessageBox.Show(ex.Message);
                return false;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return false;

        }

        public void openkoneksi()
        {
            db.Open();
        }

        public void closekoneksi()
        {
            db.Close();
        }

        public void display_report(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                string Query = "select tanggal,no_spk,nama_dpn,start_time,finish_time,timestampdiff(hour,start_time,finish_time),status_mesin,nama_mesin from t_spk,m_mesin where t_spk.id_mesin=m_mesin.id_mesin;";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void update()
        {
            try
            {
                //This is my connection string i have assigned the database file address path  
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //This is my update query in which i am taking input from the user through windows forms and update the record.  
                //string Query = "update student.studentinfo set idStudentInfo='" + this.IdTextBox.Text + "',Name='" + this.NameTextBox.Text + "',Father_Name='" + this.FnameTextBox.Text + "',Age='" + this.AgeTextBox.Text + "',Semester='" + this.SemesterTextBox.Text + "' where idStudentInfo='" + this.IdTextBox.Text + "';";
                string Query = "";
                //This is  MySqlConnection here i have created the object and pass my connection string.  


                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                //MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                MessageBox.Show("Data Updated");
                while (MyReader2.Read())
                {
                }
                db.Close();//Connection closed here  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public long addDokumen(string keterangan, int id_customer, string doc_no, int total)
        {
            openkoneksi();
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_dokumen(id_customer,keterangan,doc_no,total) " +
                    " values(" + id_customer + ",'" + keterangan + "','" + doc_no + "'," + total + ") ; select last_insert_id();";

                MySqlCommand MyCommand2 = db.CreateCommand(); // new MySqlCommand(Query, db);
                MyCommand2.CommandText = Query;
                MyCommand2.ExecuteNonQuery();
                db.Close();
                return MyCommand2.LastInsertedId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                db.Close();
                return 0;
            }
        }

        //===================Supplier
        public void addSupplier(string nama, int id, string alamat, string telp, string contact)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_supplier(nama_supplier,input_by,alamat_supplier,input_date_supplier,telp_supplier, contact_person) "+
                    " values('" + nama + "','" + id + "','" + alamat + "','" + formatForMySql + "','" + telp + "','" + contact + "' ) ; ";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void viewSupplier(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                //string Query = "select nama_supplier as NamaSupplier,m_user.nama_dpn as NamaDepan,alamat_supplier as Alamat,input_date_supplier as Tanggal,telp_supplier as Telp " +
                //    "from m_supplier,m_user where m_supplier.input_by=m_user.id_user;";


                string Query = "select nama_supplier as NamaSupplier,alamat_supplier as Alamat,input_date_supplier as Tanggal,telp_supplier as Telp, contact_person as `Contact Person` " +
                    "from m_supplier ";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                Console.WriteLine(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        //==================User
        public void addUser(string admin, string id_jabatan, string username, string password, string nama_dpn, string nama_blk, int telp)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_user(user_inputby,id_jabatan,username,password,nama_dpn,nama_blk,input_date,telp_user) values('" + getidAdmin(admin) + "','" + getIndexJabatan(id_jabatan) + "','" + username + "','" + password + "','" + nama_dpn + "','" + nama_blk + "','" + formatForMySql + "','" + telp + "');";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void viewUser(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                string Query = "select nama_dpn as NamaDepan,nama_blk as NamaBelakang,telp_user as Telp,username as Username,password as Password,jabatan.nama_jabatan as NamaJabatan" +
                    " from m_user,Jabatan where m_user.id_jabatan=Jabatan.id_jabatan;";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public int getIndexJabatan(string jabatan)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_jabatan as jabatan from Jabatan where nama_jabatan='" + jabatan + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
                if (index == 0)
                {
                    index = getNewJabatan(jabatan);
                }
            }

            return index;

        }
        
        public int getNewJabatan(string jabatanStr)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into jabatan(nama_jabatan) values('" + jabatanStr + "');";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return getIndexJabatan(jabatanStr);
        }

        //==================Customer
        public void addCustomer(int admin, string nama, string alamat, string ket, string telp, string contact)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_customer(customer_inputby,nama_customer,alamat,ketr,input_date_customer,telp_cust,contact_person) "+
                    " values('" + admin + "','" + nama + "','" + alamat + "','" + ket + "','" + formatForMySql + "','" + telp + "','"+contact+"' );";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void viewCustomer(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                //string Query = "select nama_customer as NamaCustomer,Alamat as Alamat,ketr as Keterangan,input_date_customer as Tanggal,telp_cust as Telp,m_user.nama_dpn as InputBy" +
                //    " from m_customer,m_user where m_customer.customer_inputby=m_user.id_user;";

                string Query = "select nama_customer as NamaCustomer,Alamat as Alamat,ketr as Keterangan,input_date_customer as Tanggal,telp_cust as Telp, contact_person" +
                    " from m_customer ";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        //===========Stock
        public int getIndexBarang(string namaBarang)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_barang from m_barang where nama_barang='" + namaBarang + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return index;

        }

        public int getIndexTypeBarang(string tpBarang)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_type from m_type where type='" + tpBarang + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return index;

        }

        public int getIndexUkuran(string ukuran)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_ukuran from m_ukuran where nama_ukuran='" + ukuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return index;

        }

        

        public void addType( string typeBesi)
        {
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_type(type) values('" + typeBesi + "');";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void addUkuran(string nmUkuran, string pjg, string lbr, string tinggi)
        {
            string faktor_hitung = "8";
            if(tinggi==null || tinggi.Length==0 || (tinggi.Length>0 && Double.Parse(tinggi) == 0))
            {
                tinggi = "NULL";
                faktor_hitung = "6.2";
            }
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_ukuran(nama_ukuran,panjang,lebar,tinggi,faktor_hitung) values('" + nmUkuran + "'," + pjg + "," + 
                    lbr + "," + tinggi + ",'" + faktor_hitung + "' );";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void addStock(string type, string pjg, string lbr, string tinggi, string qty, string kg, bool piltype)
        {

            try
            {
                string Query;
                string nmUkuran;
                if (tinggi != null && tinggi.Length > 0)
                    nmUkuran = lbr + " x " + pjg + " x " + tinggi;
                else
                    nmUkuran = lbr + " x " + pjg;
                int idType = 0;
                int idUkuran = 0;

                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");             

                if (piltype == true)
                {
                    idType = getIndexTypeBarang(type);
                    idUkuran = getIndexUkuran(nmUkuran);
                    if (idUkuran <= 0)
                    {
                        addUkuran(nmUkuran, pjg, lbr, tinggi);
                        idUkuran = getIndexUkuran(nmUkuran);
                    }

                    Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg) values('" + idType + "','" + idUkuran + "','" + qty + "','" + kg + "') " +
                        " ON DUPLICATE KEY UPDATE qty_stock= " + qty + " ;";
                }
                else
                {
                   
                    addType( type);
                    idType = getIndexTypeBarang(type);
                    addUkuran(nmUkuran, pjg, lbr, tinggi);
                    idUkuran = getIndexUkuran(nmUkuran);

                    Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg) values('" + idType + "','" + idUkuran + "','" + qty + "','" + kg + "');";
                }

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void addStock(string type, string pjg, string lbr, string tinggi, string qty, string kg, string harga, bool piltype)
        {

            try
            {
                string Query;
                string nmUkuran;
                if (tinggi != null && tinggi.Length > 0)
                    nmUkuran = lbr + " x " + pjg + " x " + tinggi;
                else
                    nmUkuran = lbr + " x " + pjg;
                int idType = 0;
                int idUkuran = 0;

                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");

                if (piltype == true)
                {
                    idType = getIndexTypeBarang(type);
                    idUkuran = getIndexUkuran(nmUkuran);
                    if (idUkuran <= 0)
                    {
                        addUkuran(nmUkuran, pjg, lbr, tinggi);
                        idUkuran = getIndexUkuran(nmUkuran);
                    }

                    Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg,harga_stock) values('" +
                        idType + "','" + idUkuran + "','" + qty + "','" + kg + "'," + harga + ") " +
                        " ON DUPLICATE KEY UPDATE qty_stock= " + qty + " ;";
                }
                else
                {

                    addType(type);
                    idType = getIndexTypeBarang(type);
                    addUkuran(nmUkuran, pjg, lbr, tinggi);
                    idUkuran = getIndexUkuran(nmUkuran);

                    Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg,harga_stock) values('" +
                        idType + "','" + idUkuran + "','" + qty + "','" + kg + "'," + harga + ");";
                }

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void addStockOnly(string idType, string idUkuran, string qty, string kg)
        {

            try
            {
                string Query;

                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");

                Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg) values(" + idType + "," + idUkuran + "," + qty + "," + kg + ") " +
                     " ON DUPLICATE KEY UPDATE qty_stock= " + qty + " ;";


                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void selectNamaBarang(ComboBox cb)
        {

            try
            {
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select type from m_type ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    cb.Items.Add(nama);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void selectTypeBarang(ComboBox cb, int id)
        {

            try
            {
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select type from m_type where id_barang='" + id + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    cb.Items.Add(nama);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void viewStock(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                string Query = "select id_stock, type as Type, m_ukuran.lebar, m_ukuran.panjang, m_ukuran.tinggi, nama_ukuran as Ukuran, qty_stock as QTY,kg as KG, harga_stock as Harga" +
                    " from m_stock,m_type,m_ukuran where m_stock.id_type=m_type.id_type and m_stock.id_ukuran=m_ukuran.id_ukuran ;";
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dataGridView1.Columns["id_stock"].Visible = false;
                //dataGridView1.Columns["lebar"].Visible = false;
                //dataGridView1.Columns["panjang"].Visible = false;
                //dataGridView1.Columns["tinggi"].Visible = false;

                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void viewSJ(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                string Query = "select id_sj, id_po, no_po_customer, id_creator, " +
                    " no_sj, tanggal_sj, keterangan, driver, gudang " +
                    " from m_sj ;";
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dataGridView1.Columns["id_sj"].Visible = false;

                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void viewSPB(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                string Query = "select id_spb, id_po, no_po_customer, id_creator, "+
                    " no_sj as no_spb, tanggal_sj as tanggal_spb, " +
                    " keterangan, driver, gudang " +
                    " from m_spb ;";
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dataGridView1.Columns["id_spb"].Visible = false;

                if (db != null)
                {
                    db.Close();
                }
            }
        }


        //====================Stok Awal


        public void selectBrgType(ComboBox cb, string no_po)
        {
            cb.Items.Clear();
            try
            {
                string tipe = "";
                int id_type = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT type, dpo.id_type " +
                            "from `m_type`, " +
                            "( " + 
                            "  SELECT DISTINCT id_type FROM `dt_po`,`t_po` WHERE no_po = '" + no_po + "' " +
                            " and dt_po.id_po = t_po.id_po " + 
                            ") dpo " +
                            "WHERE m_type.id_type = dpo.id_type ";

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    tipe = reader.GetString(0).ToString().ToUpper();
                    id_type = reader.GetInt32(1);
                    type itemType = new type();
                    itemType.id_type = id_type;
                    itemType.tipe = tipe;
                    //cb.Items.Add(namatipe);
                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void fillAllPO(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_po, no_po, nama_customer, alamat, nama_marketing, po_customer " +
                            " from t_po, m_customer, m_marketing " +
                            " where t_po.id_customer = m_customer.id_customer " +
                            "   and t_po.id_marketing = m_marketing.id_marketing ";

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    data_po newData = new data_po();
                    if(!reader.IsDBNull(0))
                        newData.id_po = reader.GetString(0);
                    if (!reader.IsDBNull(1))
                        newData.no_sp = reader.GetString(1);
                    if (!reader.IsDBNull(2))
                        newData.customer = reader.GetString(2);
                    if (!reader.IsDBNull(3))
                        newData.alamat_customer = reader.GetString(3);
                    if (!reader.IsDBNull(4))
                        newData.nama_marketing = reader.GetString(4);
                    if (!reader.IsDBNull(5))
                        newData.no_po_customer = reader.GetString(5);

                    cb.Items.Add(newData);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        public data_po getDataPO(string id_po)
        {
            data_po newData = new data_po();
            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_po, no_po, nama_customer, alamat, nama_marketing, po_customer " +
                            " from t_po, m_customer, m_marketing " +
                            " where t_po.id_customer = m_customer.id_customer " +
                            "   and t_po.id_marketing = m_marketing.id_marketing "+
                            "   and t_po.id_po = "+ id_po + ";  ";

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        newData.id_po = reader.GetString(0);
                    if (!reader.IsDBNull(1))
                        newData.no_sp = reader.GetString(1);
                    if (!reader.IsDBNull(2))
                        newData.customer = reader.GetString(2);
                    if (!reader.IsDBNull(3))
                        newData.alamat_customer = reader.GetString(3);
                    if (!reader.IsDBNull(4))
                        newData.nama_marketing = reader.GetString(4);
                    if (!reader.IsDBNull(5))
                        newData.no_po_customer = reader.GetString(5);

                    return newData;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return newData;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
            return newData;
        }

        public void selectAllBrgType(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                string tipe = "";
                int id_type = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT type, id_type " +
                            "from `m_type` ";

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    tipe = reader.GetString(0).ToString().ToUpper();
                    id_type = reader.GetInt32(1);
                    type itemType = new type();
                    itemType.id_type = id_type;
                    itemType.tipe = tipe;
                    //cb.Items.Add(namatipe);
                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void selectAvailableSPKTahun(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT YEAR(input_time) tahun FROM t_spk GROUP BY YEAR(input_time)";

                dbcmd.CommandText = sql;
                MySqlDataAdapter adapter = new MySqlDataAdapter(sql, db);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "t_spk");
                cb.DisplayMember = "tahun";
                cb.ValueMember = "tahun";
                cb.DataSource = ds.Tables["t_spk"];
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void selectAllCustomer(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_customer, nama_customer " +
                            "from `m_customer` WHERE nama_customer <> '' ORDER BY nama_customer";
                
                dbcmd.CommandText = sql;
                MySqlDataAdapter adapter = new MySqlDataAdapter(sql,db);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "m_customer");
                cb.DisplayMember = "nama_customer";
                cb.ValueMember = "id_customer";
                cb.DataSource = ds.Tables["m_customer"];
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void selectAllAvailableBrgType(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                string tipe = "";
                int id_type = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT type, m_type.id_type " +
                            "from `m_type`, " +
                            "( "+
                            "  select distinct id_type "+
                            "  from m_stock "+
                            "  where qty_stock > 0 "+
                            ")stock "+
                            "WHERE m_type.id_type = stock.id_type"
                            ;

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    tipe = reader.GetString(0).ToString().ToUpper();
                    id_type = reader.GetInt32(1);
                    type itemType = new type();
                    itemType.id_type = id_type;
                    itemType.tipe = tipe;
                    //cb.Items.Add(namatipe);
                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }


        public void getAvailableUkuran(ComboBox cb /*cbUkuran*/, string id_type_pilih, string id_po)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p = 0, l = 0, t = 0;
                string nama = "", idStock = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();

                string sql = "SELECT Qr.* " +
                            "FROM  ( " +
                            " SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, m_stock.qty_stock, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi, " +
                            "       m_stock.id_stock " +
                            " FROM m_stock, m_ukuran " +
                            " WHERE m_stock.id_type = " + id_type_pilih + " " +
                            " AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            " AND qty_stock > 0 " +
                            " ORDER BY lebar, panjang, tinggi " +
                            ") Qr, ( " +
                            "    select min(lebar) as lmin, min(panjang) as pmin, min(coalesce(tinggi,0)) as tmin " +
                            "    from dt_po, m_ukuran " +
                            "    where id_po = " + id_po + " " +
                            "    and dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            ")minim " +
                            "WHERE Qr.panjang >= minim.pmin " +
                            "AND Qr.lebar >= minim.lmin " +
                            "AND Qr.tinggi >= minim.tmin ";

                Console.WriteLine("query ukuranRekom = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    idStock = reader.GetString(6).ToString();

                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    itemUkuran.id_stock = idStock;
                    

                    if (itemUkuran.max_qty > 0)
                        cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getAvailableUkuran(ComboBox cb /*cbUkuran*/, string id_type_pilih, string id_po, Dictionary<string, int> dicAwalUsedQuota)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p = 0, l = 0, t = 0;
                string nama = "", idStock = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();

                string sql = "SELECT Qr.* " +
                            "FROM  ( " +
                            " SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, m_stock.qty_stock, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi, " +
                            "       m_stock.id_stock " +
                            " FROM m_stock, m_ukuran " +
                            " WHERE m_stock.id_type = " + id_type_pilih + " " +
                            " AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            " AND qty_stock > 0 " +
                            " ORDER BY lebar, panjang, tinggi " +
                            ") Qr, ( " +
                            "    select min(lebar) as lmin, min(panjang) as pmin, min(coalesce(tinggi,0)) as tmin " +
                            "    from dt_po, m_ukuran " +
                            "    where id_po = " + id_po + " " +
                            "    and dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            ")minim " +
                            "WHERE Qr.panjang >= minim.pmin " +
                            "AND Qr.lebar >= minim.lmin " +
                            "AND Qr.tinggi >= minim.tmin ";

                Console.WriteLine("query ukuranRekom = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    idStock = reader.GetString(6).ToString();

                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    itemUkuran.id_stock = idStock;

                    if(dicAwalUsedQuota.ContainsKey(id_type_pilih+"-"+ukuran_id))
                    {
                        itemUkuran.max_qty -= dicAwalUsedQuota[id_type_pilih + "-" + ukuran_id];
                    }

                    if(itemUkuran.max_qty>0)
                        cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getAllUkuran(ComboBox cb /*cbUkuran*/, string id_po)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p = 0, l = 0, t = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();

                string sql = "SELECT Qr.* "+
                            "FROM ( "+
                            " SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, 0, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi " +
                            " FROM m_ukuran " +
                            " ORDER BY lebar, panjang, tinggi "+
                            ") Qr, ( " +
                            "    select min(lebar) as lmin, min(panjang) as pmin, min(coalesce(tinggi,0)) as tmin " +
                            "    from dt_po, m_ukuran " +
                            "    where id_po = " + id_po + " " +
                            "    and dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            ")minim " +
                            "WHERE Qr.panjang >= minim.pmin " +
                            "AND Qr.lebar >= minim.lmin " +
                            "AND Qr.tinggi >= minim.tmin ";

                //Console.WriteLine("query ukuranRekom = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getAllUkuranPO(ComboBox cb /*cbUkuran*/, string id_po)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p = 0, l = 0, t = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();

                string sql = " SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, 0, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi " +
                            " FROM m_ukuran, dt_po " +
                            "    where id_po = " + id_po + " " +
                            "    and dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            " ORDER BY lebar, panjang, tinggi ";
                           

                //Console.WriteLine("query ukuranRekom = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        public void getnamaUkuran(ComboBox cb /*cbUkuran*/, string id_type, string no_po, string id_ukuran, string id_type_pilih)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p=0, l=0, t=0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                //string sql = "SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, m_stock.qty_stock, " +
                //             "       m_ukuran.panjang, m_ukuran.lebar, m_ukuran.tinggi " +
                //             "FROM m_stock, m_ukuran, " +
                //             "( " +
                //             " select m_type.id_type, m_type.type, panjang as p, lebar as l, tinggi as t " +
                //             " from dt_po, m_ukuran, m_type, t_po " +
                //             " where dt_po.id_ukuran = m_ukuran.id_ukuran " +
                //             "   AND dt_po.id_type = m_type.id_type " +
                //             "   AND dt_po.id_po = t_po.id_po " +
                //             "     AND t_po.no_po = '" + no_po + "' " +
                //             "     AND m_type.id_type = " + id_type + " " +
                //             "     AND dt_po.id_ukuran = " + id_ukuran + " " +
                //             ") ord " +
                //             "WHERE m_stock.id_type = ord.id_type " +
                //             "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                //             "AND panjang >= ord.p AND lebar>= ord.l AND tinggi >= ord.t " +
                //             "AND qty_stock > 0 ";

                string sql = "SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, m_stock.qty_stock, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi " +
                            "FROM m_stock, m_ukuran, " +
                            "( " +
                            " select m_type.id_type, m_type.type, panjang as p, lebar as l, tinggi as t " +
                            " from dt_po, m_ukuran, m_type, t_po " +
                            " where dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            "   AND dt_po.id_type = m_type.id_type " +
                            "   AND dt_po.id_po = t_po.id_po " +
                            "     AND t_po.no_po = '" + no_po + "' " +
                            "     AND m_type.id_type = " + id_type + " " +
                            "     AND dt_po.id_ukuran = " + id_ukuran + " " +
                            ") ord " +
                            "WHERE m_stock.id_type = "+ id_type_pilih + " " +
                            "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            "AND panjang >= ord.p AND lebar>= ord.l AND  COALESCE(tinggi, 0 ) >=  COALESCE(ord.t, 0 ) " +
                            "AND qty_stock > 0 ";

                Console.WriteLine("query ukuranRekom = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public int countPOWithSameSize(string id_po, string id_ukuran)
        {
            int hasil = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select count(*) from dt_po where id_po=" + id_po + " and id_ukuran = "+id_ukuran+" ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    hasil = reader.GetInt32(0);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public void getTypeRekomendasi(ComboBox cb /*cbType */, string id_type, string no_po, string id_ukuran)
        {
            cb.Items.Clear();
            try
            {
                int type_id = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();


                string sql = "SELECT distinct m_stock.id_type, m_type.type " +
                            "FROM m_stock, m_ukuran, m_type, " +
                            "( " +
                            " select m_type.id_type, m_type.type, panjang as p, lebar as l, COALESCE(tinggi,0) as t " +
                            " from dt_po, m_ukuran, m_type, t_po " +
                            " where dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            "   AND dt_po.id_type = m_type.id_type " +
                            "   AND dt_po.id_po = t_po.id_po " +
                            "     AND t_po.no_po = '" + no_po + "' " +
                            "     AND m_type.id_type = " + id_type + " " +
                            "     AND dt_po.id_ukuran = " + id_ukuran + " " +
                            ") ord " +
                            "WHERE m_stock.id_type = m_type.id_type " +
                            "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            "AND panjang >= ord.p AND lebar>= ord.l AND  COALESCE(tinggi, 0 ) >=  COALESCE(ord.t, 0 ) " +
                            "AND qty_stock > 0 ";

                Console.WriteLine("Query typeRekom = "+sql);
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    type_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    type itemType = new type();
                    itemType.id_type = type_id;
                    itemType.tipe = nama;

                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getQtyHarga(TextBox lblQty, TextBox lblKg, int id_ukuran, int id_type)
        {
            try
            {
                int qty = 0;
                int harga = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select qty_stock, harga_stock from m_stock where id_ukuran = '" + id_ukuran + "' and id_type='" + id_type + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    qty = reader.GetInt32(0);
                    harga = reader.GetInt32(1);
                    lblQty.Text = qty.ToString();
                    lblKg.Text = harga.ToString();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }
        
        public void getnamaUkuranOrder(ComboBox cb, string id, string no_po)
        {
            cb.Items.Clear();
            try
            {
                int id_ukuran = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT nama_ukuran, dpo.id_ukuran " +
                            "from `m_ukuran`, " +
                            "( " +
                            "  SELECT id_ukuran FROM `dt_po`,`t_po` WHERE no_po = '" + no_po + "' " +
                            "  and id_type = "+id+" and dt_po.id_po = t_po.id_po " +
                            ") dpo " +
                            "WHERE m_ukuran.id_ukuran = dpo.id_ukuran ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    id_ukuran = reader.GetInt32(1);
                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = id_ukuran;
                    itemUkuran.nama_ukuran = nama;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getQtyHargaOrder(Label lblQty, Label lblKg, int id_ukuran, int id_type)
        {
            try
            {
                int qty = 0;
                int harga = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select qty_po, harga_po from dt_po where id_ukuran = '" + id_ukuran + "' and id_type='" + id_type + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    qty = reader.GetInt32(0);
                    harga = reader.GetInt32(1);
                    lblQty.Text = qty.ToString();
                    lblKg.Text = harga.ToString();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getQtyKgOrder(TextBox lblQty, TextBox lblKg, int id_ukuran, int id_type)
        {
            try
            {
                int qty = 0;
                double p=0, l=0, t = 0;
                double kg = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select qty_po, panjang, lebar,  COALESCE(tinggi, 0 ) as tinggi " +
                             "from dt_po, m_ukuran where dt_po.id_ukuran = " + id_ukuran + " "+
                             "and dt_po.id_ukuran = m_ukuran.id_ukuran " +
                             "and dt_po.id_type = " + id_type + " ";

                Console.WriteLine("query kg = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    qty = reader.GetInt32(0);
                    p = reader.GetDouble(1);
                    l = reader.GetDouble(2);
                    t = reader.GetDouble(3);
                    if (t > 0)
                    {
                        kg = (p / 100) * (l / 100) * (t/100) * 8.0f * qty;
                    }else
                    {
                        kg = (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
                    }
                    lblQty.Text = qty.ToString();
                    lblKg.Text = kg.ToString();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        //=============INSERT STOCK

        public int getId_po(string no_po)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_po  from t_po where no_po='" + no_po + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public void addt_spk(int id_mesin, int inputby, int id_po, string no_spk, int status_spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into t_spk(id_mesin,admin_inputby,id_po,no_spk,input_time,status_spk) " +
                    "values('" + id_mesin + "','" + inputby + "','" + id_po + "','" + no_spk + "','" + formatForMySql + "','" + status_spk + "');";

                Console.WriteLine("insert t_spk = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string addt_spk(string inputby, string id_po, string no_spk, int status_spk)
        {
            string hasil = "0";
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into t_spk(admin_inputby,id_po,no_spk,input_time,status_spk) " +
                    " values(" + inputby + "," + id_po + ",'" + no_spk + "','" + formatForMySql + "','" + status_spk + "'); "+
                    " select last_insert_id();";

                Console.WriteLine("insert t_spk = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
            return hasil;
        }


        public string addSJ(string id_po, string id_admin, string no_sj, string tgl_sj, string telp_sj, string fax_sj, string ket_sj, string no_po)
        {
            string hasil = "0";
            try
            {
                string Query = "insert into m_sj(id_po,id_creator,no_sj,tanggal_sj,telp,fax,keterangan, no_po_customer) " +
                    " values(" + id_po + "," + id_admin + ",'" + no_sj + "','" + tgl_sj + "','" + telp_sj + "','" + fax_sj + "','"+ ket_sj + "','" + no_po + "'); " +
                    " select last_insert_id();";

                Console.WriteLine("insert m_sj = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
            return hasil;
        }

        public string addSPB(string id_po, string id_admin, string no_sj, string tgl_sj, string telp_sj, string fax_sj, string ket_sj, string no_po)
        {
            string hasil = "0";
            try
            {
                string Query = "insert into m_spb(id_po,id_creator,no_sj,tanggal_sj,telp,fax,keterangan, no_po_customer) " +
                    " values(" + id_po + "," + id_admin + ",'" + no_sj + "','" + tgl_sj + "','" + telp_sj + "','" + fax_sj + "','" + ket_sj + "','" + no_po + "'); " +
                    " select last_insert_id();";

                Console.WriteLine("insert m_spb = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
            return hasil;
        }


        public string add_dt_sj(string id_sj, string id_dt_po, string qty_deliver, string kg_deliver)
        {
            string hasil = "0";
            try
            {
                string Query = "insert into dt_sj(id_sj,id_dt_po,qty_deliver,kg_deliver) " +
                    " values(" + id_sj + "," + id_dt_po + ",'" + qty_deliver + "','" + kg_deliver + "'); " +
                    " select last_insert_id();";

                Console.WriteLine("insert dt_sj = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
            return hasil;
        }

        public string add_dt_spb(string id_sj, string id_dt_po, string qty_deliver, string kg_deliver)
        {
            string hasil = "0";
            try
            {
                string Query = "insert into dt_spb(id_spb,id_dt_po,qty_deliver,kg_deliver) " +
                    " values(" + id_sj + "," + id_dt_po + ",'" + qty_deliver + "','" + kg_deliver + "'); " +
                    " select last_insert_id();";

                Console.WriteLine("insert dt_spb = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
            return hasil;
        }

        public bool cekUkuran(string namaUkuran)
        {
            bool count = false;
            int jml = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_ukuran from m_ukuran where nama_ukuran='" + namaUkuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    jml = jml + 1;
                }

                if (jml > 0)
                    count = true;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return count;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return count;
        }

        public string getBentukUkuran(string namaUkuran)
        {
            string nama = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select type_bentuk from m_ukuran where nama_ukuran = '" + namaUkuran + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }


        public string getIdMesinOperation(string id_dt_spk)
        {
            string nama = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_mesin_operation from t_mesinoperation where id_dt_spk = '" + id_dt_spk + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public string findPOWithSameSize(string id_po, string id_ukuran, string nama_type)
        {
            string nama = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT * FROM " +
                            " ( " +
                            "  select id_dt_po, m_type.type, levenshtein(m_type.type, '" + nama_type + "') as countLeven " +
                            "  from dt_po, m_type " +
                            "  where id_po = " + id_po + " " +
                            "  and id_ukuran = " + id_ukuran + " " +
                            "  and dt_po.id_type = m_type.id_type " +
                            ") Qr " +
                            "ORDER BY countLeven ASC LIMIT 1 ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public int getId_Spk(string no_spk)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_spk  from t_spk where no_spk='" + no_spk + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public int getIdStock(int id_type, int id_ukuran)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_stock  from m_stock where id_type='" + id_type + "' and id_ukuran='" + id_ukuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                }

                if(nama==0)
                {
                    reader.Close();
                    dbcmd = db.CreateCommand();
                    sql = "insert into m_stock (id_type, id_ukuran, qty_stock, kg) values ('" + id_type + "', '" + id_ukuran + "', 0, 0 );  " +
                        "  select last_insert_id(); "; ;
                    dbcmd.CommandText = sql;
                    reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        nama = int.Parse(reader.GetString(0).ToUpper());
                        
                    }
                }

                return nama;


            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public double countKg(double p, double l, double t, decimal qtyVal)
        {
            int qty = (int)qtyVal;
            if (t > 0)
            {
                return (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;
            }
            else
            {
                return (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
            }
        }

        public ukuran getUkuran(string l, string p, string t)
        {
            int namasalah = 00;
            int nama = 0;
            ukuran hasil = new ukuran();

            string faktor_hitung = "8";
            if (t == null || t.Length == 0 || (t.Length > 0 && Double.Parse(t) == 0))
            {
                t = "NULL";
                faktor_hitung = "6.2";
            }

            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_ukuran, nama_ukuran, lebar, panjang, tinggi, faktor_hitung " +
                            " from m_ukuran "+
                            " where lebar=" + l + " and panjang=" + p + " and tinggi="+t+" ";
                if(t=="NULL")
                    sql = "select id_ukuran, nama_ukuran, lebar, panjang, tinggi, faktor_hitung " +
                            " from m_ukuran " +
                            " where lebar=" + l + " and panjang=" + p + " and tinggi IS NULL ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    hasil.id_ukuran = nama;
                    hasil.nama_ukuran = reader.GetString(1);
                    hasil.lebar = reader.GetInt32(2);
                    hasil.panjang = reader.GetInt32(3);
                    if(!reader.IsDBNull(4))
                        hasil.tinggi = reader.GetInt32(4);
                }

                if (nama == 0)
                {
                    reader.Close();
                   
                    string nama_ukuran = grupUkuran(l, p, t);

                    dbcmd = db.CreateCommand();
                    sql = "insert into m_ukuran (nama_ukuran, lebar, panjang, tinggi, faktor_hitung) " +
                        "  values ('" + nama_ukuran + "', " + l + " , " + p + " , " + t + " , " + faktor_hitung + "  );  " +
                        "  select last_insert_id(); ";
                    dbcmd.CommandText = sql;
                    reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        nama = int.Parse(reader.GetString(0).ToUpper());
                        hasil.id_ukuran = nama;
                        hasil.nama_ukuran = nama_ukuran;
                        hasil.lebar = int.Parse(l);
                        hasil.panjang = int.Parse(p);
                        if(t!="NULL")
                            hasil.tinggi = int.Parse(t);
                    }
                }

                return hasil;


            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return hasil;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public string grupUkuran(string l, string p, string t)
        {
            string gabung;
            if (t.Length > 0 && !t.Equals("NULL"))
                gabung = l + " x " + p + " x " + t;
            else
                gabung = l + " x " + p;

            return gabung;
        }

        public bool cekStock(int id_type, int id_ukuran)
        {
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select count(*) from m_stock where id_type=" + id_type + " and id_ukuran=" + id_ukuran + "";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return false;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return (nama>0);
        }

        public void adddt_spk(int id_spk, int stock_awal, int stock_order, int stock_akhir, string qty_awal, string qty_order, string qty_akhir, 
            int status, string kg_awal, string kg_order, string kg_akhir)
        {
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into dt_spk(id_spk,stock_awal,stock_order,stock_akhir,qty_awal,qty_order,qty_akhir, status_pengerjaan, "+
                               "                   kg_awal, kg_order, kg_akhir) " +
                    "values('" + id_spk + "','" + stock_awal + "','" + stock_order + "','" + stock_akhir + "','" + qty_awal + "','" + qty_order + "', " +
                    "'" + qty_akhir + "','" + status + "', "+kg_awal+ ", " + kg_order + ", " + kg_akhir + " ); ";

                Console.WriteLine("Query DT_SPK = "+Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string adddt_spk(int id_spk, int stock_awal, int stock_order, int stock_akhir, string qty_awal, string qty_order, string qty_akhir,
           int status, string kg_awal, string kg_order, string kg_akhir, string idmesin, string prequisite)
        {
            string strStockAkhir = "";
            if (stock_akhir == -1)
            {
                strStockAkhir = "NULL";
                qty_akhir = "NULL";
                kg_akhir = "NULL";
            }
            else
                strStockAkhir = stock_akhir.ToString();

            string hasil = "";
            try
            {
                DateTime now = DateTime.Now;
                string Query = " insert into dt_spk(id_spk,stock_awal,stock_order,stock_akhir,qty_awal,qty_order,qty_akhir, status_pengerjaan, " +
                               "                   kg_awal, kg_order, kg_akhir, id_mesin, prequesite_dt_spk) " +
                    "values( " + id_spk + "," + stock_awal + "," + stock_order + "," + strStockAkhir + ",'" + qty_awal + "','" + qty_order + "', " +
                    qty_akhir + ",'" + status + "', " + kg_awal + ", " + kg_order + ", " + kg_akhir + ", " + idmesin + " , " + prequisite + "  ); " +
                    " select last_insert_id();";

                Console.WriteLine("Query DT_SPK = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return hasil;
        }


        public string adddt_spk(int id_spk, int stock_awal, int stock_order, string qty_awal, string qty_order, 
           int status, string kg_awal, string kg_order, string idmesin, string ket)
        {

            string hasil = "";
            try
            {
                DateTime now = DateTime.Now;
                string Query = " insert into dt_spk(id_spk,stock_awal,stock_order,qty_awal,qty_order, status_pengerjaan, " +
                               "                   kg_awal, kg_order, id_mesin, keterangan) " +
                    "values( " + id_spk + "," + stock_awal + "," + stock_order + ",'" + qty_awal + "','" + qty_order + "', '" + status + "', " + 
                                kg_awal + ", " + kg_order + ", " + idmesin + " , '" + ket + "'  ); " +
                    " select last_insert_id();";

                Console.WriteLine("Query DT_SPK = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return hasil;
        }

        public void insertMultiple(string Query)
        {

            string hasil = "";
            try
            {

                Console.WriteLine("Query DT_SPK = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public int getId_Customer(string no_po)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_customer  from t_po where no_po='" + no_po + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public string getNameCustomer(string no_po)
        {
            string namasalah = "";
            string nama = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_customer from m_customer, " +
                    "( " +
                    " select id_customer from t_po where no_po = '" + no_po + "' " + 
                    ") tpo " +
                    "where m_customer.id_customer = tpo.id_customer ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public void updatePO(string no_po, int status)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_po set status_po='" + 1 + "' where no_po='" + no_po + "';";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void updateStock(string id_stock, string qty, string kg, string harga)
        {
            if (string.IsNullOrEmpty(qty))
                qty = "0";
            if (string.IsNullOrEmpty(kg))
                kg = "0";
            if (string.IsNullOrEmpty(harga))
                harga = "NULL";
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update m_stock set kg="+kg+", qty_stock="+qty+", harga_stock=" + harga + " where id_stock=" + id_stock + ";";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void updateAsKesalahan(string idStockAkhirSalah)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update dt_spk_real_stok_akhir set is_salah_potong=" + 1 + " where id_real_stok_akhir='" + idStockAkhirSalah + "';";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");

                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public int getId_dt_po(int id_po, int idUkuran)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_dt_po  from dt_po where id_po='" + id_po + "' and id_ukuran='" + idUkuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        // Report
        public void viewDokumen(DataGridView dataGridView1)
        {           
            try
            {
                openkoneksi();
                string sql = "SELECT md.id, mc.nama_customer AS `Nama Customer`, md.doc_no AS `No. Doc`, md.keterangan AS `Keterangan`, FORMAT(md.total,2) AS `Total`, DATE_FORMAT(md.tgl_input, '%Y/%m/%d %H:%i') AS `Tanggal` " +
                                "FROM m_dokumen md " +
                                "INNER JOIN m_customer mc ON mc.id_customer = md.id_customer " +
                                "ORDER BY md.tgl_input DESC";
                
                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable;

                dataGridView1.Columns[0].Visible = false;

                dataGridView1.Columns[1].Width = 200;
                dataGridView1.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter;
                dataGridView1.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter;
                dataGridView1.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter;
                dataGridView1.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter;
                dataGridView1.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter;

                dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
                dataGridView1.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

        }

        public void viewReport(DataGridView dataGridView1)
        {
            //tgl.spk.oprt.start.finish            
            try
            {
                openkoneksi();
                string sql = "SELECT " +
                                "DATE_FORMAT(spk.input_time, '%d/%m/%Y') AS Tanggal, " +
                                "spk.no_spk as SPK, " +
                                "DATE_FORMAT(spk.start_time, '%H:%i') AS Start, " +
                                "DATE_FORMAT(spk.finish_time, '%H:%i') AS Finish, " +
                                "TIMESTAMPDIFF(MINUTE, spk.start_time, spk.finish_time) AS Waktu, " +
                                "case spk.status_spk " +
                                "    when 0 then 'Antrian' " +
                                "    when 1 then 'Progress' " +
                                "    else 'Finish' " +
                                "end AS Status " +
                            "FROM `t_spk` spk; ";

                Console.WriteLine("QUERY DGV == "+sql);
                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                Console.WriteLine(dTable);
                dataGridView1.DataSource = dTable;               

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

        }

        public void viewReportNoInvoice(DataGridView dataGridView1)
        {
            //tgl.spk.oprt.start.finish            
            try
            {
                openkoneksi();
                string sql = "SELECT DATE_FORMAT(spk.input_time, '%d/%m/%Y') AS Tanggal, " +
                            "        spk.no_spk as SPK,  " +
                            "        DATE_FORMAT(spk.start_time, '%H:%i') AS Start, " +
                            "        DATE_FORMAT(spk.finish_time, '%H:%i') AS Finish, " +
                            "        TIMESTAMPDIFF(MINUTE, spk.start_time, spk.finish_time) AS Waktu, " +
                            "        case spk.status_spk " +
                            "           when 0 then 'Antrian' " +
                            "           when 1 then 'Progress'  " +
                            "           else 'Finish' " +
                            "        end AS Status, " +
                            "        po.no_po, po.nama_customer, po.jumlah as `total harga`, ready.jum_ready as `ready deliver` " +
                            "FROM `t_spk` spk, " +
                            "( " +
                            "   select id_po, no_po, nama_customer, jumlah " +
                            "   from t_po, m_customer " +
                            "   where t_po.id_customer = m_customer.id_customer " +
                            "   and no_invoice is null " +
                            ") po, " +
                            "( " +
                            "  select id_spk, count(*) as jum_ready " +
                            "  from `dt_spk`  " +
                            "  where is_delivered is null " +
                            "  and status_pengerjaan = 1 " +
                            "  group by id_spk " +
                            ") ready " +
                            "WHERE spk.id_po = po.id_po " +
                            "AND spk.id_spk = ready.id_spk ";

                Console.WriteLine("QUERY DGV == " + sql);
                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                Console.WriteLine(dTable);
                dataGridView1.DataSource = dTable;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

        }


        public void viewReportDtSPK(DataGridView dataGridView1)
        {
            //tgl.spk.oprt.start.finish            
            try
            {
                openkoneksi();
                string sql = "SELECT DATE_FORMAT(spk.input_time, '%d/%m/%Y') AS Tanggal, spk.no_spk as SPK, spk.id_spk, spk.id_po, "+
                            " dt.id_dt_spk as `dt spk`, dt.type, dt.nama_ukuran, "+
                            " dt.qty_order, dt.kg_order, dt.id_mesin, dt.keterangan, "+
                            " DATE_FORMAT(dt.start, '%H:%i') AS Start, DATE_FORMAT(dt.finish, '%H:%i') AS Finish, " +
                            " TIMESTAMPDIFF(MINUTE, dt.start, dt.finish) AS Waktu, " +
                            " case dt.status_pengerjaan " +
                            "     when 0 then 'Antrian' " +
                            "     when 1 then 'Progress' " +
                            "     else 'Finish' " +
                            " end AS Status " +
                            "FROM `t_spk` spk, " +
                            "( " +
                            "  SELECT qr . * , t_mesinoperation.start, t_mesinoperation.finish " +
                            "  FROM( " +
                            "    SELECT dt.id_dt_spk, dt.id_spk, m_type.type, m_ukuran.nama_ukuran, dt.qty_order, dt.kg_order, dt.id_mesin, dt.keterangan, dt.status_pengerjaan " +
                            "    FROM dt_spk dt, m_type, m_ukuran, m_stock " +
                            "    WHERE dt.stock_awal = m_stock.id_stock " +
                            "    AND m_stock.id_type = m_type.id_type " +
                            "    AND dt.stock_order = m_ukuran.id_ukuran " +
                            "  )qr " +
                            "  LEFT OUTER JOIN t_mesinoperation ON (qr.id_dt_spk = t_mesinoperation.id_dt_spk)  " +
                            ")dt " +
                            "where spk.id_spk = dt.id_spk ; ";

                Console.WriteLine("QUERY DGV == " + sql);
                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                Console.WriteLine(dTable);

                dataGridView1.DataSource = dTable;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }

                dataGridView1.Columns["id_spk"].Visible = false;
                dataGridView1.Columns["dt spk"].Visible = false;
                dataGridView1.Columns["id_po"].Visible = false;
            }

        }
        
        public void fillReadySPK(DataGridView dataGridView1, string id_spk)
        {
            try
            {
                openkoneksi();

                string Query = "select type, nama_ukuran, qty_order, kg_order " +
                               "from dt_spk, m_stock, m_type, m_ukuran " +
                    "where dt_spk.id_spk = '" + id_spk + "' " +
                    "and dt_spk.id_dt_po is not null " +
                    "and dt_spk.is_delivered is null " +
                    "and m_stock.id_stock = dt_spk.stock_order "+
                    "and m_stock.id_type = m_type.id_type " +
                    "and m_stock.id_ukuran = m_ukuran.id_ukuran " ;

                Console.WriteLine("queryQyu = " + Query);

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);

                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void fillReadyPO(DataGridView dataGridView1, string id_po)
        {
            dataGridView1.DataSource = null; 
            try
            {
                openkoneksi();

                if(!dataGridView1.Columns.Contains("Button"))
                {
                    DataGridViewButtonColumn cmdbtn = new DataGridViewButtonColumn();
                    dataGridView1.Columns.Add(cmdbtn);
                    cmdbtn.HeaderText = "X";
                    cmdbtn.Text = "X";
                    cmdbtn.Name = "Button";
                    cmdbtn.UseColumnTextForButtonValue = true;
                }


                string Query = "select id_dt_po, type as `TYPE`, nama_ukuran as `UKURAN`, qty_po as `QTY ORDER`, berat as `KG ORDER`, " +
                               "  m_ukuran.lebar, m_ukuran.panjang, m_ukuran.tinggi " +
                               "from dt_po, m_type, m_ukuran " +
                               "where dt_po.id_po = '" + id_po + "' " +
                               "and dt_po.id_type = m_type.id_type " +
                               "and dt_po.id_ukuran = m_ukuran.id_ukuran ";

                Console.WriteLine("queryQyu = " + Query);

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);

                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
                dataGridView1.Columns["id_dt_po"].Visible = false;
                dataGridView1.Columns["lebar"].Visible = false;
                dataGridView1.Columns["panjang"].Visible = false;
                dataGridView1.Columns["tinggi"].Visible = false;
            }
        }


        public void listOrder(DataGridView dataGridView1, string no_po)
        {
            try
            {
                openkoneksi();

                string Query = "select type, dt_po.id_type,  lebar, panjang, COALESCE(tinggi,0) as tinggi, qty_po " +
                               "from m_type, dt_po, t_po, m_ukuran " +
                    "where dt_po.id_po = t_po.id_po and t_po.no_po = '" + no_po + "' " +
                    "and dt_po.id_type = m_type.id_type " +
                    "and dt_po.id_ukuran = m_ukuran.id_ukuran "+
                    "order by  lebar, panjang, COALESCE(tinggi,0) ";

                Console.WriteLine("queryQyu = " + Query);

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);

                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void listRekomendasi(DataGridView dataGridView1, string type, string p, string l, string t)
        {
            if (p.Length == 0)
                p = int.MaxValue.ToString();
            if (l.Length == 0)
                l = int.MaxValue.ToString();
            if (t.Length == 0)
                t = int.MaxValue.ToString();
            try
            {
                openkoneksi();

                //string Query = "SELECT TYPE , panjang, lebar, COALESCE(tinggi,0) as tinggi, qty_stock " +
                //                "FROM m_type, m_stock, m_ukuran " +
                //                "WHERE m_stock.id_type = m_type.id_type " +
                //                "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                //                "AND type = '" + type + "' and panjang >= " + p + " and lebar>= " + l + " " +
                //                "and tinggi >= " + t + " and qty_stock > 0 ";

                string Query = "SELECT TYPE ,  lebar, panjang, COALESCE(tinggi,0) as tinggi, qty_stock " +
                                "FROM m_type, m_stock, m_ukuran " +
                                "WHERE m_stock.id_type = m_type.id_type " +
                                "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                                "AND panjang >= " + p + " and lebar>= " + l + " " +
                                "and  COALESCE( tinggi, 0 ) >= COALESCE(" + t +",0) and qty_stock > 0 ";

                Console.WriteLine("queryQyu = " + Query);

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);

                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void decreseStock(int id_stock, string qty)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "UPDATE `m_stock` SET `qty_stock` = `qty_stock` - "+ qty +" WHERE `id_stock` = "+id_stock;

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void decreseStock(int id_stock, string qty, string kg)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = " UPDATE `m_stock` SET `qty_stock` = `qty_stock` - " + qty + ", " +
                               "                      kg = kg - " + kg + "  " +
                               " WHERE `id_stock` = " + id_stock;

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void setFinishSPK(int id_spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "UPDATE `t_spk` SET status_spk = 2 WHERE id_spk = " + id_spk;

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void setDTPO(string id_dt_spk, string id_dt_po)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "UPDATE `dt_spk` SET id_dt_po = " + id_dt_po + " WHERE id_dt_spk = " + id_dt_spk;

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void setIsStokAkhirUsed(string id_dt_spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "UPDATE `dt_spk` SET IS_STOK_AKHIR_USED = 1 WHERE id_dt_spk = " + id_dt_spk;

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void getLangkahPotong(string id_dt_spk, FlowLayoutPanel lbox, FlowLayoutPanel lboxWork)
        {
            lbox.Controls.Clear();
            lboxWork.Controls.Clear();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_langkah_potong, lebar, panjang, tinggi, qty, qty_real, kg_real  " +
                            " FROM dt_spk_langkah_potong " +
                            " where id_dt_spk = " + id_dt_spk + " ;  ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        LangkahPotongPeriksa newLangkah = new LangkahPotongPeriksa();
                        WorkLangkahPotong newWork = new WorkLangkahPotong();

                        newLangkah.lo.Text = reader.GetString(1);
                        newWork.lo.Text = newLangkah.lo.Text;

                        newLangkah.po.Text = reader.GetString(2);
                        newWork.po.Text = newLangkah.po.Text;

                        if (!reader.IsDBNull(3))
                        {
                            newLangkah.to.Text = reader.GetString(3);
                            newWork.to.Text = newLangkah.to.Text;
                        }

                        newLangkah.qtyO.Text = reader.GetString(4);

                        if(!reader.IsDBNull(5))
                            newWork.qtyO.Text = reader.GetString(5);

                        if (!reader.IsDBNull(6))
                            newWork.kgO.Text = reader.GetString(6);

                        newWork.id_langkah_potong = reader.GetString(0);

                        lbox.Controls.Add(newLangkah);
                        lboxWork.Controls.Add(newWork);

                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getLangkahPotong(string id_dt_spk, FlowLayoutPanel lbox)
        {
            lbox.Controls.Clear();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_langkah_potong, lebar, panjang, tinggi, qty  " +
                            " FROM dt_spk_langkah_potong " +
                            " where id_dt_spk = " + id_dt_spk + " ;  ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        LangkahPotong newLangkah = new LangkahPotong();                      

                        newLangkah.lo.Text = reader.GetString(1);
                        newLangkah.po.Text = reader.GetString(2);
                        newLangkah.to.Text = reader.GetString(3);
                        newLangkah.qtyO.Text = reader.GetString(4);
                        
                        lbox.Controls.Add(newLangkah);
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getRealStokAkhir(string id_dt_spk, FlowLayoutPanel lbox)
        {
            lbox.Controls.Clear();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_real_stok_akhir, lebar, panjang, tinggi, qty, kg  " +
                             " FROM dt_spk_real_stok_akhir " +
                             " where id_dt_spk = " + id_dt_spk + " ;  ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        WorkStockAkhir newStokAkhir = new WorkStockAkhir();
                        newStokAkhir.l.Text = reader.GetString(1);
                        newStokAkhir.p.Text = reader.GetString(2);
                        newStokAkhir.t.Text = reader.GetString(3);
                        newStokAkhir.qtyA.Text = reader.GetString(4);
                        newStokAkhir.kgA.Text = reader.GetString(5);

                        lbox.Controls.Add(newStokAkhir);
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        public void getRealStokAkhir(string id_dt_spk, FlowLayoutPanel lbox, EventHandler cbSalahPotong_CheckStateChanged)
        {
            lbox.Controls.Clear();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_real_stok_akhir, lebar, panjang, tinggi, qty, kg  " +
                            " FROM dt_spk_real_stok_akhir " +
                            " where id_dt_spk = " + id_dt_spk + " ;  ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        WorkStockAkhir newStokAkhir = new WorkStockAkhir();
                        newStokAkhir.l.Text = reader.GetString(1);
                        newStokAkhir.p.Text = reader.GetString(2);
                        newStokAkhir.t.Text = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                        newStokAkhir.qtyA.Text = reader.GetString(4);
                        newStokAkhir.kgA.Text = reader.GetString(5);
                        newStokAkhir.cbSalahPotong.Tag = reader.GetString(0);
                        newStokAkhir.cbSalahPotong.CheckStateChanged += cbSalahPotong_CheckStateChanged;

                        lbox.Controls.Add(newStokAkhir);
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getPlanStokAkhir(string id_dt_spk, FlowLayoutPanel lbox)
        {
            lbox.Controls.Clear();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_plan_stok_akhir, lebar, panjang, tinggi, qty, kg  " +
                            " FROM dt_spk_plan_stok_akhir " +
                            " WHERE id_dt_spk = " + id_dt_spk + " ;  ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        StokAkhirPeriksa newStokAkhir = new StokAkhirPeriksa();
                        newStokAkhir.l.Text = reader.GetString(1);
                        newStokAkhir.p.Text = reader.GetString(2);
                        if(!reader.IsDBNull(3))
                        newStokAkhir.t.Text = reader.GetString(3);
                        newStokAkhir.qtyA.Text = reader.GetString(4);
                        newStokAkhir.kgA.Text = reader.GetString(5);

                        lbox.Controls.Add(newStokAkhir);
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void updateStatusPengerjaanDtSPK(string joinedID)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update dt_spk set status_pengerjaan=2 where id_dt_spk in (" + joinedID + ");";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public dt_spk getDataDtSpk(string id_dt_spk)
        {
            dt_spk hasil = new dt_spk();

            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select m_type.type, m_ukuran.nama_ukuran, qty_awal, dt.kg_awal,  " +
                            "   ukuran_order, dt.qty_order, dt.kg_order, dt.keterangan, m_type.id_type, m_stock.id_dt_pembelian " +
                            "from dt_spk dt, m_type, m_ukuran, m_stock, " +
                            " ( " +
                            "   SELECT id_dt_spk, nama_ukuran as ukuran_order " +
                            "   from dt_spk, m_ukuran " +
                            "   where dt_spk.stock_order = m_ukuran.id_ukuran " +
                            "   and dt_spk.id_dt_spk = " + id_dt_spk + " " +
                            ") q_order " +
                            "where dt.stock_awal = m_stock.id_stock " +
                            "    and m_stock.id_type = m_type.id_type " +
                            "    and m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            "    and dt.id_dt_spk = q_order.id_dt_spk; ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        hasil.type_stok_awal = reader.GetString(0);
                        hasil.ukuran_awal = reader.GetString(1);
                        hasil.qty_awal = reader.GetString(2);
                        hasil.kg_awal = reader.GetString(3);
                        hasil.ukuran_order = reader.GetString(4);
                        hasil.qty_order = reader.GetString(5);
                        hasil.kg_order = reader.GetString(6);
                        if (!reader.IsDBNull(7))
                            hasil.keterangan = reader.GetString(7);
                        else
                            hasil.keterangan = "NULL";
                        hasil.id_type = reader.GetString(8);
                        if (!reader.IsDBNull(9))
                            hasil.id_dt_pembelian = reader.GetString(9);
                        else
                            hasil.id_dt_pembelian = "NULL";

                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public data_perusahaan getDataPerusahaan()
        {
            data_perusahaan dp = null;
            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select * from data_perusahaan";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    dp = new data_perusahaan();

                    if (!reader.IsDBNull(0))
                    {
                        dp.nama_perusahaan = reader.GetString(0);
                        dp.alamat = reader.GetString(1);
                        dp.kota = reader.GetString(2);
                        dp.telp = reader.GetString(3);
                        dp.fax = reader.GetString(4);
                        dp.norek = reader.GetString(5);
                        dp.norek_nama = reader.GetString(6);
                        dp.norek_bank = reader.GetString(7);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return dp;
        }

        public void updateDataPerusahaan(data_perusahaan data)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update data_perusahaan set nama_perusahaan = '" + data.nama_perusahaan + "', alamat = '" + data.alamat + "', kota = '" + data.kota + "', telp = '" + data.telp + "', fax = '" + data.fax + "', norek = '" + data.norek + "', norek_nama = '" + data.norek_nama + "', norek_bank = '" + data.norek_bank+"';";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }

    public class data_po
    {
        public string id_po { get; set; }
        public string no_po_customer { get; set; }
        public string no_sp { get; set; }
        public string customer { get; set; }
        public string alamat_customer { get; set; }
        public string nama_marketing { get; set; }

        public override string ToString()
        {
            return no_sp;
        }
    }

    public class dt_spk
    {
        public string type_stok_awal { get; set; }
        public string ukuran_awal { get; set; }
        public string qty_awal { get; set; }
        public string kg_awal { get; set; }

        public string ukuran_order { get; set; }
        public string qty_order { get; set; }
        public string kg_order { get; set; }

        public string keterangan { get; set; }
        public string id_type { get; set; }
        public string id_dt_pembelian { get; set; }
    }

    public class t_spk
    {
        public string input_time { set; get; }
        public int no_spk { set; get; }
        public string start_time { set; get; }
        public string finish_time { set; get; }
        public int status { set; get; }
        public int mesin { set; get; }
    }

    public class t_po
    {
        public int id_po { set; get; }
        public int user_marketing { get; set; }
        public int id_customer { get; set; }
        public DateTime tanggal { get; set; }
        public string status { get; set; }
    }

    public class stock
    {
        public string nama_barang { get; set; }
        public double panjang { get; set; }
        public double lebar { get; set; }
        public double tinggi { get; set; }
        public double diameter { get; set; }
        public string bentuk_barang { get; set; }
        public int qty { get; set; }
        public double harga { get; set; }
    }

    public class supplier
    {
        public string nama_supplier { get; set; }
        public string alamat_supplier { get; set; }
        public string input_by { get; set; }
        public DateTime input_date { get; set; }
        public int telp { get; set; }

    }

    public class type
    {
        public int id_type { get; set; }
        public string tipe { get; set; }

        public override string ToString()
        {
            return tipe;
        }
       
    }

    public class ukuran
    {
        public int id_ukuran { get; set; }
        public string nama_ukuran { get; set; }
        public int max_qty { get; set; }
        public int panjang { get; set; }
        public int lebar { get; set; }
        public int tinggi { get; set; }

        public string id_stock { get; set; }

        public override string ToString()
        {
            return nama_ukuran;
        }
    }

    public class detailUkuran
    {
        public string tipe { get; set; }
        public string panjang { get; set; }
        public string lebar { get; set; }
        public string tinggi { get; set; }
        public int qty { get; set; }
    }

    public class data_perusahaan
    {
        public string nama_perusahaan { get; set; }
        public string alamat { get; set; }
        public string kota { get; set; }
        public string telp { get; set; }
        public string fax { get; set; }
        public string norek { get; set; }
        public string norek_nama { get; set; }
        public string norek_bank { get; set; }

        public override string ToString()
        {
            return nama_perusahaan;
        }
    }



}
