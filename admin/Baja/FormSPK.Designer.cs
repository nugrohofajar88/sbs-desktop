﻿namespace Baja
{
    partial class FormSPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.qty9 = new System.Windows.Forms.NumericUpDown();
            this.kgO9 = new System.Windows.Forms.TextBox();
            this.qtyO9 = new System.Windows.Forms.TextBox();
            this.kgA9 = new System.Windows.Forms.TextBox();
            this.qtyA9 = new System.Windows.Forms.TextBox();
            this.t9 = new System.Windows.Forms.TextBox();
            this.l9 = new System.Windows.Forms.TextBox();
            this.p9 = new System.Windows.Forms.TextBox();
            this.cbA9 = new System.Windows.Forms.TextBox();
            this.kg9 = new System.Windows.Forms.Label();
            this.cbUk9 = new System.Windows.Forms.ComboBox();
            this.cb9 = new System.Windows.Forms.ComboBox();
            this.cbOUk9 = new System.Windows.Forms.ComboBox();
            this.cbO9 = new System.Windows.Forms.ComboBox();
            this.label171 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.qty8 = new System.Windows.Forms.NumericUpDown();
            this.kgO8 = new System.Windows.Forms.TextBox();
            this.qtyO8 = new System.Windows.Forms.TextBox();
            this.kgA8 = new System.Windows.Forms.TextBox();
            this.qtyA8 = new System.Windows.Forms.TextBox();
            this.t8 = new System.Windows.Forms.TextBox();
            this.l8 = new System.Windows.Forms.TextBox();
            this.p8 = new System.Windows.Forms.TextBox();
            this.cbA8 = new System.Windows.Forms.TextBox();
            this.kg8 = new System.Windows.Forms.Label();
            this.cbUk8 = new System.Windows.Forms.ComboBox();
            this.cb8 = new System.Windows.Forms.ComboBox();
            this.cbOUk8 = new System.Windows.Forms.ComboBox();
            this.cbO8 = new System.Windows.Forms.ComboBox();
            this.label154 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.qty7 = new System.Windows.Forms.NumericUpDown();
            this.kgO7 = new System.Windows.Forms.TextBox();
            this.qtyO7 = new System.Windows.Forms.TextBox();
            this.kgA7 = new System.Windows.Forms.TextBox();
            this.qtyA7 = new System.Windows.Forms.TextBox();
            this.t7 = new System.Windows.Forms.TextBox();
            this.l7 = new System.Windows.Forms.TextBox();
            this.p7 = new System.Windows.Forms.TextBox();
            this.cbA7 = new System.Windows.Forms.TextBox();
            this.kg7 = new System.Windows.Forms.Label();
            this.cbUk7 = new System.Windows.Forms.ComboBox();
            this.cb7 = new System.Windows.Forms.ComboBox();
            this.cbOUk7 = new System.Windows.Forms.ComboBox();
            this.cbO7 = new System.Windows.Forms.ComboBox();
            this.label137 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.qty6 = new System.Windows.Forms.NumericUpDown();
            this.kgO6 = new System.Windows.Forms.TextBox();
            this.qtyO6 = new System.Windows.Forms.TextBox();
            this.kgA6 = new System.Windows.Forms.TextBox();
            this.qtyA6 = new System.Windows.Forms.TextBox();
            this.t6 = new System.Windows.Forms.TextBox();
            this.l6 = new System.Windows.Forms.TextBox();
            this.p6 = new System.Windows.Forms.TextBox();
            this.cbA6 = new System.Windows.Forms.TextBox();
            this.kg6 = new System.Windows.Forms.Label();
            this.cbUk6 = new System.Windows.Forms.ComboBox();
            this.cb6 = new System.Windows.Forms.ComboBox();
            this.cbOUk6 = new System.Windows.Forms.ComboBox();
            this.cbO6 = new System.Windows.Forms.ComboBox();
            this.label120 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.qty5 = new System.Windows.Forms.NumericUpDown();
            this.kgO5 = new System.Windows.Forms.TextBox();
            this.qtyO5 = new System.Windows.Forms.TextBox();
            this.kgA5 = new System.Windows.Forms.TextBox();
            this.qtyA5 = new System.Windows.Forms.TextBox();
            this.t5 = new System.Windows.Forms.TextBox();
            this.l5 = new System.Windows.Forms.TextBox();
            this.p5 = new System.Windows.Forms.TextBox();
            this.cbA5 = new System.Windows.Forms.TextBox();
            this.kg5 = new System.Windows.Forms.Label();
            this.cbUk5 = new System.Windows.Forms.ComboBox();
            this.cb5 = new System.Windows.Forms.ComboBox();
            this.cbOUk5 = new System.Windows.Forms.ComboBox();
            this.cbO5 = new System.Windows.Forms.ComboBox();
            this.label103 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.qty4 = new System.Windows.Forms.NumericUpDown();
            this.kgO4 = new System.Windows.Forms.TextBox();
            this.qtyO4 = new System.Windows.Forms.TextBox();
            this.kgA4 = new System.Windows.Forms.TextBox();
            this.qtyA4 = new System.Windows.Forms.TextBox();
            this.t4 = new System.Windows.Forms.TextBox();
            this.l4 = new System.Windows.Forms.TextBox();
            this.p4 = new System.Windows.Forms.TextBox();
            this.cbA4 = new System.Windows.Forms.TextBox();
            this.kg4 = new System.Windows.Forms.Label();
            this.cbUk4 = new System.Windows.Forms.ComboBox();
            this.cb4 = new System.Windows.Forms.ComboBox();
            this.cbOUk4 = new System.Windows.Forms.ComboBox();
            this.cbO4 = new System.Windows.Forms.ComboBox();
            this.label85 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.qty3 = new System.Windows.Forms.NumericUpDown();
            this.kgO3 = new System.Windows.Forms.TextBox();
            this.qtyO3 = new System.Windows.Forms.TextBox();
            this.kgA3 = new System.Windows.Forms.TextBox();
            this.qtyA3 = new System.Windows.Forms.TextBox();
            this.t3 = new System.Windows.Forms.TextBox();
            this.l3 = new System.Windows.Forms.TextBox();
            this.p3 = new System.Windows.Forms.TextBox();
            this.cbA3 = new System.Windows.Forms.TextBox();
            this.kg3 = new System.Windows.Forms.Label();
            this.cbUk3 = new System.Windows.Forms.ComboBox();
            this.cb3 = new System.Windows.Forms.ComboBox();
            this.cbOUk3 = new System.Windows.Forms.ComboBox();
            this.cbO3 = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.qty2 = new System.Windows.Forms.NumericUpDown();
            this.kgO2 = new System.Windows.Forms.TextBox();
            this.qtyO2 = new System.Windows.Forms.TextBox();
            this.kgA2 = new System.Windows.Forms.TextBox();
            this.qtyA2 = new System.Windows.Forms.TextBox();
            this.t2 = new System.Windows.Forms.TextBox();
            this.l2 = new System.Windows.Forms.TextBox();
            this.p2 = new System.Windows.Forms.TextBox();
            this.cbA2 = new System.Windows.Forms.TextBox();
            this.kg2 = new System.Windows.Forms.Label();
            this.cbUk2 = new System.Windows.Forms.ComboBox();
            this.cbO2 = new System.Windows.Forms.ComboBox();
            this.cbOUk2 = new System.Windows.Forms.ComboBox();
            this.cb2 = new System.Windows.Forms.ComboBox();
            this.label48 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.kg1 = new System.Windows.Forms.Label();
            this.kgO1 = new System.Windows.Forms.TextBox();
            this.qtyO1 = new System.Windows.Forms.TextBox();
            this.qty1 = new System.Windows.Forms.NumericUpDown();
            this.kgA1 = new System.Windows.Forms.TextBox();
            this.qtyA1 = new System.Windows.Forms.TextBox();
            this.t1 = new System.Windows.Forms.TextBox();
            this.l1 = new System.Windows.Forms.TextBox();
            this.p1 = new System.Windows.Forms.TextBox();
            this.cbA1 = new System.Windows.Forms.TextBox();
            this.cbOUk1 = new System.Windows.Forms.ComboBox();
            this.cbO1 = new System.Windows.Forms.ComboBox();
            this.cbUk1 = new System.Windows.Forms.ComboBox();
            this.cb1 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tgl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.noSpk = new System.Windows.Forms.TextBox();
            this.msn1 = new System.Windows.Forms.Button();
            this.msn2 = new System.Windows.Forms.Button();
            this.msn3 = new System.Windows.Forms.Button();
            this.cust = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty9)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty8)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty7)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty6)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty5)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty3)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(16, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(432, 53);
            this.label1.TabIndex = 1;
            this.label1.Text = "SPK:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(459, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(432, 53);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mesin ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(897, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(224, 52);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tanggal:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(16, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 46);
            this.label4.TabIndex = 4;
            this.label4.Text = "No.";
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(879, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(428, 23);
            this.label7.TabIndex = 7;
            this.label7.Text = "Stok Akhir";
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Location = new System.Drawing.Point(1216, 97);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 23);
            this.label16.TabIndex = 19;
            this.label16.Text = "Kg";
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label17.Location = new System.Drawing.Point(1165, 97);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 23);
            this.label17.TabIndex = 18;
            this.label17.Text = "Qty";
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Location = new System.Drawing.Point(1019, 97);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 23);
            this.label18.TabIndex = 17;
            this.label18.Text = "Lebar";
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Location = new System.Drawing.Point(879, 97);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(134, 23);
            this.label19.TabIndex = 16;
            this.label19.Text = "Type";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 123);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1326, 522);
            this.panel1.TabIndex = 30;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel10.Controls.Add(this.qty9);
            this.panel10.Controls.Add(this.kgO9);
            this.panel10.Controls.Add(this.qtyO9);
            this.panel10.Controls.Add(this.kgA9);
            this.panel10.Controls.Add(this.qtyA9);
            this.panel10.Controls.Add(this.t9);
            this.panel10.Controls.Add(this.l9);
            this.panel10.Controls.Add(this.p9);
            this.panel10.Controls.Add(this.cbA9);
            this.panel10.Controls.Add(this.kg9);
            this.panel10.Controls.Add(this.cbUk9);
            this.panel10.Controls.Add(this.cb9);
            this.panel10.Controls.Add(this.cbOUk9);
            this.panel10.Controls.Add(this.cbO9);
            this.panel10.Controls.Add(this.label171);
            this.panel10.Location = new System.Drawing.Point(6, 456);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1311, 50);
            this.panel10.TabIndex = 9;
            // 
            // qty9
            // 
            this.qty9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty9.Location = new System.Drawing.Point(691, 12);
            this.qty9.Name = "qty9";
            this.qty9.Size = new System.Drawing.Size(54, 24);
            this.qty9.TabIndex = 56;
            this.qty9.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO9
            // 
            this.kgO9.Location = new System.Drawing.Point(345, 10);
            this.kgO9.Multiline = true;
            this.kgO9.Name = "kgO9";
            this.kgO9.Size = new System.Drawing.Size(42, 30);
            this.kgO9.TabIndex = 53;
            this.kgO9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO9
            // 
            this.qtyO9.Location = new System.Drawing.Point(295, 10);
            this.qtyO9.Multiline = true;
            this.qtyO9.Name = "qtyO9";
            this.qtyO9.Size = new System.Drawing.Size(42, 30);
            this.qtyO9.TabIndex = 52;
            this.qtyO9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA9
            // 
            this.kgA9.Location = new System.Drawing.Point(1197, 9);
            this.kgA9.Multiline = true;
            this.kgA9.Name = "kgA9";
            this.kgA9.Size = new System.Drawing.Size(79, 32);
            this.kgA9.TabIndex = 40;
            this.kgA9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA9
            // 
            this.qtyA9.Location = new System.Drawing.Point(1148, 9);
            this.qtyA9.Multiline = true;
            this.qtyA9.Name = "qtyA9";
            this.qtyA9.Size = new System.Drawing.Size(40, 32);
            this.qtyA9.TabIndex = 39;
            this.qtyA9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA9.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t9
            // 
            this.t9.Location = new System.Drawing.Point(1094, 9);
            this.t9.Multiline = true;
            this.t9.Name = "t9";
            this.t9.Size = new System.Drawing.Size(40, 32);
            this.t9.TabIndex = 38;
            this.t9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t9.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l9
            // 
            this.l9.Location = new System.Drawing.Point(1004, 9);
            this.l9.Multiline = true;
            this.l9.Name = "l9";
            this.l9.Size = new System.Drawing.Size(40, 32);
            this.l9.TabIndex = 37;
            this.l9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l9.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p9
            // 
            this.p9.Location = new System.Drawing.Point(1049, 9);
            this.p9.Multiline = true;
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(40, 32);
            this.p9.TabIndex = 36;
            this.p9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p9.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA9
            // 
            this.cbA9.Location = new System.Drawing.Point(860, 10);
            this.cbA9.Multiline = true;
            this.cbA9.Name = "cbA9";
            this.cbA9.Size = new System.Drawing.Size(134, 32);
            this.cbA9.TabIndex = 29;
            this.cbA9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg9
            // 
            this.kg9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg9.Location = new System.Drawing.Point(751, 9);
            this.kg9.Name = "kg9";
            this.kg9.Size = new System.Drawing.Size(80, 30);
            this.kg9.TabIndex = 28;
            // 
            // cbUk9
            // 
            this.cbUk9.FormattingEnabled = true;
            this.cbUk9.ItemHeight = 13;
            this.cbUk9.Location = new System.Drawing.Point(553, 12);
            this.cbUk9.Name = "cbUk9";
            this.cbUk9.Size = new System.Drawing.Size(127, 21);
            this.cbUk9.TabIndex = 26;
            this.cbUk9.Text = "Pilih Ukuran";
            this.cbUk9.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb9
            // 
            this.cb9.FormattingEnabled = true;
            this.cb9.ItemHeight = 13;
            this.cb9.Items.AddRange(new object[] {
            ""});
            this.cb9.Location = new System.Drawing.Point(418, 12);
            this.cb9.Name = "cb9";
            this.cb9.Size = new System.Drawing.Size(127, 21);
            this.cb9.TabIndex = 25;
            this.cb9.Text = "Pilih Type";
            this.cb9.SelectedIndexChanged += new System.EventHandler(this.cbO9_SelectedIndexChanged);
            // 
            // cbOUk9
            // 
            this.cbOUk9.FormattingEnabled = true;
            this.cbOUk9.ItemHeight = 13;
            this.cbOUk9.Location = new System.Drawing.Point(161, 12);
            this.cbOUk9.Name = "cbOUk9";
            this.cbOUk9.Size = new System.Drawing.Size(127, 21);
            this.cbOUk9.TabIndex = 20;
            this.cbOUk9.Text = "Pilih Ukuran";
            this.cbOUk9.SelectedIndexChanged += new System.EventHandler(this.cbOUk9_SelectedIndexChanged);
            // 
            // cbO9
            // 
            this.cbO9.FormattingEnabled = true;
            this.cbO9.ItemHeight = 13;
            this.cbO9.Items.AddRange(new object[] {
            ""});
            this.cbO9.Location = new System.Drawing.Point(28, 12);
            this.cbO9.Name = "cbO9";
            this.cbO9.Size = new System.Drawing.Size(127, 21);
            this.cbO9.TabIndex = 1;
            this.cbO9.Text = "Pilih Type";
            this.cbO9.SelectedIndexChanged += new System.EventHandler(this.cbO9_SelectedIndexChanged);
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(6, 16);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(13, 13);
            this.label171.TabIndex = 0;
            this.label171.Text = "9";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel9.Controls.Add(this.qty8);
            this.panel9.Controls.Add(this.kgO8);
            this.panel9.Controls.Add(this.qtyO8);
            this.panel9.Controls.Add(this.kgA8);
            this.panel9.Controls.Add(this.qtyA8);
            this.panel9.Controls.Add(this.t8);
            this.panel9.Controls.Add(this.l8);
            this.panel9.Controls.Add(this.p8);
            this.panel9.Controls.Add(this.cbA8);
            this.panel9.Controls.Add(this.kg8);
            this.panel9.Controls.Add(this.cbUk8);
            this.panel9.Controls.Add(this.cb8);
            this.panel9.Controls.Add(this.cbOUk8);
            this.panel9.Controls.Add(this.cbO8);
            this.panel9.Controls.Add(this.label154);
            this.panel9.Location = new System.Drawing.Point(6, 400);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1311, 50);
            this.panel9.TabIndex = 8;
            // 
            // qty8
            // 
            this.qty8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty8.Location = new System.Drawing.Point(691, 10);
            this.qty8.Name = "qty8";
            this.qty8.Size = new System.Drawing.Size(54, 24);
            this.qty8.TabIndex = 55;
            this.qty8.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO8
            // 
            this.kgO8.Location = new System.Drawing.Point(345, 6);
            this.kgO8.Multiline = true;
            this.kgO8.Name = "kgO8";
            this.kgO8.Size = new System.Drawing.Size(42, 30);
            this.kgO8.TabIndex = 53;
            this.kgO8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO8
            // 
            this.qtyO8.Location = new System.Drawing.Point(295, 6);
            this.qtyO8.Multiline = true;
            this.qtyO8.Name = "qtyO8";
            this.qtyO8.Size = new System.Drawing.Size(42, 30);
            this.qtyO8.TabIndex = 52;
            this.qtyO8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA8
            // 
            this.kgA8.Location = new System.Drawing.Point(1198, 10);
            this.kgA8.Multiline = true;
            this.kgA8.Name = "kgA8";
            this.kgA8.Size = new System.Drawing.Size(79, 32);
            this.kgA8.TabIndex = 40;
            this.kgA8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA8
            // 
            this.qtyA8.Location = new System.Drawing.Point(1149, 10);
            this.qtyA8.Multiline = true;
            this.qtyA8.Name = "qtyA8";
            this.qtyA8.Size = new System.Drawing.Size(40, 32);
            this.qtyA8.TabIndex = 39;
            this.qtyA8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA8.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t8
            // 
            this.t8.Location = new System.Drawing.Point(1095, 10);
            this.t8.Multiline = true;
            this.t8.Name = "t8";
            this.t8.Size = new System.Drawing.Size(40, 32);
            this.t8.TabIndex = 38;
            this.t8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t8.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l8
            // 
            this.l8.Location = new System.Drawing.Point(1005, 10);
            this.l8.Multiline = true;
            this.l8.Name = "l8";
            this.l8.Size = new System.Drawing.Size(40, 32);
            this.l8.TabIndex = 37;
            this.l8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l8.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p8
            // 
            this.p8.Location = new System.Drawing.Point(1050, 10);
            this.p8.Multiline = true;
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(40, 32);
            this.p8.TabIndex = 36;
            this.p8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p8.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA8
            // 
            this.cbA8.Location = new System.Drawing.Point(860, 9);
            this.cbA8.Multiline = true;
            this.cbA8.Name = "cbA8";
            this.cbA8.Size = new System.Drawing.Size(134, 32);
            this.cbA8.TabIndex = 29;
            this.cbA8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg8
            // 
            this.kg8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg8.Location = new System.Drawing.Point(751, 10);
            this.kg8.Name = "kg8";
            this.kg8.Size = new System.Drawing.Size(80, 30);
            this.kg8.TabIndex = 28;
            // 
            // cbUk8
            // 
            this.cbUk8.FormattingEnabled = true;
            this.cbUk8.ItemHeight = 13;
            this.cbUk8.Location = new System.Drawing.Point(553, 12);
            this.cbUk8.Name = "cbUk8";
            this.cbUk8.Size = new System.Drawing.Size(127, 21);
            this.cbUk8.TabIndex = 26;
            this.cbUk8.Text = "Pilih Ukuran";
            this.cbUk8.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb8
            // 
            this.cb8.FormattingEnabled = true;
            this.cb8.ItemHeight = 13;
            this.cb8.Items.AddRange(new object[] {
            ""});
            this.cb8.Location = new System.Drawing.Point(418, 12);
            this.cb8.Name = "cb8";
            this.cb8.Size = new System.Drawing.Size(127, 21);
            this.cb8.TabIndex = 25;
            this.cb8.Text = "Pilih Type";
            this.cb8.SelectedIndexChanged += new System.EventHandler(this.cbO8_SelectedIndexChanged);
            // 
            // cbOUk8
            // 
            this.cbOUk8.FormattingEnabled = true;
            this.cbOUk8.ItemHeight = 13;
            this.cbOUk8.Location = new System.Drawing.Point(161, 12);
            this.cbOUk8.Name = "cbOUk8";
            this.cbOUk8.Size = new System.Drawing.Size(127, 21);
            this.cbOUk8.TabIndex = 20;
            this.cbOUk8.Text = "Pilih Ukuran";
            this.cbOUk8.SelectedIndexChanged += new System.EventHandler(this.cbOUk8_SelectedIndexChanged);
            // 
            // cbO8
            // 
            this.cbO8.FormattingEnabled = true;
            this.cbO8.ItemHeight = 13;
            this.cbO8.Items.AddRange(new object[] {
            ""});
            this.cbO8.Location = new System.Drawing.Point(28, 12);
            this.cbO8.Name = "cbO8";
            this.cbO8.Size = new System.Drawing.Size(127, 21);
            this.cbO8.TabIndex = 1;
            this.cbO8.Text = "Pilih Type";
            this.cbO8.SelectedIndexChanged += new System.EventHandler(this.cbO8_SelectedIndexChanged);
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(6, 16);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(13, 13);
            this.label154.TabIndex = 0;
            this.label154.Text = "8";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel8.Controls.Add(this.qty7);
            this.panel8.Controls.Add(this.kgO7);
            this.panel8.Controls.Add(this.qtyO7);
            this.panel8.Controls.Add(this.kgA7);
            this.panel8.Controls.Add(this.qtyA7);
            this.panel8.Controls.Add(this.t7);
            this.panel8.Controls.Add(this.l7);
            this.panel8.Controls.Add(this.p7);
            this.panel8.Controls.Add(this.cbA7);
            this.panel8.Controls.Add(this.kg7);
            this.panel8.Controls.Add(this.cbUk7);
            this.panel8.Controls.Add(this.cb7);
            this.panel8.Controls.Add(this.cbOUk7);
            this.panel8.Controls.Add(this.cbO7);
            this.panel8.Controls.Add(this.label137);
            this.panel8.Location = new System.Drawing.Point(6, 344);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1311, 50);
            this.panel8.TabIndex = 7;
            // 
            // qty7
            // 
            this.qty7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty7.Location = new System.Drawing.Point(691, 13);
            this.qty7.Name = "qty7";
            this.qty7.Size = new System.Drawing.Size(54, 24);
            this.qty7.TabIndex = 54;
            this.qty7.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO7
            // 
            this.kgO7.Location = new System.Drawing.Point(345, 7);
            this.kgO7.Multiline = true;
            this.kgO7.Name = "kgO7";
            this.kgO7.Size = new System.Drawing.Size(42, 30);
            this.kgO7.TabIndex = 53;
            this.kgO7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO7
            // 
            this.qtyO7.Location = new System.Drawing.Point(295, 7);
            this.qtyO7.Multiline = true;
            this.qtyO7.Name = "qtyO7";
            this.qtyO7.Size = new System.Drawing.Size(42, 30);
            this.qtyO7.TabIndex = 52;
            this.qtyO7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA7
            // 
            this.kgA7.Location = new System.Drawing.Point(1198, 9);
            this.kgA7.Multiline = true;
            this.kgA7.Name = "kgA7";
            this.kgA7.Size = new System.Drawing.Size(79, 32);
            this.kgA7.TabIndex = 40;
            this.kgA7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA7
            // 
            this.qtyA7.Location = new System.Drawing.Point(1149, 9);
            this.qtyA7.Multiline = true;
            this.qtyA7.Name = "qtyA7";
            this.qtyA7.Size = new System.Drawing.Size(40, 32);
            this.qtyA7.TabIndex = 39;
            this.qtyA7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA7.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t7
            // 
            this.t7.Location = new System.Drawing.Point(1095, 9);
            this.t7.Multiline = true;
            this.t7.Name = "t7";
            this.t7.Size = new System.Drawing.Size(40, 32);
            this.t7.TabIndex = 38;
            this.t7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t7.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l7
            // 
            this.l7.Location = new System.Drawing.Point(1005, 9);
            this.l7.Multiline = true;
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(40, 32);
            this.l7.TabIndex = 37;
            this.l7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l7.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p7
            // 
            this.p7.Location = new System.Drawing.Point(1050, 9);
            this.p7.Multiline = true;
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(40, 32);
            this.p7.TabIndex = 36;
            this.p7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p7.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA7
            // 
            this.cbA7.Location = new System.Drawing.Point(860, 10);
            this.cbA7.Multiline = true;
            this.cbA7.Name = "cbA7";
            this.cbA7.Size = new System.Drawing.Size(134, 32);
            this.cbA7.TabIndex = 29;
            this.cbA7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg7
            // 
            this.kg7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg7.Location = new System.Drawing.Point(751, 11);
            this.kg7.Name = "kg7";
            this.kg7.Size = new System.Drawing.Size(80, 30);
            this.kg7.TabIndex = 28;
            // 
            // cbUk7
            // 
            this.cbUk7.FormattingEnabled = true;
            this.cbUk7.ItemHeight = 13;
            this.cbUk7.Location = new System.Drawing.Point(553, 13);
            this.cbUk7.Name = "cbUk7";
            this.cbUk7.Size = new System.Drawing.Size(127, 21);
            this.cbUk7.TabIndex = 26;
            this.cbUk7.Text = "Pilih Ukuran";
            this.cbUk7.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb7
            // 
            this.cb7.FormattingEnabled = true;
            this.cb7.ItemHeight = 13;
            this.cb7.Items.AddRange(new object[] {
            ""});
            this.cb7.Location = new System.Drawing.Point(418, 12);
            this.cb7.Name = "cb7";
            this.cb7.Size = new System.Drawing.Size(127, 21);
            this.cb7.TabIndex = 25;
            this.cb7.Text = "Pilih Type";
            this.cb7.SelectedIndexChanged += new System.EventHandler(this.cbO7_SelectedIndexChanged);
            // 
            // cbOUk7
            // 
            this.cbOUk7.FormattingEnabled = true;
            this.cbOUk7.ItemHeight = 13;
            this.cbOUk7.Location = new System.Drawing.Point(161, 12);
            this.cbOUk7.Name = "cbOUk7";
            this.cbOUk7.Size = new System.Drawing.Size(127, 21);
            this.cbOUk7.TabIndex = 20;
            this.cbOUk7.Text = "Pilih Ukuran";
            this.cbOUk7.SelectedIndexChanged += new System.EventHandler(this.cbOUk7_SelectedIndexChanged);
            // 
            // cbO7
            // 
            this.cbO7.FormattingEnabled = true;
            this.cbO7.ItemHeight = 13;
            this.cbO7.Items.AddRange(new object[] {
            ""});
            this.cbO7.Location = new System.Drawing.Point(28, 12);
            this.cbO7.Name = "cbO7";
            this.cbO7.Size = new System.Drawing.Size(127, 21);
            this.cbO7.TabIndex = 1;
            this.cbO7.Text = "Pilih Type";
            this.cbO7.SelectedIndexChanged += new System.EventHandler(this.cbO7_SelectedIndexChanged);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(6, 16);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(13, 13);
            this.label137.TabIndex = 0;
            this.label137.Text = "7";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel7.Controls.Add(this.qty6);
            this.panel7.Controls.Add(this.kgO6);
            this.panel7.Controls.Add(this.qtyO6);
            this.panel7.Controls.Add(this.kgA6);
            this.panel7.Controls.Add(this.qtyA6);
            this.panel7.Controls.Add(this.t6);
            this.panel7.Controls.Add(this.l6);
            this.panel7.Controls.Add(this.p6);
            this.panel7.Controls.Add(this.cbA6);
            this.panel7.Controls.Add(this.kg6);
            this.panel7.Controls.Add(this.cbUk6);
            this.panel7.Controls.Add(this.cb6);
            this.panel7.Controls.Add(this.cbOUk6);
            this.panel7.Controls.Add(this.cbO6);
            this.panel7.Controls.Add(this.label120);
            this.panel7.Location = new System.Drawing.Point(6, 288);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1311, 50);
            this.panel7.TabIndex = 6;
            // 
            // qty6
            // 
            this.qty6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty6.Location = new System.Drawing.Point(691, 13);
            this.qty6.Name = "qty6";
            this.qty6.Size = new System.Drawing.Size(54, 24);
            this.qty6.TabIndex = 51;
            this.qty6.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO6
            // 
            this.kgO6.Location = new System.Drawing.Point(345, 10);
            this.kgO6.Multiline = true;
            this.kgO6.Name = "kgO6";
            this.kgO6.Size = new System.Drawing.Size(42, 30);
            this.kgO6.TabIndex = 50;
            this.kgO6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO6
            // 
            this.qtyO6.Location = new System.Drawing.Point(295, 10);
            this.qtyO6.Multiline = true;
            this.qtyO6.Name = "qtyO6";
            this.qtyO6.Size = new System.Drawing.Size(42, 30);
            this.qtyO6.TabIndex = 49;
            this.qtyO6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA6
            // 
            this.kgA6.Location = new System.Drawing.Point(1198, 10);
            this.kgA6.Multiline = true;
            this.kgA6.Name = "kgA6";
            this.kgA6.Size = new System.Drawing.Size(79, 32);
            this.kgA6.TabIndex = 40;
            this.kgA6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA6
            // 
            this.qtyA6.Location = new System.Drawing.Point(1149, 10);
            this.qtyA6.Multiline = true;
            this.qtyA6.Name = "qtyA6";
            this.qtyA6.Size = new System.Drawing.Size(40, 32);
            this.qtyA6.TabIndex = 39;
            this.qtyA6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA6.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t6
            // 
            this.t6.Location = new System.Drawing.Point(1095, 10);
            this.t6.Multiline = true;
            this.t6.Name = "t6";
            this.t6.Size = new System.Drawing.Size(40, 32);
            this.t6.TabIndex = 38;
            this.t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t6.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l6
            // 
            this.l6.Location = new System.Drawing.Point(1005, 10);
            this.l6.Multiline = true;
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(40, 32);
            this.l6.TabIndex = 37;
            this.l6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l6.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p6
            // 
            this.p6.Location = new System.Drawing.Point(1050, 10);
            this.p6.Multiline = true;
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(40, 32);
            this.p6.TabIndex = 36;
            this.p6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p6.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA6
            // 
            this.cbA6.Location = new System.Drawing.Point(860, 11);
            this.cbA6.Multiline = true;
            this.cbA6.Name = "cbA6";
            this.cbA6.Size = new System.Drawing.Size(134, 32);
            this.cbA6.TabIndex = 29;
            this.cbA6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg6
            // 
            this.kg6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg6.Location = new System.Drawing.Point(751, 10);
            this.kg6.Name = "kg6";
            this.kg6.Size = new System.Drawing.Size(80, 30);
            this.kg6.TabIndex = 28;
            // 
            // cbUk6
            // 
            this.cbUk6.FormattingEnabled = true;
            this.cbUk6.ItemHeight = 13;
            this.cbUk6.Location = new System.Drawing.Point(553, 13);
            this.cbUk6.Name = "cbUk6";
            this.cbUk6.Size = new System.Drawing.Size(127, 21);
            this.cbUk6.TabIndex = 26;
            this.cbUk6.Text = "Pilih Ukuran";
            this.cbUk6.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb6
            // 
            this.cb6.FormattingEnabled = true;
            this.cb6.ItemHeight = 13;
            this.cb6.Items.AddRange(new object[] {
            ""});
            this.cb6.Location = new System.Drawing.Point(418, 13);
            this.cb6.Name = "cb6";
            this.cb6.Size = new System.Drawing.Size(127, 21);
            this.cb6.TabIndex = 25;
            this.cb6.Text = "Pilih Type";
            this.cb6.SelectedIndexChanged += new System.EventHandler(this.cbO6_SelectedIndexChanged);
            // 
            // cbOUk6
            // 
            this.cbOUk6.FormattingEnabled = true;
            this.cbOUk6.ItemHeight = 13;
            this.cbOUk6.Location = new System.Drawing.Point(161, 12);
            this.cbOUk6.Name = "cbOUk6";
            this.cbOUk6.Size = new System.Drawing.Size(127, 21);
            this.cbOUk6.TabIndex = 20;
            this.cbOUk6.Text = "Pilih Ukuran";
            this.cbOUk6.SelectedIndexChanged += new System.EventHandler(this.cbOUk6_SelectedIndexChanged);
            // 
            // cbO6
            // 
            this.cbO6.FormattingEnabled = true;
            this.cbO6.ItemHeight = 13;
            this.cbO6.Items.AddRange(new object[] {
            ""});
            this.cbO6.Location = new System.Drawing.Point(28, 12);
            this.cbO6.Name = "cbO6";
            this.cbO6.Size = new System.Drawing.Size(127, 21);
            this.cbO6.TabIndex = 1;
            this.cbO6.Text = "Pilih Type";
            this.cbO6.SelectedIndexChanged += new System.EventHandler(this.cbO6_SelectedIndexChanged);
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(6, 16);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(13, 13);
            this.label120.TabIndex = 0;
            this.label120.Text = "6";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel6.Controls.Add(this.qty5);
            this.panel6.Controls.Add(this.kgO5);
            this.panel6.Controls.Add(this.qtyO5);
            this.panel6.Controls.Add(this.kgA5);
            this.panel6.Controls.Add(this.qtyA5);
            this.panel6.Controls.Add(this.t5);
            this.panel6.Controls.Add(this.l5);
            this.panel6.Controls.Add(this.p5);
            this.panel6.Controls.Add(this.cbA5);
            this.panel6.Controls.Add(this.kg5);
            this.panel6.Controls.Add(this.cbUk5);
            this.panel6.Controls.Add(this.cb5);
            this.panel6.Controls.Add(this.cbOUk5);
            this.panel6.Controls.Add(this.cbO5);
            this.panel6.Controls.Add(this.label103);
            this.panel6.Location = new System.Drawing.Point(6, 232);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1311, 50);
            this.panel6.TabIndex = 5;
            // 
            // qty5
            // 
            this.qty5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty5.Location = new System.Drawing.Point(691, 12);
            this.qty5.Name = "qty5";
            this.qty5.Size = new System.Drawing.Size(54, 24);
            this.qty5.TabIndex = 48;
            this.qty5.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO5
            // 
            this.kgO5.Location = new System.Drawing.Point(345, 9);
            this.kgO5.Multiline = true;
            this.kgO5.Name = "kgO5";
            this.kgO5.Size = new System.Drawing.Size(42, 30);
            this.kgO5.TabIndex = 47;
            this.kgO5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO5
            // 
            this.qtyO5.Location = new System.Drawing.Point(295, 9);
            this.qtyO5.Multiline = true;
            this.qtyO5.Name = "qtyO5";
            this.qtyO5.Size = new System.Drawing.Size(42, 30);
            this.qtyO5.TabIndex = 46;
            this.qtyO5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA5
            // 
            this.kgA5.Location = new System.Drawing.Point(1198, 9);
            this.kgA5.Multiline = true;
            this.kgA5.Name = "kgA5";
            this.kgA5.Size = new System.Drawing.Size(79, 32);
            this.kgA5.TabIndex = 40;
            this.kgA5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA5
            // 
            this.qtyA5.Location = new System.Drawing.Point(1149, 9);
            this.qtyA5.Multiline = true;
            this.qtyA5.Name = "qtyA5";
            this.qtyA5.Size = new System.Drawing.Size(40, 32);
            this.qtyA5.TabIndex = 39;
            this.qtyA5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA5.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t5
            // 
            this.t5.Location = new System.Drawing.Point(1095, 9);
            this.t5.Multiline = true;
            this.t5.Name = "t5";
            this.t5.Size = new System.Drawing.Size(40, 32);
            this.t5.TabIndex = 38;
            this.t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t5.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l5
            // 
            this.l5.Location = new System.Drawing.Point(1005, 9);
            this.l5.Multiline = true;
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(40, 32);
            this.l5.TabIndex = 37;
            this.l5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l5.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p5
            // 
            this.p5.Location = new System.Drawing.Point(1050, 9);
            this.p5.Multiline = true;
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(40, 32);
            this.p5.TabIndex = 36;
            this.p5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p5.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA5
            // 
            this.cbA5.Location = new System.Drawing.Point(861, 8);
            this.cbA5.Multiline = true;
            this.cbA5.Name = "cbA5";
            this.cbA5.Size = new System.Drawing.Size(134, 32);
            this.cbA5.TabIndex = 29;
            this.cbA5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg5
            // 
            this.kg5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg5.Location = new System.Drawing.Point(751, 9);
            this.kg5.Name = "kg5";
            this.kg5.Size = new System.Drawing.Size(80, 30);
            this.kg5.TabIndex = 28;
            // 
            // cbUk5
            // 
            this.cbUk5.FormattingEnabled = true;
            this.cbUk5.ItemHeight = 13;
            this.cbUk5.Location = new System.Drawing.Point(553, 12);
            this.cbUk5.Name = "cbUk5";
            this.cbUk5.Size = new System.Drawing.Size(127, 21);
            this.cbUk5.TabIndex = 26;
            this.cbUk5.Text = "Pilih Ukuran";
            this.cbUk5.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb5
            // 
            this.cb5.FormattingEnabled = true;
            this.cb5.ItemHeight = 13;
            this.cb5.Items.AddRange(new object[] {
            ""});
            this.cb5.Location = new System.Drawing.Point(418, 12);
            this.cb5.Name = "cb5";
            this.cb5.Size = new System.Drawing.Size(127, 21);
            this.cb5.TabIndex = 25;
            this.cb5.Text = "Pilih Type";
            this.cb5.SelectedIndexChanged += new System.EventHandler(this.cbO5_SelectedIndexChanged);
            // 
            // cbOUk5
            // 
            this.cbOUk5.FormattingEnabled = true;
            this.cbOUk5.ItemHeight = 13;
            this.cbOUk5.Location = new System.Drawing.Point(161, 12);
            this.cbOUk5.Name = "cbOUk5";
            this.cbOUk5.Size = new System.Drawing.Size(127, 21);
            this.cbOUk5.TabIndex = 20;
            this.cbOUk5.Text = "Pilih Ukuran";
            this.cbOUk5.SelectedIndexChanged += new System.EventHandler(this.cbOUk5_SelectedIndexChanged);
            // 
            // cbO5
            // 
            this.cbO5.FormattingEnabled = true;
            this.cbO5.ItemHeight = 13;
            this.cbO5.Items.AddRange(new object[] {
            ""});
            this.cbO5.Location = new System.Drawing.Point(28, 12);
            this.cbO5.Name = "cbO5";
            this.cbO5.Size = new System.Drawing.Size(127, 21);
            this.cbO5.TabIndex = 1;
            this.cbO5.Text = "Pilih Type";
            this.cbO5.SelectedIndexChanged += new System.EventHandler(this.cbO5_SelectedIndexChanged);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 16);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(13, 13);
            this.label103.TabIndex = 0;
            this.label103.Text = "5";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel5.Controls.Add(this.qty4);
            this.panel5.Controls.Add(this.kgO4);
            this.panel5.Controls.Add(this.qtyO4);
            this.panel5.Controls.Add(this.kgA4);
            this.panel5.Controls.Add(this.qtyA4);
            this.panel5.Controls.Add(this.t4);
            this.panel5.Controls.Add(this.l4);
            this.panel5.Controls.Add(this.p4);
            this.panel5.Controls.Add(this.cbA4);
            this.panel5.Controls.Add(this.kg4);
            this.panel5.Controls.Add(this.cbUk4);
            this.panel5.Controls.Add(this.cb4);
            this.panel5.Controls.Add(this.cbOUk4);
            this.panel5.Controls.Add(this.cbO4);
            this.panel5.Controls.Add(this.label85);
            this.panel5.Location = new System.Drawing.Point(6, 176);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1311, 50);
            this.panel5.TabIndex = 4;
            // 
            // qty4
            // 
            this.qty4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty4.Location = new System.Drawing.Point(691, 13);
            this.qty4.Name = "qty4";
            this.qty4.Size = new System.Drawing.Size(54, 24);
            this.qty4.TabIndex = 45;
            this.qty4.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO4
            // 
            this.kgO4.Location = new System.Drawing.Point(345, 8);
            this.kgO4.Multiline = true;
            this.kgO4.Name = "kgO4";
            this.kgO4.Size = new System.Drawing.Size(42, 30);
            this.kgO4.TabIndex = 44;
            this.kgO4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO4
            // 
            this.qtyO4.Location = new System.Drawing.Point(295, 8);
            this.qtyO4.Multiline = true;
            this.qtyO4.Name = "qtyO4";
            this.qtyO4.Size = new System.Drawing.Size(42, 30);
            this.qtyO4.TabIndex = 43;
            this.qtyO4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA4
            // 
            this.kgA4.Location = new System.Drawing.Point(1197, 11);
            this.kgA4.Multiline = true;
            this.kgA4.Name = "kgA4";
            this.kgA4.Size = new System.Drawing.Size(79, 32);
            this.kgA4.TabIndex = 35;
            this.kgA4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA4
            // 
            this.qtyA4.Location = new System.Drawing.Point(1148, 11);
            this.qtyA4.Multiline = true;
            this.qtyA4.Name = "qtyA4";
            this.qtyA4.Size = new System.Drawing.Size(40, 32);
            this.qtyA4.TabIndex = 34;
            this.qtyA4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA4.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t4
            // 
            this.t4.Location = new System.Drawing.Point(1094, 11);
            this.t4.Multiline = true;
            this.t4.Name = "t4";
            this.t4.Size = new System.Drawing.Size(40, 32);
            this.t4.TabIndex = 33;
            this.t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t4.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l4
            // 
            this.l4.Location = new System.Drawing.Point(1004, 11);
            this.l4.Multiline = true;
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(40, 32);
            this.l4.TabIndex = 32;
            this.l4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l4.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p4
            // 
            this.p4.Location = new System.Drawing.Point(1049, 11);
            this.p4.Multiline = true;
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(40, 32);
            this.p4.TabIndex = 31;
            this.p4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p4.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA4
            // 
            this.cbA4.Location = new System.Drawing.Point(861, 11);
            this.cbA4.Multiline = true;
            this.cbA4.Name = "cbA4";
            this.cbA4.Size = new System.Drawing.Size(134, 32);
            this.cbA4.TabIndex = 29;
            this.cbA4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg4
            // 
            this.kg4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg4.Location = new System.Drawing.Point(751, 11);
            this.kg4.Name = "kg4";
            this.kg4.Size = new System.Drawing.Size(80, 30);
            this.kg4.TabIndex = 28;
            // 
            // cbUk4
            // 
            this.cbUk4.FormattingEnabled = true;
            this.cbUk4.ItemHeight = 13;
            this.cbUk4.Location = new System.Drawing.Point(553, 13);
            this.cbUk4.Name = "cbUk4";
            this.cbUk4.Size = new System.Drawing.Size(127, 21);
            this.cbUk4.TabIndex = 26;
            this.cbUk4.Text = "Pilih Ukuran";
            this.cbUk4.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb4
            // 
            this.cb4.FormattingEnabled = true;
            this.cb4.ItemHeight = 13;
            this.cb4.Items.AddRange(new object[] {
            ""});
            this.cb4.Location = new System.Drawing.Point(418, 13);
            this.cb4.Name = "cb4";
            this.cb4.Size = new System.Drawing.Size(127, 21);
            this.cb4.TabIndex = 25;
            this.cb4.Text = "Pilih Type";
            this.cb4.SelectedIndexChanged += new System.EventHandler(this.cbO4_SelectedIndexChanged);
            // 
            // cbOUk4
            // 
            this.cbOUk4.FormattingEnabled = true;
            this.cbOUk4.ItemHeight = 13;
            this.cbOUk4.Location = new System.Drawing.Point(161, 12);
            this.cbOUk4.Name = "cbOUk4";
            this.cbOUk4.Size = new System.Drawing.Size(127, 21);
            this.cbOUk4.TabIndex = 20;
            this.cbOUk4.Text = "Pilih Ukuran";
            this.cbOUk4.SelectedIndexChanged += new System.EventHandler(this.cbOUk4_SelectedIndexChanged);
            // 
            // cbO4
            // 
            this.cbO4.FormattingEnabled = true;
            this.cbO4.ItemHeight = 13;
            this.cbO4.Items.AddRange(new object[] {
            ""});
            this.cbO4.Location = new System.Drawing.Point(28, 12);
            this.cbO4.Name = "cbO4";
            this.cbO4.Size = new System.Drawing.Size(127, 21);
            this.cbO4.TabIndex = 1;
            this.cbO4.Text = "Pilih Type";
            this.cbO4.SelectedIndexChanged += new System.EventHandler(this.cbO4_SelectedIndexChanged);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(6, 16);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(13, 13);
            this.label85.TabIndex = 0;
            this.label85.Text = "4";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel4.Controls.Add(this.qty3);
            this.panel4.Controls.Add(this.kgO3);
            this.panel4.Controls.Add(this.qtyO3);
            this.panel4.Controls.Add(this.kgA3);
            this.panel4.Controls.Add(this.qtyA3);
            this.panel4.Controls.Add(this.t3);
            this.panel4.Controls.Add(this.l3);
            this.panel4.Controls.Add(this.p3);
            this.panel4.Controls.Add(this.cbA3);
            this.panel4.Controls.Add(this.kg3);
            this.panel4.Controls.Add(this.cbUk3);
            this.panel4.Controls.Add(this.cb3);
            this.panel4.Controls.Add(this.cbOUk3);
            this.panel4.Controls.Add(this.cbO3);
            this.panel4.Controls.Add(this.label66);
            this.panel4.Location = new System.Drawing.Point(6, 120);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1311, 50);
            this.panel4.TabIndex = 3;
            // 
            // qty3
            // 
            this.qty3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty3.Location = new System.Drawing.Point(691, 14);
            this.qty3.Name = "qty3";
            this.qty3.Size = new System.Drawing.Size(54, 24);
            this.qty3.TabIndex = 42;
            this.qty3.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO3
            // 
            this.kgO3.Location = new System.Drawing.Point(347, 8);
            this.kgO3.Multiline = true;
            this.kgO3.Name = "kgO3";
            this.kgO3.Size = new System.Drawing.Size(42, 30);
            this.kgO3.TabIndex = 41;
            this.kgO3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO3
            // 
            this.qtyO3.Location = new System.Drawing.Point(297, 8);
            this.qtyO3.Multiline = true;
            this.qtyO3.Name = "qtyO3";
            this.qtyO3.Size = new System.Drawing.Size(42, 30);
            this.qtyO3.TabIndex = 40;
            this.qtyO3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA3
            // 
            this.kgA3.Location = new System.Drawing.Point(1197, 8);
            this.kgA3.Multiline = true;
            this.kgA3.Name = "kgA3";
            this.kgA3.Size = new System.Drawing.Size(79, 32);
            this.kgA3.TabIndex = 35;
            this.kgA3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA3
            // 
            this.qtyA3.Location = new System.Drawing.Point(1148, 8);
            this.qtyA3.Multiline = true;
            this.qtyA3.Name = "qtyA3";
            this.qtyA3.Size = new System.Drawing.Size(40, 32);
            this.qtyA3.TabIndex = 34;
            this.qtyA3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA3.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t3
            // 
            this.t3.Location = new System.Drawing.Point(1094, 8);
            this.t3.Multiline = true;
            this.t3.Name = "t3";
            this.t3.Size = new System.Drawing.Size(40, 32);
            this.t3.TabIndex = 33;
            this.t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t3.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l3
            // 
            this.l3.Location = new System.Drawing.Point(1004, 8);
            this.l3.Multiline = true;
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(40, 32);
            this.l3.TabIndex = 32;
            this.l3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l3.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p3
            // 
            this.p3.Location = new System.Drawing.Point(1049, 8);
            this.p3.Multiline = true;
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(40, 32);
            this.p3.TabIndex = 31;
            this.p3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p3.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA3
            // 
            this.cbA3.Location = new System.Drawing.Point(861, 8);
            this.cbA3.Multiline = true;
            this.cbA3.Name = "cbA3";
            this.cbA3.Size = new System.Drawing.Size(134, 32);
            this.cbA3.TabIndex = 29;
            this.cbA3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg3
            // 
            this.kg3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg3.Location = new System.Drawing.Point(752, 11);
            this.kg3.Name = "kg3";
            this.kg3.Size = new System.Drawing.Size(79, 30);
            this.kg3.TabIndex = 28;
            // 
            // cbUk3
            // 
            this.cbUk3.FormattingEnabled = true;
            this.cbUk3.ItemHeight = 13;
            this.cbUk3.Location = new System.Drawing.Point(553, 13);
            this.cbUk3.Name = "cbUk3";
            this.cbUk3.Size = new System.Drawing.Size(127, 21);
            this.cbUk3.TabIndex = 26;
            this.cbUk3.Text = "Pilih Ukuran";
            this.cbUk3.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb3
            // 
            this.cb3.FormattingEnabled = true;
            this.cb3.ItemHeight = 13;
            this.cb3.Items.AddRange(new object[] {
            ""});
            this.cb3.Location = new System.Drawing.Point(418, 13);
            this.cb3.Name = "cb3";
            this.cb3.Size = new System.Drawing.Size(127, 21);
            this.cb3.TabIndex = 25;
            this.cb3.Text = "Pilih Type";
            this.cb3.SelectedIndexChanged += new System.EventHandler(this.cbO3_SelectedIndexChanged);
            // 
            // cbOUk3
            // 
            this.cbOUk3.FormattingEnabled = true;
            this.cbOUk3.ItemHeight = 13;
            this.cbOUk3.Location = new System.Drawing.Point(161, 12);
            this.cbOUk3.Name = "cbOUk3";
            this.cbOUk3.Size = new System.Drawing.Size(127, 21);
            this.cbOUk3.TabIndex = 20;
            this.cbOUk3.Text = "Pilih Ukuran";
            this.cbOUk3.SelectedIndexChanged += new System.EventHandler(this.cbOUk3_SelectedIndexChanged);
            // 
            // cbO3
            // 
            this.cbO3.FormattingEnabled = true;
            this.cbO3.ItemHeight = 13;
            this.cbO3.Items.AddRange(new object[] {
            ""});
            this.cbO3.Location = new System.Drawing.Point(28, 12);
            this.cbO3.Name = "cbO3";
            this.cbO3.Size = new System.Drawing.Size(127, 21);
            this.cbO3.TabIndex = 1;
            this.cbO3.Text = "Pilih Type";
            this.cbO3.SelectedIndexChanged += new System.EventHandler(this.cbO3_SelectedIndexChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 16);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(13, 13);
            this.label66.TabIndex = 0;
            this.label66.Text = "3";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel3.Controls.Add(this.qty2);
            this.panel3.Controls.Add(this.kgO2);
            this.panel3.Controls.Add(this.qtyO2);
            this.panel3.Controls.Add(this.kgA2);
            this.panel3.Controls.Add(this.qtyA2);
            this.panel3.Controls.Add(this.t2);
            this.panel3.Controls.Add(this.l2);
            this.panel3.Controls.Add(this.p2);
            this.panel3.Controls.Add(this.cbA2);
            this.panel3.Controls.Add(this.kg2);
            this.panel3.Controls.Add(this.cbUk2);
            this.panel3.Controls.Add(this.cbO2);
            this.panel3.Controls.Add(this.cbOUk2);
            this.panel3.Controls.Add(this.cb2);
            this.panel3.Controls.Add(this.label48);
            this.panel3.Location = new System.Drawing.Point(6, 64);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1311, 50);
            this.panel3.TabIndex = 2;
            // 
            // qty2
            // 
            this.qty2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty2.Location = new System.Drawing.Point(692, 10);
            this.qty2.Name = "qty2";
            this.qty2.Size = new System.Drawing.Size(54, 24);
            this.qty2.TabIndex = 36;
            this.qty2.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgO2
            // 
            this.kgO2.Location = new System.Drawing.Point(347, 9);
            this.kgO2.Multiline = true;
            this.kgO2.Name = "kgO2";
            this.kgO2.Size = new System.Drawing.Size(42, 30);
            this.kgO2.TabIndex = 39;
            this.kgO2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO2
            // 
            this.qtyO2.Location = new System.Drawing.Point(297, 9);
            this.qtyO2.Multiline = true;
            this.qtyO2.Name = "qtyO2";
            this.qtyO2.Size = new System.Drawing.Size(42, 30);
            this.qtyO2.TabIndex = 38;
            this.qtyO2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA2
            // 
            this.kgA2.Location = new System.Drawing.Point(1197, 7);
            this.kgA2.Multiline = true;
            this.kgA2.Name = "kgA2";
            this.kgA2.Size = new System.Drawing.Size(79, 32);
            this.kgA2.TabIndex = 35;
            this.kgA2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA2
            // 
            this.qtyA2.Location = new System.Drawing.Point(1148, 7);
            this.qtyA2.Multiline = true;
            this.qtyA2.Name = "qtyA2";
            this.qtyA2.Size = new System.Drawing.Size(40, 32);
            this.qtyA2.TabIndex = 34;
            this.qtyA2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA2.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t2
            // 
            this.t2.Location = new System.Drawing.Point(1094, 7);
            this.t2.Multiline = true;
            this.t2.Name = "t2";
            this.t2.Size = new System.Drawing.Size(40, 32);
            this.t2.TabIndex = 33;
            this.t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t2.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l2
            // 
            this.l2.Location = new System.Drawing.Point(1004, 7);
            this.l2.Multiline = true;
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(40, 32);
            this.l2.TabIndex = 32;
            this.l2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l2.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p2
            // 
            this.p2.Location = new System.Drawing.Point(1049, 7);
            this.p2.Multiline = true;
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(40, 32);
            this.p2.TabIndex = 31;
            this.p2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p2.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA2
            // 
            this.cbA2.Location = new System.Drawing.Point(861, 7);
            this.cbA2.Multiline = true;
            this.cbA2.Name = "cbA2";
            this.cbA2.Size = new System.Drawing.Size(134, 32);
            this.cbA2.TabIndex = 29;
            this.cbA2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg2
            // 
            this.kg2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg2.Location = new System.Drawing.Point(752, 9);
            this.kg2.Name = "kg2";
            this.kg2.Size = new System.Drawing.Size(79, 30);
            this.kg2.TabIndex = 28;
            // 
            // cbUk2
            // 
            this.cbUk2.FormattingEnabled = true;
            this.cbUk2.ItemHeight = 13;
            this.cbUk2.Location = new System.Drawing.Point(553, 12);
            this.cbUk2.Name = "cbUk2";
            this.cbUk2.Size = new System.Drawing.Size(127, 21);
            this.cbUk2.TabIndex = 26;
            this.cbUk2.Text = "Pilih Ukuran";
            this.cbUk2.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cbO2
            // 
            this.cbO2.FormattingEnabled = true;
            this.cbO2.ItemHeight = 13;
            this.cbO2.Items.AddRange(new object[] {
            ""});
            this.cbO2.Location = new System.Drawing.Point(28, 12);
            this.cbO2.Name = "cbO2";
            this.cbO2.Size = new System.Drawing.Size(127, 21);
            this.cbO2.TabIndex = 25;
            this.cbO2.Text = "Pilih Type";
            this.cbO2.SelectedIndexChanged += new System.EventHandler(this.cbO2_SelectedIndexChanged);
            // 
            // cbOUk2
            // 
            this.cbOUk2.FormattingEnabled = true;
            this.cbOUk2.ItemHeight = 13;
            this.cbOUk2.Location = new System.Drawing.Point(161, 12);
            this.cbOUk2.Name = "cbOUk2";
            this.cbOUk2.Size = new System.Drawing.Size(127, 21);
            this.cbOUk2.TabIndex = 20;
            this.cbOUk2.Text = "Pilih Ukuran";
            this.cbOUk2.SelectedIndexChanged += new System.EventHandler(this.cbOUk2_SelectedIndexChanged);
            // 
            // cb2
            // 
            this.cb2.FormattingEnabled = true;
            this.cb2.ItemHeight = 13;
            this.cb2.Items.AddRange(new object[] {
            ""});
            this.cb2.Location = new System.Drawing.Point(418, 12);
            this.cb2.Name = "cb2";
            this.cb2.Size = new System.Drawing.Size(127, 21);
            this.cb2.TabIndex = 1;
            this.cb2.Text = "Pilih Type";
            this.cb2.SelectedIndexChanged += new System.EventHandler(this.cb2_SelectedIndexChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 16);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "2";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel2.Controls.Add(this.kg1);
            this.panel2.Controls.Add(this.kgO1);
            this.panel2.Controls.Add(this.qtyO1);
            this.panel2.Controls.Add(this.qty1);
            this.panel2.Controls.Add(this.kgA1);
            this.panel2.Controls.Add(this.qtyA1);
            this.panel2.Controls.Add(this.t1);
            this.panel2.Controls.Add(this.l1);
            this.panel2.Controls.Add(this.p1);
            this.panel2.Controls.Add(this.cbA1);
            this.panel2.Controls.Add(this.cbOUk1);
            this.panel2.Controls.Add(this.cbO1);
            this.panel2.Controls.Add(this.cbUk1);
            this.panel2.Controls.Add(this.cb1);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Location = new System.Drawing.Point(6, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1311, 50);
            this.panel2.TabIndex = 1;
            // 
            // kg1
            // 
            this.kg1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.kg1.Location = new System.Drawing.Point(752, 9);
            this.kg1.Name = "kg1";
            this.kg1.Size = new System.Drawing.Size(79, 32);
            this.kg1.TabIndex = 42;
            // 
            // kgO1
            // 
            this.kgO1.Location = new System.Drawing.Point(347, 11);
            this.kgO1.Multiline = true;
            this.kgO1.Name = "kgO1";
            this.kgO1.Size = new System.Drawing.Size(42, 30);
            this.kgO1.TabIndex = 41;
            this.kgO1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyO1
            // 
            this.qtyO1.Location = new System.Drawing.Point(297, 11);
            this.qtyO1.Multiline = true;
            this.qtyO1.Name = "qtyO1";
            this.qtyO1.Size = new System.Drawing.Size(42, 30);
            this.qtyO1.TabIndex = 40;
            this.qtyO1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qty1
            // 
            this.qty1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty1.Location = new System.Drawing.Point(691, 11);
            this.qty1.Name = "qty1";
            this.qty1.Size = new System.Drawing.Size(55, 24);
            this.qty1.TabIndex = 35;
            this.qty1.ValueChanged += new System.EventHandler(this.qty1_ValueChanged);
            // 
            // kgA1
            // 
            this.kgA1.Location = new System.Drawing.Point(1196, 10);
            this.kgA1.Multiline = true;
            this.kgA1.Name = "kgA1";
            this.kgA1.Size = new System.Drawing.Size(79, 32);
            this.kgA1.TabIndex = 30;
            this.kgA1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA1
            // 
            this.qtyA1.Location = new System.Drawing.Point(1147, 10);
            this.qtyA1.Multiline = true;
            this.qtyA1.Name = "qtyA1";
            this.qtyA1.Size = new System.Drawing.Size(40, 32);
            this.qtyA1.TabIndex = 29;
            this.qtyA1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qtyA1.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // t1
            // 
            this.t1.Location = new System.Drawing.Point(1093, 10);
            this.t1.Multiline = true;
            this.t1.Name = "t1";
            this.t1.Size = new System.Drawing.Size(40, 32);
            this.t1.TabIndex = 28;
            this.t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t1.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // l1
            // 
            this.l1.Location = new System.Drawing.Point(1003, 10);
            this.l1.Multiline = true;
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(40, 32);
            this.l1.TabIndex = 27;
            this.l1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l1.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // p1
            // 
            this.p1.Location = new System.Drawing.Point(1048, 10);
            this.p1.Multiline = true;
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(40, 32);
            this.p1.TabIndex = 26;
            this.p1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.p1.TextChanged += new System.EventHandler(this.p1_TextChanged);
            // 
            // cbA1
            // 
            this.cbA1.Location = new System.Drawing.Point(861, 10);
            this.cbA1.Multiline = true;
            this.cbA1.Name = "cbA1";
            this.cbA1.Size = new System.Drawing.Size(134, 32);
            this.cbA1.TabIndex = 25;
            this.cbA1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.cbA1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbA1_MouseClick);
            // 
            // cbOUk1
            // 
            this.cbOUk1.FormattingEnabled = true;
            this.cbOUk1.ItemHeight = 13;
            this.cbOUk1.Location = new System.Drawing.Point(161, 13);
            this.cbOUk1.Name = "cbOUk1";
            this.cbOUk1.Size = new System.Drawing.Size(127, 21);
            this.cbOUk1.TabIndex = 22;
            this.cbOUk1.Text = "Pilih Ukuran";
            this.cbOUk1.SelectedIndexChanged += new System.EventHandler(this.cbOUk1_SelectedIndexChanged);
            // 
            // cbO1
            // 
            this.cbO1.FormattingEnabled = true;
            this.cbO1.ItemHeight = 13;
            this.cbO1.Items.AddRange(new object[] {
            ""});
            this.cbO1.Location = new System.Drawing.Point(28, 13);
            this.cbO1.Name = "cbO1";
            this.cbO1.Size = new System.Drawing.Size(127, 21);
            this.cbO1.TabIndex = 21;
            this.cbO1.Text = "Pilih Type";
            this.cbO1.SelectedIndexChanged += new System.EventHandler(this.cbO1_SelectedIndexChanged);
            // 
            // cbUk1
            // 
            this.cbUk1.FormattingEnabled = true;
            this.cbUk1.ItemHeight = 13;
            this.cbUk1.Location = new System.Drawing.Point(551, 13);
            this.cbUk1.Name = "cbUk1";
            this.cbUk1.Size = new System.Drawing.Size(127, 21);
            this.cbUk1.TabIndex = 20;
            this.cbUk1.Text = "Pilih Ukuran";
            this.cbUk1.SelectedIndexChanged += new System.EventHandler(this.cbUk1_SelectedIndexChanged);
            // 
            // cb1
            // 
            this.cb1.FormattingEnabled = true;
            this.cb1.ItemHeight = 13;
            this.cb1.Items.AddRange(new object[] {
            ""});
            this.cb1.Location = new System.Drawing.Point(418, 13);
            this.cb1.Name = "cb1";
            this.cb1.Size = new System.Drawing.Size(127, 21);
            this.cb1.TabIndex = 1;
            this.cb1.Text = "Pilih Type";
            this.cb1.SelectedIndexChanged += new System.EventHandler(this.cb1_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "1";
            // 
            // tgl
            // 
            this.tgl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tgl.Location = new System.Drawing.Point(954, 16);
            this.tgl.Name = "tgl";
            this.tgl.Size = new System.Drawing.Size(152, 38);
            this.tgl.TabIndex = 33;
            this.tgl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(770, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 23);
            this.label5.TabIndex = 38;
            this.label5.Text = "Kg";
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(709, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 23);
            this.label6.TabIndex = 37;
            this.label6.Text = "Qty";
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(569, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 23);
            this.label8.TabIndex = 36;
            this.label8.Text = "Ukuran";
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Location = new System.Drawing.Point(436, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 23);
            this.label9.TabIndex = 35;
            this.label9.Text = "Type";
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Location = new System.Drawing.Point(436, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(413, 23);
            this.label10.TabIndex = 34;
            this.label10.Text = "Stok Awal";
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Location = new System.Drawing.Point(363, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 23);
            this.label11.TabIndex = 43;
            this.label11.Text = "Kg";
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Location = new System.Drawing.Point(312, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 23);
            this.label12.TabIndex = 42;
            this.label12.Text = "Qty";
            // 
            // label13
            // 
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Location = new System.Drawing.Point(179, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 23);
            this.label13.TabIndex = 41;
            this.label13.Text = "Ukuran";
            // 
            // label14
            // 
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Location = new System.Drawing.Point(46, 97);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(127, 23);
            this.label14.TabIndex = 40;
            this.label14.Text = "Type";
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Location = new System.Drawing.Point(46, 74);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(361, 23);
            this.label15.TabIndex = 39;
            this.label15.Text = "Stok Order";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(1147, 651);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(191, 67);
            this.btnOk.TabIndex = 44;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(943, 651);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(191, 67);
            this.button1.TabIndex = 45;
            this.button1.Text = "Pending";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // noSpk
            // 
            this.noSpk.Location = new System.Drawing.Point(56, 19);
            this.noSpk.Multiline = true;
            this.noSpk.Name = "noSpk";
            this.noSpk.Size = new System.Drawing.Size(134, 32);
            this.noSpk.TabIndex = 46;
            this.noSpk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // msn1
            // 
            this.msn1.Location = new System.Drawing.Point(524, 13);
            this.msn1.Name = "msn1";
            this.msn1.Size = new System.Drawing.Size(93, 44);
            this.msn1.TabIndex = 47;
            this.msn1.Tag = "1";
            this.msn1.Text = "1";
            this.msn1.UseVisualStyleBackColor = true;
            this.msn1.Click += new System.EventHandler(this.msn1_Click);
            // 
            // msn2
            // 
            this.msn2.Location = new System.Drawing.Point(642, 13);
            this.msn2.Name = "msn2";
            this.msn2.Size = new System.Drawing.Size(93, 44);
            this.msn2.TabIndex = 48;
            this.msn2.Tag = "2";
            this.msn2.Text = "2";
            this.msn2.UseVisualStyleBackColor = true;
            this.msn2.Click += new System.EventHandler(this.msn2_Click);
            // 
            // msn3
            // 
            this.msn3.Location = new System.Drawing.Point(756, 13);
            this.msn3.Name = "msn3";
            this.msn3.Size = new System.Drawing.Size(93, 44);
            this.msn3.TabIndex = 49;
            this.msn3.Tag = "3";
            this.msn3.Text = "3";
            this.msn3.UseVisualStyleBackColor = true;
            this.msn3.Click += new System.EventHandler(this.msn3_Click);
            // 
            // cust
            // 
            this.cust.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cust.Location = new System.Drawing.Point(1186, 16);
            this.cust.Name = "cust";
            this.cust.Size = new System.Drawing.Size(137, 38);
            this.cust.TabIndex = 53;
            this.cust.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Location = new System.Drawing.Point(1129, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(209, 52);
            this.label24.TabIndex = 52;
            this.label24.Text = "Customer:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label21.Location = new System.Drawing.Point(1059, 97);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 23);
            this.label21.TabIndex = 54;
            this.label21.Text = "Panjang";
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Location = new System.Drawing.Point(1111, 97);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 23);
            this.label22.TabIndex = 55;
            this.label22.Text = "Tinggi";
            // 
            // FormSPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.cust);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.msn3);
            this.Controls.Add(this.msn2);
            this.Controls.Add(this.msn1);
            this.Controls.Add(this.noSpk);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tgl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormSPK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form SPK";
            this.Load += new System.EventHandler(this.FormSPK_Load);
            this.panel1.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty9)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty8)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty7)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty6)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty5)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qty1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cb1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label tgl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbUk1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.ComboBox cbOUk9;
        private System.Windows.Forms.ComboBox cbO9;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.ComboBox cbOUk8;
        private System.Windows.Forms.ComboBox cbO8;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox cbOUk7;
        private System.Windows.Forms.ComboBox cbO7;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox cbOUk6;
        private System.Windows.Forms.ComboBox cbO6;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox cbOUk5;
        private System.Windows.Forms.ComboBox cbO5;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox cbOUk4;
        private System.Windows.Forms.ComboBox cbO4;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox cbOUk3;
        private System.Windows.Forms.ComboBox cbO3;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cbOUk2;
        private System.Windows.Forms.ComboBox cb2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox cbOUk1;
        private System.Windows.Forms.ComboBox cbO1;
        private System.Windows.Forms.Label kg9;
        private System.Windows.Forms.ComboBox cbUk9;
        private System.Windows.Forms.ComboBox cb9;
        private System.Windows.Forms.Label kg8;
        private System.Windows.Forms.ComboBox cbUk8;
        private System.Windows.Forms.ComboBox cb8;
        private System.Windows.Forms.Label kg7;
        private System.Windows.Forms.ComboBox cbUk7;
        private System.Windows.Forms.ComboBox cb7;
        private System.Windows.Forms.Label kg6;
        private System.Windows.Forms.ComboBox cbUk6;
        private System.Windows.Forms.ComboBox cb6;
        private System.Windows.Forms.Label kg5;
        private System.Windows.Forms.ComboBox cbUk5;
        private System.Windows.Forms.ComboBox cb5;
        private System.Windows.Forms.Label kg4;
        private System.Windows.Forms.ComboBox cbUk4;
        private System.Windows.Forms.ComboBox cb4;
        private System.Windows.Forms.Label kg3;
        private System.Windows.Forms.ComboBox cbUk3;
        private System.Windows.Forms.ComboBox cb3;
        private System.Windows.Forms.Label kg2;
        private System.Windows.Forms.ComboBox cbUk2;
        private System.Windows.Forms.ComboBox cbO2;
        private System.Windows.Forms.TextBox kgA9;
        private System.Windows.Forms.TextBox qtyA9;
        private System.Windows.Forms.TextBox t9;
        private System.Windows.Forms.TextBox l9;
        private System.Windows.Forms.TextBox p9;
        private System.Windows.Forms.TextBox cbA9;
        private System.Windows.Forms.TextBox kgA8;
        private System.Windows.Forms.TextBox qtyA8;
        private System.Windows.Forms.TextBox t8;
        private System.Windows.Forms.TextBox l8;
        private System.Windows.Forms.TextBox p8;
        private System.Windows.Forms.TextBox cbA8;
        private System.Windows.Forms.TextBox kgA7;
        private System.Windows.Forms.TextBox qtyA7;
        private System.Windows.Forms.TextBox t7;
        private System.Windows.Forms.TextBox l7;
        private System.Windows.Forms.TextBox p7;
        private System.Windows.Forms.TextBox cbA7;
        private System.Windows.Forms.TextBox kgA6;
        private System.Windows.Forms.TextBox qtyA6;
        private System.Windows.Forms.TextBox t6;
        private System.Windows.Forms.TextBox l6;
        private System.Windows.Forms.TextBox p6;
        private System.Windows.Forms.TextBox cbA6;
        private System.Windows.Forms.TextBox kgA5;
        private System.Windows.Forms.TextBox qtyA5;
        private System.Windows.Forms.TextBox t5;
        private System.Windows.Forms.TextBox l5;
        private System.Windows.Forms.TextBox p5;
        private System.Windows.Forms.TextBox cbA5;
        private System.Windows.Forms.TextBox kgA4;
        private System.Windows.Forms.TextBox qtyA4;
        private System.Windows.Forms.TextBox t4;
        private System.Windows.Forms.TextBox l4;
        private System.Windows.Forms.TextBox p4;
        private System.Windows.Forms.TextBox cbA4;
        private System.Windows.Forms.TextBox kgA3;
        private System.Windows.Forms.TextBox qtyA3;
        private System.Windows.Forms.TextBox t3;
        private System.Windows.Forms.TextBox l3;
        private System.Windows.Forms.TextBox p3;
        private System.Windows.Forms.TextBox cbA3;
        private System.Windows.Forms.TextBox kgA2;
        private System.Windows.Forms.TextBox qtyA2;
        private System.Windows.Forms.TextBox t2;
        private System.Windows.Forms.TextBox l2;
        private System.Windows.Forms.TextBox p2;
        private System.Windows.Forms.TextBox cbA2;
        private System.Windows.Forms.TextBox kgA1;
        private System.Windows.Forms.TextBox qtyA1;
        private System.Windows.Forms.TextBox t1;
        private System.Windows.Forms.TextBox l1;
        private System.Windows.Forms.TextBox p1;
        private System.Windows.Forms.TextBox cbA1;
        private System.Windows.Forms.TextBox noSpk;
        private System.Windows.Forms.Button msn1;
        private System.Windows.Forms.Button msn2;
        private System.Windows.Forms.Button msn3;
        private System.Windows.Forms.TextBox kgO2;
        private System.Windows.Forms.TextBox qtyO2;
        private System.Windows.Forms.TextBox kgO3;
        private System.Windows.Forms.TextBox qtyO3;
        private System.Windows.Forms.TextBox kgO7;
        private System.Windows.Forms.TextBox qtyO7;
        private System.Windows.Forms.TextBox kgO6;
        private System.Windows.Forms.TextBox qtyO6;
        private System.Windows.Forms.TextBox kgO5;
        private System.Windows.Forms.TextBox qtyO5;
        private System.Windows.Forms.TextBox kgO4;
        private System.Windows.Forms.TextBox qtyO4;
        private System.Windows.Forms.TextBox kgO9;
        private System.Windows.Forms.TextBox qtyO9;
        private System.Windows.Forms.TextBox kgO8;
        private System.Windows.Forms.TextBox qtyO8;
        private System.Windows.Forms.Label cust;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown qty1;
        private System.Windows.Forms.NumericUpDown qty2;
        private System.Windows.Forms.NumericUpDown qty3;
        private System.Windows.Forms.NumericUpDown qty4;
        private System.Windows.Forms.NumericUpDown qty5;
        private System.Windows.Forms.NumericUpDown qty6;
        private System.Windows.Forms.NumericUpDown qty7;
        private System.Windows.Forms.NumericUpDown qty8;
        private System.Windows.Forms.NumericUpDown qty9;
        private System.Windows.Forms.TextBox kgO1;
        private System.Windows.Forms.TextBox qtyO1;
        private System.Windows.Forms.Label kg1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
    }
}