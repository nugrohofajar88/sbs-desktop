﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baja
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            
            
            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("en-US");

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;


            Random rand = new Random();
            Console.Write("test     {0}     ", rand.NextDouble());


            //FormAdmin frmMain = new FormAdmin();
            //frmMain.Show();


            FormLogin _formLogin = new FormLogin();

            if (_formLogin.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new FormAdmin(_formLogin.username));
            }
            else
            {
                Application.Exit();
            }
            

            //Application.Run(new FormAddSpk());

        }
    }
}
