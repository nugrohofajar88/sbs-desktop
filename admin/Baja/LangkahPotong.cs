﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class LangkahPotong : UserControl
    {
        public LangkahPotong()
        {
            InitializeComponent();
        }

        private void qtyO_Enter(object sender, EventArgs e)
        {
            NumericUpDown numUD = (NumericUpDown)sender;
            numUD.Select(0, numUD.Text.Length);
        }
    }
}
