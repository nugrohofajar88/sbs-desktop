﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Baja
{
    class SettingManager
    {
        Dictionary<string, string> settings;
        string _filename;

        public SettingManager(string filename)
        {
            _filename= filename;
            settings = new Dictionary<string, string>();
            Load(filename);
            //Save(filename);
        }

        void Load(string filename)
        {
            XmlTextReader reader = new XmlTextReader(filename);
            reader.WhitespaceHandling = WhitespaceHandling.None;

            while (reader.Read())
            {
                //Console.WriteLine(reader.Name);
                if (reader.IsStartElement())
                {
                    if (reader.Name == "set")
                    {
                        if (!settings.ContainsKey(reader["key"]))
                        {
                            //Console.WriteLine("adding: " + reader["key"] + ", " + reader["value"]);
                            settings.Add(reader["key"], reader["value"]);
                        }
                        else
                        {
                            throw new Exception("duplicate key");
                        }
                    }
                }
            }

            reader.Close();

        }

        public void AddSetting(string key, string value)
        {
            if (!settings.ContainsKey(key))
                settings.Add(key, value);
            else
                return;
        }

        public string GetValue(string key)
        {
            return settings[key];
        }

        public void Save()
        {
            Save(_filename);
        }
        
        void Save(string filename)
        {
            XmlWriter writer = XmlTextWriter.Create(filename);

            writer.WriteStartDocument();
            writer.WriteStartElement("settings");
            writer.WriteWhitespace("\n");

            foreach (KeyValuePair<string, string> kvp in settings)
            {
                writer.WriteStartElement("set");
                writer.WriteAttributeString("key", kvp.Key);
                writer.WriteAttributeString("value", kvp.Value);
                writer.WriteEndElement();
                writer.WriteWhitespace("\n");
            }
            writer.WriteEndDocument();
            writer.Close();
        }

    }
}
