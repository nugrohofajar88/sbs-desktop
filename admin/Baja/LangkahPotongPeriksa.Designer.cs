﻿namespace Baja
{
    partial class LangkahPotongPeriksa
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qtyO = new System.Windows.Forms.TextBox();
            this.to = new System.Windows.Forms.TextBox();
            this.lo = new System.Windows.Forms.TextBox();
            this.po = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // qtyO
            // 
            this.qtyO.Location = new System.Drawing.Point(160, 3);
            this.qtyO.Multiline = true;
            this.qtyO.Name = "qtyO";
            this.qtyO.ReadOnly = true;
            this.qtyO.Size = new System.Drawing.Size(37, 32);
            this.qtyO.TabIndex = 14;
            this.qtyO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // to
            // 
            this.to.Location = new System.Drawing.Point(108, 3);
            this.to.Multiline = true;
            this.to.Name = "to";
            this.to.ReadOnly = true;
            this.to.Size = new System.Drawing.Size(50, 32);
            this.to.TabIndex = 13;
            this.to.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lo
            // 
            this.lo.Location = new System.Drawing.Point(4, 3);
            this.lo.Multiline = true;
            this.lo.Name = "lo";
            this.lo.ReadOnly = true;
            this.lo.Size = new System.Drawing.Size(50, 32);
            this.lo.TabIndex = 11;
            this.lo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // po
            // 
            this.po.Location = new System.Drawing.Point(56, 3);
            this.po.Multiline = true;
            this.po.Name = "po";
            this.po.ReadOnly = true;
            this.po.Size = new System.Drawing.Size(50, 32);
            this.po.TabIndex = 12;
            this.po.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LangkahPotongPeriksa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.qtyO);
            this.Controls.Add(this.to);
            this.Controls.Add(this.lo);
            this.Controls.Add(this.po);
            this.Name = "LangkahPotongPeriksa";
            this.Size = new System.Drawing.Size(200, 38);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox qtyO;
        public System.Windows.Forms.TextBox to;
        public System.Windows.Forms.TextBox lo;
        public System.Windows.Forms.TextBox po;
    }
}
