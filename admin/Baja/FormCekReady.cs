﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Baja
{
    public partial class FormCekReady : Form
    {
        koneksi konek;
        int mesin = 0;
        public string admin = "";
        public string no_po = "";

        Dictionary<string, detailUkuran> grupUkuran;
        Dictionary<string, detailUkuran> stok;
        FormSPK _formspk;
        FormAddSpk _formAddSPK;

        public FormCekReady(string noPO, string adm)
        {
            InitializeComponent();
            konek = new koneksi();

            no_po = noPO;
            admin = adm;

            grup();
            Allstock();
        }

        private void FormCekReady_Load(object sender, EventArgs e)
        {
          

            //foreach (detailUkuran item in grupUkuran.Values)
            //{
            //    Console.WriteLine(item.tipe);
            //}

            //foreach (detailUkuran iteme in stok.Values)
            //{
            //    Console.WriteLine(iteme.tipe);
            //}


        }

        
        private void grup()
        {
            konek.listOrder(dataGridView1, no_po);
            showRekomendasi(0);
        }

        private void Allstock()
        {
            //stok = new Dictionary<string, detailUkuran>();
            //detailUkuran _detailUk = new detailUkuran();

            //try
            //{
            //    konek.openkoneksi();
            //    MySqlCommand dbcmd = konek.db.CreateCommand();
            //    string sql = "select type,panjang,lebar,tinggi ,qty_stock from m_type,m_stock,m_ukuran where m_stock.id_type=m_type.id_type and m_stock.id_ukuran=m_ukuran.id_ukuran  ";
            //    dbcmd.CommandText = sql;
            //    MySqlDataReader reader = dbcmd.ExecuteReader();
            //    while (reader.Read())
            //    {
            //        _detailUk.tipe = reader.GetString(0).ToString().ToUpper();
            //        _detailUk.panjang = reader.GetString(1).ToString().ToUpper();
            //        _detailUk.lebar = reader.GetString(2).ToString().ToUpper();
            //        _detailUk.tinggi = reader.GetString(3).ToString().ToUpper();
            //        _detailUk.qty = int.Parse(reader.GetString(4).ToString().ToUpper());

            //        stok.Add(_detailUk.tipe, _detailUk);
            //    }
            //}
            //catch (MySqlException ex)
            //{
            //    Console.WriteLine("Error: {0}", ex.ToString());

            //}
            //finally
            //{
            //    if (konek.db != null)
            //    {
            //        konek.closekoneksi();
            //    }
            //}
        }

        private void btnCreateSpk_Click(object sender, EventArgs e)
        {
            //_formspk = new FormSPK(no_po,admin);
            //_formspk.FormClosed += _formspk_FormClosed;
            //_formspk.Show();

            _formAddSPK = new FormAddSpk(no_po, admin);
            _formAddSPK.FormClosed += _formspk_FormClosed;
            _formAddSPK.Show();
        }

        private void _formspk_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            showRekomendasi(rowIndex);
        }

        private void showRekomendasi(int rowIndex)
        {
            if (dataGridView1.RowCount == 0)
                return;
            DataGridViewRow row = dataGridView1.Rows[rowIndex];
            if (row.Cells[0].Value == null)
                return;
            String typeSelected = row.Cells[0].Value.ToString();
            String lSelected = row.Cells[2].Value.ToString();
            String pSelected = row.Cells[3].Value.ToString();
            String tSelected = row.Cells[4].Value.ToString();
            konek.listRekomendasi(dataGridView2, typeSelected, pSelected, lSelected, tSelected);
        }

        private void btnPending_Click(object sender, EventArgs e)
        {
            //meerubah status t_po menjadi 2
             
            this.Close();
        }

        private void timernotif_Tick(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }

}
