﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class ViewStock : Form
    {
        public bool isFirstTime = true;
        koneksi konek;
        public ViewStock()
        {
            InitializeComponent();

            konek = new koneksi();

        }

        private void dgvStock_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            int idColumn = dgvStock.Columns["id_stock"].Index;
            int qtyColumn = dgvStock.Columns["QTY"].Index;
            int kgColumn = dgvStock.Columns["KG"].Index;
            int hargaColumn = dgvStock.Columns["Harga"].Index;

            string idStock = dgvStock[idColumn, e.RowIndex].Value.ToString();
            string qty = dgvStock[qtyColumn, e.RowIndex].Value.ToString();
            string kg = dgvStock[kgColumn, e.RowIndex].Value.ToString();
            string harga = dgvStock[hargaColumn, e.RowIndex].Value.ToString();
            konek.updateStock(idStock,qty,kg,harga);
        }

        private void dgvStock_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;
            for (int i = 0; i < dgvStock.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvStock[i, e.RowIndex].Style.BackColor = Color.Yellow;
            }
        }

        private void dgvStock_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;

            for (int i = 0; i < dgvStock.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvStock[i, e.RowIndex].Style.BackColor = Color.Empty;
            }
        }

        private void tbType_TextChanged(object sender, EventArgs e)
        {
            DataTable dtTable = (DataTable)dgvStock.DataSource;
            isFirstTime = true;
            dtTable.DefaultView.RowFilter = constructFilter();
            dgvStock.Columns["lebar"].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            isFirstTime = false;
        }

        private string constructFilter()
        {
            int countFilter = 0;
            string result = "";

            if (!string.IsNullOrEmpty(tbType.Text))
            {
                result += strFilterType(countFilter);
                countFilter++;
            }
            if (!string.IsNullOrEmpty(tbL.Text))
            {
                result += strFilterLebar(countFilter);
                countFilter++;
            }
            if (!string.IsNullOrEmpty(tbP.Text))
            {
                result += strFilterPanjang(countFilter);
                countFilter++;
            }

            if(!cbRound.Checked)
            {
                if (!string.IsNullOrEmpty(tbT.Text))
                {
                    result += strFilterTinggi(countFilter);
                    countFilter++;
                }
            }else
            {
                result += strFilterRoundOnly(countFilter);
                countFilter++;
            }
            if (!string.IsNullOrEmpty(tbQty.Text))
            {
                result += strFilterQty(countFilter);
                countFilter++;
            }



            return result;
        }

        private string strFilterType(int countFilter)
        {
            if(countFilter>0)
                return string.Format(" AND [{0}] LIKE '%{1}%' ", "Type", tbType.Text);
            else
                return string.Format(" [{0}] LIKE '%{1}%' ", "Type", tbType.Text);

        }

        private string strFilterLebar(int countFilter)
        {
            if (countFilter > 0)
                return string.Format(" AND [{0}] >= {1} ", "lebar", tbL.Text);
            else
                return string.Format(" [{0}] >= {1} ", "lebar", tbL.Text);

        }
        private string strFilterPanjang(int countFilter)
        {
            if (countFilter > 0)
                return string.Format(" AND [{0}] >= {1} ", "panjang", tbP.Text);
            else
                return string.Format(" [{0}] >= {1} ", "panjang", tbP.Text);

        }
        private string strFilterTinggi(int countFilter)
        {
            if (countFilter > 0)
                return string.Format(" AND [{0}] >= {1} ", "tinggi", tbT.Text);
            else
                return string.Format(" [{0}] >= {1} ", "tinggi", tbT.Text);

        }
        private string strFilterRoundOnly(int countFilter)
        {
            if (countFilter > 0)
                return string.Format(" AND [{0}] IS NULL ", "tinggi");
            else
                return string.Format(" [{0}] IS NULL ", "tinggi");

        }
        private string strFilterQty(int countFilter)
        {
            if (countFilter > 0)
                return string.Format(" AND [{0}] >= {1} ", "QTY", tbQty.Text);
            else
                return string.Format(" [{0}] >= {1} ", "QTY", tbQty.Text);

        }

        private void cbRound_CheckedChanged(object sender, EventArgs e)
        {
            DataTable dtTable = (DataTable)dgvStock.DataSource;
            isFirstTime = true;
            dtTable.DefaultView.RowFilter = constructFilter();
            dgvStock.Columns["lebar"].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            isFirstTime = false;
        }
    }
}
