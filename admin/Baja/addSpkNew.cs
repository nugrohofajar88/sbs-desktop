﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class addSpkNew : UserControl
    {
        public string no_po = "1/Y";
        public string id_po = "3";

        private LangkahPotong currentLangkahPotong;
        private StokAkhir currentStokAkhir;

        koneksi konek;

        public int idspk { get; set; }
        public int idUserControl { get; set; }

        public type selectedType { get; set; }
        public ukuran selectedUkuranAwal { get; set; }
        public ukuran selectedUkuranOrder { get; set; }
        public ukuran selectedUkuranAkhir { get; set; }
        public string selectedMesin { get; set; }
        public string insertedId { get; set; }

        public addSpkNew(string idpo)
        {
            InitializeComponent();
            konek = new koneksi();
            id_po = idpo;
            reloadData();

            currentLangkahPotong = new LangkahPotong();
            currentLangkahPotong.btnPlus.Click += BtnPlus_Click;

            currentStokAkhir = new StokAkhir();
            currentStokAkhir.btnPlus.Click += BtnPlus_Click1;

            lbox.Controls.Add(currentLangkahPotong);
            lbox2.Controls.Add(currentStokAkhir);
        }

        public addSpkNew(string idpo, int id_spk)
        {
            InitializeComponent();
            konek = new koneksi();
            id_po = idpo;
            idspk = id_spk;
            reloadData();

            currentLangkahPotong = new LangkahPotong();
            currentLangkahPotong.btnPlus.Click += BtnPlus_Click;

            currentStokAkhir = new StokAkhir();
            currentStokAkhir.btnPlus.Click += BtnPlus_Click1;

            lbox.Controls.Add(currentLangkahPotong);
            lbox2.Controls.Add(currentStokAkhir);
        }

        private void reloadData()
        {
            konek.selectAllAvailableBrgType(cb);
        }

        public void disableEditing()
        {
            //setelah add item baru, maka tidak bisa diedit
            mesinCb.Enabled = false;
            tbKet.Enabled = false;
            kgO.Enabled = false;
            qtyO.Enabled = false;
            lbox2.Enabled = false;
            cbOUk.Enabled = false;
            kg.Enabled = false;
            qty.Enabled = false;
            cbUk.Enabled = false;
            cb.Enabled = false;
            lbox.Enabled = false;
            

        }


        private void cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedType = (type)((ComboBox)sender).SelectedItem;

            //filling ukuran awal
            konek.getAvailableUkuran(cbUk, selectedType.id_type.ToString(), id_po);
            
            //filling ukuran order with all kind of ukuran, but greater than minimum Ukuran from id_po
            konek.getAllUkuranPO(cbOUk, id_po);

        }

        private void cbUk_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            selectedUkuranAwal = (ukuran)combo.SelectedItem;
            qty.Maximum = selectedUkuranAwal.max_qty;

        }

        private void cbOUk_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedUkuranOrder = (ukuran)((ComboBox)sender).SelectedItem;
        }

        private void qty_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numQty = (NumericUpDown)sender;
            double kgVal = 0;
            if (selectedUkuranAwal != null)
                kgVal = countKg(selectedUkuranAwal.panjang, selectedUkuranAwal.lebar, selectedUkuranAwal.tinggi, numQty.Value);
            kg.Text = kgVal.ToString();
        }

        private void qtyO_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numQty = (NumericUpDown)sender;
            double kgVal = 0;
            if (selectedUkuranOrder != null)
            {
                kgVal = countKg(selectedUkuranOrder.panjang, selectedUkuranOrder.lebar, selectedUkuranOrder.tinggi, numQty.Value);
                selectedUkuranOrder.max_qty = (int)numQty.Value;
            }

            kgO.Text = kgVal.ToString();
        }


        private double countKg(double p, double l, double t, decimal qtyVal)
        {
            int qty = (int)qtyVal;
            if (t > 0)
            {
                return (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;
            }
            else
            {
                return (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
            }
        }


        private void BtnPlus_Click1(object sender, EventArgs e)
        {
            currentStokAkhir = new StokAkhir();
            currentStokAkhir.btnPlus.Click += BtnPlus_Click1;
            
            lbox2.Controls.Add(currentStokAkhir);
        }

        private void BtnPlus_Click(object sender, EventArgs e)
        {
            currentLangkahPotong = new LangkahPotong();
            currentLangkahPotong.btnPlus.Click += BtnPlus_Click;

            lbox.Controls.Add(currentLangkahPotong);
        }

        private void qty_Enter(object sender, EventArgs e)
        {
            NumericUpDown numUD = (NumericUpDown)sender;
            numUD.Select(0, numUD.Text.Length);
        }

        private void mesinCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            selectedMesin = combo.Text.ToString();
        }

        public void saveToDB()
        {
            //insert into dt_spk
            string hasil = "";

            hasil = konek.adddt_spk(idspk, int.Parse(selectedUkuranAwal.id_stock), selectedUkuranOrder.id_ukuran,
                    qty.Value.ToString(), qtyO.Value.ToString(), 0, kg.Text, kgO.Text, selectedMesin, tbKet.Text);

            string queryMultiInsert = "";

            for(int i=0; i<lbox.Controls.Count; i++)
            {
                LangkahPotong item = (LangkahPotong)lbox.Controls[i];
                if(item.lo.Text != null && item.lo.Text.Length>0)
                {
                    string tinggi = "NULL";
                    if (item.to.Text != null && item.to.Text.Length > 0)
                        tinggi = item.to.Text;
                    queryMultiInsert += "insert into dt_spk_langkah_potong ( " +
                        "id_dt_spk,lebar,panjang,tinggi,qty ) values ( " +
                        hasil + " , " + item.lo.Text + "," + item.po.Text + "," + tinggi + "," +
                        item.qtyO.Value.ToString() + " ); ";
                }
            }
            
            for(int i=0;i<lbox2.Controls.Count;i++)
            {
                StokAkhir item = (StokAkhir)lbox2.Controls[i];
                if (item.l.Text != null && item.l.Text.Length > 0)
                {
                    string tinggi = "NULL";
                    if (item.t.Text != null && item.t.Text.Length > 0)
                        tinggi = item.t.Text;
                    queryMultiInsert += "insert into dt_spk_plan_stok_akhir ( " +
                        "id_dt_spk,lebar,panjang,tinggi,qty,kg ) values ( " +
                        hasil + " , " + item.l.Text + "," + item.p.Text + "," + tinggi + "," +
                        item.qtyA.Value.ToString() + "," + item.kgA.Text + " ); ";
                }
            }
            if (queryMultiInsert.Length > 0)
                konek.insertMultiple(queryMultiInsert);

            konek.decreseStock(int.Parse(selectedUkuranAwal.id_stock), qty.Value.ToString(), kg.Text);

        }

    }
}
