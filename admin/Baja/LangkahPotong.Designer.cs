﻿namespace Baja
{
    partial class LangkahPotong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qtyO = new System.Windows.Forms.NumericUpDown();
            this.to = new System.Windows.Forms.TextBox();
            this.lo = new System.Windows.Forms.TextBox();
            this.po = new System.Windows.Forms.TextBox();
            this.btnPlus = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).BeginInit();
            this.SuspendLayout();
            // 
            // qtyO
            // 
            this.qtyO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyO.Location = new System.Drawing.Point(104, 3);
            this.qtyO.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qtyO.Name = "qtyO";
            this.qtyO.Size = new System.Drawing.Size(44, 24);
            this.qtyO.TabIndex = 4;
            this.qtyO.Enter += new System.EventHandler(this.qtyO_Enter);
            // 
            // to
            // 
            this.to.Location = new System.Drawing.Point(67, 1);
            this.to.Multiline = true;
            this.to.Name = "to";
            this.to.Size = new System.Drawing.Size(35, 32);
            this.to.TabIndex = 3;
            this.to.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lo
            // 
            this.lo.Location = new System.Drawing.Point(3, 1);
            this.lo.Multiline = true;
            this.lo.Name = "lo";
            this.lo.Size = new System.Drawing.Size(30, 32);
            this.lo.TabIndex = 1;
            this.lo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // po
            // 
            this.po.Location = new System.Drawing.Point(35, 1);
            this.po.Multiline = true;
            this.po.Name = "po";
            this.po.Size = new System.Drawing.Size(30, 32);
            this.po.TabIndex = 2;
            this.po.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlus.Location = new System.Drawing.Point(151, 0);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(30, 30);
            this.btnPlus.TabIndex = 6;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            // 
            // LangkahPotong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.qtyO);
            this.Controls.Add(this.to);
            this.Controls.Add(this.lo);
            this.Controls.Add(this.po);
            this.Controls.Add(this.btnPlus);
            this.Name = "LangkahPotong";
            this.Size = new System.Drawing.Size(183, 34);
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.NumericUpDown qtyO;
        public System.Windows.Forms.TextBox to;
        public System.Windows.Forms.TextBox lo;
        public System.Windows.Forms.TextBox po;
        public System.Windows.Forms.Button btnPlus;
    }
}
