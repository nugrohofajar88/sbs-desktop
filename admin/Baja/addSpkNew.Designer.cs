﻿namespace Baja
{
    partial class addSpkNew
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb = new System.Windows.Forms.ComboBox();
            this.cbUk = new System.Windows.Forms.ComboBox();
            this.qty = new System.Windows.Forms.NumericUpDown();
            this.lbox = new System.Windows.Forms.FlowLayoutPanel();
            this.lbox2 = new System.Windows.Forms.FlowLayoutPanel();
            this.mesinCb = new System.Windows.Forms.ComboBox();
            this.cbOUk = new System.Windows.Forms.ComboBox();
            this.qtyO = new System.Windows.Forms.NumericUpDown();
            this.kgO = new System.Windows.Forms.TextBox();
            this.kg = new System.Windows.Forms.TextBox();
            this.tbKet = new System.Windows.Forms.TextBox();
            this.btnCari = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).BeginInit();
            this.SuspendLayout();
            // 
            // cb
            // 
            this.cb.FormattingEnabled = true;
            this.cb.ItemHeight = 13;
            this.cb.Items.AddRange(new object[] {
            ""});
            this.cb.Location = new System.Drawing.Point(7, 9);
            this.cb.Name = "cb";
            this.cb.Size = new System.Drawing.Size(114, 21);
            this.cb.TabIndex = 1;
            this.cb.Text = "Pilih Type";
            this.cb.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);
            // 
            // cbUk
            // 
            this.cbUk.FormattingEnabled = true;
            this.cbUk.ItemHeight = 13;
            this.cbUk.Location = new System.Drawing.Point(124, 9);
            this.cbUk.Name = "cbUk";
            this.cbUk.Size = new System.Drawing.Size(119, 21);
            this.cbUk.TabIndex = 2;
            this.cbUk.Text = "Pilih Ukuran";
            this.cbUk.SelectedIndexChanged += new System.EventHandler(this.cbUk_SelectedIndexChanged);
            // 
            // qty
            // 
            this.qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty.Location = new System.Drawing.Point(295, 9);
            this.qty.Name = "qty";
            this.qty.Size = new System.Drawing.Size(39, 24);
            this.qty.TabIndex = 4;
            this.qty.TextChanged += new System.EventHandler(this.qty_ValueChanged);
            this.qty.Enter += new System.EventHandler(this.qty_Enter);
            // 
            // lbox
            // 
            this.lbox.AutoScroll = true;
            this.lbox.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lbox.Location = new System.Drawing.Point(640, 3);
            this.lbox.Name = "lbox";
            this.lbox.Size = new System.Drawing.Size(208, 127);
            this.lbox.TabIndex = 9;
            // 
            // lbox2
            // 
            this.lbox2.AutoScroll = true;
            this.lbox2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lbox2.Location = new System.Drawing.Point(850, 3);
            this.lbox2.Name = "lbox2";
            this.lbox2.Size = new System.Drawing.Size(262, 127);
            this.lbox2.TabIndex = 10;
            // 
            // mesinCb
            // 
            this.mesinCb.FormattingEnabled = true;
            this.mesinCb.ItemHeight = 13;
            this.mesinCb.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.mesinCb.Location = new System.Drawing.Point(1116, 4);
            this.mesinCb.Name = "mesinCb";
            this.mesinCb.Size = new System.Drawing.Size(78, 21);
            this.mesinCb.TabIndex = 11;
            this.mesinCb.Text = "Pilih Mesin";
            this.mesinCb.SelectedIndexChanged += new System.EventHandler(this.mesinCb_SelectedIndexChanged);
            this.mesinCb.TextChanged += new System.EventHandler(this.mesinCb_SelectedIndexChanged);
            // 
            // cbOUk
            // 
            this.cbOUk.FormattingEnabled = true;
            this.cbOUk.ItemHeight = 13;
            this.cbOUk.Location = new System.Drawing.Point(398, 9);
            this.cbOUk.Name = "cbOUk";
            this.cbOUk.Size = new System.Drawing.Size(135, 21);
            this.cbOUk.TabIndex = 6;
            this.cbOUk.Text = "Pilih Ukuran";
            this.cbOUk.SelectedIndexChanged += new System.EventHandler(this.cbOUk_SelectedIndexChanged);
            // 
            // qtyO
            // 
            this.qtyO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyO.Location = new System.Drawing.Point(536, 9);
            this.qtyO.Name = "qtyO";
            this.qtyO.Size = new System.Drawing.Size(42, 24);
            this.qtyO.TabIndex = 7;
            this.qtyO.TextChanged += new System.EventHandler(this.qtyO_ValueChanged);
            this.qtyO.Enter += new System.EventHandler(this.qty_Enter);
            // 
            // kgO
            // 
            this.kgO.Location = new System.Drawing.Point(580, 8);
            this.kgO.Multiline = true;
            this.kgO.Name = "kgO";
            this.kgO.Size = new System.Drawing.Size(54, 30);
            this.kgO.TabIndex = 8;
            this.kgO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kg
            // 
            this.kg.Location = new System.Drawing.Point(337, 8);
            this.kg.Multiline = true;
            this.kg.Name = "kg";
            this.kg.Size = new System.Drawing.Size(58, 30);
            this.kg.TabIndex = 5;
            this.kg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbKet
            // 
            this.tbKet.Location = new System.Drawing.Point(1198, 4);
            this.tbKet.Multiline = true;
            this.tbKet.Name = "tbKet";
            this.tbKet.Size = new System.Drawing.Size(110, 116);
            this.tbKet.TabIndex = 12;
            // 
            // btnCari
            // 
            this.btnCari.Location = new System.Drawing.Point(245, 9);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(45, 25);
            this.btnCari.TabIndex = 3;
            this.btnCari.Text = "CARI";
            this.btnCari.UseVisualStyleBackColor = true;
            // 
            // addSpkNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.btnCari);
            this.Controls.Add(this.tbKet);
            this.Controls.Add(this.kg);
            this.Controls.Add(this.kgO);
            this.Controls.Add(this.qtyO);
            this.Controls.Add(this.cbOUk);
            this.Controls.Add(this.mesinCb);
            this.Controls.Add(this.lbox2);
            this.Controls.Add(this.lbox);
            this.Controls.Add(this.qty);
            this.Controls.Add(this.cbUk);
            this.Controls.Add(this.cb);
            this.Name = "addSpkNew";
            this.Size = new System.Drawing.Size(1311, 133);
            ((System.ComponentModel.ISupportInitialize)(this.qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox cb;
        public System.Windows.Forms.ComboBox cbUk;
        public System.Windows.Forms.NumericUpDown qty;
        private System.Windows.Forms.FlowLayoutPanel lbox;
        private System.Windows.Forms.FlowLayoutPanel lbox2;
        public System.Windows.Forms.ComboBox mesinCb;
        public System.Windows.Forms.ComboBox cbOUk;
        public System.Windows.Forms.NumericUpDown qtyO;
        public System.Windows.Forms.TextBox kgO;
        public System.Windows.Forms.TextBox kg;
        public System.Windows.Forms.TextBox tbKet;
        private System.Windows.Forms.Button btnCari;
    }
}
