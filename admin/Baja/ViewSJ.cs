﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class ViewSJ : Form
    {
        public bool isFirstTime = true;
        public string id_sj="";

        public ViewSJ()
        {
            InitializeComponent();
        }

        private void dgvSJ_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;
            for (int i = 0; i < dgvSJ.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvSJ[i, e.RowIndex].Style.BackColor = Color.Yellow;
            }
        }

        private void dgvSJ_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;

            for (int i = 0; i < dgvSJ.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvSJ[i, e.RowIndex].Style.BackColor = Color.Empty;
            }

        }

        private void btnPPN_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(id_sj))
                return;
            SJReportPPN report = new SJReportPPN();

            report.Parameters["parameterIdSJ"].Value = id_sj;
            report.Parameters["parameterIdSJ"].Visible = false;

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        private void btnNonPPN_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(id_sj))
                return;
            SJReportNonPPN report = new SJReportNonPPN();

            report.Parameters["parameterIdSJ"].Value = id_sj;
            report.Parameters["parameterIdSJ"].Visible = false;

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        private void dgvSJ_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgvSJ.Rows[rowIndex];
            id_sj = row.Cells["id_sj"].Value.ToString();
        }
    }
}
