﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormUser : Form
    {
        koneksi konek;
        ViewUser vsUser;
        public string admin;
        public FormUser()
        {
            InitializeComponent();
            konek = new koneksi();
            konek.fillJabatan(cbJabatan);
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            konek.addUser(admin, cbJabatan.Text, tbUsername.Text, tbPassword.Text, tbNama_dpn.Text, tbNama_blk.Text, int.Parse(tbTelp.Text));
            reset();
        }

        public void reset()
        {
            tbNama_dpn.Text = "";
            tbNama_blk.Text = "";
            tbUsername.Text = "";
            tbPassword.Text = "";
            tbTelp.Text = "";
            cbJabatan.Text = "";
            konek.fillJabatan(cbJabatan);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (!IsDisposed)
            {
                vsUser = new ViewUser();
                vsUser.FormClosed += VsUser_FormClosed;
            }
            konek.viewUser(vsUser.dgvUser);
            vsUser.Show();
            
        }

        private void VsUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            konek.closekoneksi();
        }
    }
}
