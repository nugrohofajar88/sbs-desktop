﻿namespace Baja
{
    partial class ViewSJ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSJ = new System.Windows.Forms.DataGridView();
            this.btnPPN = new System.Windows.Forms.Button();
            this.btnNonPPN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSJ)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSJ
            // 
            this.dgvSJ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSJ.Location = new System.Drawing.Point(22, 32);
            this.dgvSJ.Name = "dgvSJ";
            this.dgvSJ.Size = new System.Drawing.Size(1102, 399);
            this.dgvSJ.TabIndex = 0;
            this.dgvSJ.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSJ_CellClick);
            this.dgvSJ.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSJ_RowEnter);
            this.dgvSJ.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSJ_RowLeave);
            // 
            // btnPPN
            // 
            this.btnPPN.Location = new System.Drawing.Point(903, 451);
            this.btnPPN.Name = "btnPPN";
            this.btnPPN.Size = new System.Drawing.Size(221, 50);
            this.btnPPN.TabIndex = 1;
            this.btnPPN.Text = "Cetak PPN";
            this.btnPPN.UseVisualStyleBackColor = true;
            this.btnPPN.Click += new System.EventHandler(this.btnPPN_Click);
            // 
            // btnNonPPN
            // 
            this.btnNonPPN.Location = new System.Drawing.Point(673, 451);
            this.btnNonPPN.Name = "btnNonPPN";
            this.btnNonPPN.Size = new System.Drawing.Size(200, 50);
            this.btnNonPPN.TabIndex = 2;
            this.btnNonPPN.Text = "Cetak Non PPN";
            this.btnNonPPN.UseVisualStyleBackColor = true;
            this.btnNonPPN.Visible = false;
            this.btnNonPPN.Click += new System.EventHandler(this.btnNonPPN_Click);
            // 
            // ViewSJ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 513);
            this.Controls.Add(this.btnNonPPN);
            this.Controls.Add(this.btnPPN);
            this.Controls.Add(this.dgvSJ);
            this.Name = "ViewSJ";
            this.Text = "List Surat Jalan ";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSJ)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnPPN;
        private System.Windows.Forms.Button btnNonPPN;
        public System.Windows.Forms.DataGridView dgvSJ;
    }
}