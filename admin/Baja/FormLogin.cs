﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;

namespace Baja
{
    public partial class FormLogin : Form
    {
        koneksi konek;
        public String username = "";
        public FormLogin()
        {
            
            InitializeComponent();
            konek = new koneksi();
        }

        private void btnok_Click(object sender, EventArgs e)
        {

            if (konek.LoginAdmin(tbusername.Text, tbpassword.Text) == true)
            {
                this.username =konek.namaAdmin(tbusername.Text);
                this.Close();
                DialogResult = DialogResult.OK;

            }else
            {
                MessageBox.Show("Wrong Password!");
            }
            
        }

       
    }
}
