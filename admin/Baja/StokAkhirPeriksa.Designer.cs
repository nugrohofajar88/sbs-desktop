﻿namespace Baja
{
    partial class StokAkhirPeriksa
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qtyA = new System.Windows.Forms.TextBox();
            this.kgA = new System.Windows.Forms.TextBox();
            this.t = new System.Windows.Forms.TextBox();
            this.l = new System.Windows.Forms.TextBox();
            this.p = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // qtyA
            // 
            this.qtyA.Location = new System.Drawing.Point(141, 2);
            this.qtyA.Multiline = true;
            this.qtyA.Name = "qtyA";
            this.qtyA.ReadOnly = true;
            this.qtyA.Size = new System.Drawing.Size(37, 32);
            this.qtyA.TabIndex = 17;
            this.qtyA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgA
            // 
            this.kgA.Location = new System.Drawing.Point(179, 2);
            this.kgA.Multiline = true;
            this.kgA.Name = "kgA";
            this.kgA.ReadOnly = true;
            this.kgA.Size = new System.Drawing.Size(50, 32);
            this.kgA.TabIndex = 16;
            this.kgA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // t
            // 
            this.t.Location = new System.Drawing.Point(95, 2);
            this.t.Multiline = true;
            this.t.Name = "t";
            this.t.ReadOnly = true;
            this.t.Size = new System.Drawing.Size(45, 32);
            this.t.TabIndex = 15;
            this.t.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // l
            // 
            this.l.Location = new System.Drawing.Point(3, 2);
            this.l.Multiline = true;
            this.l.Name = "l";
            this.l.ReadOnly = true;
            this.l.Size = new System.Drawing.Size(45, 32);
            this.l.TabIndex = 13;
            this.l.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // p
            // 
            this.p.Location = new System.Drawing.Point(49, 2);
            this.p.Multiline = true;
            this.p.Name = "p";
            this.p.ReadOnly = true;
            this.p.Size = new System.Drawing.Size(45, 32);
            this.p.TabIndex = 14;
            this.p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // StokAkhirPeriksa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.qtyA);
            this.Controls.Add(this.kgA);
            this.Controls.Add(this.t);
            this.Controls.Add(this.l);
            this.Controls.Add(this.p);
            this.Name = "StokAkhirPeriksa";
            this.Size = new System.Drawing.Size(232, 36);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox qtyA;
        public System.Windows.Forms.TextBox kgA;
        public System.Windows.Forms.TextBox t;
        public System.Windows.Forms.TextBox l;
        public System.Windows.Forms.TextBox p;
    }
}
