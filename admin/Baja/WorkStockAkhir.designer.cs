﻿namespace Baja
{
    partial class WorkStockAkhir
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kgA = new System.Windows.Forms.TextBox();
            this.t = new System.Windows.Forms.TextBox();
            this.l = new System.Windows.Forms.TextBox();
            this.p = new System.Windows.Forms.TextBox();
            this.cbSalahPotong = new System.Windows.Forms.CheckBox();
            this.qtyA = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // kgA
            // 
            this.kgA.Location = new System.Drawing.Point(185, 1);
            this.kgA.Multiline = true;
            this.kgA.Name = "kgA";
            this.kgA.Size = new System.Drawing.Size(52, 32);
            this.kgA.TabIndex = 11;
            this.kgA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // t
            // 
            this.t.Location = new System.Drawing.Point(93, 1);
            this.t.Multiline = true;
            this.t.Name = "t";
            this.t.Size = new System.Drawing.Size(45, 32);
            this.t.TabIndex = 9;
            this.t.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // l
            // 
            this.l.Location = new System.Drawing.Point(-1, 1);
            this.l.Multiline = true;
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(45, 32);
            this.l.TabIndex = 7;
            this.l.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // p
            // 
            this.p.Location = new System.Drawing.Point(46, 1);
            this.p.Multiline = true;
            this.p.Name = "p";
            this.p.Size = new System.Drawing.Size(45, 32);
            this.p.TabIndex = 8;
            this.p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbSalahPotong
            // 
            this.cbSalahPotong.Location = new System.Drawing.Point(243, 3);
            this.cbSalahPotong.Name = "cbSalahPotong";
            this.cbSalahPotong.Size = new System.Drawing.Size(87, 27);
            this.cbSalahPotong.TabIndex = 12;
            this.cbSalahPotong.Text = "salah potong";
            this.cbSalahPotong.UseVisualStyleBackColor = true;
            // 
            // qtyA
            // 
            this.qtyA.Location = new System.Drawing.Point(139, 1);
            this.qtyA.Multiline = true;
            this.qtyA.Name = "qtyA";
            this.qtyA.Size = new System.Drawing.Size(45, 32);
            this.qtyA.TabIndex = 13;
            this.qtyA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // WorkStockAkhir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.qtyA);
            this.Controls.Add(this.cbSalahPotong);
            this.Controls.Add(this.kgA);
            this.Controls.Add(this.t);
            this.Controls.Add(this.l);
            this.Controls.Add(this.p);
            this.Name = "WorkStockAkhir";
            this.Size = new System.Drawing.Size(346, 32);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox kgA;
        public System.Windows.Forms.TextBox t;
        public System.Windows.Forms.TextBox l;
        public System.Windows.Forms.TextBox p;
        public System.Windows.Forms.CheckBox cbSalahPotong;
        public System.Windows.Forms.TextBox qtyA;
    }
}
