﻿namespace Baja
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbox = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.timernotif = new System.Windows.Forms.Timer(this.components);
            this.btnAddUser = new System.Windows.Forms.Button();
            this.btnAddSuplier = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.tbnamalogin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddStock = new System.Windows.Forms.Button();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.btnCetak = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnViewSJ = new System.Windows.Forms.Button();
            this.btnReload = new System.Windows.Forms.Button();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnViewSPB = new System.Windows.Forms.Button();
            this.btnBuatSPB = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbox);
            this.groupBox1.Location = new System.Drawing.Point(24, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1037, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Notifikasi";
            // 
            // lbox
            // 
            this.lbox.AutoScroll = true;
            this.lbox.Location = new System.Drawing.Point(6, 19);
            this.lbox.Name = "lbox";
            this.lbox.Size = new System.Drawing.Size(1025, 75);
            this.lbox.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv);
            this.groupBox2.Location = new System.Drawing.Point(25, 157);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1313, 503);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Report";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(3, 16);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(1307, 484);
            this.dgv.TabIndex = 1;
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            this.dgv.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_CellFormatting);
            this.dgv.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgv_Scroll);
            // 
            // timernotif
            // 
            this.timernotif.Interval = 1000;
            this.timernotif.Tick += new System.EventHandler(this.timernotif_Tick);
            // 
            // btnAddUser
            // 
            this.btnAddUser.Location = new System.Drawing.Point(617, 16);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(108, 35);
            this.btnAddUser.TabIndex = 2;
            this.btnAddUser.Text = "Add user";
            this.btnAddUser.UseVisualStyleBackColor = true;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // btnAddSuplier
            // 
            this.btnAddSuplier.Location = new System.Drawing.Point(372, 15);
            this.btnAddSuplier.Name = "btnAddSuplier";
            this.btnAddSuplier.Size = new System.Drawing.Size(103, 36);
            this.btnAddSuplier.TabIndex = 1;
            this.btnAddSuplier.Text = "Add Supplier";
            this.btnAddSuplier.UseVisualStyleBackColor = true;
            this.btnAddSuplier.Click += new System.EventHandler(this.btnAddSuplier_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.tbnamalogin);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1079, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(259, 137);
            this.panel1.TabIndex = 4;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(148, 70);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Logout";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tbnamalogin
            // 
            this.tbnamalogin.Location = new System.Drawing.Point(85, 10);
            this.tbnamalogin.Name = "tbnamalogin";
            this.tbnamalogin.ReadOnly = true;
            this.tbnamalogin.Size = new System.Drawing.Size(152, 20);
            this.tbnamalogin.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Admin Login";
            // 
            // btnAddStock
            // 
            this.btnAddStock.Location = new System.Drawing.Point(915, 17);
            this.btnAddStock.Name = "btnAddStock";
            this.btnAddStock.Size = new System.Drawing.Size(146, 34);
            this.btnAddStock.TabIndex = 4;
            this.btnAddStock.Text = "Add Stock";
            this.btnAddStock.UseVisualStyleBackColor = true;
            this.btnAddStock.Click += new System.EventHandler(this.btnAddStock_Click);
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Location = new System.Drawing.Point(731, 17);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(146, 34);
            this.btnAddCustomer.TabIndex = 3;
            this.btnAddCustomer.Text = "Add Customer";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // btnCetak
            // 
            this.btnCetak.Enabled = false;
            this.btnCetak.Location = new System.Drawing.Point(761, 672);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(151, 50);
            this.btnCetak.TabIndex = 7;
            this.btnCetak.Text = "BUAT SURAT JALAN";
            this.btnCetak.UseVisualStyleBackColor = true;
            this.btnCetak.Click += new System.EventHandler(this.btnCetak_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(226, 672);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 50);
            this.button1.TabIndex = 8;
            this.button1.Text = "REPORT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnViewSJ
            // 
            this.btnViewSJ.Location = new System.Drawing.Point(590, 672);
            this.btnViewSJ.Name = "btnViewSJ";
            this.btnViewSJ.Size = new System.Drawing.Size(165, 50);
            this.btnViewSJ.TabIndex = 9;
            this.btnViewSJ.Text = "VIEW SJ";
            this.btnViewSJ.UseVisualStyleBackColor = true;
            this.btnViewSJ.Click += new System.EventHandler(this.btnViewSJ_Click);
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(30, 13);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(330, 38);
            this.btnReload.TabIndex = 10;
            this.btnReload.Text = "RELOAD";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.onDoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.onRunWorkerCompleted);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(28, 672);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(192, 50);
            this.button2.TabIndex = 11;
            this.button2.Text = "VIEW SPK FINISH";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(408, 672);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(175, 50);
            this.button3.TabIndex = 12;
            this.button3.Text = "TANDA TERIMA";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(494, 14);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(103, 36);
            this.button5.TabIndex = 13;
            this.button5.Text = "Company Profile";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnViewSPB
            // 
            this.btnViewSPB.Location = new System.Drawing.Point(918, 672);
            this.btnViewSPB.Name = "btnViewSPB";
            this.btnViewSPB.Size = new System.Drawing.Size(134, 50);
            this.btnViewSPB.TabIndex = 14;
            this.btnViewSPB.Text = "VIEW SPB";
            this.btnViewSPB.UseVisualStyleBackColor = true;
            this.btnViewSPB.Click += new System.EventHandler(this.btnViewSPB_Click);
            // 
            // btnBuatSPB
            // 
            this.btnBuatSPB.Enabled = false;
            this.btnBuatSPB.Location = new System.Drawing.Point(1058, 672);
            this.btnBuatSPB.Name = "btnBuatSPB";
            this.btnBuatSPB.Size = new System.Drawing.Size(198, 50);
            this.btnBuatSPB.TabIndex = 15;
            this.btnBuatSPB.Text = "BUAT SURAT PENGIRIMAN BARANG";
            this.btnBuatSPB.UseVisualStyleBackColor = true;
            this.btnBuatSPB.Click += new System.EventHandler(this.btnBuatSPB_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Baja.Properties.Resources.sbs1;
            this.pictureBox1.Location = new System.Drawing.Point(17, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 94);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 730);
            this.Controls.Add(this.btnBuatSPB);
            this.Controls.Add(this.btnViewSPB);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.btnViewSJ);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCetak);
            this.Controls.Add(this.btnAddCustomer);
            this.Controls.Add(this.btnAddStock);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAddSuplier);
            this.Controls.Add(this.btnAddUser);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form Admin";
            this.Load += new System.EventHandler(this.FormAdmin_Load_1);
            this.Shown += new System.EventHandler(this.FormAdmin_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Timer timernotif;
        private System.Windows.Forms.Button btnAddUser;
        private System.Windows.Forms.Button btnAddSuplier;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox tbnamalogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddStock;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.FlowLayoutPanel lbox;
        public System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnCetak;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnViewSJ;
        private System.Windows.Forms.Button btnReload;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnViewSPB;
        private System.Windows.Forms.Button btnBuatSPB;
    }
}