﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.XtraReports.UI;

namespace Baja
{
    public partial class TandaTerima : Form
    {
        koneksi konek;

        public TandaTerima()
        {
            InitializeComponent();
            konek = new koneksi();

            InitializeCustomer();
        }

        public void InitializeCustomer()
        {
            konek.selectAllCustomer(this.comboBox1);
            konek.viewDokumen(this.dataGridView1);
            if (this.comboBox1.Items.Count > 0)
            {
                this.button1.Enabled = true;
            }
            else
            {
                this.button1.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.comboBox1.SelectedIndex = 0;
            this.textBox1.Clear();
            this.textBox2.Clear();
            this.textBox3.Clear();
            this.label9.Text = "";

            this.comboBox1.Enabled = true;
            this.textBox1.Enabled = true;
            this.textBox2.Enabled = true;
            this.textBox3.Enabled = true;
            this.button1.Enabled = true;
            this.button3.Enabled = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id_cuz = this.comboBox1.SelectedValue.ToString();
            string ket = this.textBox1.Text;
            string doc_no = this.textBox2.Text;
            string total = this.textBox3.Text;

            if (!id_cuz.Equals("") && !ket.Equals("") && !doc_no.Equals("") && !total.Equals(""))
            {
                long success = konek.addDokumen(ket, int.Parse(id_cuz), doc_no, int.Parse(total));
                if (success > 0)
                {
                    DialogResult dialogResult = MessageBox.Show("Data berhasil disimpan. Lanjutkan mencetak laporan?", "Informasi", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string id_dokumen = success.ToString();
                        TTDokumen report = new TTDokumen();
                        report.Parameters["id_dokumen"].Value = id_dokumen;
                        report.Parameters["ttl_dokumen"].Value = "Surabaya, " + DateTime.Today.Day + "/" + DateTime.Today.Month + "/" + DateTime.Today.Year;
                        ReportPrintTool print = new ReportPrintTool(report);
                        report.ShowPreviewDialog();
                    }
                    konek.viewDokumen(this.dataGridView1);
                    this.comboBox1.SelectedIndex = 0;
                    this.textBox1.Clear();
                    this.textBox2.Clear();
                    this.textBox3.Clear();
                    this.label9.Text = "";
                }
                else
                {
                    MessageBox.Show("Data gagal disimpan.");
                }
            }
            else
            {
                MessageBox.Show("Mohon lengkapi data.");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedValue.ToString() != "")
            {
                this.button1.Enabled = true;
            }
            else
            {
                this.button1.Enabled = false;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "\\d+"))
                e.Handled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string id_dokumen = this.label9.Text;
            TTDokumen report = new TTDokumen();
            report.Parameters["id_dokumen"].Value = id_dokumen;
            report.Parameters["ttl_dokumen"].Value = "Surabaya, " + DateTime.Today.Day + "/" + DateTime.Today.Month + "/" + DateTime.Today.Year;
            ReportPrintTool print = new ReportPrintTool(report);
            report.ShowPreviewDialog();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                string id_dokumen = this.dataGridView1.Rows[index].Cells[0].Value.ToString();
                string nama_customer = this.dataGridView1.Rows[index].Cells[1].Value.ToString();
                string doc_no = this.dataGridView1.Rows[index].Cells[2].Value.ToString();
                string keterangan = this.dataGridView1.Rows[index].Cells[3].Value.ToString();
                string total = this.dataGridView1.Rows[index].Cells[4].Value.ToString();

                this.label9.Text = id_dokumen;
                this.comboBox1.SelectedText = nama_customer;
                this.textBox1.Text = keterangan;
                this.textBox2.Text = doc_no;
                this.textBox3.Text = total;

                this.comboBox1.Enabled = false;
                this.textBox1.Enabled = false;
                this.textBox2.Enabled = false;
                this.textBox3.Enabled = false;
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }
    }
}
