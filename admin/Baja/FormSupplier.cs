﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormSupplier : Form
    {
        koneksi konek;
        ViewSupplier vs;
        public string namaAdmin;

        public FormSupplier()
        {
            InitializeComponent();
            konek = new koneksi();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            int id_user = konek.getidAdmin(namaAdmin);
            konek.addSupplier(tbNama.Text,id_user, tbAlamat.Text, tbTelp.Text, tbCP.Text);
            reset();
        }

        public void reset()
        {
            tbNama.Text = "";
            tbAlamat.Text = "";
            tbTelp.Text = "";
            tbCP.Text = "";

        }

        private void btnViewSupplier_Click(object sender, EventArgs e)
        {
            if (!IsDisposed)
            {
                vs = new ViewSupplier();
                vs.FormClosed += Vs_FormClosed;
            }
            konek.viewSupplier(vs.dgvSupplier);
            vs.Show();
        }

        private void Vs_FormClosed(object sender, FormClosedEventArgs e)
        {
            konek.closekoneksi();
        }
    }
}
