﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormLoading : Form
    {
        public FormLoading()
        {
            InitializeComponent();

            //backgroundWorker.RunWorkerAsync();            

        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            pbLoading.Image = Properties.Resources.LoadingImage;
            pbLoading.Visible = true;
        }
    }
}
