﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baja
{
    public partial class WorkStockAkhir : UserControl
    {
        public string id_dt_spk { get; set; }
        public string id_mesin_operation { get; set; }
        public string id_type { get; set; }
        public string id_dt_pembelian { get; set; }

        private koneksi konek;

        public WorkStockAkhir()
        {
            InitializeComponent();
            konek = new koneksi();
   
        }

        private void qty_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numQty = (NumericUpDown)sender;
            double pVal, lVal, tVal, kgVal = 0;
            double.TryParse(p.Text, out pVal);
            double.TryParse(l.Text, out lVal);
            double.TryParse(t.Text, out tVal);

            kgVal = countKg(pVal, lVal, tVal, numQty.Value);
            kgA.Text = kgVal.ToString();
        }

        private double countKg(double p, double l, double t, decimal qtyVal)
        {
            int qty = (int)qtyVal;
            if (t > 0)
            {
                return (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;
            }
            else
            {
                return (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
            }
        }
        
    }
}
