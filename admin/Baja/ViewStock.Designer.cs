﻿namespace Baja
{
    partial class ViewStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvStock = new System.Windows.Forms.DataGridView();
            this.tbType = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbT = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbRound = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvStock
            // 
            this.dgvStock.AllowUserToOrderColumns = true;
            this.dgvStock.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStock.Location = new System.Drawing.Point(18, 71);
            this.dgvStock.Name = "dgvStock";
            this.dgvStock.Size = new System.Drawing.Size(887, 369);
            this.dgvStock.TabIndex = 7;
            this.dgvStock.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStock_RowEnter);
            this.dgvStock.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStock_RowLeave);
            this.dgvStock.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStock_RowValidated);
            // 
            // tbType
            // 
            this.tbType.Location = new System.Drawing.Point(59, 40);
            this.tbType.Name = "tbType";
            this.tbType.Size = new System.Drawing.Size(127, 20);
            this.tbType.TabIndex = 1;
            this.tbType.TextChanged += new System.EventHandler(this.tbType_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "TYPE";
            // 
            // tbL
            // 
            this.tbL.Location = new System.Drawing.Point(239, 40);
            this.tbL.Name = "tbL";
            this.tbL.Size = new System.Drawing.Size(48, 20);
            this.tbL.TabIndex = 2;
            this.tbL.TextChanged += new System.EventHandler(this.tbType_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(202, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "L >= ";
            // 
            // tbP
            // 
            this.tbP.Location = new System.Drawing.Point(332, 40);
            this.tbP.Name = "tbP";
            this.tbP.Size = new System.Drawing.Size(48, 20);
            this.tbP.TabIndex = 3;
            this.tbP.TextChanged += new System.EventHandler(this.tbType_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "P >= ";
            // 
            // tbT
            // 
            this.tbT.Location = new System.Drawing.Point(424, 40);
            this.tbT.Name = "tbT";
            this.tbT.Size = new System.Drawing.Size(48, 20);
            this.tbT.TabIndex = 4;
            this.tbT.TextChanged += new System.EventHandler(this.tbType_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "T >= ";
            // 
            // tbQty
            // 
            this.tbQty.Location = new System.Drawing.Point(544, 40);
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(48, 20);
            this.tbQty.TabIndex = 5;
            this.tbQty.TextChanged += new System.EventHandler(this.tbType_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(491, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "QTY >= ";
            // 
            // cbRound
            // 
            this.cbRound.AutoSize = true;
            this.cbRound.Location = new System.Drawing.Point(21, 13);
            this.cbRound.Name = "cbRound";
            this.cbRound.Size = new System.Drawing.Size(82, 17);
            this.cbRound.TabIndex = 18;
            this.cbRound.Text = "Round Only";
            this.cbRound.UseVisualStyleBackColor = true;
            this.cbRound.CheckedChanged += new System.EventHandler(this.cbRound_CheckedChanged);
            // 
            // ViewStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 452);
            this.Controls.Add(this.cbRound);
            this.Controls.Add(this.tbQty);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbT);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbType);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvStock);
            this.Name = "ViewStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ViewStock";
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dgvStock;
        private System.Windows.Forms.TextBox tbType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbRound;
    }
}