﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormSPKFinish : Form
    {
        FormLoading _formLoading;

        koneksi konek;

        String selectedCetakNoSPK = "";
        String selected_IdSPK = "";
        String selected_IdDtSPK = "";
        String selected_IdPO = "";

        DataTable dTable;
        DataTable dt = null;
        int pageIndex, pageSize = 100;
        bool inProcessed = false;

        public FormSPKFinish()
        {
            InitializeComponent();
            konek = new koneksi();
        }

        private void InitializedData()
        {
            _formLoading = new FormLoading();
            _formLoading.Show();

            if (dt == null)
            {
                pageIndex = 0;
                dt = new DataTable();
            }

            dTable = konek.dataReportDtSPK(true);
            backgroundWorker.RunWorkerAsync();
        }

        public void loadTable()
        {
            inProcessed = true;

            if (dt == null)
            {
                pageIndex = 0;

                dt = new DataTable();

            }

            DataTable temp = dTable.AsEnumerable().Select(x => x).Skip((pageIndex * pageSize)).Take(pageSize).CopyToDataTable();
            if (temp.Rows.Count > 0)
            {
                dt.Merge(temp);

                if (dt != null)
                {
                    DataTable resultTable = new DataTable();
                    DataColumn numberColumn = new DataColumn();
                    numberColumn.ColumnName = "No.";
                    numberColumn.DataType = typeof(int);
                    numberColumn.AutoIncrement = true;
                    numberColumn.AutoIncrementSeed = 1;
                    numberColumn.AutoIncrementStep = 1;
                    resultTable.Columns.Add(numberColumn);
                    resultTable.Merge(dt);

                    setDataSource(resultTable);
                }
            }
            else
                MessageBox.Show("Data is null");
        }

        public int GetDisplayedRowsCount()
        {
            int count = dgv.Rows[dgv.FirstDisplayedScrollingRowIndex].Height;
            count = dgv.Height / count;
            return count;
        }

        internal delegate void SetDataSourceDelegate(DataTable table);
        private void setDataSource(DataTable table)
        {
            // Invoke method if required:
            if (this.InvokeRequired)
            {
                this.Invoke(new SetDataSourceDelegate(setDataSource), table);
            }
            else
            {
                dgv.DataSource = table;
                dgv.ClearSelection();

                inProcessed = false;
                if (_formLoading != null)
                    _formLoading.Dispose();
            }
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow Myrow in dgv.Rows)
            {
                if (Myrow.Cells.Count > 5)
                {
                    Object status = Myrow.Cells["status"].Value;
                    if (status != null)
                    {
                        string strStatus = status.ToString().ToLower();
                        if (strStatus.Equals("antrian"))
                        {
                            Myrow.Cells["status"].Style.BackColor = Color.Red;
                        }
                        else if (strStatus.Equals("progress"))
                        {
                            Myrow.Cells["status"].Style.BackColor = Color.Green;
                        }
                        else if (strStatus.Equals("finish"))
                        {
                            Myrow.Cells["status"].Style.BackColor = Color.Yellow;
                        }
                    }
                }
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgv.Rows[rowIndex];
            if (row.Cells[0].Value == null)
                return;
            String statusSelected = row.Cells["status"].Value.ToString();
            String selectedIdSPK = row.Cells["id_spk"].Value.ToString();
            String selectedIdDtSPK = row.Cells["dt spk"].Value.ToString();
            String selectedIdPO = row.Cells["id_po"].Value.ToString();

            FormPeriksaSPK _formPeriksa = new FormPeriksaSPK(selectedIdSPK, selectedIdDtSPK, selectedIdPO);
            _formPeriksa.Show();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            loadTable();
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_formLoading != null)
                _formLoading.Dispose();

            if (pageIndex > 0)
            {
                int display = dgv.Rows.Count - pageSize;
                dgv.FirstDisplayedScrollingRowIndex = display;
            }
        }

        private void FormSPKFinish_Shown(object sender, EventArgs e)
        {
            InitializedData();
        }

        private void dgv_Scroll(object sender, ScrollEventArgs e)
        {
            int display = dgv.Rows.Count - dgv.DisplayedColumnCount(false);
            if (e.Type == ScrollEventType.SmallIncrement || e.Type == ScrollEventType.LargeIncrement)
            {
                if (e.NewValue >= dgv.Rows.Count - GetDisplayedRowsCount() && (e.NewValue + GetDisplayedRowsCount()) < dTable.Rows.Count && !inProcessed)
                {
                    MessageBox.Show((e.NewValue + GetDisplayedRowsCount()).ToString() +"-"+ dTable.Rows.Count);
                    if (backgroundWorker != null && backgroundWorker.IsBusy)
                        backgroundWorker.CancelAsync();
                    else if (backgroundWorker != null && !backgroundWorker.CancellationPending)
                    {
                        pageIndex++;
                        _formLoading = new FormLoading();
                        _formLoading.Show();

                        backgroundWorker.RunWorkerAsync();
                    }
                }
            }
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgv.Rows[rowIndex];
            if (row.Cells[0].Value == null)
                return;
            String statusSelected = row.Cells["status"].Value.ToString();
            
            selectedCetakNoSPK = row.Cells["id_spk"].Value.ToString();
            selected_IdSPK = row.Cells["id_spk"].Value.ToString();
            selected_IdDtSPK = row.Cells["dt spk"].Value.ToString();
            selected_IdPO = row.Cells["id_po"].Value.ToString();
        }
    }
}
