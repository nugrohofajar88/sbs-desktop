﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class ViewSPB : Form
    {
        public bool isFirstTime = true;
        public string id_spb="";

        public ViewSPB()
        {
            InitializeComponent();
        }

        private void dgvSJ_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;
            for (int i = 0; i < dgvSJ.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvSJ[i, e.RowIndex].Style.BackColor = Color.Yellow;
            }
        }

        private void dgvSJ_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;

            for (int i = 0; i < dgvSJ.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvSJ[i, e.RowIndex].Style.BackColor = Color.Empty;
            }

        }

        private void btnPPN_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(id_spb))
                return;
            SPBReportPPN report = new SPBReportPPN();

            report.Parameters["parameterIdSPB"].Value = id_spb;
            report.Parameters["parameterIdSPB"].Visible = false;

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        private void btnNonPPN_Click(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(id_spb))
            //    return;
            //SJReportNonPPN report = new SJReportNonPPN();

            //report.Parameters["parameterIdSJ"].Value = id_spb;
            //report.Parameters["parameterIdSJ"].Visible = false;

            //ReportPrintTool printTool = new ReportPrintTool(report);
            //printTool.ShowPreviewDialog();
        }

        private void dgvSJ_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgvSJ.Rows[rowIndex];
            id_spb = row.Cells["id_spb"].Value.ToString();
        }
    }
}
