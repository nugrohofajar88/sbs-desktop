﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Baja
{
    public partial class FormStock : Form
    {
        ViewStock _viewStock;
        koneksi konek;
        public FormStock()
        {
            InitializeComponent();
            konek = new koneksi();
        }

        private void FormStock_Load(object sender, EventArgs e)
        {
            konek.selectNamaBarang(cbTypeBesi);
            
        }
        

        private void btnNewType_Click(object sender, EventArgs e)
        {
            if (cbTypeBesi.Visible == false)
            {
                tbTpbesi.Visible = false;
                cbTypeBesi.Visible = true;
            }
            else
            {
                tbTpbesi.Text = "";
                tbTpbesi.Visible = true;
                cbTypeBesi.Visible = false;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            double kg;
            if (!string.IsNullOrEmpty(tbTinggi.Text))
            {
                kg = hitungKg(double.Parse(tbPjg.Text),double.Parse(tbLbr.Text),double.Parse(tbTinggi.Text),true);
            }
            else
                kg = hitungKg(double.Parse(tbPjg.Text), double.Parse(tbLbr.Text), 0, false);



            if (string.IsNullOrEmpty(tbTpbesi.Text) || tbTpbesi.Visible == false)//pilih CBtipe
            {
               if(string.IsNullOrEmpty(tbHarga.Text))
                konek.addStock( cbTypeBesi.SelectedItem.ToString(), tbPjg.Text, tbLbr.Text, tbTinggi.Text, /* cbBntuk.SelectedItem.ToString(), */ tbqty.Text,kg.ToString(), true);
               else
                    konek.addStock(cbTypeBesi.SelectedItem.ToString(), tbPjg.Text, tbLbr.Text, tbTinggi.Text, /* cbBntuk.SelectedItem.ToString(), */ tbqty.Text, kg.ToString(),tbHarga.Text, true);

            }
            else
            {
                if (string.IsNullOrEmpty(tbHarga.Text))
                    konek.addStock(tbTpbesi.Text, tbPjg.Text, tbLbr.Text, tbTinggi.Text, /* cbBntuk.SelectedItem.ToString(), */ tbqty.Text,kg.ToString(), false);
                else
                    konek.addStock(tbTpbesi.Text, tbPjg.Text, tbLbr.Text, tbTinggi.Text, /* cbBntuk.SelectedItem.ToString(), */ tbqty.Text, kg.ToString(), tbHarga.Text, true);

            }

            reset();
        }

        public double hitungKg(double p,double l,double t, bool flat)
        {
            double kg = 00;
            if (flat)
            {
                kg = ((p / 100) * (l / 100) * (t / 100)) * 8;
                return kg;
            }
            else
            {
                kg = ((p / 100) * (l / 100)) * (6.2);
                return kg;
            }
           
        }

       

        public void reset()
        {
            //cbNamaBrg.SelectedIndex = -1;
            cbTypeBesi.SelectedIndex = -1;           
            tbTpbesi.Text = "";
            tbPjg.Text = "";
            tbLbr.Text = "";
            tbTinggi.Text = "";
            cbTypeBesi.SelectedIndex = -1;
            tbqty.Text = "";
            tbHarga.Text = "";

        }

        private void btnView_Click(object sender, EventArgs e)
        {
            _viewStock = new ViewStock();
            konek.viewStock(_viewStock.dgvStock);
            _viewStock.Show();
            _viewStock.isFirstTime = false;
        }
    }
}
