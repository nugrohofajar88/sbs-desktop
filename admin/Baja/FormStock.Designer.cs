﻿namespace Baja
{
    partial class FormStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbTypeBesi = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbPjg = new System.Windows.Forms.TextBox();
            this.tbLbr = new System.Windows.Forms.TextBox();
            this.tbTinggi = new System.Windows.Forms.TextBox();
            this.tbqty = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnNewType = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tbTpbesi = new System.Windows.Forms.TextBox();
            this.tbHarga = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type Besi";
            // 
            // cbTypeBesi
            // 
            this.cbTypeBesi.FormattingEnabled = true;
            this.cbTypeBesi.Location = new System.Drawing.Point(109, 35);
            this.cbTypeBesi.Name = "cbTypeBesi";
            this.cbTypeBesi.Size = new System.Drawing.Size(121, 21);
            this.cbTypeBesi.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Panjang";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Lebar/Diameter";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tinggi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "QTY";
            // 
            // tbPjg
            // 
            this.tbPjg.Location = new System.Drawing.Point(109, 102);
            this.tbPjg.Name = "tbPjg";
            this.tbPjg.Size = new System.Drawing.Size(121, 20);
            this.tbPjg.TabIndex = 5;
            // 
            // tbLbr
            // 
            this.tbLbr.Location = new System.Drawing.Point(109, 67);
            this.tbLbr.Name = "tbLbr";
            this.tbLbr.Size = new System.Drawing.Size(121, 20);
            this.tbLbr.TabIndex = 4;
            // 
            // tbTinggi
            // 
            this.tbTinggi.Location = new System.Drawing.Point(109, 140);
            this.tbTinggi.Name = "tbTinggi";
            this.tbTinggi.Size = new System.Drawing.Size(121, 20);
            this.tbTinggi.TabIndex = 6;
            // 
            // tbqty
            // 
            this.tbqty.Location = new System.Drawing.Point(109, 171);
            this.tbqty.Name = "tbqty";
            this.tbqty.Size = new System.Drawing.Size(121, 20);
            this.tbqty.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(554, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 85);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(554, 135);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(108, 85);
            this.btnView.TabIndex = 10;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnNewType
            // 
            this.btnNewType.Location = new System.Drawing.Point(260, 31);
            this.btnNewType.Name = "btnNewType";
            this.btnNewType.Size = new System.Drawing.Size(42, 31);
            this.btnNewType.TabIndex = 2;
            this.btnNewType.Text = "new";
            this.btnNewType.UseVisualStyleBackColor = true;
            this.btnNewType.Click += new System.EventHandler(this.btnNewType_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(236, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Or";
            // 
            // tbTpbesi
            // 
            this.tbTpbesi.Location = new System.Drawing.Point(310, 37);
            this.tbTpbesi.Name = "tbTpbesi";
            this.tbTpbesi.Size = new System.Drawing.Size(121, 20);
            this.tbTpbesi.TabIndex = 3;
            this.tbTpbesi.Visible = false;
            // 
            // tbHarga
            // 
            this.tbHarga.Location = new System.Drawing.Point(109, 201);
            this.tbHarga.Name = "tbHarga";
            this.tbHarga.Size = new System.Drawing.Size(121, 20);
            this.tbHarga.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "HARGA/KG";
            // 
            // FormStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 260);
            this.Controls.Add(this.tbHarga);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnNewType);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbTpbesi);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbqty);
            this.Controls.Add(this.tbTinggi);
            this.Controls.Add(this.tbLbr);
            this.Controls.Add(this.tbPjg);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbTypeBesi);
            this.Controls.Add(this.label1);
            this.Name = "FormStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form Stock";
            this.Load += new System.EventHandler(this.FormStock_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTypeBesi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbPjg;
        private System.Windows.Forms.TextBox tbLbr;
        private System.Windows.Forms.TextBox tbTinggi;
        private System.Windows.Forms.TextBox tbqty;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnNewType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbTpbesi;
        private System.Windows.Forms.TextBox tbHarga;
        private System.Windows.Forms.Label label5;
    }
}