﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SalesBaja
{
    public static class DataUtils
    {
        public static int FindIndex(DataTable table, int Keyval)
        {
            int hasil = -1;
            int i = 0;
            int tblcount = table.Rows.Count;
            bool ketemu = false;
            int tblId;
            string sTblId;

            while ((tblcount > 0) && (i < tblcount) && (ketemu == false))
            {
                // kolom id_dtpenawaran indexnya = 0
                sTblId = table.Rows[i][0].ToString();
                tblId = Convert.ToInt32(sTblId == "" ? "0" : sTblId);
                if (tblId == Keyval)
                {
                    ketemu = true;
                    hasil = i;
                }
                i++;
            }
            return hasil;
        }

        public static void UpdateTable(ref DataTable tabel, int RowIdx, int ColIdx, object val)
        {
            tabel.Rows[RowIdx][ColIdx] = val;
        }

        public static void InsertTable(ref DataTable tabel, int ColIdx, object val)
        {
            DataRow RowBaru = tabel.NewRow();
            RowBaru[ColIdx] = val;
            tabel.Rows.Add(RowBaru);
        }

        public static int DateTimeToInt(DateTime theDate)
        {
            //int Totaldays = (int)(theDate.Date - new DateTime(1900, 1, 1)).TotalDays + 2;
            var timeDiff = theDate - new DateTime(1970, 1, 1);
            var totalTime = timeDiff.TotalMilliseconds;
            return (int)totalTime;
        }

        public static int GenRowID()
        {
            DateTime skr = DateTime.Now;
            int year = skr.Year;
            int bln = skr.Month;
            int tgl = skr.Day;
            int jam = skr.Hour;
            int mnt = skr.Minute;
            int dtk = skr.Second;
            int IdR = year + bln + tgl + jam + mnt + dtk;
            return IdR;
        }

        public static string NamaBulan(int bulan)
        {
            string hasil = "";
            switch (bulan)
            {
                case 1:
                    {
                        hasil = "Januari";
                        break;
                    }
                case 2:
                    {
                        hasil = "Februari";
                        break;
                    }
                case 3:
                    {
                        hasil = "Maret";
                        break;
                    }
                case 4:
                    {
                        hasil = "April";
                        break;
                    }
                case 5:
                    {
                        hasil = "Mei";
                        break;
                    }
                case 6:
                    {
                        hasil = "Juni";
                        break;
                    }
                case 7:
                    {
                        hasil = "Juli";
                        break;
                    }
                case 8:
                    {
                        hasil = "Agustus";
                        break;
                    }
                case 9:
                    {
                        hasil = "September";
                        break;
                    }
                case 10:
                    {
                        hasil = "Oktober";
                        break;
                    }
                case 11:
                    {
                        hasil = "November";
                        break;
                    }
                case 12:
                    {
                        hasil = "Desember";
                        break;
                    }
            }
            return hasil;
        }

        private static int BilanganBulan(string bulan)
        {
            int hasil = 0;
            switch (bulan)
            {
                case "Januari":
                    {
                        hasil = 1;
                        break;
                    }
                case "Februari":
                    {
                        hasil = 2;
                        break;
                    }
                case "Maret":
                    {
                        hasil = 3;
                        break;
                    }
                case "April":
                    {
                        hasil = 4;
                        break;
                    }
                case "Mei":
                    {
                        hasil = 5;
                        break;
                    }
                case "Juni":
                    {
                        hasil = 6;
                        break;
                    }
                case "Juli":
                    {
                        hasil = 7;
                        break;
                    }
                case "Agustus":
                    {
                        hasil = 8;
                        break;
                    }
                case "September":
                    {
                        hasil = 9;
                        break;
                    }
                case "Oktober":
                    {
                        hasil = 10;
                        break;
                    }
                case "November":
                    {
                        hasil = 11;
                        break;
                    }
                case "Desember":
                    {
                        hasil = 12;
                        break;
                    }
            }
            return hasil;
        }
    }
}
