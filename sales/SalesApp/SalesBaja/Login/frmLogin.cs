﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
//using ListCustomer;

namespace SalesBaja
{
    public partial class frm_login : Form
    {
        private static frm_login openForm = null;

        // No need for locking - you'll be doing all this on the UI thread...
        public static frm_login GetInstance()
        {
            if (openForm == null)
            {
                openForm = new frm_login();
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        private MySqlConnection uConn;
        private string _userconnected;
        private string _userfunction;
        private int _userid;

        public string Userconnected
        {
            get
            {
                return _userconnected;
            }

            set
            {
                _userconnected = value;
            }
        }

        public string Userfunction
        {
            get
            {
                return _userfunction;
            }

            set
            {
                _userfunction = value;
            }
        }

        public int Userid
        {
            get
            {
                return _userid;
            }

            set
            {
                _userid = value;
            }
        }

        public frm_login()
        {
            InitializeComponent();
        }

        private bool UserMatchPaswd(string username, string passwd)
        {
            bool hasil = true;

            uConn = new MySqlConnection();
            uConn.ConnectionString = "server=" + GlobVar.hAddress + "; uid=" + 
                GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                GlobVar.hdbase;

            StringBuilder sqlcmdusr = new StringBuilder();

            MySqlDataReader reader;

            sqlcmdusr.AppendLine("SELECT id_user, username, password");
            sqlcmdusr.AppendLine("    FROM m_user WHERE username =  @usrname");
            MySqlParameter prmusrnm = new MySqlParameter("usrname", username);
            MySqlCommand uComm = new MySqlCommand(sqlcmdusr.ToString(), uConn);
            
            uComm.Parameters.Add(prmusrnm);

            try
            {
                uConn.Open();

                string user;
                string passw;

                reader = uComm.ExecuteReader();

                if (reader.Read())
                {
                    user = reader.GetString(1);
                    if (user == username)
                    {
                        passw = reader.GetString(2);

                        if (passw == passwd)
                        {
                            // Jika benar maka masuk 
                            hasil = true;
                            _userconnected = user;
                            _userid = reader.GetInt32(0);
                        }
                        else
                        {
                            hasil = false;
                            throw new Exception("Username do not match with password!");
                        }
                    }
                    else
                    {
                        hasil = false;
                        throw new Exception("User not found");
                    }
                }

            }
            catch (Exception ex)
            {
                hasil = false;
                MessageBox.Show(ex.Message);
            }

            return hasil;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (UserMatchPaswd(tbUserName.Text, tbPasswd.Text)) {
                MessageBox.Show("Selamat " + _userconnected + " Anda berhasil masuk!");
                this.DialogResult = DialogResult.OK;
                fr_MainMenu Main = fr_MainMenu.GetInstance();
                Main.Userconnected = _userconnected;
                Main.Userid = _userid;
                Main.Show();
                GlobVar.UserActive = _userconnected;
                GlobVar.IdUserActive = _userid;
                this.Hide();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public void ClearUserPassword()
        {
            tbUserName.Clear();
            tbPasswd.Clear();
        }
    }
}
