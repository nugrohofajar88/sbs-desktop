﻿namespace SalesBaja.BonusCustomer
{
    partial class frmBonusCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbCp = new System.Windows.Forms.Label();
            this.lbAlamat = new System.Windows.Forms.Label();
            this.lbNamaCustomer = new System.Windows.Forms.Label();
            this.btnCari = new System.Windows.Forms.Button();
            this.tbCustID = new System.Windows.Forms.TextBox();
            this.tbBonus = new System.Windows.Forms.TextBox();
            this.chbpersen = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbCp);
            this.groupBox1.Controls.Add(this.lbAlamat);
            this.groupBox1.Controls.Add(this.lbNamaCustomer);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.tbCustID);
            this.groupBox1.Controls.Add(this.tbBonus);
            this.groupBox1.Controls.Add(this.chbpersen);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(748, 163);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Bonus";
            // 
            // lbCp
            // 
            this.lbCp.AutoSize = true;
            this.lbCp.Location = new System.Drawing.Point(218, 91);
            this.lbCp.Name = "lbCp";
            this.lbCp.Size = new System.Drawing.Size(119, 20);
            this.lbCp.TabIndex = 9;
            this.lbCp.Text = "Contact Person";
            // 
            // lbAlamat
            // 
            this.lbAlamat.AutoSize = true;
            this.lbAlamat.Location = new System.Drawing.Point(218, 65);
            this.lbAlamat.Name = "lbAlamat";
            this.lbAlamat.Size = new System.Drawing.Size(132, 20);
            this.lbAlamat.TabIndex = 8;
            this.lbAlamat.Text = "Alamat Customer";
            // 
            // lbNamaCustomer
            // 
            this.lbNamaCustomer.AutoSize = true;
            this.lbNamaCustomer.Location = new System.Drawing.Point(218, 39);
            this.lbNamaCustomer.Name = "lbNamaCustomer";
            this.lbNamaCustomer.Size = new System.Drawing.Size(124, 20);
            this.lbNamaCustomer.TabIndex = 7;
            this.lbNamaCustomer.Text = "Nama Customer";
            // 
            // btnCari
            // 
            this.btnCari.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCari.Location = new System.Drawing.Point(175, 37);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(38, 25);
            this.btnCari.TabIndex = 2;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = true;
            this.btnCari.Click += new System.EventHandler(this.btnCari_Click);
            // 
            // tbCustID
            // 
            this.tbCustID.Location = new System.Drawing.Point(104, 36);
            this.tbCustID.Name = "tbCustID";
            this.tbCustID.Size = new System.Drawing.Size(64, 26);
            this.tbCustID.TabIndex = 1;
            // 
            // tbBonus
            // 
            this.tbBonus.Location = new System.Drawing.Point(104, 114);
            this.tbBonus.Name = "tbBonus";
            this.tbBonus.Size = new System.Drawing.Size(64, 26);
            this.tbBonus.TabIndex = 3;
            // 
            // chbpersen
            // 
            this.chbpersen.AutoSize = true;
            this.chbpersen.Location = new System.Drawing.Point(175, 116);
            this.chbpersen.Name = "chbpersen";
            this.chbpersen.Size = new System.Drawing.Size(106, 24);
            this.chbpersen.TabIndex = 4;
            this.chbpersen.Text = "% (Persen)";
            this.chbpersen.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bonus";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSimpan);
            this.panel1.Location = new System.Drawing.Point(12, 181);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(748, 30);
            this.panel1.TabIndex = 1;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(670, 3);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 23);
            this.btnSimpan.TabIndex = 0;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // frmBonusCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 216);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmBonusCustomer";
            this.Text = "Bonus Customer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chbpersen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbCustID;
        private System.Windows.Forms.TextBox tbBonus;
        private System.Windows.Forms.Label lbCp;
        private System.Windows.Forms.Label lbAlamat;
        private System.Windows.Forms.Label lbNamaCustomer;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.Button btnSimpan;
    }
}