﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SalesBaja.BonusCustomer
{
    public partial class frmBonusCustomer : Form
    {
        private static frmBonusCustomer openForm = null;

        // No need for locking - you'll be doing all this on the UI thread...
        public static frmBonusCustomer GetInstance()
        {
            if (openForm == null)
            {
                openForm = new frmBonusCustomer();
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        private string dtConnString;
        private string _userconnected;
        private string _userfunction;
        private int _userid;

        public string Userconnected
        {
            get
            {
                return _userconnected;
            }

            set
            {
                _userconnected = value;
            }
        }

        public string Userfunction
        {
            get
            {
                return _userfunction;
            }

            set
            {
                _userfunction = value;
            }
        }

        public int Userid
        {
            get
            {
                return _userid;
            }

            set
            {
                _userid = value;
            }
        }

        public frmBonusCustomer()
        {
            InitializeComponent();
            InitConnString();
        }

        private void InitConnString()
        {
            dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase;
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            //Tampilkan windows daftar customer
            form_List_Cust fListCustomer = new form_List_Cust();
            fListCustomer.ShowDialog(this);
            tbCustID.Text = fListCustomer.SelectedID;
            lbNamaCustomer.Text = fListCustomer.SelectedNama;
            lbAlamat.Text = fListCustomer.SelectedAlamat;
            lbCp.Text = fListCustomer.SelectedCP;
            fListCustomer.Dispose();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            SaveBonus();
            KosongkanField();
            MessageBox.Show("Data telah tersimpan!");
        }

        private void SaveBonus()
        {
            MySqlConnection dConn = new MySqlConnection(dtConnString);
            MySqlCommand tcmd = new MySqlCommand();
            StringBuilder tSql = new StringBuilder();
            tSql.AppendLine("INSERT INTO m_bonuscustomer (id_customer, persen, bonus, ");
            tSql.AppendLine(" id_userinput, input_time)");
            tSql.AppendLine("VALUES (@idcust, @prsen, @bnus, @iduser, now())");
            tSql.AppendLine("ON DUPLICATE KEY UPDATE persen = @prsen, bonus = @bnus,");
            tSql.AppendLine(" id_userinput = @iduser, input_time = now();");

            MySqlParameter prmidcust = new MySqlParameter("idcust", MySqlDbType.Int32);
            MySqlParameter prmpersen = new MySqlParameter("prsen", MySqlDbType.Byte);
            MySqlParameter prmbonus = new MySqlParameter("bnus", MySqlDbType.Int32);
            MySqlParameter prmidusr = new MySqlParameter("iduser", MySqlDbType.Int32);

            try
            {
                dConn.Open();
                tcmd.CommandText = tSql.ToString();
                tcmd.Connection = dConn;
                prmidcust.Value = Convert.ToInt32(tbCustID.Text);
                prmidusr.Value = _userid;
                prmbonus.Value = Convert.ToInt32(tbBonus.Text);
                prmpersen.Value = chbpersen.Checked ? 1 : 0;

                tcmd.Parameters.Add(prmidcust);
                tcmd.Parameters.Add(prmidusr);
                tcmd.Parameters.Add(prmbonus);
                tcmd.Parameters.Add(prmpersen);

                tcmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }

            dConn.Close();
        }

        private void KosongkanField()
        {
            tbCustID.Clear();
            tbBonus.Clear();
            chbpersen.Checked = false;
            lbNamaCustomer.Text = "";
            lbAlamat.Text = "";
            lbCp.Text = "";
        }
    }
}
