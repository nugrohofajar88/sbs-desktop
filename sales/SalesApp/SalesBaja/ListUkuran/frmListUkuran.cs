﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
//using ListCustomer;
//using ListCustomer.NewUkuran;

namespace SalesBaja
{
    public partial class fr_ListUkuran : Form
    {
        private MySqlConnection conn;
        private string connString;
        private MySqlDataAdapter dtAdapter;
        private MySqlCommand sqlCommand;
        private DataTable tbl;

        private string panjang;
        private string lebar;
        private string tinggi;
        private string idUkuran;
        private string _faktorHitung;

        public string SelectedPanjang
        {
            get { return panjang; }
            set { }
        }

        public string SelectedLebar
        {
            get { return lebar; }
            set { }
        }

        public string SelectedTinggi
        {
            get { return tinggi; }
            set { }
        }

        public string SelectedUkuranId
        {
            get { return idUkuran; }
            set { }
        }

        public string SelFaktorHitung
        {
            get { return _faktorHitung; }
            set { }
        }

        private static string[] param = new string[]
        {
            "lebar",
            "panjang",
            "tinggi"
        };

        public fr_ListUkuran()
        {
            InitializeComponent();
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            string sqlcmd = "SELECT id_ukuran, nama_ukuran as nama, lebar, panjang, tinggi, " +
                "faktor_hitung FROM m_ukuran ";
            int seltd = 0;
            StringBuilder prmstrbld = new StringBuilder();

            #region Combobox checking 

            /// <summary>
            /// Bagian ini untuk mengecek apakah control combo box 
            /// dicentang atau tidak? Jika dicentang tambahkan parameter
            /// ke string SQL. 
            /// </summary>

            if (cbLength.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[1] + " = " + tbLength.Text );
            }

            if (cbWidth.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[0] + " = " + tbWidth.Text );
            }

            if (cbDiameter.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[2] + " = " + tbDiameter.Text );
            }

            #endregion

            string prmstr = prmstrbld.ToString();
            string[] prmstrarr = prmstr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (seltd >= 1)
            {
                sqlcmd = sqlcmd + "WHERE ";
                int i = 0;
                do
                {
                    sqlcmd = sqlcmd + prmstrarr[i];
                    if (i < seltd - 1)
                    {
                        sqlcmd = sqlcmd + " AND ";
                    };
                    i++;
                } while (i < seltd);

                if (rbRound.Checked)
                {
                    sqlcmd = sqlcmd + " AND faktor_hitung = 6.20";
                }

                if (rbFlat.Checked)
                {
                    sqlcmd = sqlcmd + " AND faktor_hitung = 8.00";
                }
            }
            else
            { 
                if (rbRound.Checked)
                {
                    sqlcmd = sqlcmd + "WHERE faktor_hitung = 6.20";
                }
                if (rbFlat.Checked)
                {
                    sqlcmd = sqlcmd + "WHERE faktor_hitung = 8.00";
                }
            }

            //Console.WriteLine(sqlcmd);
            SQLGetData(sqlcmd);
            SetViewGridCol();

            //MessageBox.Show(sqlcmd);
        }

        private void SQLGetData(string sql)
        {
            try
            {
                connString = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase; ;
                conn = new MySqlConnection(connString);
                dtAdapter = new MySqlDataAdapter(sql, conn);
                tbl = new DataTable();
                dtAdapter.Fill(tbl);
                dgViewListUkuran.DataSource = tbl;
                conn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgViewListUkuran_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Double _lebar, _panjang, _tinggi, _faktor;
            //Double dBerat;

            idUkuran = dgViewListUkuran.CurrentRow.Cells["id_ukuran"].Value.ToString();
            lebar = string.Format("{0:0,000.00}", dgViewListUkuran.CurrentRow.Cells["lebar"].Value.ToString());
            panjang = string.Format("{0:0,000.00}", dgViewListUkuran.CurrentRow.Cells["panjang"].Value.ToString());
            tinggi = string.Format("{0:0,000.00}", dgViewListUkuran.CurrentRow.Cells["tinggi"].Value.ToString());
            _faktorHitung = dgViewListUkuran.CurrentRow.Cells["faktor_hitung"].Value.ToString();

            //_lebar = Convert.ToDouble(lebar);
            //_panjang = Convert.ToDouble(panjang);
            //_tinggi = (tinggi == "") ? 0 : Convert.ToDouble(tinggi);
            //_faktor = Convert.ToDouble(_faktorHitung);

            //dBerat = HitungBerat(_lebar, _panjang, _tinggi, _faktor);

            //_faktorHitung = dBerat.ToString();
            conn.Close();
            Close();
        }

        private Double HitungBerat(Double Lebar, Double Panjang, Double Tebal, Double Faktor)
        {
            Double Berat = 0.00;

            if (Faktor == 6.2)
            {
                Berat = (Lebar / 100) * (Lebar / 100) * (Panjang / 100) * Faktor;
            }
            else if (Faktor == 8)
            {
                Berat = (Lebar / 100) * (Tebal / 100) * (Panjang /100) * Faktor;
            }

            return Berat;
        }

        private void tbLength_TextChanged(object sender, EventArgs e)
        {
            if (tbLength.Text != "")
            {
                cbLength.Checked = true;
            }
            else cbLength.Checked = false;

        }

        private void tbWidth_TextChanged(object sender, EventArgs e)
        {
            if (tbWidth.Text != "")
            {
                cbWidth.Checked = true;
            }
            else cbWidth.Checked = false;

        }

        private void tbDiameter_TextChanged(object sender, EventArgs e)
        {
            if (tbDiameter.Text != "")
            {
                cbDiameter.Checked = true;
            }
            else cbDiameter.Checked = false;

        }

        private void btnBaru_Click(object sender, EventArgs e)
        {
            // Isi data ukuran baru
            frmNewUkuran UkuranBaru = new frmNewUkuran();
            UkuranBaru.ShowDialog();
            if (UkuranBaru.DialogResult == DialogResult.OK)
            {
                // Save data to table
                SaveUkuranBaru(UkuranBaru.Lebar, UkuranBaru.Panjang, UkuranBaru.Tebal, 
                    UkuranBaru.fkt_hitung);
            }
        }

        private void SaveUkuranBaru(Double lbr, Double pjg, Double Tgg, Double Faktor)
        {
            MySqlParameter prmnmukuran = new MySqlParameter("namaukuran", MySqlDbType.String);
            MySqlParameter prmlebar = new MySqlParameter("lebar", MySqlDbType.Double);
            MySqlParameter prmpanjang = new MySqlParameter("panjang", MySqlDbType.Double);
            MySqlParameter prmtinggi = new MySqlParameter("tinggi", MySqlDbType.Double);
            MySqlParameter prmfaktor = new MySqlParameter("fakt", MySqlDbType.Double);

            string nmUkuran = "";
            if (Faktor == 8)
            {
                nmUkuran = lbr.ToString() + " X " + pjg.ToString() + " X " + Tgg.ToString();
                prmtinggi.Value = Tgg;
            }
            else if (Faktor == 6.20)
            {
                nmUkuran = lbr.ToString() + " X " + pjg.ToString();
                prmtinggi.Value = DBNull.Value;
            }

            string strSaveUkuran = "INSERT INTO m_ukuran(nama_ukuran, lebar, panjang, tinggi, " + 
                " faktor_hitung) VALUES(@namaukuran, @lebar, @panjang, @tinggi, @fakt)" ;
            MySqlCommand UkuranBaru = new MySqlCommand(strSaveUkuran , conn);

            prmnmukuran.Value = nmUkuran;
            prmlebar.Value = lbr;
            prmpanjang.Value = pjg;
            prmfaktor.Value = Faktor;

            UkuranBaru.Parameters.Add(prmnmukuran);
            UkuranBaru.Parameters.Add(prmlebar);
            UkuranBaru.Parameters.Add(prmpanjang);
            UkuranBaru.Parameters.Add(prmtinggi);
            UkuranBaru.Parameters.Add(prmfaktor);

            UkuranBaru.ExecuteNonQuery();
        }

        private void rbRound_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRound.Checked)
            {
                cbDiameter.Visible = false;
                label4.Visible = false;
                tbDiameter.Visible = false;
            }

            if (rbFlat.Checked)
            {
                cbDiameter.Visible = true;
                label4.Visible = true;
                tbDiameter.Visible = true;
            }
        }

        private void SetViewGridCol()
        {
            dgViewListUkuran.Columns[0].Visible = false;
            dgViewListUkuran.Columns[1].HeaderText = "Nama";
            dgViewListUkuran.Columns[2].HeaderText = "Ukuran 1";
            dgViewListUkuran.Columns[2].DefaultCellStyle.Alignment = 
                DataGridViewContentAlignment.MiddleRight;
            dgViewListUkuran.Columns[3].HeaderText = "Ukuran 2";
            dgViewListUkuran.Columns[3].DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            dgViewListUkuran.Columns[4].HeaderText = "Ukuran 3";
            dgViewListUkuran.Columns[4].DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
            dgViewListUkuran.Columns[5].HeaderText = "Faktor Hitung";
            dgViewListUkuran.Columns[5].DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleRight;
        }
    }

}
