﻿namespace SalesBaja
{
    partial class fr_ListUkuran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fr_ListUkuran));
            this.dgViewListUkuran = new System.Windows.Forms.DataGridView();
            this.tbDiameter = new System.Windows.Forms.TextBox();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbDiameter = new System.Windows.Forms.CheckBox();
            this.btnCari = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbWidth = new System.Windows.Forms.CheckBox();
            this.cbLength = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbFlat = new System.Windows.Forms.RadioButton();
            this.rbRound = new System.Windows.Forms.RadioButton();
            this.btnBaru = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewListUkuran)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgViewListUkuran
            // 
            this.dgViewListUkuran.AllowUserToAddRows = false;
            this.dgViewListUkuran.AllowUserToDeleteRows = false;
            this.dgViewListUkuran.Location = new System.Drawing.Point(12, 116);
            this.dgViewListUkuran.Name = "dgViewListUkuran";
            this.dgViewListUkuran.ReadOnly = true;
            this.dgViewListUkuran.RowHeadersVisible = false;
            this.dgViewListUkuran.Size = new System.Drawing.Size(669, 324);
            this.dgViewListUkuran.TabIndex = 0;
            this.dgViewListUkuran.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgViewListUkuran_CellDoubleClick);
            // 
            // tbDiameter
            // 
            this.tbDiameter.Location = new System.Drawing.Point(82, 65);
            this.tbDiameter.Name = "tbDiameter";
            this.tbDiameter.Size = new System.Drawing.Size(100, 20);
            this.tbDiameter.TabIndex = 8;
            this.tbDiameter.Visible = false;
            this.tbDiameter.TextChanged += new System.EventHandler(this.tbDiameter_TextChanged);
            // 
            // tbWidth
            // 
            this.tbWidth.Location = new System.Drawing.Point(82, 19);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(100, 20);
            this.tbWidth.TabIndex = 4;
            this.tbWidth.TextChanged += new System.EventHandler(this.tbWidth_TextChanged);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(82, 42);
            this.tbLength.Name = "tbLength";
            this.tbLength.Size = new System.Drawing.Size(100, 20);
            this.tbLength.TabIndex = 6;
            this.tbLength.TextChanged += new System.EventHandler(this.tbLength_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(71, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = ":";
            this.label4.Visible = false;
            // 
            // cbDiameter
            // 
            this.cbDiameter.AutoSize = true;
            this.cbDiameter.Location = new System.Drawing.Point(6, 67);
            this.cbDiameter.Name = "cbDiameter";
            this.cbDiameter.Size = new System.Drawing.Size(53, 17);
            this.cbDiameter.TabIndex = 7;
            this.cbDiameter.TabStop = false;
            this.cbDiameter.Text = "Tebal";
            this.cbDiameter.UseVisualStyleBackColor = true;
            this.cbDiameter.Visible = false;
            // 
            // btnCari
            // 
            this.btnCari.Location = new System.Drawing.Point(188, 63);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(75, 23);
            this.btnCari.TabIndex = 9;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = true;
            this.btnCari.Click += new System.EventHandler(this.btnCari_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = ":";
            // 
            // cbWidth
            // 
            this.cbWidth.AutoSize = true;
            this.cbWidth.Location = new System.Drawing.Point(6, 21);
            this.cbWidth.Name = "cbWidth";
            this.cbWidth.Size = new System.Drawing.Size(53, 17);
            this.cbWidth.TabIndex = 3;
            this.cbWidth.TabStop = false;
            this.cbWidth.Text = "Lebar";
            this.cbWidth.UseVisualStyleBackColor = true;
            // 
            // cbLength
            // 
            this.cbLength.AutoSize = true;
            this.cbLength.Location = new System.Drawing.Point(6, 44);
            this.cbLength.Name = "cbLength";
            this.cbLength.Size = new System.Drawing.Size(65, 17);
            this.cbLength.TabIndex = 5;
            this.cbLength.TabStop = false;
            this.cbLength.Text = "Panjang";
            this.cbLength.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.rbFlat);
            this.groupBox1.Controls.Add(this.rbRound);
            this.groupBox1.Controls.Add(this.btnBaru);
            this.groupBox1.Controls.Add(this.tbDiameter);
            this.groupBox1.Controls.Add(this.tbWidth);
            this.groupBox1.Controls.Add(this.tbLength);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbDiameter);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbWidth);
            this.groupBox1.Controls.Add(this.cbLength);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(669, 98);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search";
            // 
            // rbFlat
            // 
            this.rbFlat.AutoSize = true;
            this.rbFlat.Location = new System.Drawing.Point(315, 20);
            this.rbFlat.Name = "rbFlat";
            this.rbFlat.Size = new System.Drawing.Size(48, 17);
            this.rbFlat.TabIndex = 16;
            this.rbFlat.Text = "Flate";
            this.rbFlat.UseVisualStyleBackColor = true;
            // 
            // rbRound
            // 
            this.rbRound.AutoSize = true;
            this.rbRound.Checked = true;
            this.rbRound.Location = new System.Drawing.Point(222, 20);
            this.rbRound.Name = "rbRound";
            this.rbRound.Size = new System.Drawing.Size(57, 17);
            this.rbRound.TabIndex = 15;
            this.rbRound.TabStop = true;
            this.rbRound.Text = "Round";
            this.rbRound.UseVisualStyleBackColor = true;
            this.rbRound.CheckedChanged += new System.EventHandler(this.rbRound_CheckedChanged);
            // 
            // btnBaru
            // 
            this.btnBaru.Location = new System.Drawing.Point(587, 63);
            this.btnBaru.Name = "btnBaru";
            this.btnBaru.Size = new System.Drawing.Size(75, 23);
            this.btnBaru.TabIndex = 14;
            this.btnBaru.Text = "Baru";
            this.btnBaru.UseVisualStyleBackColor = true;
            this.btnBaru.Click += new System.EventHandler(this.btnBaru_Click);
            // 
            // fr_ListUkuran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 452);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgViewListUkuran);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fr_ListUkuran";
            this.Text = "List Ukuran";
            ((System.ComponentModel.ISupportInitialize)(this.dgViewListUkuran)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgViewListUkuran;
        private System.Windows.Forms.TextBox tbDiameter;
        private System.Windows.Forms.TextBox tbWidth;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbDiameter;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbWidth;
        private System.Windows.Forms.CheckBox cbLength;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBaru;
        private System.Windows.Forms.RadioButton rbFlat;
        private System.Windows.Forms.RadioButton rbRound;
    }
}

