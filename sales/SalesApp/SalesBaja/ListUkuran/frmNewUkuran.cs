﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesBaja
{
    public partial class frmNewUkuran : Form
    {
        public Double Lebar;
        public Double Panjang;
        public Double Tebal;
        public Double fkt_hitung;

        public frmNewUkuran()
        {
            InitializeComponent();
            fkt_hitung = 0;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (fieldValidation(tbLebar))
            {
                Lebar = Convert.ToDouble(tbLebar.Text);
            }
            else
            {
                tbLebar.SelectionStart = 0;
                tbLebar.SelectionLength = tbLebar.Text.Length;
                tbLebar.Select();
                return;
            }
            
            if (fieldValidation(tbPanjang))
            {
                Panjang = Convert.ToDouble(tbPanjang.Text);
            }
            else
            {
                tbPanjang.SelectionStart = 0;
                tbPanjang.SelectionLength = tbPanjang.Text.Length;
                tbPanjang.Select();
                return;
            }

            if (rbFlat.Checked)
            {
                fkt_hitung = 8.00;
                if (fieldValidation(tbTebal))
                {
                    Tebal = Convert.ToDouble(tbTebal.Text);
                }
                else
                {
                    tbTebal.SelectionStart = 0;
                    tbTebal.SelectionLength = tbTebal.Text.Length;
                    tbTebal.Select();
                    return;
                }
            }

            if (rbRound.Checked)
            {
                Tebal = 0.00;
                fkt_hitung = 6.20;
            }

            DialogResult = DialogResult.OK;
        }

        private void rbRound_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRound.Checked)
            {
                tbTebal.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                fkt_hitung = 6.20;
            }
            else
            {
                tbTebal.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                fkt_hitung = 8.00;
            }
        }

        private void rbFlat_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFlat.Checked)
            {
                tbTebal.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                fkt_hitung = 8.00;
            }
            else
            {
                tbTebal.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                fkt_hitung = 6.20;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private bool fieldValidation(object Field1)
        {
            bool hsl = false;
            TextBox f1 = Field1 as TextBox;
            string f1Val = f1.Text;
            decimal d;
            if (decimal.TryParse(f1Val, out d))
            {
                hsl = true;
            }
            else
            {
                hsl = false;
            }
            //if (!IsAlphaNum(f1Val))
            //{
            //    hsl = false;
            //}
            //else
            //{
            //    hsl = true;
            //}

            return hsl;
        }

        private bool IsAlphaNum(string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            return (str.ToCharArray().All(c => Char.IsNumber(c)));
        }
    }
}
