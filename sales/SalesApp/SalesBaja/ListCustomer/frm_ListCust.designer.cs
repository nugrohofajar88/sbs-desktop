﻿namespace SalesBaja
{
    partial class form_List_Cust
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_List_Cust));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBaru = new System.Windows.Forms.Button();
            this.btnCari = new System.Windows.Forms.Button();
            this.tbTelp = new System.Windows.Forms.TextBox();
            this.tbAlamat = new System.Windows.Forms.TextBox();
            this.tbNama = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTelp = new System.Windows.Forms.CheckBox();
            this.cbAlamat = new System.Windows.Forms.CheckBox();
            this.cbNama = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgViewLstCust = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewLstCust)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnBaru);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.tbTelp);
            this.groupBox1.Controls.Add(this.tbAlamat);
            this.groupBox1.Controls.Add(this.tbNama);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbTelp);
            this.groupBox1.Controls.Add(this.cbAlamat);
            this.groupBox1.Controls.Add(this.cbNama);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(593, 91);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search";
            // 
            // btnBaru
            // 
            this.btnBaru.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBaru.Location = new System.Drawing.Point(512, 61);
            this.btnBaru.Name = "btnBaru";
            this.btnBaru.Size = new System.Drawing.Size(75, 23);
            this.btnBaru.TabIndex = 11;
            this.btnBaru.Text = "Baru";
            this.btnBaru.UseVisualStyleBackColor = true;
            this.btnBaru.Click += new System.EventHandler(this.btnBaru_Click);
            // 
            // btnCari
            // 
            this.btnCari.Location = new System.Drawing.Point(188, 61);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(75, 23);
            this.btnCari.TabIndex = 10;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = true;
            this.btnCari.Click += new System.EventHandler(this.btnCari_Click);
            // 
            // tbTelp
            // 
            this.tbTelp.Location = new System.Drawing.Point(82, 63);
            this.tbTelp.Name = "tbTelp";
            this.tbTelp.Size = new System.Drawing.Size(100, 20);
            this.tbTelp.TabIndex = 9;
            this.tbTelp.TextChanged += new System.EventHandler(this.tbTelp_TextChanged);
            // 
            // tbAlamat
            // 
            this.tbAlamat.Location = new System.Drawing.Point(82, 40);
            this.tbAlamat.Name = "tbAlamat";
            this.tbAlamat.Size = new System.Drawing.Size(100, 20);
            this.tbAlamat.TabIndex = 8;
            this.tbAlamat.TextChanged += new System.EventHandler(this.tbAlamat_TextChanged);
            // 
            // tbNama
            // 
            this.tbNama.Location = new System.Drawing.Point(82, 17);
            this.tbNama.Name = "tbNama";
            this.tbNama.Size = new System.Drawing.Size(100, 20);
            this.tbNama.TabIndex = 7;
            this.tbNama.TextChanged += new System.EventHandler(this.tbNama_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = ":";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = ":";
            // 
            // cbTelp
            // 
            this.cbTelp.AutoSize = true;
            this.cbTelp.Location = new System.Drawing.Point(6, 65);
            this.cbTelp.Name = "cbTelp";
            this.cbTelp.Size = new System.Drawing.Size(47, 17);
            this.cbTelp.TabIndex = 3;
            this.cbTelp.Text = "Telp";
            this.cbTelp.UseVisualStyleBackColor = true;
            // 
            // cbAlamat
            // 
            this.cbAlamat.AutoSize = true;
            this.cbAlamat.Location = new System.Drawing.Point(6, 42);
            this.cbAlamat.Name = "cbAlamat";
            this.cbAlamat.Size = new System.Drawing.Size(58, 17);
            this.cbAlamat.TabIndex = 2;
            this.cbAlamat.Text = "Alamat";
            this.cbAlamat.UseVisualStyleBackColor = true;
            // 
            // cbNama
            // 
            this.cbNama.AutoSize = true;
            this.cbNama.Location = new System.Drawing.Point(6, 19);
            this.cbNama.Name = "cbNama";
            this.cbNama.Size = new System.Drawing.Size(54, 17);
            this.cbNama.TabIndex = 1;
            this.cbNama.Text = "Nama";
            this.cbNama.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgViewLstCust);
            this.panel1.Location = new System.Drawing.Point(12, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(593, 327);
            this.panel1.TabIndex = 1;
            // 
            // dgViewLstCust
            // 
            this.dgViewLstCust.AllowUserToAddRows = false;
            this.dgViewLstCust.AllowUserToDeleteRows = false;
            this.dgViewLstCust.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgViewLstCust.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewLstCust.Location = new System.Drawing.Point(0, 0);
            this.dgViewLstCust.Name = "dgViewLstCust";
            this.dgViewLstCust.ReadOnly = true;
            this.dgViewLstCust.RowHeadersVisible = false;
            this.dgViewLstCust.Size = new System.Drawing.Size(593, 327);
            this.dgViewLstCust.TabIndex = 0;
            this.dgViewLstCust.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgViewLstCust_CellDoubleClick);
            // 
            // form_List_Cust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 448);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "form_List_Cust";
            this.Text = "List Customer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgViewLstCust)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbTelp;
        private System.Windows.Forms.CheckBox cbAlamat;
        private System.Windows.Forms.CheckBox cbNama;
        private System.Windows.Forms.TextBox tbTelp;
        private System.Windows.Forms.TextBox tbAlamat;
        private System.Windows.Forms.TextBox tbNama;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgViewLstCust;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.Button btnBaru;
    }
}

