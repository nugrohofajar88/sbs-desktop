﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
//using NewCustomer;

namespace SalesBaja
{
    public partial class form_List_Cust : Form
    {
        private MySqlConnection conn;
        private string connString;
        private MySqlDataAdapter dtAdapter;
        private MySqlCommand sqlCommand;
        private DataTable tbl;

        private string IdSelectedCust;
        public string SelectedID
        {
            get { return IdSelectedCust; }
            set { }
        }

        private string NamaSelectedCust;
        public string SelectedNama
        {
            get { return NamaSelectedCust; }
            set { }
        }

        private string AlmtSelectedCust;
        public string SelectedAlamat
        {
            get { return AlmtSelectedCust; }
            set { }
        }

        private string CpSelectedCust;
        public string SelectedCP
        {
            get { return CpSelectedCust; }
            set { }
        }

        private static string[] param = new string[]
        {
            "nama_customer",
            "alamat",
            "telp_cust"
        };

        public form_List_Cust()
        {
            InitializeComponent();
            //InitSQLConnect();
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            string sqlcmd = "SELECT id_customer, nama_customer, alamat, telp_cust, contact_person, ketr " +
                "FROM m_customer ";
            int seltd = 0;
            StringBuilder prmstrbld = new StringBuilder();

            #region Combobox checking 

            /// <summary>
            /// Bagian ini untuk mengecek apakah control combo box 
            /// dicentang atau tidak? Jika dicentang tambahkan parameter
            /// ke string SQL. 
            /// </summary>

            if (cbNama.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[0] + " LIKE '%" + tbNama.Text + "'"); 
            }

            if (cbAlamat.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[1] + " LIKE '%" + tbAlamat.Text + "'");
            }

            if (cbTelp.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[2] + " LIKE '%" + tbTelp.Text + "'");
            }

            #endregion

            string prmstr = prmstrbld.ToString();
            string[] prmstrarr = prmstr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (seltd >= 1)
            {
                sqlcmd = sqlcmd + "WHERE ";
                int i = 0;
                do
                {
                    sqlcmd = sqlcmd + prmstrarr[i];
                    if (i < seltd - 1)
                    {
                        sqlcmd = sqlcmd + " AND ";
                    };
                    i++;
                } while (i < seltd);
            }

            SQLGetData(sqlcmd);

            //MessageBox.Show(sqlcmd);
        }

        private void InitSQLConnect()
        {
            connString = "server=127.0.0.1;uid=root;pwd=Enterprise1;database=sbs2";
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = connString;
                conn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SQLGetData(string sql)
        {
            try
            {
                connString = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase;
                conn = new MySqlConnection(connString);
                dtAdapter = new MySqlDataAdapter(sql, conn);
                tbl = new DataTable();
                dtAdapter.Fill(tbl);
                dgViewLstCust.DataSource = tbl;
                conn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgViewLstCust_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NamaSelectedCust = dgViewLstCust.CurrentRow.Cells["nama_customer"].Value.ToString();
            IdSelectedCust = dgViewLstCust.CurrentRow.Cells["id_customer"].Value.ToString();
            CpSelectedCust = dgViewLstCust.CurrentRow.Cells["contact_person"].Value.ToString();
            AlmtSelectedCust = dgViewLstCust.CurrentRow.Cells["alamat"].Value.ToString();
            Close();
        }

        private void btnBaru_Click(object sender, EventArgs e)
        {
            //frm_EntryCustomer tambahCust = new frm_EntryCustomer();
            //tambahCust.ShowDialog(this);
            //btnCari_Click(this, e);
            //tambahCust.Dispose();
        }

        private void tbNama_TextChanged(object sender, EventArgs e)
        {
            if (tbNama.Text != "")
            {
                cbNama.Checked = true;
            }
            else cbNama.Checked = false;
        }

        private void tbAlamat_TextChanged(object sender, EventArgs e)
        {
            if (tbAlamat.Text != "")
            {
                cbAlamat.Checked = true;
            }
            else cbAlamat.Checked = false;
        }

        private void tbTelp_TextChanged(object sender, EventArgs e)
        {
            if (tbTelp.Text != "")
            {
                cbTelp.Checked = true;
            }
            else cbTelp.Checked = false;
        }
    }
}
