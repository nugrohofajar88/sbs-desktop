﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SalesBaja.BonusCustomer;
using SalesBaja.Laporan;
using SalesBaja.Pembelian;

namespace SalesBaja
{
    public partial class fr_MainMenu : Form
    {
        private static fr_MainMenu openForm = null;

        // No need for locking - you'll be doing all this on the UI thread...
        public static fr_MainMenu GetInstance()
        {
            if (openForm == null)
            {
                openForm = new fr_MainMenu();
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        private frm_login LoginFrm;

        private string _userconnected;
        private int _userid;

        public string Userconnected
        {
            get
            {
                return _userconnected;
            }

            set
            {
                _userconnected = value;
            }
        }

        public int Userid
        {
            get
            {
                return _userid;
            }

            set
            {
                _userid = value;
            }
        }

        public fr_MainMenu()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void fr_MainMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void fr_MainMenu_Load(object sender, EventArgs e)
        {
            lb_username.Text = _userconnected;
        }

        private void addPenawaranToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fr_penawaranNew AddPenawaran = fr_penawaranNew.GetInstance();
            AddPenawaran.frmMode = FormOpenMode.fomNew;
            AddPenawaran.MdiParent = this;
            AddPenawaran.WindowState = FormWindowState.Maximized;
            AddPenawaran.Userconnected = _userconnected;
            AddPenawaran.Userid = _userid;
            AddPenawaran.Show();
        }

        private void listPenawaranToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fr_ListPenawaran DaftarPenawaran = fr_ListPenawaran.GetInstance();
            DaftarPenawaran.MdiParent = this;
            DaftarPenawaran.WindowState = FormWindowState.Maximized;
            DaftarPenawaran.FillMasterView();
            DaftarPenawaran.Show();
        }

        private void listOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fr_ListPO DaftarPO = fr_ListPO.GetInstance();
            DaftarPO.MdiParent = this;
            DaftarPO.WindowState = FormWindowState.Maximized;
            DaftarPO.FillMasterView();
            DaftarPO.Show();
        }

        private void addOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fr_t_PO AddPO = fr_t_PO.GetInstance();
            AddPO.frmMode = FormOpenMode.fomNew;
            AddPO.MdiParent = this;
            AddPO.WindowState = FormWindowState.Maximized;
            AddPO.Userconnected = _userconnected;
            AddPO.Userid = _userid;
            AddPO.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoginFrm = frm_login.GetInstance();
            LoginFrm.ClearUserPassword();
            this.Hide();
            LoginFrm.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmBonusCustomer IsiBonus = frmBonusCustomer.GetInstance();
            IsiBonus.Userconnected = _userconnected;
            IsiBonus.Userid = _userid;
            IsiBonus.Show();
        }

        private void penjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Laporan penjualan
            frmLapPenjualan LapPenjualan = new frmLapPenjualan();
            LapPenjualan.ShowDialog();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmPembelian Pembelian = new frmPembelian();
            Pembelian.ShowDialog();
        }
    }
}
