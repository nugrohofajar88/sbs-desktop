﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SalesBaja
{
    public partial class fr_FindUkuran : Form
    {
        private MySqlConnection conn;
        private string connString;

        public Double Lebar;
        public Double Panjang;
        public double? Tebal;
        public Double fkt_hitung;
        public Int32 IDUkuran;
        public String NamaUkuran;

        public fr_FindUkuran()
        {
            InitializeComponent();
            InitConn();
            fkt_hitung = 0.00;
            cbBentuk.SelectedIndex = 0;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (fieldValidation(tbLebar))
            {
                Lebar = Convert.ToDouble(tbLebar.Text);
            }
            else
            {
                tbLebar.SelectionStart = 0;
                tbLebar.SelectionLength = tbLebar.Text.Length;
                tbLebar.Select();
                return;
            }
            
            if (fieldValidation(tbPanjang))
            {
                Panjang = Convert.ToDouble(tbPanjang.Text);
            }
            else
            {
                tbPanjang.SelectionStart = 0;
                tbPanjang.SelectionLength = tbPanjang.Text.Length;
                tbPanjang.Select();
                return;
            }

            if (cbBentuk.SelectedIndex == 1)
            {
                fkt_hitung = 8.00;
                if (fieldValidation(tbTebal))
                {
                    Tebal = Convert.ToDouble(tbTebal.Text);
                }
                else
                {
                    tbTebal.SelectionStart = 0;
                    tbTebal.SelectionLength = tbTebal.Text.Length;
                    tbTebal.Select();
                    return;
                }
            }

            if (cbBentuk.SelectedIndex == 0)
            {
                Tebal = null;
                fkt_hitung = 6.20;
            }

            if (!UkuranAda()) {
                SimpanUkuran();
            }

            DialogResult = DialogResult.OK;
        }

        //private void rbRound_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbRound.Checked)
        //    {
        //        tbTebal.Visible = false;
        //        label3.Visible = false;
        //        label4.Visible = false;
        //        fkt_hitung = 6.20;
        //    }
        //    else
        //    {
        //        tbTebal.Visible = true;
        //        label3.Visible = true;
        //        label4.Visible = true;
        //        fkt_hitung = 8.00;
        //    }
        //}

        //private void rbFlat_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbFlat.Checked)
        //    {
        //        tbTebal.Visible = true;
        //        label3.Visible = true;
        //        label4.Visible = true;
        //        fkt_hitung = 8.00;
        //    }
        //    else
        //    {
        //        tbTebal.Visible = false;
        //        label3.Visible = false;
        //        label4.Visible = false;
        //        fkt_hitung = 6.20;
        //    }
        //}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private bool fieldValidation(object Field1)
        {
            bool hsl = false;
            TextBox f1 = Field1 as TextBox;
            string f1Val = f1.Text;
            decimal d;
            if (decimal.TryParse(f1Val, out d))
            {
                hsl = true;
            }
            else
            {
                hsl = false;
            }
            //if (!IsAlphaNum(f1Val))
            //{
            //    hsl = false;
            //}
            //else
            //{
            //    hsl = true;
            //}

            return hsl;
        }

        private bool IsAlphaNum(string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            return (str.ToCharArray().All(c => Char.IsNumber(c)));
        }

        private void cbBentuk_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbBentuk.SelectedIndex == 0)
            {
                tbTebal.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                fkt_hitung = 6.20;
            }
            else
            {
                tbTebal.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                fkt_hitung = 8.00;
            }
        }

        private bool UkuranAda()
        {
            conn.Open();

            string sqlcmd = "";
            if (cbBentuk.SelectedIndex == 1)
            {
                sqlcmd = "SELECT id_ukuran, nama_ukuran as nama, lebar, panjang, tinggi, " +
                    "faktor_hitung FROM m_ukuran WHERE lebar = @uk1 AND panjang = @uk2 AND " +
                    "tinggi = @uk3";
            }
            else if (cbBentuk.SelectedIndex == 0)
            {
                sqlcmd = "SELECT id_ukuran, nama_ukuran as nama, lebar, panjang, tinggi, " +
                    "faktor_hitung FROM m_ukuran WHERE lebar = @uk1 AND panjang = @uk2 AND " +
                    "tinggi is @uk3";
            } 

            MySqlParameter puk1 = new MySqlParameter("uk1", MySqlDbType.Decimal);
            MySqlParameter puk2 = new MySqlParameter("uk2", MySqlDbType.Decimal);
            MySqlParameter puk3 = new MySqlParameter("uk3", MySqlDbType.Decimal);

            puk1.Value = Lebar;
            puk2.Value = Panjang;
            puk3.Value = (object)Tebal ?? DBNull.Value;

            MySqlCommand cekcommand = new MySqlCommand(sqlcmd, conn);
            cekcommand.Parameters.Add(puk1);
            cekcommand.Parameters.Add(puk2);
            cekcommand.Parameters.Add(puk3);

            MySqlDataReader reader = cekcommand.ExecuteReader();

            if (reader.HasRows) { 
                while (reader.Read())
                {
                    IDUkuran = reader.GetInt32(0);
                    NamaUkuran = cbBentuk.Text +  " " + reader.GetString(1);
                }
                return true;
            }
            
            reader.Close();
            conn.Close();
            return false;
        }

        private void SimpanUkuran()
        {
            conn.Open();

            MySqlParameter prmnmukuran = new MySqlParameter("namaukuran", MySqlDbType.String);
            MySqlParameter prmlebar = new MySqlParameter("lebar", MySqlDbType.Double);
            MySqlParameter prmpanjang = new MySqlParameter("panjang", MySqlDbType.Double);
            MySqlParameter prmtinggi = new MySqlParameter("tinggi", MySqlDbType.Double);
            MySqlParameter prmfaktor = new MySqlParameter("fakt", MySqlDbType.Double);

            string nmUkuran = "";
            if (cbBentuk.SelectedIndex == 1)
            {
                nmUkuran = Lebar.ToString() + " X " + Panjang.ToString() + " X " + Tebal.ToString();
                prmtinggi.Value = Tebal;

            }
            else if (cbBentuk.SelectedIndex == 0)
            {
                nmUkuran = Lebar.ToString() + " X " + Panjang.ToString();
                prmtinggi.Value = DBNull.Value;
            }

            string strSaveUkuran = "INSERT INTO m_ukuran(nama_ukuran, lebar, panjang, tinggi, " +
                " faktor_hitung) VALUES(@namaukuran, @lebar, @panjang, @tinggi, @fakt)";
            MySqlCommand UkuranBaru = new MySqlCommand(strSaveUkuran, conn);

            prmnmukuran.Value = nmUkuran;
            prmlebar.Value = Lebar;
            prmpanjang.Value = Panjang;
            prmfaktor.Value = fkt_hitung;

            UkuranBaru.Parameters.Add(prmnmukuran);
            UkuranBaru.Parameters.Add(prmlebar);
            UkuranBaru.Parameters.Add(prmpanjang);
            UkuranBaru.Parameters.Add(prmtinggi);
            UkuranBaru.Parameters.Add(prmfaktor);

            UkuranBaru.ExecuteNonQuery();
            long theLastId = UkuranBaru.LastInsertedId;

            NamaUkuran = cbBentuk.Text + " " + nmUkuran;
            IDUkuran = (int)theLastId;

            conn.Close();
        }

        private void InitConn()
        {
            connString = "server=" + GlobVar.hAddress + "; uid=" +
              GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
              GlobVar.hdbase;
            //connString = "server=localhost; uid=root; pwd=Enterprise1; database=datasbs4;";
            conn = new MySqlConnection(connString);
        }
    }
}
