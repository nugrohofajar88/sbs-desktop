﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;

namespace SalesBaja
{
    public partial class fr_penawaranNew : Form
    {
        private static fr_penawaranNew openForm = null;

        // No need for locking - you'll be doing all this on the UI thread...
        public static fr_penawaranNew GetInstance()
        {
            if (openForm == null)
            {
                openForm = new fr_penawaranNew();
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        public FormOpenMode frmMode;
        public int IDTransaksi;

        private MySqlConnection dtConn;
        private string dtConnString;

        private int _userid;
        private string _userconnected;

        private Int32 RegNomor;
        public Double TotalHarga;
        public Double Discpersen;
        public Double Discnominal;
        public Double TotalNominal;
        public Double PpnNominal;

        private DataTable MarketingTable;
        private DataTable DetilTable;
        private DataTable DeletedTable;
        private DataTable EditedTable;
        private DataTable InsertedTable;

        private string OldDiminta;
        private string OldSQty;
        private string OldsBerat;
        private string OldsHarga;

        //private BindingSource bind1;
        public int Userid
        {
            get
            {
                return _userid;
            }

            set
            {
                _userid = value;
            }
        }

        public string Userconnected
        {
            get
            {
                return _userconnected;
            }

            set
            {
                _userconnected = value;
            }
        }

        public fr_penawaranNew()
        {
            InitializeComponent();
            dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
              GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
              GlobVar.hdbase;
            //dtConnString = "server=localhost; uid=root; pwd=Enterprise1; database=datasbs4;";
            dtConn = new MySqlConnection(dtConnString);
            InitMarketingData();
        }

        private void btnGeneratNo_Click(object sender, EventArgs e)
        {
            MySqlConnection NGConn = new MySqlConnection(dtConnString);
            Int32 tahun = dtTglPO.Value.Year;
            string strcmd = "SELECT val FROM reg_no WHERE id = 1 AND year = " + tahun.ToString();
            MySqlCommand regcmd = new MySqlCommand(strcmd, NGConn);
            NGConn.Open();
            MySqlDataReader dread = regcmd.ExecuteReader();
            RegNomor = 0;

            while (dread.Read())
            {
                RegNomor = dread.GetInt32(0);
            }

            dread.Close();

            string strthn = tahun.ToString().Substring(2);
            Int32 bln = dtTglPO.Value.Month;
            string strbln;
            string No = "";
            RegNomor++;

            switch (bln)
            {
                case 1:
                    {
                        strbln = "I";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 2:
                    {
                        strbln = "II";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 3:
                    {
                        strbln = "III";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 4:
                    {
                        strbln = "IV";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 5:
                    {
                        strbln = "V";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 6:
                    {
                        strbln = "VI";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 7:
                    {
                        strbln = "VII";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 8:
                    {
                        strbln = "VIII";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 9:
                    {
                        strbln = "IX";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 10:
                    {
                        strbln = "X";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 11:
                    {
                        strbln = "XI";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }

                case 12:
                    {
                        strbln = "XII";
                        No = RegNomor.ToString() + "/PH/SBS/" + strbln + "/" + strthn;
                        //Console.WriteLine(No);
                        break;
                    }
            }

            NGConn.Close();
            tbNoPenawaran.Text = No;

        } //end of btnGeneratNo_Click

        private void btnCari_Click(object sender, EventArgs e)
        {
            form_List_Cust fListCustomer = new form_List_Cust();
            fListCustomer.ShowDialog(this);
            tbCustID.Text = fListCustomer.SelectedID;
            lbNamaCustomer.Text = fListCustomer.SelectedNama;
            fListCustomer.Dispose();
        } // end of btnCari_Click

        private void btnRowAdd_Click(object sender, EventArgs e)
        {
            DataGridViewRow r = new DataGridViewRow();
            r.CreateCells(dgDetil);
            int IdSKR = DataUtils.GenRowID();
            r.Cells[20].Value = IdSKR.ToString();
            dgDetil.Rows.Add(r);
        } //end of btnRowAdd_Click

        private void btnDel_Click(object sender, EventArgs e)
        {
            // Menghapus row yang terselect
            string _delJumlah;
            int i;
            int Id_Dtl;
            int EditedRowIdx;
            int InsertedRowIdx;

            for (i = dgDetil.RowCount - 1; i >= 0; i--)
            {
                DataGridViewCheckBoxCell cellcheck = dgDetil.Rows[i].Cells[0] as DataGridViewCheckBoxCell;
                if (Convert.ToBoolean(cellcheck.Value))
                {
                    //slected++;
                    _delJumlah = (dgDetil.Rows[i].Cells[18].Value == null) ? "0" :
                        dgDetil.Rows[i].Cells[18].Value.ToString();
                    Double DeletedJumlah = Convert.ToDouble(_delJumlah);
                    TotalHarga = TotalHarga - DeletedJumlah;
                    //idDtl = int.Parse(dgDetil.Rows[i].Cells[19].Value.ToString());
                    if (frmMode == FormOpenMode.fomEdit)
                    {
                        //    MarkDeletedRow(idDtl);
                        Id_Dtl = Convert.ToInt32(dgDetil.Rows[i].Cells[19].Value.ToString());
                        EditedRowIdx = DataUtils.FindIndex(EditedTable, Id_Dtl);
                        InsertedRowIdx = DataUtils.FindIndex(InsertedTable, Id_Dtl);
                        if (EditedRowIdx != -1)
                        {
                            EditedTable.Rows[EditedRowIdx].Delete();
                        } else 
                        if (InsertedRowIdx != -1)
                        {
                            InsertedTable.Rows[InsertedRowIdx].Delete();
                        } else AddToDeletedRow(i);
                    }
                    dgDetil.Rows.RemoveAt(i);
                }
            }

            tbJumlah.Text = TotalHarga.ToString("#,##0.00");
        } // end of btnDel_Click

        private void dgDetil_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var SenderGrid = (DataGridView)sender;
            string OldIdUkuran;

            if (SenderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                switch (e.ColumnIndex)
                {
                    case 2:  // Ukuran diminta
                        {
                            if (frmMode == FormOpenMode.fomNew)
                            {
                                AddNewUkuranMinta(e.RowIndex);
                            }
                            else
                            {
                                EditingNewUkuranMinta(e.RowIndex);
                            }
                            break;
                        }

                    case 5:  // type barang
                        {
                            if (frmMode == FormOpenMode.fomNew)
                            {
                                AddNewType(e.RowIndex);
                            }
                            else
                            {
                                EditingType(e.RowIndex);
                            }
                            break;
                        }

                    case 8:
                        {
                            if (frmMode == FormOpenMode.fomNew)
                            {
                                AddNewUkuranTawar(e.RowIndex);
                            }
                            else
                            {
                                EditingNewUkuranTawar(e.RowIndex);
                            }
                            break;
                        }
                }
            }
        }

        private Double HitungBerat(Double Lebar, Double Panjang, Double Tebal, Double Faktor)
        {
            Double Berat = 0.00;

            if (Faktor == 6.2)
            {
                Berat = (Lebar / 100) * (Lebar / 100) * (Panjang / 100) * Faktor;
            }
            else if (Faktor == 8)
            {
                Berat = (Lebar / 100) * (Tebal / 100) * (Panjang / 100) * Faktor;
            }

            return Berat;
        }

        private Double HitBerat(Int32 Qty, Double BeratSat)
        {
            Double BeratAll = 0;

            BeratAll = Qty * BeratSat;

            return BeratAll;
        }

        private void dgDetil_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            var SenderGrid = (DataGridView)sender;

            Double BeratperKg, OldBerat;
            Double Jumlah, OldJumlah;
            string sQty0, sQty, sHrgSat0, sHargaSat, sOberat;
            Double lbr, pjg, tbl;
            Double _fkt;
            Int32 OldQty, OldHarga;

            switch (e.ColumnIndex)
            {
                case 1: // diminta
                    {
                        OldDiminta = (SenderGrid.Rows[e.RowIndex].Cells[1].Value == null) ?
                            "" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        break;
                    }
                case 15: // qty
                    {
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        OldSQty = sQty0;
                        OldQty = Convert.ToInt32(sQty0);

                        sOberat = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        OldBerat = Convert.ToDouble(sOberat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[17].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[17].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        OldHarga = Convert.ToInt32(sHargaSat);

                        OldJumlah = Math.Round(OldBerat * OldHarga, 2, MidpointRounding.AwayFromZero);

                        TotalHarga = TotalHarga - OldJumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");
                        break;
                    }
                case 16: // Berat
                    {
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        OldQty = Convert.ToInt32(sQty0);

                        sOberat = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        OldBerat = Convert.ToDouble(sOberat);
                        OldsBerat = sOberat;

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[17].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[17].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        OldHarga = Convert.ToInt32(sHargaSat);

                        OldJumlah = Math.Round(OldBerat * OldHarga, 2, MidpointRounding.AwayFromZero);

                        TotalHarga = TotalHarga - OldJumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        break;
                    }
                case 17: // Harga
                    {
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        OldQty = Convert.ToInt32(sQty0);

                        sOberat = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        OldBerat = Convert.ToDouble(sOberat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[17].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[17].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        OldHarga = Convert.ToInt32(sHargaSat);
                        OldsHarga = sHrgSat0;

                        OldJumlah = Math.Round(OldBerat * OldHarga, 2, MidpointRounding.AwayFromZero);

                        TotalHarga = TotalHarga - OldJumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");
                        break;
                    }
            }
        }

        private void dgDetil_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var SenderGrid = (DataGridView)sender;

            Double BeratperKg;
            Double Jumlah;
            Int32 _qty = 0;
            Int32 HargaSat;
            Double lbr, pjg, tbl;
            Double _fkt;
            string sQty0, sQty, sHrgSat0, sHargaSat, sBerat;

            switch (e.ColumnIndex)
            {
                case 1: // diminta
                    {
                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldDiminta != SenderGrid.Rows[e.RowIndex].Cells[1].Value.ToString())
                            {
                                EditingDiminta(e.RowIndex);
                            }
                        }
                        break;
                    }

                case 15: // qty
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        string _lbr = (SenderGrid.Rows[e.RowIndex].Cells[11].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[11].Value.ToString();
                        //_lbr = new string(_lbr.Where(char.IsDigit).ToArray());
                        string _pjg = (SenderGrid.Rows[e.RowIndex].Cells[12].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[12].Value.ToString();
                        //_pjg = new string(_pjg.Where(char.IsDigit).ToArray());
                        string _tbl = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        _tbl = (_tbl == "") ? "0" : _tbl;

                        lbr = Convert.ToDouble(_lbr);
                        pjg = Convert.ToDouble(_pjg);
                        tbl = Convert.ToDouble(_tbl);
                        _fkt = Convert.ToDouble(SenderGrid.Rows[e.RowIndex].Cells[14].Value);

                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());

                        try
                        {
                            _qty = Convert.ToInt32(sQty0);
                        }
                        catch (System.FormatException ex)
                        {
                            MessageBox.Show("Qty harus bilangan bulat");
                        }

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[17].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[17].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        HargaSat = Convert.ToInt32(sHargaSat);

                        BeratperKg = HitungBerat(lbr, pjg, tbl, _fkt);
                        BeratperKg = Math.Round(HitBerat(_qty, BeratperKg), 2, MidpointRounding.AwayFromZero);
                        Jumlah = Math.Round((BeratperKg * HargaSat), 2, MidpointRounding.AwayFromZero);

                        dgDetil.Rows[e.RowIndex].Cells[15].Value = _qty.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[16].Value = BeratperKg.ToString("#,##0.##");
                        dgDetil.Rows[e.RowIndex].Cells[17].Value = HargaSat.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[18].Value = Jumlah.ToString("#,##0.##");
                        TotalHarga = TotalHarga + Jumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                        Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                        TotalNominal = TotalHarga - Discnominal;
                        PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;

                        tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                        tbTotal.Text = TotalNominal.ToString("#,##0.##");
                        tbPPN.Text = PpnNominal.ToString("#,##0.##");

                        tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldSQty != SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString())
                            {
                                EditingQty(e.RowIndex);
                            }
                        }

                        break;
                    }

                case 16: // Berat
                    {
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());

                        try
                        {
                            _qty = Convert.ToInt32(sQty0);
                        }
                        catch (System.FormatException ex)
                        {
                            MessageBox.Show("Qty harus bilangan bulat");
                        }

                        sBerat = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        BeratperKg = Convert.ToDouble(sBerat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[17].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[17].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        HargaSat = Convert.ToInt32(sHargaSat);

                        Jumlah = Math.Round((BeratperKg * HargaSat), 2, MidpointRounding.AwayFromZero);

                        dgDetil.Rows[e.RowIndex].Cells[15].Value = _qty.ToString("#,##0");
                        //dgDetil.Rows[e.RowIndex].Cells[16].Value = BeratperKg.ToString("#,###.00");
                        dgDetil.Rows[e.RowIndex].Cells[17].Value = HargaSat.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[18].Value = Jumlah.ToString("#,##0.##");
                        TotalHarga = TotalHarga + Jumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                        Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                        TotalNominal = TotalHarga - Discnominal;
                        PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;

                        tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                        tbTotal.Text = TotalNominal.ToString("#,##0.##");
                        tbPPN.Text = PpnNominal.ToString("#,##0.##");

                        tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldsBerat != SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString())
                            {
                                EditingBerat(e.RowIndex);
                            }
                        }

                        break;
                    }

                case 17: // harga
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        string _lbr = (SenderGrid.Rows[e.RowIndex].Cells[11].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[11].Value.ToString();
                        //_lbr = new string(_lbr.Where(char.IsDigit).ToArray());
                        string _pjg = (SenderGrid.Rows[e.RowIndex].Cells[12].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[12].Value.ToString();
                        //_pjg = new string(_pjg.Where(char.IsDigit).ToArray());
                        string _tbl = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        _tbl = (_tbl == "") ? "0" : _tbl;

                        lbr = Convert.ToDouble(_lbr);
                        pjg = Convert.ToDouble(_pjg);
                        tbl = Convert.ToDouble(_tbl);
                        _fkt = Convert.ToDouble(SenderGrid.Rows[e.RowIndex].Cells[14].Value);

                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());

                        try
                        {
                            _qty = Convert.ToInt32(sQty0);
                        }
                        catch (System.FormatException ex)
                        {
                            MessageBox.Show("Qty harus bilangan bulat");
                        }

                        sBerat = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        BeratperKg = Convert.ToDouble(sBerat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[17].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[17].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        HargaSat = Convert.ToInt32(sHargaSat);

                        if (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null)
                        {
                            BeratperKg = HitungBerat(lbr, pjg, tbl, _fkt);
                            BeratperKg = Math.Round(HitBerat(_qty, BeratperKg), 2, MidpointRounding.AwayFromZero);
                        }

                        Jumlah = Math.Round((BeratperKg * HargaSat), 2, MidpointRounding.AwayFromZero);

                        dgDetil.Rows[e.RowIndex].Cells[15].Value = _qty.ToString("#,##0");
                        //dgDetil.Rows[e.RowIndex].Cells[16].Value = BeratperKg.ToString("#,###.00");
                        dgDetil.Rows[e.RowIndex].Cells[17].Value = HargaSat.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[18].Value = Jumlah.ToString("#,##0.##");
                        TotalHarga = TotalHarga + Jumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                        Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                        TotalNominal = TotalHarga - Discnominal;
                        PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;

                        tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                        tbTotal.Text = TotalNominal.ToString("#,##0.##");
                        tbPPN.Text = PpnNominal.ToString("#,##0.##");

                        tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldsHarga != SenderGrid.Rows[e.RowIndex].Cells[17].Value.ToString())
                            {
                                EditingHarga(e.RowIndex);
                            }
                        }

                        break;
                    }
            }
        }

        private void tbDiscPersen_Leave(object sender, EventArgs e)
        {
            Discpersen = Convert.ToDouble(tbDiscPersen.Text);
            Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
            TotalNominal = TotalHarga - Discnominal;
            PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;
            //GrandTot = TotalNominal - PpnNominal;

            tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
            tbTotal.Text = TotalNominal.ToString("#,##0.##");
            tbPPN.Text = PpnNominal.ToString("#,##0.##");

            tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");
        }

        private void cbPPn_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPPn.Checked)
            {
                Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                TotalNominal = TotalHarga - Discnominal;
                PpnNominal = Math.Ceiling(TotalNominal * 0.1);

                tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                tbTotal.Text = TotalNominal.ToString("#,##0.##");
                tbPPN.Text = PpnNominal.ToString("#,##0.##");

                tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");
            }
            else
            {
                Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                TotalNominal = TotalHarga - Discnominal;
                PpnNominal = 0.00;

                tbTotal.Text = TotalNominal.ToString("#,##0.##");
                tbPPN.Text = PpnNominal.ToString("#,##0.##");
                tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");
            }
        }

        private int ShowLastId()
        {
            MySqlCommand comm = new MySqlCommand("SELECT MAX(Id_penawaran) FROM t_penawaran");
            comm.Connection = dtConn;
            object qryrslt = comm.ExecuteScalar();
            Int32 hasil = (Convert.IsDBNull(qryrslt) ? 0 : Convert.ToInt32(qryrslt));
            //MessageBox.Show("Berhasil mencari nilai " + hasil.ToString());
            return hasil;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            dtConn.Open();
            if (frmMode == FormOpenMode.fomNew)
            {
                Int32 TxID = ShowLastId() + 1;

                //Menyimpan master
                SaveMasterTrx(TxID);

                // Menyimpan detil 
                SaveDtlTrx(TxID);
                UpdateReg(RegNomor, dtTglPO.Value.Year);

                MessageBox.Show("Data telah tersimpan!");
            }
            else if (frmMode == FormOpenMode.fomEdit)
            {
                UpdateMasterTrx(IDTransaksi);
                UpdateDtlTrx(IDTransaksi);
                MessageBox.Show("Data telah terupdate!");
            }
            dtConn.Close();
            KosongkanField();
        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            //if (tbNoPenawaran.Text == "")
            //{
            //    btnGeneratNo_Click(sender, e);
            //}

            dtConn.Open();
            Int32 TxID = ShowLastId() + 1;

            //Menyimpan header/master
            SaveMasterTrx(TxID);

            // Menyimpan detil 
            SaveDtlTrx(TxID);
            UpdateReg(RegNomor, dtTglPO.Value.Year);

            // Mencetak dokumen
            PenawaranPrint PrintPenawaran = new PenawaranPrint();

            PrintPenawaran.Parameters["ParamID"].Value = TxID;
            PrintPenawaran.Parameters["ParamID"].Visible = false;
            using (ReportPrintTool printTool = new ReportPrintTool(PrintPenawaran))
            {
                printTool.ShowPreviewDialog();
            }

            dtConn.Close();
            KosongkanField();
        }

        private void SaveMasterTrx(Int32 TxId)
        {
            MySqlCommand tcmd = new MySqlCommand();

            StringBuilder tSql = new StringBuilder();

            string tglTrx = dtTglPO.Value.ToString("yyyy-MM-dd");

            MySqlParameter prmIdMst = new MySqlParameter("IdMst", TxId);
            MySqlParameter prmtgl = new MySqlParameter("tgl", tglTrx);
            MySqlParameter prmnopenawaran = new MySqlParameter("nopenawaran", tbNoPenawaran.Text);
            MySqlParameter prmidmkt = new MySqlParameter("idmrkt", _userid);
            MySqlParameter prmidcust = new MySqlParameter("idcust", tbCustID.Text);
            MySqlParameter prmJml = new MySqlParameter("jumlah", MySqlDbType.Double);
            MySqlParameter prmDisc = new MySqlParameter("disc", MySqlDbType.Int32);
            MySqlParameter prmPpn = new MySqlParameter("ppn", MySqlDbType.Double);
            MySqlParameter prmKet = new MySqlParameter("ketrangan", tbKetrangan.Text);
            MySqlParameter prmMark = new MySqlParameter("marketing", MySqlDbType.Int32);
            MySqlParameter prmReff = new MySqlParameter("reff", tbReff.Text);
            MySqlParameter prmPengiriman = new MySqlParameter("pengiriman", MySqlDbType.String);
            MySqlParameter prmKredit = new MySqlParameter("kredit", MySqlDbType.Int32);

            tSql.AppendLine("INSERT INTO t_penawaran(id_penawaran, tanggal, No_Penawaran, id_userinput, id_customer, ");
            tSql.AppendLine("    jumlah, disc, ppn, keterangan, id_marketing, reff, input_time, pengiriman, kredit) ");
            tSql.AppendLine("VALUES (@IdMst, @tgl, @nopenawaran, @idmrkt, @idcust, ");
            tSql.AppendLine("    @jumlah, @disc, @ppn, @ketrangan, @marketing, @reff, now(), @pengiriman, @kredit)");

            try
            {
                tcmd.CommandText = tSql.ToString();
                prmJml.Value = TotalHarga;
                prmDisc.Value = (int)Discpersen;
                prmPpn.Value = PpnNominal;
                prmMark.Value = MarketingTable.Rows[tbMarketing.SelectedIndex][0];
                prmPengiriman.Value = tbPengiriman.Text;
                prmKredit.Value = seKredit.Value;
                tcmd.Parameters.Add(prmIdMst);
                tcmd.Parameters.Add(prmtgl);
                tcmd.Parameters.Add(prmnopenawaran);
                tcmd.Parameters.Add(prmidmkt);
                tcmd.Parameters.Add(prmidcust);
                tcmd.Parameters.Add(prmJml);
                tcmd.Parameters.Add(prmDisc);
                tcmd.Parameters.Add(prmPpn);
                tcmd.Parameters.Add(prmKet);
                tcmd.Parameters.Add(prmMark);
                tcmd.Parameters.Add(prmReff);
                tcmd.Parameters.Add(prmPengiriman);
                tcmd.Parameters.Add(prmKredit);
                tcmd.Connection = dtConn;
                tcmd.ExecuteNonQuery();
                //MessageBox.Show("Data Master telah tersimpan");
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void SaveDtlTrx(Int32 TxId)
        {
            MySqlCommand dtlCommand = new MySqlCommand();

            StringBuilder dtlSql = new StringBuilder();
            dtlSql.AppendLine("INSERT INTO dt_penawaran (id_penawaran, id_type, id_ukuran, ");
            dtlSql.AppendLine("   qty, berat, harga, diminta, id_ukdiminta) ");
            dtlSql.AppendLine("VALUES (@idTx, @idbrg, @idukr, @qty, @berat, ");
            dtlSql.AppendLine("  @harga, @minta, @ukminta)");

            try
            {
                MySqlParameter prmIdTx = new MySqlParameter("idTx", MySqlDbType.Int32);
                MySqlParameter prmIdBrg = new MySqlParameter("idbrg", MySqlDbType.Int32);
                MySqlParameter prmQty = new MySqlParameter("qty", MySqlDbType.Int32);
                MySqlParameter prmHrg = new MySqlParameter("harga", MySqlDbType.Double);
                MySqlParameter prmUkuran = new MySqlParameter("idukr", MySqlDbType.Int32);
                MySqlParameter prmminta = new MySqlParameter("minta", MySqlDbType.String);
                MySqlParameter prmberat = new MySqlParameter("berat", MySqlDbType.Double);
                MySqlParameter prmUkMinta = new MySqlParameter("ukminta", MySqlDbType.Int32);

                foreach (DataGridViewRow row in dgDetil.Rows)
                {
                    string brgDiminta = row.Cells[1].Value.ToString();
                    int ukrDiminta = Convert.ToInt32(row.Cells[4].Value);
                    int BrgID = Convert.ToInt32(row.Cells[6].Value);
                    int UkuranID = Convert.ToInt32(row.Cells[10].Value);
                    int BrgQty = Convert.ToInt32(row.Cells[15].Value);
                    Double BeratBrg = Convert.ToDouble(row.Cells[16].Value);
                    Double BrgHarga = Convert.ToDouble(row.Cells[17].Value);

                    dtlCommand.CommandText = dtlSql.ToString();
                    prmIdTx.Value = TxId;
                    dtlCommand.Parameters.Add(prmIdTx);
                    prmIdBrg.Value = BrgID;
                    dtlCommand.Parameters.Add(prmIdBrg);
                    prmQty.Value = BrgQty;
                    dtlCommand.Parameters.Add(prmUkuran);
                    prmUkuran.Value = UkuranID;
                    dtlCommand.Parameters.Add(prmQty);
                    prmHrg.Value = BrgHarga;
                    dtlCommand.Parameters.Add(prmHrg);
                    prmminta.Value = brgDiminta;
                    dtlCommand.Parameters.Add(prmminta);
                    prmberat.Value = BeratBrg;
                    dtlCommand.Parameters.Add(prmberat);
                    prmUkMinta.Value = ukrDiminta;
                    dtlCommand.Parameters.Add(prmUkMinta);
                    dtlCommand.Connection = dtConn;

                    dtlCommand.ExecuteNonQuery();
                    dtlCommand.Parameters.Clear();
                    //MessageBox.Show("Data detil telah tersimpan");
                }
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void UpdateReg(int Nomor, int thn)
        {
            string strcommand = "INSERT INTO reg_no(id, year, val) VALUES (@idreg, @tahun, @regval) " +
                "ON DUPLICATE KEY UPDATE val = @regval";

            MySqlCommand dtlCommand = new MySqlCommand(strcommand, dtConn);

            MySqlParameter prmidreg = new MySqlParameter("idreg", MySqlDbType.Int32);
            MySqlParameter prmyear = new MySqlParameter("tahun", MySqlDbType.Int32);
            MySqlParameter prmval = new MySqlParameter("regval", MySqlDbType.Int32);

            prmidreg.Value = 1;
            prmyear.Value = thn;
            prmval.Value = Nomor;

            dtlCommand.Parameters.Add(prmidreg);
            dtlCommand.Parameters.Add(prmyear);
            dtlCommand.Parameters.Add(prmval);
            dtlCommand.ExecuteNonQuery();
        }

        private void KosongkanField()
        {
            tbCustID.Text = "";
            lbNamaCustomer.Text = "";
            dtTglPO.Text = "";
            tbNoPenawaran.Text = "";
            tbJumlah.Text = "";
            tbDiscPersen.Text = "";
            tbDiscNominal.Text = "";
            tbTotal.Text = "";
            tbPPN.Text = "";
            tbGrandTotal.Text = "";
            tbKetrangan.Text = "";
            tbMarketing.Text = "";
            tbReff.Text = "";
            tbPengiriman.Text = "";
            seKredit.Value = 30;

            int i;
            for (i = dgDetil.RowCount - 1; i >= 0; i--)
            {
                dgDetil.Rows.RemoveAt(i);
            }
        }

        private void tbCustID_Leave(object sender, EventArgs e)
        {
            if (tbCustID.Text != "")
            {
                if (dtConn != null && dtConn.State == ConnectionState.Closed)
                {
                    dtConn.Open();
                }

                string sqlcommand = "SELECT nama_customer FROM m_customer " +
                    "WHERE id_customer = @idcustomer";
                try
                {
                    MySqlCommand sqlcomm = new MySqlCommand(sqlcommand, dtConn);
                    MySqlParameter prmidcust = new MySqlParameter("idcustomer", MySqlDbType.Int32);
                    prmidcust.Value = Int32.Parse(tbCustID.Text);
                    sqlcomm.Parameters.Add(prmidcust);
                    MySqlDataReader dread = sqlcomm.ExecuteReader();
                    while (dread.Read())
                    {
                        lbNamaCustomer.Text = dread.GetString(0);
                    }

                    dread.Close();
                }
                catch (MySqlException ex)
                {
                    MessageBox.Show("Terjadi error di database");
                }

                dtConn.Close();
            }
            else btnCari_Click(sender, e);
        }

        private void InitMarketingData()
        {
            MarketingTable = new DataTable();

            if (dtConn != null && dtConn.State == ConnectionState.Closed)
            {
                dtConn.Open();
            }

            string sqlcommand = "SELECT id_marketing, nama_marketing, inisial FROM m_marketing";
            MySqlDataAdapter MarketingAdapter = new MySqlDataAdapter(sqlcommand, dtConn);
            MarketingAdapter.Fill(MarketingTable);
            tbMarketing.DataSource = MarketingTable;
            tbMarketing.DisplayMember = "nama_marketing";

            dtConn.Close();
        }

        private void UpdateMasterTrx(int TxId)
        {
            MySqlCommand tcmd = new MySqlCommand();

            StringBuilder tSql = new StringBuilder();

            string tglTrx = dtTglPO.Value.ToString("yyyy-MM-dd");

            MySqlParameter prmIdMst = new MySqlParameter("IdMst", TxId);
            MySqlParameter prmtgl = new MySqlParameter("tgl", tglTrx);
            MySqlParameter prmnopenawaran = new MySqlParameter("nopenawaran", tbNoPenawaran.Text);
            //MySqlParameter prmidmkt = new MySqlParameter("idmrkt", _userid);
            MySqlParameter prmidcust = new MySqlParameter("idcust", tbCustID.Text);
            MySqlParameter prmJml = new MySqlParameter("jumlah", MySqlDbType.Double);
            MySqlParameter prmDisc = new MySqlParameter("disc", MySqlDbType.Int32);
            MySqlParameter prmPpn = new MySqlParameter("ppn", MySqlDbType.Double);
            MySqlParameter prmKet = new MySqlParameter("ketrangan", tbKetrangan.Text);
            MySqlParameter prmMark = new MySqlParameter("marketing", MySqlDbType.Int32);
            MySqlParameter prmReff = new MySqlParameter("reff", tbReff.Text);
            MySqlParameter prmPengiriman = new MySqlParameter("pengiriman", MySqlDbType.String);
            MySqlParameter prmKredit = new MySqlParameter("kredit", MySqlDbType.Int32);

            tSql.AppendLine("UPDATE t_penawaran SET tanggal = @tgl, No_Penawaran = @nopenawaran, ");
            tSql.AppendLine("  id_customer = @idcust, jumlah = @jumlah, disc = @disc, ppn = @ppn, ");
            tSql.AppendLine("  keterangan = @ketrangan, id_marketing = @marketing, reff = @reff, ");
            tSql.AppendLine("  pengiriman = @pengiriman, kredit = @kredit ");
            tSql.AppendLine("WHERE id_penawaran = @IdMst");

            try
            {
                tcmd.CommandText = tSql.ToString();
                prmJml.Value = TotalHarga;
                prmDisc.Value = (int)Discpersen;
                prmPpn.Value = PpnNominal;
                prmMark.Value = MarketingTable.Rows[tbMarketing.SelectedIndex][0];
                prmPengiriman.Value = tbPengiriman.Text;
                prmKredit.Value = seKredit.Value;
                tcmd.Parameters.Add(prmIdMst);
                tcmd.Parameters.Add(prmtgl);
                tcmd.Parameters.Add(prmnopenawaran);
                //tcmd.Parameters.Add(prmidmkt);
                tcmd.Parameters.Add(prmidcust);
                tcmd.Parameters.Add(prmJml);
                tcmd.Parameters.Add(prmDisc);
                tcmd.Parameters.Add(prmPpn);
                tcmd.Parameters.Add(prmKet);
                tcmd.Parameters.Add(prmMark);
                tcmd.Parameters.Add(prmReff);
                tcmd.Parameters.Add(prmPengiriman);
                tcmd.Parameters.Add(prmKredit);
                tcmd.Connection = dtConn;
                tcmd.ExecuteNonQuery();
                //MessageBox.Show("Data Master telah tersimpan");
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void LoadMasterData(Int32 MasterId)
        {
            dtConn.Open();
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT p.No_Penawaran, p.tanggal, p.id_customer, c.NAMA_CUSTOMER, ");
            qry.AppendLine("    p.pengiriman, p.kredit, p.id_marketing, m.nama_marketing, p.reff, ");
            qry.AppendLine("    p.keterangan, p.jumlah, p.disc, p.ppn ");
            qry.AppendLine("FROM t_penawaran p");
            qry.AppendLine("INNER JOIN m_customer c ON p.id_customer = c.ID_CUSTOMER");
            qry.AppendLine("INNER JOIN m_marketing m ON p.id_marketing = m.id_marketing");
            qry.AppendLine("WHERE p.id_penawaran = @Key");

            MySqlCommand edComm = new MySqlCommand(qry.ToString(), dtConn);

            MySqlParameter Kid = new MySqlParameter("Key", MySqlDbType.Int32);
            Kid.Value = MasterId;
            //MessageBox.Show(MasterId.ToString());
            edComm.Parameters.Add(Kid);

            MySqlDataReader rd = edComm.ExecuteReader();

            while (rd.Read())
            {
                PlaceTheData((IDataRecord)rd);
            }

            rd.Close();
            LoadDetilData(MasterId);
            dtConn.Close();
        }

        private void PlaceTheData(IDataRecord recrd)
        {
            dtTglPO.Text = recrd[1].ToString();
            tbCustID.Text = recrd[2].ToString();
            lbNamaCustomer.Text = recrd[3].ToString();
            tbPengiriman.Text = recrd[4].ToString();
            seKredit.Value = recrd.GetInt32(5);
            tbNoPenawaran.Text = recrd[0].ToString();
            tbMarketing.SelectedIndex = tbMarketing.FindString(recrd[7].ToString());
            tbReff.Text = recrd[8].ToString();

            tbKetrangan.Text = recrd[9].ToString();
            TotalHarga = recrd.GetDouble(10);
            tbJumlah.Text = TotalHarga.ToString("#,##0.##");
            Discpersen = recrd.GetInt32(11);
            Discnominal = Math.Round(TotalHarga * (Discpersen / 100),2 , MidpointRounding.AwayFromZero);
            tbDiscPersen.Text = Discpersen.ToString();
            tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
            PpnNominal = recrd.GetDouble(12);
            cbPPn.Checked = (PpnNominal > 0);
            tbPPN.Text = PpnNominal.ToString("#,##0.##");

            TotalNominal = TotalHarga - Discnominal;

            tbTotal.Text = TotalNominal.ToString("#,##0.##");

            tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

        }

        private void LoadDetilData(Int32 DetilID)
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT dp.id_dtpenawaran, dp.diminta, dp.id_ukdiminta, mu0.nama_ukuran As nama_ukuran_minta, ");
            qry.AppendLine("    dp.id_type, mt.`type`, dp.id_ukuran, mu.nama_ukuran, mu.lebar, ");
            qry.AppendLine("    mu.panjang, mu.tinggi, mu.faktor_hitung, dp.qty, dp.berat, dp.harga, ");
            qry.AppendLine("    mu0.faktor_hitung as fkt_htg");
            qry.AppendLine("FROM dt_penawaran dp");
            qry.AppendLine("INNER JOIN m_type mt ON dp.id_type = mt.id_type");
            qry.AppendLine("INNER JOIN m_ukuran mu0 ON mu0.id_ukuran = dp.id_ukdiminta");
            qry.AppendLine("INNER JOIN m_ukuran mu ON dp.id_ukuran = mu.id_ukuran");
            qry.AppendLine("WHERE dp.id_penawaran = @Key");

            MySqlCommand edComm = new MySqlCommand(qry.ToString(), dtConn);

            MySqlParameter Kid = new MySqlParameter("Key", MySqlDbType.Int32);
            Kid.Value = DetilID;
            edComm.Parameters.Add(Kid);

            MySqlDataAdapter dtAdapter = new MySqlDataAdapter(edComm);
            DetilTable = new DataTable();
            dtAdapter.Fill(DetilTable);

            CloneDetilTbl();

            DetilTable.Columns.Add("edited", typeof(bool));
            DetilTable.Columns.Add("deleted", typeof(bool));
            PlaceDetilData();
        }

        private void PlaceDetilData()
        {
            double faktor = 0;
            string nama_ukuran = "";
            int HrgSatuan;
            Double Berat;
            Double Jumlah;
            foreach (DataRow row in DetilTable.Rows)
            {
                DataGridViewRow rw = new DataGridViewRow();
                rw.CreateCells(dgDetil);

                rw.Cells[1].Value = row[1].ToString();    // Diminta
                //rw.Cells[2].Value = "";                 {button}
                faktor = double.Parse(row[15].ToString());
                if (faktor == 6.20)
                {
                    nama_ukuran = "Ø " + row[3].ToString();
                }
                else if (faktor == 8.00)
                {
                    nama_ukuran = "≠ " + row[3].ToString();
                }
                rw.Cells[3].Value = nama_ukuran;    // nama_ukuran_minta
                rw.Cells[4].Value = row[2].ToString();    // Id_ukdiminta 
                //rw.Cells[5].Value = row[4].ToString();   {button}
                rw.Cells[6].Value = row[4].ToString();    // Id_type
                rw.Cells[7].Value = row[5].ToString();    // type
                //rw.Cells[8].Value = row[10].ToString();  {button}
                faktor = double.Parse(row[11].ToString());
                if (faktor == 6.20)
                {
                    nama_ukuran = "Ø " + row[7].ToString();
                }
                else if (faktor == 8.00)
                {
                    nama_ukuran = "≠ " + row[7].ToString();
                }
                rw.Cells[9].Value = nama_ukuran;    // Nama Ukuran
                rw.Cells[10].Value = row[6].ToString();   // id_ukuran
                rw.Cells[11].Value = row[8].ToString();   // lebar
                rw.Cells[12].Value = row[9].ToString();   // panjang
                rw.Cells[13].Value = row[10].ToString();  // tinggi
                rw.Cells[14].Value = row[11].ToString();  // faktor
                rw.Cells[15].Value = row[12].ToString();  // qty 

                HrgSatuan = int.Parse(row[14].ToString());
                Berat = Double.Parse(row[13].ToString());
                Jumlah = Math.Round((Berat * HrgSatuan), 2, MidpointRounding.AwayFromZero);

                rw.Cells[16].Value = Berat.ToString("#,##0.##");      // berat
                rw.Cells[17].Value = HrgSatuan.ToString("#,##0.##");  // harga/kg
                rw.Cells[18].Value = Jumlah.ToString("#,##0.##");     // jumlah
                rw.Cells[19].Value = row[0].ToString();               // id_dtlpenawaran
                row["edited"] = false;
                row["deleted"] = false;
                dgDetil.Rows.Add(rw);
            }
        }

        private void CekEdited()
        {
            // Untuk mengecek setiap row mana yang sudah diedit/diubah
            int idrow;
            DataRow drow;
            string sqldelcommand;
            StringBuilder dtlSql = new StringBuilder();
            MySqlCommand command = new MySqlCommand();

            dtlSql.AppendLine("DELETE FROM dt_penawaran WHERE id_dtpenawaran = @idDtl;");

            dtlSql.AppendLine("INSERT INTO dt_penawaran (id_penawaran, id_type, id_ukuran, ");
            dtlSql.AppendLine("   qty, berat, harga, diminta, id_ukdiminta) ");
            dtlSql.AppendLine("VALUES (@idTx, @idbrg, @idukr, @qty, @berat, ");
            dtlSql.AppendLine("  @harga, @minta, @ukminta);");

            MySqlParameter prmIdTx = new MySqlParameter("idTx", MySqlDbType.Int32);
            MySqlParameter prmIdBrg = new MySqlParameter("idbrg", MySqlDbType.Int32);
            MySqlParameter prmQty = new MySqlParameter("qty", MySqlDbType.Int32);
            MySqlParameter prmHrg = new MySqlParameter("harga", MySqlDbType.Double);
            MySqlParameter prmUkuran = new MySqlParameter("idukr", MySqlDbType.Int32);
            MySqlParameter prmminta = new MySqlParameter("minta", MySqlDbType.String);
            MySqlParameter prmberat = new MySqlParameter("berat", MySqlDbType.Double);
            MySqlParameter prmUkMinta = new MySqlParameter("ukminta", MySqlDbType.Int32);
            MySqlParameter prmIdDtlTx = new MySqlParameter("idDtl", MySqlDbType.Int32);

            string brgDiminta;
            int ukrDiminta;
            int BrgID;
            int UkuranID;
            int BrgQty;
            Double BeratBrg;
            Double BrgHarga;
            foreach (DataGridViewRow dgrow in dgDetil.Rows)
            {
                drow = DetilTable.Rows[dgrow.Index];
                idrow = int.Parse(drow[0].ToString());
                //cek apakah id row di datagridview sama dengan yang di Datatable
                //kalau di datagridview id row kosong berarti row yang lama sudah dihapus
                //dan jika sudah dihapus di datagridview maka harus dihapus dari table
                if (dgrow.Cells[19].ToString() == "")
                {
                    //hapus dari table untuk id row ini
                    command.Connection = dtConn;
                    prmIdDtlTx.Value = idrow;
                    command.Parameters.Add(prmIdDtlTx);

                    //setelah dihapus, insert baru
                    brgDiminta = dgrow.Cells[1].Value.ToString();
                    ukrDiminta = Convert.ToInt32(dgrow.Cells[4].Value);
                    BrgID = Convert.ToInt32(dgrow.Cells[6].Value);
                    UkuranID = Convert.ToInt32(dgrow.Cells[10].Value);
                    BrgQty = Convert.ToInt32(dgrow.Cells[15].Value);
                    BeratBrg = Convert.ToDouble(dgrow.Cells[16].Value);
                    BrgHarga = Convert.ToDouble(dgrow.Cells[17].Value);

                    command.CommandText = dtlSql.ToString();
                    prmIdTx.Value = IDTransaksi;
                    command.Parameters.Add(prmIdTx);
                    prmIdBrg.Value = BrgID;
                    command.Parameters.Add(prmIdBrg);
                    prmQty.Value = BrgQty;
                    command.Parameters.Add(prmUkuran);
                    prmUkuran.Value = UkuranID;
                    command.Parameters.Add(prmQty);
                    prmHrg.Value = BrgHarga;
                    command.Parameters.Add(prmHrg);
                    prmminta.Value = brgDiminta;
                    command.Parameters.Add(prmminta);
                    prmberat.Value = BeratBrg;
                    command.Parameters.Add(prmberat);
                    prmUkMinta.Value = ukrDiminta;
                    command.Parameters.Add(prmUkMinta);
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();
                }
            }
        }

        private void MarkDeletedRow(int idDtl)
        {
            int i = 0;
            bool ketemu = false;
            while (ketemu == false)
            {
                if (Convert.ToInt32(DetilTable.Rows[i]["id_dtpenawaran"].ToString()) == idDtl)
                {
                    DetilTable.Rows[i]["deleted"] = true;
                    ketemu = true;
                }
                i++;
            }
        }

        private void AddToDeletedRow(int idx)
        {
            DataRow deletedRow = DeletedTable.NewRow();
            string sHrgSat0, sHargaSat;

            deletedRow[0] = dgDetil.Rows[idx].Cells[19];   // id_dtPenawran
            deletedRow[1] = dgDetil.Rows[idx].Cells[1];    // diminta
            deletedRow[2] = dgDetil.Rows[idx].Cells[4];    // Id_ukdiminta
            deletedRow[3] = dgDetil.Rows[idx].Cells[3];    // nama_ukuran_minta
            deletedRow[4] = dgDetil.Rows[idx].Cells[6];    // id_type
            deletedRow[5] = dgDetil.Rows[idx].Cells[7];    // type
            deletedRow[6] = dgDetil.Rows[idx].Cells[10];   // Id_ukuran
            deletedRow[7] = dgDetil.Rows[idx].Cells[9];    // Nama Ukuran
            deletedRow[8] = dgDetil.Rows[idx].Cells[11];   // Lebar
            deletedRow[9] = dgDetil.Rows[idx].Cells[12];   // Panjang
            deletedRow[10] = (dgDetil.Rows[idx].Cells[13].Value.ToString() == "") ?
                DBNull.Value : dgDetil.Rows[idx].Cells[13].Value;  // tinggi
            deletedRow[11] = dgDetil.Rows[idx].Cells[14];  // faktor hitung
            deletedRow[12] = dgDetil.Rows[idx].Cells[15];  // qty
            deletedRow[13] = dgDetil.Rows[idx].Cells[16];  // berat

            sHrgSat0 = (dgDetil.Rows[idx].Cells[17].Value == null) ?
                "0" : dgDetil.Rows[idx].Cells[17].Value.ToString();
            sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());

            deletedRow[14] = sHargaSat;                    // harga / kg
            deletedRow[15] = dgDetil.Rows[idx].Cells[14];  // faktor hitung minta
            DeletedTable.Rows.Add(deletedRow);
        }

        private void AddToEditedRow(int idx)
        {
            DataRow EditedRow = EditedTable.NewRow();
            string sHrgSat0, sHargaSat;

            EditedRow[0] = dgDetil.Rows[idx].Cells[19].Value;   // id_dtPenawran
            EditedRow[1] = dgDetil.Rows[idx].Cells[1].Value;    // diminta
            EditedRow[2] = dgDetil.Rows[idx].Cells[4].Value;    // Id_ukdiminta
            EditedRow[3] = dgDetil.Rows[idx].Cells[3].Value;    // nama_ukuran_minta
            EditedRow[4] = dgDetil.Rows[idx].Cells[6].Value;    // id_type
            EditedRow[5] = dgDetil.Rows[idx].Cells[7].Value;    // type
            EditedRow[6] = dgDetil.Rows[idx].Cells[10].Value;   // Id_ukuran
            EditedRow[7] = dgDetil.Rows[idx].Cells[9].Value;    // Nama Ukuran
            EditedRow[8] = dgDetil.Rows[idx].Cells[11].Value;   // Lebar
            EditedRow[9] = dgDetil.Rows[idx].Cells[12].Value;   // Panjang
            EditedRow[10] = (dgDetil.Rows[idx].Cells[13].Value.ToString() == "") ? 
                DBNull.Value : dgDetil.Rows[idx].Cells[13].Value;  // tinggi
            EditedRow[11] = dgDetil.Rows[idx].Cells[14].Value;  // faktor hitung
            EditedRow[12] = dgDetil.Rows[idx].Cells[15].Value;  // qty
            EditedRow[13] = dgDetil.Rows[idx].Cells[16].Value;  // berat

            sHrgSat0 = (dgDetil.Rows[idx].Cells[17].Value == null) ?
                "0" : dgDetil.Rows[idx].Cells[17].Value.ToString();
            sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
            EditedRow[14] = sHargaSat;                          // harga / kg
            EditedRow[15] = dgDetil.Rows[idx].Cells[14].Value;  // faktor hitung minta
            EditedTable.Rows.Add(EditedRow);
        }

        private void AddToInsertedRow(int idx)
        {
            DataRow InserteddRow = InsertedTable.NewRow();
            string sHrgSat0, sHargaSat;

            InserteddRow[0] = idx;                           // masih ksong jadi diisi dengan gridrow index
            InserteddRow[1] = dgDetil.Rows[idx].Cells[1];    // diminta
            InserteddRow[2] = dgDetil.Rows[idx].Cells[4];    // Id_ukdiminta
            InserteddRow[3] = dgDetil.Rows[idx].Cells[3];    // nama_ukuran_minta
            InserteddRow[4] = dgDetil.Rows[idx].Cells[6];    // id_type
            InserteddRow[5] = dgDetil.Rows[idx].Cells[7];    // type
            InserteddRow[6] = dgDetil.Rows[idx].Cells[10];   // Id_ukuran
            InserteddRow[7] = dgDetil.Rows[idx].Cells[9];    // Nama Ukuran
            InserteddRow[8] = dgDetil.Rows[idx].Cells[11];   // Lebar
            InserteddRow[9] = dgDetil.Rows[idx].Cells[12];   // Panjang
            InserteddRow[10] = (dgDetil.Rows[idx].Cells[13].Value.ToString() == "") ?
                DBNull.Value : dgDetil.Rows[idx].Cells[13].Value;  // tinggi
            InserteddRow[11] = dgDetil.Rows[idx].Cells[14];  // faktor hitung
            InserteddRow[12] = dgDetil.Rows[idx].Cells[15];  // qty
            InserteddRow[13] = dgDetil.Rows[idx].Cells[16];  // berat

            sHrgSat0 = (dgDetil.Rows[idx].Cells[17].Value == null) ?
                "0" : dgDetil.Rows[idx].Cells[17].Value.ToString();
            sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
            InserteddRow[14] = sHargaSat;                    // harga / kg
            InserteddRow[15] = dgDetil.Rows[idx].Cells[14];  // faktor hitung minta
        }

        private void CloneDetilTbl()
        {
            DeletedTable = DetilTable.Clone();
            EditedTable = DetilTable.Clone();
            InsertedTable = DetilTable.Clone();
        }

        private void AddNewUkuranMinta(int RowIdx)
        {
            // Menampilkan list ukuran untuk dipilih
            fr_FindUkuran LstUkuran = new fr_FindUkuran();
            LstUkuran.ShowDialog(this);
            if (LstUkuran.DialogResult == DialogResult.OK)
            {
                // Memindahkan NamaUkuran dan IdUkuran dari listUkuran ke grid
                dgDetil.Rows[RowIdx].Cells[3].Value = LstUkuran.NamaUkuran;
                dgDetil.Rows[RowIdx].Cells[4].Value = LstUkuran.IDUkuran;
            }
            LstUkuran.Dispose();
        }

        private void EditingNewUkuranMinta(int RowIdx)
        {
            string OldIdUkuran;
            int Id_dtl;
            int TblrowIndex;

            // Pindahkan IdUkuran di grid ke OldIdUkuran
            OldIdUkuran = (dgDetil.Rows[RowIdx].Cells[4].Value == null) ?
                "" : dgDetil.Rows[RowIdx].Cells[4].Value.ToString();
            AddNewUkuranMinta(RowIdx);

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[19].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                // Cek jika OldIdUkuran != (IdUkuran di grid), maka editing
                if (OldIdUkuran != dgDetil.Rows[RowIdx].Cells[4].Value.ToString())
                {
                    // Add/Edit EditedTable
                    // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                    TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                    if (TblrowIndex != -1)
                    {
                        // Update EditedTable
                        // Update idnya saja, karena namanya tidak perlu diupdate
                        DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 2, dgDetil.Rows[RowIdx].Cells[4].Value);
                    }
                    else
                    {
                        AddToEditedRow(RowIdx);
                    }
                }
            }
            else
            {
                // Update InsertedTable
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[20].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 2, dgDetil.Rows[RowIdx].Cells[4].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[20].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 2, dgDetil.Rows[RowIdx].Cells[4].Value);
                }
            }
        }

        private void AddNewUkuranTawar(int RowIdx)
        {
            fr_FindUkuran LstUkuran = new fr_FindUkuran();
            LstUkuran.ShowDialog(this);
            if (LstUkuran.DialogResult == DialogResult.OK)
            {

                dgDetil.Rows[RowIdx].Cells[9].Value = LstUkuran.NamaUkuran;
                dgDetil.Rows[RowIdx].Cells[10].Value = LstUkuran.IDUkuran;
                dgDetil.Rows[RowIdx].Cells[11].Value = LstUkuran.Lebar;
                dgDetil.Rows[RowIdx].Cells[12].Value = LstUkuran.Panjang;
                dgDetil.Rows[RowIdx].Cells[13].Value = LstUkuran.Tebal;
                dgDetil.Rows[RowIdx].Cells[14].Value = LstUkuran.fkt_hitung;
                // fokus to the next cell
                dgDetil.CurrentCell = dgDetil.Rows[RowIdx].Cells[15];
            }
            LstUkuran.Dispose();
        }

        private void EditingNewUkuranTawar(int RowIdx)
        {
            string OldIdUkuran;
            int Id_dtl;
            int TblrowIndex;

            // Pindahkan IdUkuran di grid ke OldIdUkuran
            OldIdUkuran = (dgDetil.Rows[RowIdx].Cells[10].Value == null) ?
                "" : dgDetil.Rows[RowIdx].Cells[10].Value.ToString();

            AddNewUkuranTawar(RowIdx);

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[19].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                // Cek jika OldIdUkuran != (IdUkuran di grid), maka editing
                if (OldIdUkuran != dgDetil.Rows[RowIdx].Cells[10].Value.ToString())
                {
                    // Add/Edit EditedTable
                    // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                    TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                    if (TblrowIndex != -1)
                    {
                        // Update EditedTable
                        // Update idnya saja, karena namanya tidak perlu diupdate
                        DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 6, dgDetil.Rows[RowIdx].Cells[10].Value);
                    }
                    else
                    {
                        AddToEditedRow(RowIdx);
                    }
                }
            }
            else
            {
                // Update InsertedTable
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[20].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 6, dgDetil.Rows[RowIdx].Cells[10].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[20].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 6, dgDetil.Rows[RowIdx].Cells[10].Value);
                }
            }
        }

        private void AddNewType(int RowIdx)
        {
            fr_listBarang LstBarang = new fr_listBarang();
            LstBarang.ShowDialog(this);
            dgDetil.Rows[RowIdx].Cells[6].Value = LstBarang.SelectedTypeID;
            dgDetil.Rows[RowIdx].Cells[7].Value = LstBarang.SelectedTypeNama;
            LstBarang.Dispose();
        }

        private void EditingType(int RowIdx)
        {
            string OldIdType;
            int Id_dtl;
            int TblrowIndex;

            OldIdType = (dgDetil.Rows[RowIdx].Cells[6].Value == null) ?
                "" : dgDetil.Rows[RowIdx].Cells[6].Value.ToString();

            AddNewType(RowIdx);

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[19].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                // Cek jika OldIdType != (Idtype di grid), maka editing
                if (OldIdType != dgDetil.Rows[RowIdx].Cells[6].Value.ToString())
                {
                    // Add/Edit EditedTable
                    // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                    TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                    if (TblrowIndex != -1)
                    {
                        // Update EditedTable
                        // Update idnya saja, karena namanya tidak perlu diupdate
                        DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 4, dgDetil.Rows[RowIdx].Cells[6].Value);
                    }
                    else
                    {
                        AddToEditedRow(RowIdx);
                    }
                }
            }
            else
            {
                // Update InsertedTable
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[20].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 4, dgDetil.Rows[RowIdx].Cells[6].Value);
                }
                else
                {                    
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[20].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 4, dgDetil.Rows[RowIdx].Cells[6].Value);
                }
            }
        }

        private void EditingDiminta(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[19].Value != null) 
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 4, dgDetil.Rows[RowIdx].Cells[1].Value);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }

            } else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[20].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 1, dgDetil.Rows[RowIdx].Cells[1].Value);
                } else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[20].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 1, dgDetil.Rows[RowIdx].Cells[1].Value);
                }
            }
        }

        private void EditingQty(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[19].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 12, dgDetil.Rows[RowIdx].Cells[15].Value);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[20].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 12, dgDetil.Rows[RowIdx].Cells[15].Value);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[20].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 12, dgDetil.Rows[RowIdx].Cells[15].Value);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
            }
        }

        private void EditingBerat(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[19].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[20].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[20].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
            }
        }

        private void EditingHarga(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;
            string sHrgSat0 = (dgDetil.Rows[RowIdx].Cells[17].Value == null) ?
                "0" : dgDetil.Rows[RowIdx].Cells[17].Value.ToString();
            string sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
            Int32 hrg = Convert.ToInt32(sHargaSat);

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[19].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 14, hrg);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[20].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 14, hrg);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[20].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 14, hrg);
                }
            }
        }

        private void UpdateDtlTrx(int TxId)
        {
            int i, rowCount;

            rowCount = DeletedTable.Rows.Count;
            if (rowCount > 0)
            {
                foreach (DataRow rw in DeletedTable.Rows)
                {
                    DeleteDetil(Convert.ToInt32(rw[0].ToString()));
                }
            }

            rowCount = EditedTable.Rows.Count;
            if (rowCount > 0)
            {
                foreach (DataRow rw in EditedTable.Rows)
                {
                    UpdateDetil(rw);
                }
            }

            rowCount = InsertedTable.Rows.Count;
            if (rowCount > 0)
            {
                foreach (DataRow rw in InsertedTable.Rows)
                {
                    InsertDetil(rw, TxId);
                }
            }
        }

        private void DeleteDetil(int TxId)
        {
            string strSqlCmd = "DELETE FROM dt_penawaran WHERE id_dt_penawaran = @idkey";
            MySqlCommand command = new MySqlCommand(strSqlCmd, dtConn);
            MySqlParameter prmId = new MySqlParameter("idkey", MySqlDbType.Int32);

            prmId.Value = TxId;
            command.Parameters.Add(prmId);
            command.ExecuteNonQuery();
        }

        private void UpdateDetil(DataRow row)
        {
            StringBuilder dtlSql = new StringBuilder();
            dtlSql.AppendLine("UPDATE dt_penawaran SET id_type = @idbrg, ");
            dtlSql.AppendLine("    id_ukuran = @idukr, qty = @qty, berat = @berat, harga = @harga, ");
            dtlSql.AppendLine("    diminta = @minta, id_ukdiminta = @ukminta ");
            dtlSql.AppendLine("WHERE id_dtpenawaran = @iddttx");

            MySqlParameter prmIdtype = new MySqlParameter("Idbrg", MySqlDbType.Int32);
            MySqlParameter prmIdUkurTawar = new MySqlParameter("idukr", MySqlDbType.Int32);
            MySqlParameter prmQty = new MySqlParameter("qty", MySqlDbType.Int32);
            MySqlParameter prmBerat = new MySqlParameter("berat", MySqlDbType.Double);
            MySqlParameter prmHarga = new MySqlParameter("harga", MySqlDbType.Int32);
            MySqlParameter prmMinta = new MySqlParameter("minta", MySqlDbType.VarChar);
            MySqlParameter prmIdUkurMinta = new MySqlParameter("ukminta", MySqlDbType.UInt32);
            MySqlParameter prmIdDtTx = new MySqlParameter("iddttx", MySqlDbType.Int32);

            prmIdtype.Value = Convert.ToInt32(row[4].ToString());
            prmIdUkurTawar.Value = Convert.ToInt32(row[6].ToString());
            prmQty.Value = Convert.ToInt32(row[12].ToString());
            prmBerat.Value = Convert.ToDouble(row[13].ToString());
            prmHarga.Value = Convert.ToInt32(row[14].ToString());
            prmMinta.Value = row[1].ToString();
            prmIdUkurMinta.Value = Convert.ToInt32(row[2].ToString());
            prmIdDtTx.Value = Convert.ToInt32(row[0].ToString());

            MySqlCommand command = new MySqlCommand(dtlSql.ToString(), dtConn);
            command.Parameters.Add(prmIdtype);
            command.Parameters.Add(prmIdUkurTawar);
            command.Parameters.Add(prmQty);
            command.Parameters.Add(prmBerat);
            command.Parameters.Add(prmHarga);
            command.Parameters.Add(prmMinta);
            command.Parameters.Add(prmIdUkurMinta);
            command.Parameters.Add(prmIdDtTx);
            command.ExecuteNonQuery();

            command.Parameters.Clear();
        }

        private void InsertDetil(DataRow row, int TxId)
        {
            StringBuilder dtlSql = new StringBuilder();
            dtlSql.AppendLine("INSERT INTO dt_penawaran (id_penawaran, id_type, id_ukuran, ");
            dtlSql.AppendLine("   qty, berat, harga, diminta, id_ukdiminta) ");
            dtlSql.AppendLine("VALUES (@idTx, @idbrg, @idukr, @qty, @berat, ");
            dtlSql.AppendLine("  @harga, @minta, @ukminta)");

            try
            {
                MySqlCommand dtlCommand = new MySqlCommand(dtlSql.ToString(), dtConn);

                MySqlParameter prmIdTx = new MySqlParameter("idTx", MySqlDbType.Int32);
                MySqlParameter prmIdBrg = new MySqlParameter("idbrg", MySqlDbType.Int32);
                MySqlParameter prmQty = new MySqlParameter("qty", MySqlDbType.Int32);
                MySqlParameter prmHrg = new MySqlParameter("harga", MySqlDbType.Double);
                MySqlParameter prmUkuran = new MySqlParameter("idukr", MySqlDbType.Int32);
                MySqlParameter prmminta = new MySqlParameter("minta", MySqlDbType.VarChar);
                MySqlParameter prmberat = new MySqlParameter("berat", MySqlDbType.Double);
                MySqlParameter prmUkMinta = new MySqlParameter("ukminta", MySqlDbType.Int32);

                prmIdTx.Value = TxId;
                prmIdBrg.Value = Convert.ToInt32(row[4].ToString());
                prmQty.Value = Convert.ToInt32(row[12].ToString());
                prmUkuran.Value = Convert.ToInt32(row[6].ToString());
                prmHrg.Value = Convert.ToInt32(row[14].ToString());
                prmminta.Value = row[1].ToString();
                prmberat.Value = Convert.ToDouble(row[13].ToString());
                prmUkMinta.Value = Convert.ToInt32(row[2].ToString());

                dtlCommand.Parameters.Add(prmIdTx);
                dtlCommand.Parameters.Add(prmIdBrg);
                dtlCommand.Parameters.Add(prmUkuran);
                dtlCommand.Parameters.Add(prmQty);
                dtlCommand.Parameters.Add(prmHrg);
                dtlCommand.Parameters.Add(prmminta);
                dtlCommand.Parameters.Add(prmberat);
                dtlCommand.Parameters.Add(prmUkMinta);

                dtlCommand.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

    }
}
