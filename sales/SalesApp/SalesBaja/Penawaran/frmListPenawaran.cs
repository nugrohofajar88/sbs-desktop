﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SalesBaja
{
    public partial class fr_ListPenawaran : Form
    {
        private static fr_ListPenawaran openForm = null;

        // No need for locking - you'll be doing all this on the UI thread...
        public static fr_ListPenawaran GetInstance()
        {
            if (openForm == null)
            {
                openForm = new fr_ListPenawaran();
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        private MySqlConnection dtConn;
        private MySqlDataAdapter dtAdapter;
        private MySqlDataAdapter detilDtAdpater;
        private DataTable masterTbl;
        private DataTable detilTbl;

        private string dtConnString;

        public fr_ListPenawaran()
        {
            InitializeComponent();
            dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
                GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                GlobVar.hdbase;
            dtConn = new MySqlConnection(dtConnString);
            dtConn.Open();
        }

        public void FillMasterView()
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT p.id_penawaran, p.tanggal, p.No_Penawaran, p.id_customer, c.nama_customer, ");
            qry.AppendLine("    p.id_marketing, m.nama_marketing, p.pengiriman, p.kredit, p.id_userinput, u.username");
            qry.AppendLine("FROM t_penawaran p");
            qry.AppendLine("INNER JOIN m_user u ON p.id_userinput = u.Id_user");
            qry.AppendLine("INNER JOIN m_customer c ON p.Id_customer = c.id_customer");
            qry.AppendLine("INNER JOIN m_marketing m ON m.id_marketing = p.id_marketing");
            //qry.AppendLine("WHERE user_marketing = " + Userid.ToString());
            //MessageBox.Show(qry.ToString());
            try
            {
                dtAdapter = new MySqlDataAdapter(qry.ToString(), dtConn);
                masterTbl = new DataTable();
                dtAdapter.Fill(masterTbl);
                dgvMaster.DataSource = masterTbl;
                SetMasterGridColumn();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetMasterGridColumn()
        {
            dgvMaster.Columns[0].Visible = false;
            dgvMaster.Columns[1].HeaderText = "Tanggal Penawaran";
            dgvMaster.Columns[1].Width = 70;
            dgvMaster.Columns[2].HeaderText = "Nomor Penawaran";
            dgvMaster.Columns[2].Width = 105;
            dgvMaster.Columns[3].Visible = false;
            dgvMaster.Columns[4].HeaderText = "Customer";
            dgvMaster.Columns[4].Width = 125;
            dgvMaster.Columns[5].Visible = false; // id_marketing;
            dgvMaster.Columns[6].HeaderText = "Marketing";
            dgvMaster.Columns[6].Width = 90;
            dgvMaster.Columns[7].HeaderText = "Pengiriman";
            dgvMaster.Columns[7].Width = 125;
            dgvMaster.Columns[8].HeaderText = "Kredit";
            dgvMaster.Columns[8].Width = 45;
            dgvMaster.Columns[9].Visible = false;
            dgvMaster.Columns[10].Visible = false;
        }

        private void SetDetilGridColumns()
        {
            dgvDetil.Columns[0].Visible = false;
            dgvDetil.Columns[1].Visible = false;
            dgvDetil.Columns[2].HeaderText = "Diminta";
            dgvDetil.Columns[3].Visible = false;
            dgvDetil.Columns[4].HeaderText = "Ukuran Diminta";
            dgvDetil.Columns[5].Visible = false;
            dgvDetil.Columns[6].HeaderText = "Type Ditawarkan";
            dgvDetil.Columns[7].Visible = false;
            dgvDetil.Columns[8].HeaderText = "Ukuran Ditawarkan";
            dgvDetil.Columns[9].HeaderText = "Qty";
            dgvDetil.Columns[10].HeaderText = "Berat";
            dgvDetil.Columns[11].HeaderText = "Harga";
        }

        private void FillDetilView(int MasterId)
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT d.id_penawaran, d.id_dtpenawaran, d.diminta, d.id_ukdiminta, u2.nama_ukuran,");
            qry.AppendLine("  d.id_type, t.type, d.id_ukuran, u.nama_ukuran, d.qty, d.berat, d.harga");
            qry.AppendLine("FROM dt_penawaran d");
            qry.AppendLine("INNER JOIN m_type t ON d.id_type = t.id_type");
            qry.AppendLine("INNER JOIN m_ukuran u ON d.id_ukuran = u.id_ukuran");
            qry.AppendLine("INNER JOIN m_ukuran u2 ON d.id_ukdiminta = u2.id_ukuran");
            qry.AppendLine("WHERE id_penawaran = @IdMaster");

            MySqlCommand ViewCommand = new MySqlCommand(qry.ToString(), dtConn);
            MySqlParameter paramId = new MySqlParameter("IdMaster", MasterId);
            ViewCommand.Parameters.Add(paramId);

            detilDtAdpater = new MySqlDataAdapter();
            detilDtAdpater.SelectCommand = ViewCommand;
            detilTbl = new DataTable();
            detilDtAdpater.Fill(detilTbl);
            dgvDetil.DataSource = detilTbl;
            SetDetilGridColumns();
        }

        private void dgvMaster_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            int IdTrx = Convert.ToInt32(dgvMaster.Rows[e.RowIndex].Cells[0].Value);
            FillDetilView(IdTrx);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            // Open frmPenawaranNew for editing the specified data
            fr_penawaranNew EditPenawaran = fr_penawaranNew.GetInstance();
            EditPenawaran.MdiParent = this.MdiParent;
            EditPenawaran.LoadMasterData(Convert.ToInt32(dgvMaster.Rows[dgvMaster.CurrentRow.Index].Cells[0].Value));
            EditPenawaran.IDTransaksi = Convert.ToInt32(dgvMaster.Rows[dgvMaster.CurrentRow.Index].Cells[0].Value);
            EditPenawaran.frmMode = FormOpenMode.fomEdit;
            EditPenawaran.Show();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            fr_penawaranNew AddPenawaran = fr_penawaranNew.GetInstance();
            AddPenawaran.frmMode = FormOpenMode.fomNew;
            AddPenawaran.MdiParent = this.MdiParent;
            AddPenawaran.WindowState = FormWindowState.Maximized;
            AddPenawaran.Userconnected = GlobVar.UserActive;
            AddPenawaran.Userid = GlobVar.IdUserActive;
            AddPenawaran.Show();
        }
    }
}
