﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using MySql.Data;
using DevExpress.DataAccess.Sql;

namespace SalesBaja
{
    public partial class PenawaranPrint : DevExpress.XtraReports.UI.XtraReport
    {
        public Int32 paraIDPenawaran;
        public DevExpress.DataAccess.ConnectionParameters.MySqlConnectionParameters connParams;

        public PenawaranPrint()
        {
            InitializeComponent();
            //InisialisasiCetak();
            //InisialisasiKoneksi();
            //BindData();
        }

        public void BindData()
        {
            dsCetak.Connection.ConnectionString =
                "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase;
            //IDPenawaran.Value = ParamID;
            //sqlDataSource1.Connection.Open();
        }

        public void InisialisasiKoneksi()
        {
            SqlDataSource dtSource = this.DataSource as SqlDataSource;

            DevExpress.DataAccess.ConnectionParameters.MySqlConnectionParameters connParam = 
              new DevExpress.DataAccess.ConnectionParameters.MySqlConnectionParameters();

            //customize
            connParam.ServerName = GlobVar.hAddress;
            connParam.DatabaseName = GlobVar.hdbase;
            connParam.UserName = GlobVar.hUser;
            connParam.Password = GlobVar.hPasswd;

            dtSource.ConnectionParameters = null;
            dtSource.ConnectionParameters = connParam;

        }

        private void InisialisasiCetak()
        {
            ParamID.Value = paraIDPenawaran;
        }
    }
}
