﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using SalesBaja.Print;
using DevExpress.XtraReports.UI;

namespace SalesBaja.Laporan
{
    public partial class frmLapPenjualan : Form
    {
        private string connstring;

        public frmLapPenjualan()
        {
            InitializeComponent();
            InitConnString();
            InitPeriode();
        }

        private void InitConnString()
        {
            connstring = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase;
        }

        private void InitPeriode()
        {
            MySqlConnection lapConn = new MySqlConnection(connstring);
            string sqlstr = "select distinct month(tanggal_po), Year(tanggal_po) from t_po";
            lapConn.Open();

            try
            {
                MySqlCommand perCommand = new MySqlCommand(sqlstr, lapConn);
                MySqlDataReader reader = perCommand.ExecuteReader();
                string Period;

                while (reader.Read())
                {
                    Period = DataUtils.NamaBulan(reader.GetInt32(0)) + " " + 
                        reader[1].ToString();
                    cbPeriode.Items.Add(Period);
                }

                reader.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Terjadi kesalahan pada database saat inisialisasi periode!");
            }

            lapConn.Close();
        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            ReportPenjualan PrintLaporan = new ReportPenjualan();

            using (ReportPrintTool printTool = new ReportPrintTool(PrintLaporan))
            {
                printTool.ShowPreviewDialog();
            }

        }
    }
}
