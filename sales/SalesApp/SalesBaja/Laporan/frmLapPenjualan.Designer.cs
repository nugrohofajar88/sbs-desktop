﻿namespace SalesBaja.Laporan
{
    partial class frmLapPenjualan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbPeriode = new System.Windows.Forms.Label();
            this.cbPeriode = new System.Windows.Forms.ComboBox();
            this.btnCetak = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbPeriode
            // 
            this.lbPeriode.AutoSize = true;
            this.lbPeriode.Location = new System.Drawing.Point(12, 20);
            this.lbPeriode.Name = "lbPeriode";
            this.lbPeriode.Size = new System.Drawing.Size(43, 13);
            this.lbPeriode.TabIndex = 0;
            this.lbPeriode.Text = "Periode";
            // 
            // cbPeriode
            // 
            this.cbPeriode.FormattingEnabled = true;
            this.cbPeriode.Location = new System.Drawing.Point(74, 17);
            this.cbPeriode.Name = "cbPeriode";
            this.cbPeriode.Size = new System.Drawing.Size(165, 21);
            this.cbPeriode.TabIndex = 1;
            // 
            // btnCetak
            // 
            this.btnCetak.Location = new System.Drawing.Point(258, 120);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(75, 23);
            this.btnCetak.TabIndex = 3;
            this.btnCetak.Text = "Cetak";
            this.btnCetak.UseVisualStyleBackColor = true;
            this.btnCetak.Click += new System.EventHandler(this.btnCetak_Click);
            // 
            // frmLapPenjualan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 162);
            this.Controls.Add(this.btnCetak);
            this.Controls.Add(this.cbPeriode);
            this.Controls.Add(this.lbPeriode);
            this.Name = "frmLapPenjualan";
            this.Text = "Laporan Penjualan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbPeriode;
        private System.Windows.Forms.ComboBox cbPeriode;
        private System.Windows.Forms.Button btnCetak;
    }
}