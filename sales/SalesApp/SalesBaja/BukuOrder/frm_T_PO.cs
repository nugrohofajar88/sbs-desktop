﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;

namespace SalesBaja
{
    public partial class fr_t_PO : Form
    {
        private static fr_t_PO openForm = null;

        // No need for locking - you'll be doing all this on the UI thread...
        public static fr_t_PO GetInstance()
        {
            if (openForm == null)
            {
                openForm = new fr_t_PO();
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        public FormOpenMode frmMode;
        public int IDTransaksi;

        public MySqlConnection dtConn;
        private string dtConnString;
        private string _userconnected;
        private string _userfunction;
        private int _userid;
        private Int32 RegNomor;
        public byte FormMode; // 0=Add; 1=Edit;
        public Double TotalHarga;
        public Double Discpersen;
        public Double Discnominal;
        public Double TotalNominal;
        public Double PpnNominal;
        //public Double GrandTot;

        private DataTable EditableData;
        private DataTable seldtTable;
        private DataTable MarketingTable;
        private DataTable DeletedTable;
        private DataTable EditedTable;
        private DataTable InsertedTable;

        private string OldSQty;
        private string OldsBerat;
        private string OldsHarga;
        private string OldsDiscItem;
        private string OldBonusCust;
        private Boolean OldPersen;

        public string Userconnected
        {
            get
            {
                return _userconnected;
            }

            set
            {
                _userconnected = value;
            }
        }

        public string Userfunction
        {
            get
            {
                return _userfunction;
            }

            set
            {
                _userfunction = value;
            }
        }

        public int Userid
        {
            get
            {
                return _userid;
            }

            set
            {
                _userid = value;
            }
        }

        public fr_t_PO()
        {
            InitializeComponent();
            InitDataConnection();
            InitMarketingData();
        }

        private void InitDataConnection()
        {
            dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase; 
            try
            {
                dtConn = new MySqlConnection();
                dtConn.ConnectionString = dtConnString;
                //dtConn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            //Tampilkan windows daftar customer
            form_List_Cust fListCustomer = new form_List_Cust();
            fListCustomer.ShowDialog(this);
            tbCustID.Text = fListCustomer.SelectedID;
            lbNamaCustomer.Text = fListCustomer.SelectedNama;
            fListCustomer.Dispose();
        }

        private void btnRowAdd_Click(object sender, EventArgs e)
        {
            // Menambahkan row di Gridview
            DataGridViewRow r = new DataGridViewRow();
            r.CreateCells(dgDetil);
            int IdSKR = DataUtils.GenRowID();
            r.Cells[19].Value = IdSKR.ToString();
            dgDetil.Rows.Add(r);
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            // Menghapus row yang terselect
            string _delJumlah;
            int i;
            int Id_Dtl;
            int EditedRowIdx;
            int InsertedRowIdx;

            for (i = dgDetil.RowCount - 1; i >= 0; i--)
            {
                DataGridViewCheckBoxCell cellcheck = dgDetil.Rows[i].Cells[0] as DataGridViewCheckBoxCell;
                if (Convert.ToBoolean(cellcheck.Value))
                {
                    //slected++;
                    _delJumlah = (dgDetil.Rows[i].Cells[14].Value == null) ? "0" :
                        dgDetil.Rows[i].Cells[14].Value.ToString();
                    Double DeletedJumlah = Convert.ToDouble(_delJumlah);
                    TotalHarga = TotalHarga - DeletedJumlah;
                    if (frmMode == FormOpenMode.fomEdit)
                    {
                        //    MarkDeletedRow(idDtl);
                        Id_Dtl = Convert.ToInt32(dgDetil.Rows[i].Cells[19].Value.ToString());
                        EditedRowIdx = DataUtils.FindIndex(EditedTable, Id_Dtl);
                        InsertedRowIdx = DataUtils.FindIndex(InsertedTable, Id_Dtl);
                        if (EditedRowIdx != -1)
                        {
                            EditedTable.Rows[EditedRowIdx].Delete();
                        }
                        else
                        if (InsertedRowIdx != -1)
                        {
                            InsertedTable.Rows[InsertedRowIdx].Delete();
                        }
                        else AddToDeletedRow(i);
                    }

                    dgDetil.Rows.RemoveAt(i);
                }
            }

            tbJumlah.Text = TotalHarga.ToString("#,##0.00");
        }

        private int ShowLastId()
        {
            MySqlCommand comm = new MySqlCommand("SELECT MAX(Id_po) FROM t_po");
            comm.Connection = dtConn;
            object qryrslt = comm.ExecuteScalar();
            Int32 hasil = (Convert.IsDBNull(qryrslt) ? 0 : Convert.ToInt32(qryrslt));
            //MessageBox.Show("Berhasil mencari nilai " + hasil.ToString());
            return hasil;
        }

        private void SaveMasterTrx(Int32 TxId)
        {
            MySqlCommand tcmd = new MySqlCommand();

            StringBuilder tSql = new StringBuilder();

            string tglTrx = dtTglPO.Value.ToString("yyyy-MM-dd");
            Int32 status = 0;


            if (this.FormMode == 0)  // Entry baru
            {
                MySqlParameter prmIdMst = new MySqlParameter("IdMst", TxId);
                MySqlParameter prmtgl = new MySqlParameter("tgl", tglTrx);
                MySqlParameter prmnopo = new MySqlParameter("nopo", MySqlDbType.String);
                MySqlParameter prmpocust = new MySqlParameter("pocust", MySqlDbType.String);
                MySqlParameter prmnopenawaran = new MySqlParameter("nopenawaran", MySqlDbType.String);
                MySqlParameter prmidmkt = new MySqlParameter("idmrkt", _userid);
                MySqlParameter prmidcust = new MySqlParameter("idcust", tbCustID.Text);
                MySqlParameter prmstat = new MySqlParameter("stat", status);
                MySqlParameter prmJml = new MySqlParameter("jumlah", MySqlDbType.Double);
                MySqlParameter prmDisc = new MySqlParameter("disc", MySqlDbType.Int32);
                MySqlParameter prmPpn = new MySqlParameter("ppn", MySqlDbType.Double);
                MySqlParameter prmKet = new MySqlParameter("ketrangan", tbKetrangan.Text);
                MySqlParameter prmMark = new MySqlParameter("marketing", MySqlDbType.Int32);
                MySqlParameter prmReff = new MySqlParameter("reff", tbReff.Text);
                MySqlParameter prmKrdit = new MySqlParameter("krdit", MySqlDbType.UInt16);

                tSql.AppendLine("INSERT INTO t_po(id_po, tanggal_po, No_Penawaran, id_userinput, id_customer, ");
                tSql.AppendLine("    status_po, jumlah, disc, ppn, keterangan, id_marketing, reff, input_time, ");
                tSql.AppendLine("    no_po, po_customer, kredit) ");
                tSql.AppendLine("VALUES (@IdMst, @tgl, @nopenawaran, @idmrkt, @idcust, @stat, ");
                tSql.AppendLine("    @jumlah, @disc, @ppn, @ketrangan, @marketing, @reff, now(), ");
                tSql.AppendLine("    @nopo, @pocust, @krdit)");

                try
                {
                    tcmd.CommandText = tSql.ToString();
                    prmnopo.Value = tbNoPO.Text;
                    if (tb_PoCustomer.Text == "")
                    {
                        prmpocust.Value = DBNull.Value;
                    }
                    else prmpocust.Value = tb_PoCustomer.Text;
                    if (tbNoPenawaran.Text == "")
                    {
                        prmnopenawaran.Value = DBNull.Value;
                    } else prmnopenawaran.Value = tbNoPenawaran.Text;
                    prmJml.Value = TotalHarga;
                    prmDisc.Value = (int)Discpersen;
                    if (cbPPn.Checked)
                    {
                        prmPpn.Value = PpnNominal;
                    }
                    else prmPpn.Value = DBNull.Value;
                    prmMark.Value = MarketingTable.Rows[tbMarketing.SelectedIndex][0];
                    prmKrdit.Value = seKredit.Value;
                    tcmd.Parameters.Add(prmIdMst);
                    tcmd.Parameters.Add(prmtgl);
                    tcmd.Parameters.Add(prmnopo);
                    tcmd.Parameters.Add(prmpocust);
                    tcmd.Parameters.Add(prmnopenawaran);
                    tcmd.Parameters.Add(prmidmkt);
                    tcmd.Parameters.Add(prmidcust);
                    tcmd.Parameters.Add(prmstat);
                    tcmd.Parameters.Add(prmJml);
                    tcmd.Parameters.Add(prmDisc);
                    tcmd.Parameters.Add(prmPpn);
                    tcmd.Parameters.Add(prmKet);
                    tcmd.Parameters.Add(prmMark);
                    tcmd.Parameters.Add(prmReff);
                    tcmd.Parameters.Add(prmKrdit);
                    tcmd.Connection = dtConn;
                    tcmd.ExecuteNonQuery();
                    //MessageBox.Show("Data Master telah tersimpan");
                }
                catch (MySqlException e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        private void SaveDtlTrx(Int32 TxId)
        {
            if (this.FormMode == 0)
            {
                MySqlCommand dtlCommand = new MySqlCommand();

                StringBuilder dtlSql = new StringBuilder();
                dtlSql.AppendLine("INSERT INTO dt_po (id_po, id_type, id_ukuran, qty_po, berat, ");
                dtlSql.AppendLine("  harga_po, disc_item, bonus_cust, persen) ");
                dtlSql.AppendLine("VALUES (@idTx, @idbrg, @idukr, @qty, @berat, @harga, @diskon,");
                dtlSql.AppendLine("  @bnscust, @prsn)");

                try
                {
                    MySqlParameter prmIdTx = new MySqlParameter("idTx", MySqlDbType.Int32);
                    MySqlParameter prmIdBrg = new MySqlParameter("idbrg", MySqlDbType.Int32);
                    MySqlParameter prmQty = new MySqlParameter("qty", MySqlDbType.Int32);
                    MySqlParameter prmHrg = new MySqlParameter("harga", MySqlDbType.Double);
                    MySqlParameter prmUkuran = new MySqlParameter("idukr", MySqlDbType.Int32);
                    MySqlParameter prmDiskItem = new MySqlParameter("diskon", MySqlDbType.Int32);
                    MySqlParameter prmberat = new MySqlParameter("berat", MySqlDbType.Double);
                    MySqlParameter prmbonuscust = new MySqlParameter("bnscust", MySqlDbType.Int32);
                    MySqlParameter prmpersen = new MySqlParameter("prsn", MySqlDbType.Byte);

                    foreach (DataGridViewRow row in dgDetil.Rows)
                    {
                        //string brgDiminta = row.Cells[1].Value.ToString();
                        //string ukrDiminta = row.Cells[2].Value.ToString();
                        int BrgID = Convert.ToInt32(row.Cells[4].Value);
                        int UkuranID = Convert.ToInt32(row.Cells[8].Value);
                        int BrgQty = Convert.ToInt32(row.Cells[13].Value);
                        Double BeratBrg = Convert.ToDouble(row.Cells[14].Value);
                        Double BrgHarga = Convert.ToDouble(row.Cells[15].Value);
                        int DiskonItem = Convert.ToInt32(row.Cells[16].Value);
                        prmbonuscust.Value = Convert.ToInt32(row.Cells[20].Value);
                        prmpersen.Value = row.Cells[21].Value;

                        dtlCommand.CommandText = dtlSql.ToString();
                        prmIdTx.Value = TxId;
                        dtlCommand.Parameters.Add(prmIdTx);
                        prmIdBrg.Value = BrgID;
                        dtlCommand.Parameters.Add(prmIdBrg);
                        prmQty.Value = BrgQty;
                        dtlCommand.Parameters.Add(prmUkuran);
                        prmUkuran.Value = UkuranID;
                        dtlCommand.Parameters.Add(prmQty);
                        prmHrg.Value = BrgHarga;
                        dtlCommand.Parameters.Add(prmHrg);
                        prmDiskItem.Value = DiskonItem;
                        dtlCommand.Parameters.Add(prmDiskItem);
                        prmberat.Value = BeratBrg;
                        dtlCommand.Parameters.Add(prmberat);
                        dtlCommand.Parameters.Add(prmbonuscust);
                        dtlCommand.Parameters.Add(prmpersen);
                        dtlCommand.Connection = dtConn;
                        dtlCommand.ExecuteNonQuery();
                        dtlCommand.Parameters.Clear();
                        //MessageBox.Show("Data detil telah tersimpan");
                    }
                }
                catch (MySqlException e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            //if (tbNoPenawaran.Text == "")
            //{
            //    btnGeneratNo_Click(sender, e);
            //}

            if (dtConn != null && dtConn.State == ConnectionState.Closed)
            {
                dtConn.Open();
            }
            if (frmMode == FormOpenMode.fomNew)
            {
                Int32 TxID = ShowLastId() + 1;

                //Menyimpan header/master
                SaveMasterTrx(TxID);

                // Menyimpan detil 
                SaveDtlTrx(TxID);
                UpdateReg(RegNomor, dtTglPO.Value.Year);

                MessageBox.Show("Data telah tersimpan!");
            } else if (frmMode == FormOpenMode.fomEdit)
            {
                UpdateMasterTrx(IDTransaksi);
                UpdateDtlTrx(IDTransaksi);
                MessageBox.Show("Data telah terupdate!");
            }
            dtConn.Close();
            KosongkanField();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var SenderGrid = (DataGridView)sender;

            if (SenderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                //MessageBox.Show("Button Clik " + e.RowIndex.ToString());
                switch (e.ColumnIndex)
                {
                    case 3: //type
                        {
                            if (frmMode == FormOpenMode.fomNew)
                            {
                                AddNewType(e.RowIndex);
                            }
                            else
                            {
                                EditingType(e.RowIndex);
                            }
                            break;
                        }
                    case 6: // ukuran
                        {
                            if (frmMode == FormOpenMode.fomNew)
                            {
                                AddUkuran(e.RowIndex);
                            }
                            else
                            {
                                EditingUkuran(e.RowIndex);
                            }

                            break;
                        }
                }
            } else if (SenderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {
                switch (e.ColumnIndex)
                {
                    case 21: // Persen
                        {
                            if (frmMode == FormOpenMode.fomEdit)
                            {
                                EditingBonusCust(e.RowIndex);
                            }
                            break;
                        }
                }
            }
        }

        public void LoadMasterData(Int32 MasterId)
        {
            dtConn.Open();
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT p.id_po, p.no_penawaran, p.id_customer, p.status_po, p.tanggal_po,");
            qry.AppendLine("   c.nama_customer, p.jumlah, p.disc, p.ppn, p.keterangan, p.no_po,");
            qry.AppendLine("   p.PO_CUSTOMER, p.id_marketing, mrk.nama_marketing, p.REFF, p.KREDIT, ");
            qry.AppendLine("   p.id_userinput, u.username");
            qry.AppendLine("FROM t_po p");
            qry.AppendLine("INNER JOIN m_customer c ON p.Id_Customer = c.id_customer");
            qry.AppendLine("INNER JOIN m_user u ON p.id_userinput = u.id_user");
            qry.AppendLine("INNER JOIN m_marketing mrk ON mrk.id_marketing = p.id_marketing");
            qry.AppendLine("WHERE p.id_po = @Key");

            MySqlCommand edComm = new MySqlCommand(qry.ToString(), dtConn);

            MySqlParameter Kid = new MySqlParameter("Key", MySqlDbType.Int32);
            Kid.Value = MasterId;
            //MessageBox.Show(MasterId.ToString());
            edComm.Parameters.Add(Kid);

            MySqlDataReader rd = edComm.ExecuteReader();

            while (rd.Read())
            {
                PlaceTheData((IDataRecord)rd);
            }

            rd.Close();
            //SetFormMode();

            LoadDetilData(MasterId);
            dtConn.Close();
        }

        private void PlaceTheData(IDataRecord recrd)
        {
            tbNoPenawaran.Text = recrd[1].ToString();
            tbCustID.Text = recrd[2].ToString();
            lbNamaCustomer.Text = recrd[5].ToString();
            dtTglPO.Text = recrd[4].ToString();
            tbNoPO.Text = recrd[10].ToString();
            tb_PoCustomer.Text = recrd[11].ToString();
            seKredit.Value = recrd.GetInt32(15);
            tbMarketing.SelectedIndex = tbMarketing.FindString(recrd[13].ToString());

            TotalHarga = recrd.GetDouble(6);
            tbJumlah.Text = TotalHarga.ToString("#,##0.##");
            Discpersen = recrd.GetInt32(7);
            Discnominal = Math.Round(TotalHarga * (Discpersen / 100), 2, MidpointRounding.AwayFromZero);
            tbDiscPersen.Text = Discpersen.ToString();
            tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
            PpnNominal = recrd.GetDouble(8);
            cbPPn.Checked = (PpnNominal > 0);
            tbPPN.Text = PpnNominal.ToString("#,##0.##");

            tbKetrangan.Text = recrd[9].ToString();

            TotalNominal = TotalHarga - Discnominal;

            tbTotal.Text = TotalNominal.ToString("#,##0.##");

            tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

        }

        private void SetFormMode()
        {
            switch (FormMode)
            {
                //case 0:
                //    {
                //        dtTglPO.Enabled = false;
                //        tbNoPenawaran.ReadOnly = true;
                //        tbCustID.ReadOnly = true;
                //        btnCari.Enabled = false;
                //        tbKetrangan.ReadOnly = true;
                    
                //        break;
                //    }
                case 1:
                    {
                        dtTglPO.Enabled = false;
                        tbNoPenawaran.ReadOnly = true;
                        tbCustID.ReadOnly = true;
                        btnCari.Enabled = false;
                        tbKetrangan.ReadOnly = true;
                        btnRowAdd.Enabled = false;
                        btnDel.Enabled = false;
                        dgDetil.Enabled = false;
                        break;
                    }
            }

        }

        private void LoadDetilData(Int32 DetilID)
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT dp.id_dt_po, dp.id_type, mt.`type`, dp.id_ukuran,");
            qry.AppendLine("    dp.qty_po, dp.harga_po, mu.nama_ukuran, mu.lebar, mu.panjang,");
            qry.AppendLine("    mu.tinggi, mu.faktor_hitung, dp.berat, dp.disc_item, ");
            qry.AppendLine("    dp.bonus_cust, dp.persen");
            qry.AppendLine("FROM DT_PO dp");
            qry.AppendLine("INNER JOIN m_type mt ON dp.id_type = mt.id_type");
            qry.AppendLine("INNER JOIN m_ukuran mu ON dp.id_ukuran = mu.id_ukuran");
            qry.AppendLine("WHERE dp.id_po = @Key");

            MySqlCommand edComm = new MySqlCommand(qry.ToString(), dtConn);

            MySqlParameter Kid = new MySqlParameter("Key", MySqlDbType.Int32);
            Kid.Value = DetilID;
            edComm.Parameters.Add(Kid);

            MySqlDataAdapter dtAdapter = new MySqlDataAdapter(edComm);
            EditableData = new DataTable();
            dtAdapter.Fill(EditableData);

            CloneDetilTbl();
            //EditableData.Columns.Add("edited", typeof(bool));
            //EditableData.Columns.Add("deleted", typeof(bool));

            PlaceDetilData();
        }
        
        private void PlaceDetilData()
        {
            double faktor = 0;
            string nama_ukuran = "";
            int HrgSatuan;
            Double Berat;
            Double Jumlah;
            int Diskon;
            Double Diskonnominal;
            int BonusCust;

            foreach (DataRow row in EditableData.Rows)
            {
                DataGridViewRow rw = new DataGridViewRow();
                rw.CreateCells(dgDetil);

                rw.Cells[1].Value = "";                   // Diminta
                rw.Cells[2].Value = "";                   // Ukuran diminta
                rw.Cells[4].Value = row[1].ToString();    // Id_type 
                rw.Cells[5].Value = row[2].ToString();    // Type

                faktor = double.Parse(row[10].ToString());
                if (faktor == 6.20)
                {
                    nama_ukuran = "Ø " + row[6].ToString();
                }
                else if (faktor == 8.00)
                {
                    nama_ukuran = "≠ " + row[6].ToString();
                }   

                rw.Cells[7].Value = nama_ukuran;           // nama ukuran
                rw.Cells[8].Value = row[3].ToString();     // id_ukuran
                rw.Cells[9].Value = row[7].ToString();     // lebar
                rw.Cells[10].Value = row[8].ToString();    // panjang
                rw.Cells[11].Value = row[9].ToString();    // tinggi
                rw.Cells[12].Value = row[10].ToString();   // faktor hitung
                rw.Cells[13].Value = row[4].ToString();    // qty

                HrgSatuan = int.Parse(row[5].ToString());
                Berat = Double.Parse(row[11].ToString());
                Diskon = int.Parse(row[12].ToString());
                Diskonnominal = (Berat * HrgSatuan) * (Diskon / 100);
                Jumlah = Math.Round((Berat * HrgSatuan) - Diskonnominal, 2, MidpointRounding.AwayFromZero);

                rw.Cells[14].Value = Berat.ToString("#,##0.##");
                rw.Cells[15].Value = HrgSatuan.ToString("#,##0.##");
                rw.Cells[16].Value = Diskon.ToString();
                rw.Cells[17].Value = Jumlah.ToString("#,##0.##");
                rw.Cells[18].Value = row[0].ToString();         // id_dtpo
                BonusCust = int.Parse(row[13].ToString());
                rw.Cells[20].Value = BonusCust.ToString("#,##0.##");
                rw.Cells[21].Value = row[14];
                dgDetil.Rows.Add(rw);
            }
        }

        private void KosongkanField()
        {
            BersihkanHead();
            BersihkanGrid();
        }

        private void BersihkanHead()
        {
            tbNoPO.Text = "";
            tbCustID.Text = "";
            lbNamaCustomer.Text = "";
            dtTglPO.Text = "";
            tbNoPenawaran.Text = "";
            tbJumlah.Text = "";
            tbDiscPersen.Text = "";
            tbDiscNominal.Text = "";
            tbTotal.Text = "";
            tbPPN.Text = "";
            tbGrandTotal.Text = "";
            tbKetrangan.Text = "";
            tbMarketing.SelectedIndex = 0;
            tbReff.Text = "";
            tb_PoCustomer.Text = "";
        }

        private void BersihkanGrid()
        {
            int i;
            for (i = dgDetil.RowCount - 1; i >= 0; i--)
            {
                dgDetil.Rows.RemoveAt(i);
            }
        }

        private Double HitungBerat(Double Lebar, Double Panjang, Double Tebal, Double Faktor)
        {
            Double Berat = 0.00;

            if (Faktor == 6.2)
            {
                Berat = (Lebar / 100) * (Lebar / 100) * (Panjang / 100) * Faktor;
            }
            else if (Faktor == 8)
            {
                Berat = (Lebar / 100) * (Tebal / 100) * (Panjang / 100) * Faktor;
            }

            return Berat;
        }

        private Double HitBerat(Int32 Qty, Double BeratSat)
        {
            Double BeratAll = 0;

            BeratAll = Qty * BeratSat;

            return BeratAll;
        }

        private void dgDetil_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var SenderGrid = (DataGridView)sender;

            Double BeratperKg;
            Double Jumlah;
            Int32 _qty = 0;
            Int32 HargaSat;
            Double lbr, pjg, tbl;
            Double _fkt;
            string sQty0, sQty, sHrgSat0, sHargaSat, sBerat;
            string sDiscItem;
            int DiscntItem;
            Double DiscnItemNominal;
            Boolean curpersen;

            switch (e.ColumnIndex)
            {

                case 13:  //qty
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        string _lbr = (SenderGrid.Rows[e.RowIndex].Cells[9].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[9].Value.ToString();
                        //_lbr = new string(_lbr.Where(char.IsDigit).ToArray());
                        string _pjg = (SenderGrid.Rows[e.RowIndex].Cells[10].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[10].Value.ToString();
                        //_pjg = new string(_pjg.Where(char.IsDigit).ToArray());
                        string _tbl = (SenderGrid.Rows[e.RowIndex].Cells[11].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[11].Value.ToString();
                        _tbl = (_tbl == "") ? "0" : _tbl;

                        lbr = Convert.ToDouble(_lbr);
                        pjg = Convert.ToDouble(_pjg);
                        tbl = Convert.ToDouble(_tbl);
                        _fkt = Convert.ToDouble(SenderGrid.Rows[e.RowIndex].Cells[12].Value);

                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());

                        try
                        {
                            _qty = Convert.ToInt32(sQty0);
                        }
                        catch (System.FormatException ex)
                        {
                            MessageBox.Show("Qty harus bilangan bulat");
                        }

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        HargaSat = Convert.ToInt32(sHargaSat);

                        BeratperKg = HitungBerat(lbr, pjg, tbl, _fkt);
                        BeratperKg = HitBerat(_qty, BeratperKg);

                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        DiscntItem = Convert.ToInt32(sDiscItem);
                        DiscnItemNominal = BeratperKg * HargaSat * DiscntItem / 100;

                        Jumlah = Math.Round((BeratperKg * HargaSat) - DiscnItemNominal,
                            2, MidpointRounding.AwayFromZero);

                        dgDetil.Rows[e.RowIndex].Cells[13].Value = _qty.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[14].Value = BeratperKg.ToString("#,##0.##");
                        dgDetil.Rows[e.RowIndex].Cells[15].Value = HargaSat.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[16].Value = DiscntItem.ToString("0");
                        dgDetil.Rows[e.RowIndex].Cells[17].Value = Jumlah.ToString("#,##0.##");
                        TotalHarga = TotalHarga + Jumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        Discpersen = Convert.ToDouble((tbDiscPersen.Text.Equals("")) ? "0" : tbDiscPersen.Text);
                        Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                        TotalNominal = TotalHarga - Discnominal;
                        PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;

                        tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                        tbTotal.Text = TotalNominal.ToString("#,##0.##");
                        tbPPN.Text = PpnNominal.ToString("#,##0.##");

                        tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldSQty != SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString())
                            {
                                EditingQty(e.RowIndex);
                            }
                        }

                        break;
                    }

                case 14: //berat
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());

                        try
                        {
                            _qty = Convert.ToInt32(sQty0);
                        }
                        catch (System.FormatException ex)
                        {
                            MessageBox.Show("Qty harus bilangan bulat");
                        }

                        sBerat = (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString();
                        BeratperKg = Convert.ToDouble(sBerat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        HargaSat = Convert.ToInt32(sHargaSat);

                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        DiscntItem = Convert.ToInt32(sDiscItem);
                        DiscnItemNominal = BeratperKg * HargaSat * DiscntItem / 100;

                        Jumlah = Math.Round((BeratperKg * HargaSat) - DiscnItemNominal,
                            2, MidpointRounding.AwayFromZero);

                        dgDetil.Rows[e.RowIndex].Cells[13].Value = _qty.ToString("#,##0");
                        //dgDetil.Rows[e.RowIndex].Cells[13].Value = BeratperKg.ToString("#,##0.##");
                        dgDetil.Rows[e.RowIndex].Cells[15].Value = HargaSat.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[16].Value = DiscntItem.ToString("0");
                        dgDetil.Rows[e.RowIndex].Cells[17].Value = Jumlah.ToString("#,##0.##");
                        TotalHarga = TotalHarga + Jumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                        Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                        TotalNominal = TotalHarga - Discnominal;
                        PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;

                        tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                        tbTotal.Text = TotalNominal.ToString("#,##0.##");
                        tbPPN.Text = PpnNominal.ToString("#,##0.##");

                        tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldsBerat != SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString())
                            {
                                EditingBerat(e.RowIndex);
                            }
                        }

                        break;
                    }

                case 15:  //harga
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        string _lbr = (SenderGrid.Rows[e.RowIndex].Cells[9].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[9].Value.ToString();
                        //_lbr = new string(_lbr.Where(char.IsDigit).ToArray());
                        string _pjg = (SenderGrid.Rows[e.RowIndex].Cells[10].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[10].Value.ToString();
                        //_pjg = new string(_pjg.Where(char.IsDigit).ToArray());
                        string _tbl = (SenderGrid.Rows[e.RowIndex].Cells[11].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[11].Value.ToString();
                        _tbl = (_tbl == "") ? "0" : _tbl;

                        lbr = Convert.ToDouble(_lbr);
                        pjg = Convert.ToDouble(_pjg);
                        tbl = Convert.ToDouble(_tbl);
                        _fkt = Convert.ToDouble(SenderGrid.Rows[e.RowIndex].Cells[12].Value);

                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());

                        try
                        {
                            _qty = Convert.ToInt32(sQty0);
                        }
                        catch (System.FormatException ex)
                        {
                            MessageBox.Show("Qty harus bilangan bulat");
                        }

                        sBerat = (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString();
                        BeratperKg = Convert.ToDouble(sBerat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        HargaSat = Convert.ToInt32(sHargaSat);

                        if (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null)
                        {
                            BeratperKg = HitungBerat(lbr, pjg, tbl, _fkt);
                            BeratperKg = HitBerat(_qty, BeratperKg);
                        }

                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        DiscntItem = Convert.ToInt32(sDiscItem);
                        DiscnItemNominal = BeratperKg * HargaSat * DiscntItem / 100;

                        Jumlah = Math.Round((BeratperKg * HargaSat) - DiscnItemNominal,
                            2, MidpointRounding.AwayFromZero);

                        dgDetil.Rows[e.RowIndex].Cells[13].Value = _qty.ToString("#,##0");
                        //dgDetil.Rows[e.RowIndex].Cells[13].Value = BeratperKg.ToString("#,##0.##");
                        dgDetil.Rows[e.RowIndex].Cells[15].Value = HargaSat.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[16].Value = DiscntItem.ToString("0");
                        dgDetil.Rows[e.RowIndex].Cells[17].Value = Jumlah.ToString("#,##0.##");
                        TotalHarga = TotalHarga + Jumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                        Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                        TotalNominal = TotalHarga - Discnominal;
                        PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;

                        tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                        tbTotal.Text = TotalNominal.ToString("#,##0.##");
                        tbPPN.Text = PpnNominal.ToString("#,##0.##");

                        tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldsHarga != SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString())
                            {
                                EditingHarga(e.RowIndex);
                            }
                        }

                        break;
                    }
                case 16: // disc
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        string _lbr = (SenderGrid.Rows[e.RowIndex].Cells[9].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[9].Value.ToString();
                        //_lbr = new string(_lbr.Where(char.IsDigit).ToArray());
                        string _pjg = (SenderGrid.Rows[e.RowIndex].Cells[10].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[10].Value.ToString();
                        //_pjg = new string(_pjg.Where(char.IsDigit).ToArray());
                        string _tbl = (SenderGrid.Rows[e.RowIndex].Cells[11].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[11].Value.ToString();
                        _tbl = (_tbl == "") ? "0" : _tbl;

                        lbr = Convert.ToDouble(_lbr);
                        pjg = Convert.ToDouble(_pjg);
                        tbl = Convert.ToDouble(_tbl);
                        _fkt = Convert.ToDouble(SenderGrid.Rows[e.RowIndex].Cells[12].Value);

                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());

                        try
                        {
                            _qty = Convert.ToInt32(sQty0);
                        }
                        catch (System.FormatException ex)
                        {
                            MessageBox.Show("Qty harus bilangan bulat");
                        }

                        sBerat = (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString();
                        BeratperKg = Convert.ToDouble(sBerat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        HargaSat = Convert.ToInt32(sHargaSat);

                        if (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null)
                        {
                            BeratperKg = HitungBerat(lbr, pjg, tbl, _fkt);
                            BeratperKg = HitBerat(_qty, BeratperKg);
                        }

                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        DiscntItem = Convert.ToInt32(sDiscItem);
                        DiscnItemNominal = BeratperKg * HargaSat * DiscntItem / 100;

                        Jumlah = Math.Round((BeratperKg * HargaSat) - DiscnItemNominal, 
                            2, MidpointRounding.AwayFromZero) ;

                        dgDetil.Rows[e.RowIndex].Cells[13].Value = _qty.ToString("#,##0");
                        //dgDetil.Rows[e.RowIndex].Cells[13].Value = BeratperKg.ToString("#,##0.##");
                        dgDetil.Rows[e.RowIndex].Cells[15].Value = HargaSat.ToString("#,##0");
                        dgDetil.Rows[e.RowIndex].Cells[16].Value = DiscntItem.ToString("0");
                        dgDetil.Rows[e.RowIndex].Cells[17].Value = Jumlah.ToString("#,##0.##");
                        TotalHarga = TotalHarga + Jumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");

                        Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                        Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                        TotalNominal = TotalHarga - Discnominal;
                        PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;

                        tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                        tbTotal.Text = TotalNominal.ToString("#,##0.##");
                        tbPPN.Text = PpnNominal.ToString("#,##0.##");

                        tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");

                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldsDiscItem != SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString())
                            {
                                EditingDiscItem(e.RowIndex);
                            }
                        }

                        break;
                    }
                case 20: // bonus customer
                    {
                        if (frmMode == FormOpenMode.fomEdit)
                        {
                            if (OldBonusCust != SenderGrid.Rows[e.RowIndex].Cells[20].Value.ToString())
                            {
                                EditingBonusCust(e.RowIndex);
                            }
                        }
                        break;
                    }
                //case 21: // Persen
                //    {
                //        if (frmMode == FormOpenMode.fomEdit)
                //        {
                //            curpersen = (SenderGrid.Rows[e.RowIndex].Cells[21].Value == null) ? 
                //                false : true;
                //            if (OldPersen != curpersen)
                //            {
                //                EditingBonusCust(e.RowIndex);
                //            }
                //        }
                //        break;
                //    }
            }
        }

        private void tbDiscPersen_Leave(object sender, EventArgs e)
        {
            Discpersen = Convert.ToDouble(tbDiscPersen.Text);
            Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
            TotalNominal = TotalHarga - Discnominal;
            PpnNominal = (cbPPn.Checked) ? TotalNominal * 0.1 : 0;
            //GrandTot = TotalNominal - PpnNominal;

            tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
            tbTotal.Text = TotalNominal.ToString("#,##0.##");
            tbPPN.Text = PpnNominal.ToString("#,##0.##");

            tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");
        }

        private void dgDetil_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            var SenderGrid = (DataGridView)sender;

            Double BeratperKg, OldBerat;
            Double Jumlah, OldJumlah;
            string sQty0, sQty, sHrgSat0, sHargaSat, sOberat;
            Double lbr, pjg, tbl;
            Double _fkt;
            Int32 OldQty, OldHarga;
            int OldDiscItem;
            Double DiscItemNominal;
            string sDiscItem;
            string sOldBonusCust;

            switch (e.ColumnIndex)
            {
                case 13: //qty
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        OldSQty = sQty0;
                        OldQty = Convert.ToInt32(sQty0);

                        sOberat = (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString();
                        OldBerat = Convert.ToDouble(sOberat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        OldHarga = Convert.ToInt32(sHargaSat);

                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        OldDiscItem = Convert.ToInt32(sDiscItem);
                        DiscItemNominal = OldBerat * OldHarga * OldDiscItem / 100;

                        OldJumlah = Math.Round(OldBerat * OldHarga - DiscItemNominal, 2, MidpointRounding.AwayFromZero);

                        TotalHarga = TotalHarga - OldJumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");
                        break;
                    }
                case 14: //berat
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());
                        OldQty = Convert.ToInt32(sQty0);

                        sOberat = (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString();
                        OldBerat = Convert.ToDouble(sOberat);
                        OldsBerat = sOberat;

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        OldHarga = Convert.ToInt32(sHargaSat);

                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        OldDiscItem = Convert.ToInt32(sDiscItem);
                        DiscItemNominal = OldBerat * OldHarga * OldDiscItem / 100;

                        OldJumlah = Math.Round(OldBerat * OldHarga - DiscItemNominal, 2, MidpointRounding.AwayFromZero);

                        TotalHarga = TotalHarga - OldJumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");
                        break;
                    }
                case 15: //harga
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());
                        //OldQty = Convert.ToInt32(sQty0);
                        OldQty = int.Parse(sQty0, System.Globalization.NumberStyles.AllowThousands);

                        sOberat = (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString();
                        OldBerat = Convert.ToDouble(sOberat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        OldHarga = Convert.ToInt32(sHargaSat);
                        OldsHarga = sHrgSat0;

                        //BeratperKg = HitungBerat(lbr, pjg, tbl, _fkt);
                        //BeratperKg = HitBerat(OldQty, BeratperKg);
                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        OldDiscItem = Convert.ToInt32(sDiscItem);
                        DiscItemNominal = OldBerat * OldHarga * OldDiscItem / 100;

                        OldJumlah = Math.Round(OldBerat * OldHarga - DiscItemNominal, 2, MidpointRounding.AwayFromZero);

                        TotalHarga = TotalHarga - OldJumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");
                        break;
                    }
                case 16: //Disc
                    {
                        // Menghitung Berat/Kg dan Jumlah
                        sQty0 = (SenderGrid.Rows[e.RowIndex].Cells[13].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[13].Value.ToString();
                        //sQty = new string(sQty0.Where(char.IsDigit).ToArray());
                        OldQty = Convert.ToInt32(sQty0);

                        sOberat = (SenderGrid.Rows[e.RowIndex].Cells[14].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[14].Value.ToString();
                        OldBerat = Convert.ToDouble(sOberat);

                        sHrgSat0 = (SenderGrid.Rows[e.RowIndex].Cells[15].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[15].Value.ToString();
                        sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
                        OldHarga = Convert.ToInt32(sHargaSat);

                        sDiscItem = (SenderGrid.Rows[e.RowIndex].Cells[16].Value == null) ? 
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[16].Value.ToString();
                        OldDiscItem = Convert.ToInt32(sDiscItem);
                        DiscItemNominal = OldBerat * OldHarga * OldDiscItem / 100;
                        OldsDiscItem = sDiscItem;

                        OldJumlah = Math.Round(OldBerat * OldHarga - DiscItemNominal, 2, MidpointRounding.AwayFromZero);

                        TotalHarga = TotalHarga - OldJumlah;
                        tbJumlah.Text = TotalHarga.ToString("#,##0.##");
                        break;
                    }
                case 20: //Bonus Customer
                    {
                        sOldBonusCust = (SenderGrid.Rows[e.RowIndex].Cells[20].Value == null) ?
                            "0" : SenderGrid.Rows[e.RowIndex].Cells[20].Value.ToString();
                        OldBonusCust = sOldBonusCust;
                        break;
                    }
                //case 21: //Persen
                //    {
                //        OldPersen = (SenderGrid.Rows[e.RowIndex].Cells[21].Value == null) ? 
                //            false : true;
                //        break;
                //    }
            }
        }

        private void cbPPn_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPPn.Checked)
            {
                Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                TotalNominal = TotalHarga - Discnominal;
                PpnNominal = Math.Ceiling(TotalNominal * 0.1);

                tbDiscNominal.Text = Discnominal.ToString("#,##0.##");
                tbTotal.Text = TotalNominal.ToString("#,##0.##");
                tbPPN.Text = PpnNominal.ToString("#,##0.##");

                tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");
            }
            else
            {
                Discpersen = Convert.ToDouble(tbDiscPersen.Text);
                Discnominal = Math.Ceiling(TotalHarga * (Discpersen / 100));
                TotalNominal = TotalHarga - Discnominal;
                PpnNominal = 0.00;

                tbTotal.Text = TotalNominal.ToString("#,##0.##");
                tbPPN.Text = PpnNominal.ToString("#,##0.##");
                tbGrandTotal.Text = (TotalNominal + PpnNominal).ToString("#,##0.##");
            }
        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            if (tbNoPenawaran.Text == "")
            {
                btnGeneratNo_Click(sender, e);
            }

            dtConn.Open();
            Int32 TxID = ShowLastId() + 1;

            //Menyimpan header/master
            SaveMasterTrx(TxID);

            // Menyimpan detil 
            SaveDtlTrx(TxID);
            UpdateReg(RegNomor, dtTglPO.Value.Year);

            // Mencetak dokumen
            //PenawaranPrint PrintPenawaran = new PenawaranPrint();

            //PrintPenawaran.Parameters["ParamID"].Value = TxID;
            //PrintPenawaran.Parameters["ParamID"].Visible = false;
            //using (ReportPrintTool printTool = new ReportPrintTool(PrintPenawaran))
            //{
            //    printTool.ShowPreviewDialog();
            //}

            dtConn.Close();
            KosongkanField();
        }

        private void btnGeneratNo_Click(object sender, EventArgs e)
        {
            //if (dtConn != null && dtConn.State == ConnectionState.Closed)
            //{
            //    dtConn.Open();
            //}
            MySqlConnection Conn2 = new MySqlConnection(dtConnString);
            Conn2.Open();
            Int32 tahun = dtTglPO.Value.Year;
            string strcmd = "SELECT val FROM reg_no WHERE id = 2 AND year = " + tahun.ToString();
            MySqlCommand regcmd = new MySqlCommand(strcmd, Conn2);
            MySqlDataReader dread = regcmd.ExecuteReader();
            RegNomor = 0;

            while (dread.Read())
            {
                RegNomor = dread.GetInt32(0);
            }

            dread.Close();

            RegNomor++;
            string No = RegNomor.ToString() + "/" + MarketingTable.Rows[tbMarketing.SelectedIndex][2].ToString();
            tbNoPO.Text = No;
            Conn2.Close();
        }

        private void UpdateReg(int Nomor, int thn)
        {
            string strcommand = "INSERT INTO reg_no(id, year, val) VALUES (@idreg, @tahun, @regval) " +
                "ON DUPLICATE KEY UPDATE val = @regval";

            MySqlCommand dtlCommand = new MySqlCommand(strcommand, dtConn);

            MySqlParameter prmidreg = new MySqlParameter("idreg", MySqlDbType.Int32);
            MySqlParameter prmyear = new MySqlParameter("tahun", MySqlDbType.Int32);
            MySqlParameter prmval = new MySqlParameter("regval", MySqlDbType.Int32);

            prmidreg.Value = 2;
            prmyear.Value = thn;
            prmval.Value = Nomor;

            dtlCommand.Parameters.Add(prmidreg);
            dtlCommand.Parameters.Add(prmyear);
            dtlCommand.Parameters.Add(prmval);
            dtlCommand.ExecuteNonQuery();
        }

        private void tbCustID_Leave(object sender, EventArgs e)
        {
            if (tbCustID.Text != "")
            {
                if (dtConn != null && dtConn.State == ConnectionState.Closed)
                {
                    dtConn.Open();
                }

                string sqlcommand = "SELECT nama_customer FROM m_customer " +
                    "WHERE id_customer = @idcustomer";
                try
                {
                    MySqlCommand sqlcomm = new MySqlCommand(sqlcommand, dtConn);
                    MySqlParameter prmidcust = new MySqlParameter("idcustomer", MySqlDbType.Int32);
                    prmidcust.Value = Int32.Parse(tbCustID.Text);
                    sqlcomm.Parameters.Add(prmidcust);
                    MySqlDataReader dread = sqlcomm.ExecuteReader();
                    while (dread.Read())
                    {
                        lbNamaCustomer.Text = dread.GetString(0);
                    }

                    dread.Close();
                }
                catch (MySqlException ex)
                {
                    MessageBox.Show("Terjadi error di database");
                }

                dtConn.Close();
            }
            else btnCari_Click(sender, e);
        }

        private void btnCariPenawaran_Click(object sender, EventArgs e)
        {
            fr_CariPenawaran CariPenawaran = new fr_CariPenawaran();
            CariPenawaran.ShowDialog(this);
            if (CariPenawaran.DialogResult == DialogResult.OK)
            {
                BersihkanHead();
                tbNoPenawaran.Text = CariPenawaran.SelNoPenawaran;

                tbCustID.Text = CariPenawaran.IdCustom;
                lbNamaCustomer.Text = CariPenawaran.NmCustom;

                tbMarketing.Text = CariPenawaran.NmMarketing;
                seldtTable = CariPenawaran.detilTbl;
                BersihkanGrid();
                InsertPenawaranDetilToGrid();
                tbJumlah.Text = CariPenawaran.JmlKotor.ToString("#,##0.##");
                tbDiscPersen.Text = CariPenawaran.Diskon.ToString();
                tbDiscPersen_Leave(sender, e);
                tbPPN.Text = CariPenawaran.Pajak.ToString("#,##0.##");
            }
            CariPenawaran.Dispose();
        }

        private void InsertPenawaranDetilToGrid()
        {
            double brt;
            double hrg;
            double jumlah;
            double faktor = 0;
            string nama_ukuran = "";

            foreach (DataRow rw in seldtTable.Rows)
            {
                DataGridViewRow r = new DataGridViewRow();
                r.CreateCells(dgDetil);

                r.Cells[1].Value = "";    // Diminta
                r.Cells[2].Value = "";    // Ukuran diminta
                r.Cells[4].Value = rw[3].ToString();    // Id_type 
                r.Cells[5].Value = rw[4].ToString();    // Type

                faktor = double.Parse(rw[12].ToString());
                if (faktor == 6.20)
                {
                    nama_ukuran = "Ø " + rw[8].ToString();
                }
                else if (faktor == 8.00)
                {
                    nama_ukuran = "≠ " + rw[8].ToString();
                }

                r.Cells[7].Value = nama_ukuran;   // nama ukuran
                r.Cells[8].Value = rw[7].ToString();    // id_ukuran
                r.Cells[9].Value = rw[9].ToString();    // lebar
                r.Cells[10].Value = rw[10].ToString();  // panjang
                r.Cells[11].Value = rw[11].ToString();  // tinggi
                r.Cells[12].Value = rw[12].ToString();  // faktor hitung
                r.Cells[13].Value = rw[13].ToString();  // qty

                brt = double.Parse(rw[14].ToString());
                hrg = double.Parse(rw[15].ToString());
                jumlah = Math.Round(brt * hrg, 2, MidpointRounding.AwayFromZero);

                r.Cells[14].Value = brt.ToString("#,##0.##");   // berat
                r.Cells[15].Value = hrg.ToString("#,##0.##");   // harga
                r.Cells[17].Value = jumlah.ToString("#,##0.##"); 
                dgDetil.Rows.Add(r);

                TotalHarga = TotalHarga + jumlah;
            }

            tbJumlah.Text = TotalHarga.ToString("#,##0.##");
        }

        private void InitMarketingData()
        {
            MarketingTable = new DataTable();

            if (dtConn != null && dtConn.State == ConnectionState.Closed)
            {
                dtConn.Open();
            }

            string sqlcommand = "SELECT id_marketing, nama_marketing, inisial FROM m_marketing";
            MySqlDataAdapter MarketingAdapter = new MySqlDataAdapter(sqlcommand, dtConn);
            MarketingAdapter.Fill(MarketingTable);
            tbMarketing.DataSource = MarketingTable;
            tbMarketing.DisplayMember = "nama_marketing";

            dtConn.Close();
        }

        private void tbMarketing_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnGeneratNo_Click(sender, e);
        }

        private void CloneDetilTbl()
        {
            DeletedTable = EditableData.Clone();
            EditedTable = EditableData.Clone();
            InsertedTable = EditableData.Clone();
        }

        private void AddToDeletedRow(int idx)
        {
            DataRow deletedRow = DeletedTable.NewRow();
            string sHrgSat0, sHargaSat;

            deletedRow[0] = dgDetil.Rows[idx].Cells[18];   // id_dtpo
            deletedRow[1] = dgDetil.Rows[idx].Cells[4];    // Id_type
            deletedRow[2] = dgDetil.Rows[idx].Cells[5];    // nama_type
            deletedRow[3] = dgDetil.Rows[idx].Cells[8];    // id_ukuran
            deletedRow[4] = dgDetil.Rows[idx].Cells[13];   // qty
            deletedRow[6] = dgDetil.Rows[idx].Cells[7];    // Nama Ukuran
            deletedRow[7] = dgDetil.Rows[idx].Cells[9];    // Lebar
            deletedRow[8] = dgDetil.Rows[idx].Cells[10];   // Panjang
            deletedRow[9] = dgDetil.Rows[idx].Cells[11];   // tinggi
            deletedRow[10] = dgDetil.Rows[idx].Cells[12];  // faktor hitung
            deletedRow[11] = dgDetil.Rows[idx].Cells[14];  // berat
            deletedRow[12] = dgDetil.Rows[idx].Cells[16];  // disc_item

            sHrgSat0 = (dgDetil.Rows[idx].Cells[15].Value == null) ?
                "0" : dgDetil.Rows[idx].Cells[15].Value.ToString();
            sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());

            deletedRow[5] = sHargaSat;                     // harga
            DeletedTable.Rows.Add(deletedRow);
        }

        private void AddToEditedRow(int idx)
        {
            DataRow EditedRow = EditedTable.NewRow();
            string sHrgSat0, sHargaSat;
            string sBonusCust, sBonusCustomer;

            EditedRow[0] = dgDetil.Rows[idx].Cells[18].Value;   // id_dtpo
            EditedRow[1] = dgDetil.Rows[idx].Cells[4].Value;    // Id_type
            EditedRow[2] = dgDetil.Rows[idx].Cells[5].Value;    // nama_type
            EditedRow[3] = dgDetil.Rows[idx].Cells[8].Value;    // id_ukuran
            EditedRow[4] = dgDetil.Rows[idx].Cells[13].Value;   // qty
            EditedRow[6] = dgDetil.Rows[idx].Cells[7].Value;    // Nama Ukuran
            EditedRow[7] = dgDetil.Rows[idx].Cells[9].Value;    // Lebar
            EditedRow[8] = dgDetil.Rows[idx].Cells[10].Value;   // Panjang
            if ((dgDetil.Rows[idx].Cells[11].Value == "") | 
                    (dgDetil.Rows[idx].Cells[11].Value == null))
            {
                EditedRow[9] = DBNull.Value;
            }
            else EditedRow[9] = dgDetil.Rows[idx].Cells[11].Value;   // tinggi
            EditedRow[10] = dgDetil.Rows[idx].Cells[12].Value;  // faktor hitung
            EditedRow[11] = dgDetil.Rows[idx].Cells[14].Value;  // berat
            EditedRow[12] = dgDetil.Rows[idx].Cells[16].Value;  // disc_item

            sHrgSat0 = (dgDetil.Rows[idx].Cells[15].Value == null) ?
                "0" : dgDetil.Rows[idx].Cells[15].Value.ToString();
            sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());

            EditedRow[5] = sHargaSat;                     // harga

            sBonusCust = (dgDetil.Rows[idx].Cells[20].Value == null) ?
                "0" : dgDetil.Rows[idx].Cells[20].Value.ToString();
            sBonusCustomer = new string(sBonusCust.Where(char.IsDigit).ToArray());

            EditedRow[13] = sBonusCustomer;                      // Bonus Customer
            EditedRow[14] = dgDetil.Rows[idx].Cells[21].Value;   // Persen
            EditedTable.Rows.Add(EditedRow);
        }

        private void UpdateMasterTrx(int TxId)
        {
            MySqlCommand tcmd = new MySqlCommand();

            StringBuilder tSql = new StringBuilder();

            string tglTrx = dtTglPO.Value.ToString("yyyy-MM-dd");

            MySqlParameter prmIdMst = new MySqlParameter("IdMst", TxId);
            MySqlParameter prmtgl = new MySqlParameter("tgl", tglTrx);
            MySqlParameter prmnopo = new MySqlParameter("nopo", MySqlDbType.String);
            MySqlParameter prmpocust = new MySqlParameter("pocust", MySqlDbType.String);
            MySqlParameter prmnopenawaran = new MySqlParameter("nopenawaran", MySqlDbType.String);
            MySqlParameter prmidcust = new MySqlParameter("idcust", MySqlDbType.Int32);
            MySqlParameter prmJml = new MySqlParameter("jumlah", MySqlDbType.Double);
            MySqlParameter prmDisc = new MySqlParameter("disc", MySqlDbType.Int32);
            MySqlParameter prmPpn = new MySqlParameter("ppn", MySqlDbType.Double);
            MySqlParameter prmKet = new MySqlParameter("ketrangan", tbKetrangan.Text);
            MySqlParameter prmMark = new MySqlParameter("marketing", MySqlDbType.Int32);
            MySqlParameter prmReff = new MySqlParameter("reff", MySqlDbType.VarChar);
            MySqlParameter prmKrdit = new MySqlParameter("krdit", MySqlDbType.UInt16);

            tSql.AppendLine("UPDATE t_po SET tanggal_po = @tgl, No_Penawaran = @nopenawaran,");
            tSql.AppendLine("  id_customer = @idcust, jumlah = @jumlah,");
            tSql.AppendLine("  disc = @disc, ppn = @ppn, keterangan = @ketrangan,");
            tSql.AppendLine("  id_marketing = @marketing, reff = @reff,");
            tSql.AppendLine("  no_po = @nopo, po_customer = @pocust, kredit = @krdit");
            tSql.AppendLine("WHERE id_po = @IdMst");

            try
            {
                tcmd.CommandText = tSql.ToString();
                prmnopo.Value = tbNoPO.Text;
                if (tb_PoCustomer.Text == "")
                {
                    prmpocust.Value = DBNull.Value;
                }
                else prmpocust.Value = tb_PoCustomer.Text;
                if (tbNoPenawaran.Text == "")
                {
                    prmnopenawaran.Value = DBNull.Value;
                }
                else prmnopenawaran.Value = tbNoPenawaran.Text;
                prmidcust.Value = tbCustID.Text;
                prmJml.Value = TotalHarga;
                prmDisc.Value = (int)Discpersen;
                if (cbPPn.Checked)
                {
                    prmPpn.Value = PpnNominal;
                }
                else prmPpn.Value = DBNull.Value;
                prmMark.Value = MarketingTable.Rows[tbMarketing.SelectedIndex][0];
                prmKrdit.Value = seKredit.Value;
                prmReff.Value = tbReff.Text;

                tcmd.Parameters.Add(prmIdMst);
                tcmd.Parameters.Add(prmtgl);
                tcmd.Parameters.Add(prmnopo);
                tcmd.Parameters.Add(prmpocust);
                tcmd.Parameters.Add(prmnopenawaran);
                tcmd.Parameters.Add(prmidcust);
                tcmd.Parameters.Add(prmJml);
                tcmd.Parameters.Add(prmDisc);
                tcmd.Parameters.Add(prmPpn);
                tcmd.Parameters.Add(prmKet);
                tcmd.Parameters.Add(prmMark);
                tcmd.Parameters.Add(prmReff);
                tcmd.Parameters.Add(prmKrdit);
                tcmd.Connection = dtConn;
                tcmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void AddNewType(int RowIdx)
        {
            // Menampilkan list barang untuk dipilih
            fr_listBarang LstBarang = new fr_listBarang();
            LstBarang.ShowDialog(this);
            dgDetil.Rows[RowIdx].Cells[4].Value = LstBarang.SelectedTypeID;
            dgDetil.Rows[RowIdx].Cells[5].Value = LstBarang.SelectedTypeNama;
            LstBarang.Dispose();
        }

        private void AddUkuran(int RowIdx)
        {
            fr_FindUkuran LstUkuran = new fr_FindUkuran();
            LstUkuran.ShowDialog(this);
            if (LstUkuran.DialogResult == DialogResult.OK)
            {

                dgDetil.Rows[RowIdx].Cells[7].Value = LstUkuran.NamaUkuran;
                dgDetil.Rows[RowIdx].Cells[8].Value = LstUkuran.IDUkuran;
                dgDetil.Rows[RowIdx].Cells[9].Value = LstUkuran.Lebar;
                dgDetil.Rows[RowIdx].Cells[10].Value = LstUkuran.Panjang;
                dgDetil.Rows[RowIdx].Cells[11].Value = LstUkuran.Tebal;
                dgDetil.Rows[RowIdx].Cells[12].Value = LstUkuran.fkt_hitung;
                // fokus to the next cell
                dgDetil.CurrentCell = dgDetil.Rows[RowIdx].Cells[13];
            }
            LstUkuran.Dispose();
        }

        private void EditingType(int RowIdx)
        {
            string OldIdType;
            int Id_dtl;
            int TblrowIndex;

            OldIdType = (dgDetil.Rows[RowIdx].Cells[4].Value == null) ?
                "" : dgDetil.Rows[RowIdx].Cells[4].Value.ToString();

            AddNewType(RowIdx);

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[18].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[18].Value.ToString());
                // Cek jika OldIdType != (Idtype di grid), maka editing
                if (OldIdType != dgDetil.Rows[RowIdx].Cells[4].Value.ToString())
                {
                    // Add/Edit EditedTable
                    // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                    TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                    if (TblrowIndex != -1)
                    {
                        // Update EditedTable
                        // Update idnya saja, karena namanya tidak perlu diupdate
                        DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 1, dgDetil.Rows[RowIdx].Cells[4].Value);
                    }
                    else
                    {
                        AddToEditedRow(RowIdx);
                    }
                }
            }
            else
            {
                // Update InsertedTable
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 1, dgDetil.Rows[RowIdx].Cells[4].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[19].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 1, dgDetil.Rows[RowIdx].Cells[4].Value);
                }
            }
        }

        private void EditingUkuran(int RowIdx)
        {
            string OldIdUkuran;
            int Id_dtl;
            int TblrowIndex;

            // Pindahkan IdUkuran di grid ke OldIdUkuran
            OldIdUkuran = (dgDetil.Rows[RowIdx].Cells[8].Value == null) ?
                "" : dgDetil.Rows[RowIdx].Cells[8].Value.ToString();

            AddUkuran(RowIdx);

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[18].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[18].Value.ToString());
                // Cek jika OldIdUkuran != (IdUkuran di grid), maka editing
                if (OldIdUkuran != dgDetil.Rows[RowIdx].Cells[8].Value.ToString())
                {
                    // Add/Edit EditedTable
                    // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                    TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                    if (TblrowIndex != -1)
                    {
                        // Update EditedTable
                        // Update idnya saja, karena namanya tidak perlu diupdate
                        DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 3, dgDetil.Rows[RowIdx].Cells[8].Value);
                    }
                    else
                    {
                        AddToEditedRow(RowIdx);
                    }
                }
            }
            else
            {
                // Update InsertedTable
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 3, dgDetil.Rows[RowIdx].Cells[8].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[19].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 3, dgDetil.Rows[RowIdx].Cells[8].Value);
                }
            }
        }

        private void EditingQty(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[18].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[18].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 4, dgDetil.Rows[RowIdx].Cells[13].Value);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 4, dgDetil.Rows[RowIdx].Cells[13].Value);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 11, dgDetil.Rows[RowIdx].Cells[14].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[19].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 4, dgDetil.Rows[RowIdx].Cells[13].Value);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 11, dgDetil.Rows[RowIdx].Cells[14].Value);
                }
            }
        }

        private void EditingBerat(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[18].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[18].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 11, dgDetil.Rows[RowIdx].Cells[14].Value);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 11, dgDetil.Rows[RowIdx].Cells[14].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[19].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 11, dgDetil.Rows[RowIdx].Cells[14].Value);
                }
            }
        }

        private void EditingHarga(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;
            string sHrgSat0 = (dgDetil.Rows[RowIdx].Cells[15].Value == null) ?
                "0" : dgDetil.Rows[RowIdx].Cells[15].Value.ToString();
            string sHargaSat = new string(sHrgSat0.Where(char.IsDigit).ToArray());
            Int32 hrg = Convert.ToInt32(sHargaSat);

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[18].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[18].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 5, hrg);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 5, hrg);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[19].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 5, hrg);
                }
            }
        }

        private void EditingDiscItem(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[18].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[18].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 12, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 12, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[19].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 12, dgDetil.Rows[RowIdx].Cells[16].Value);
                }
            }
        }

        private void EditingBonusCust(int RowIdx)
        {
            int Id_dtl;
            int TblrowIndex;

            // Cek dulu apakah IdCol kosong
            if (dgDetil.Rows[RowIdx].Cells[18].Value != null)
            {
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[18].Value.ToString());
                // Add/Edit EditedTable
                // Jika ada id_dtlpenawaran yang sama di EditedTable => Edit
                TblrowIndex = DataUtils.FindIndex(EditedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    // Update EditedTable
                    // Update idnya saja, karena namanya tidak perlu diupdate
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[20].Value);
                    DataUtils.UpdateTable(ref EditedTable, TblrowIndex, 14, dgDetil.Rows[RowIdx].Cells[21].Value);
                }
                else
                {
                    AddToEditedRow(RowIdx);
                }
            }
            else
            {
                // Update row di Inserted
                Id_dtl = Convert.ToInt32(dgDetil.Rows[RowIdx].Cells[19].Value.ToString());
                TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                if (TblrowIndex != -1)
                {
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[20].Value);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 14, dgDetil.Rows[RowIdx].Cells[21].Value);
                }
                else
                {
                    DataUtils.InsertTable(ref InsertedTable, 0, dgDetil.Rows[RowIdx].Cells[19].Value);
                    TblrowIndex = DataUtils.FindIndex(InsertedTable, Id_dtl);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 13, dgDetil.Rows[RowIdx].Cells[20].Value);
                    DataUtils.UpdateTable(ref InsertedTable, TblrowIndex, 14, dgDetil.Rows[RowIdx].Cells[21].Value);
                }
            }
        }

        private void UpdateDtlTrx(int TxId)
        {
            int i, rowCount;

            rowCount = DeletedTable.Rows.Count;
            if (rowCount > 0)
            {
                foreach (DataRow rw in DeletedTable.Rows)
                {
                    DeleteDetil(Convert.ToInt32(rw[0].ToString()));
                }
            }

            rowCount = EditedTable.Rows.Count;
            if (rowCount > 0)
            {
                foreach (DataRow rw in EditedTable.Rows)
                {
                    UpdateDetil(rw);
                }
            }

            rowCount = InsertedTable.Rows.Count;
            if (rowCount > 0)
            {
                foreach (DataRow rw in InsertedTable.Rows)
                {
                    InsertDetil(rw, TxId);
                }
            }
        }

        private void DeleteDetil(int TxId)
        {
            string strSqlCmd = "DELETE FROM dt_po WHERE id_dt_po = @idkey";
            MySqlCommand command = new MySqlCommand(strSqlCmd, dtConn);
            MySqlParameter prmId = new MySqlParameter("idkey", MySqlDbType.Int32);

            prmId.Value = TxId;
            command.Parameters.Add(prmId);
            command.ExecuteNonQuery();
        }

        private void UpdateDetil(DataRow row)
        {
            StringBuilder dtlSql = new StringBuilder();
            dtlSql.AppendLine("UPDATE dt_po SET id_type = @idbrg, disc_item = @discitm, ");
            dtlSql.AppendLine("  id_ukuran = @idukr, qty_po = @qty, berat = @berat, harga_po = @harga, ");
            dtlSql.AppendLine("  bonus_cust = @bnscust, persen = @prsen");
            dtlSql.AppendLine("WHERE id_dt_po = @iddttx");

            MySqlParameter prmIdtype = new MySqlParameter("Idbrg", MySqlDbType.Int32);
            MySqlParameter prmIdUkur = new MySqlParameter("idukr", MySqlDbType.Int32);
            MySqlParameter prmQty = new MySqlParameter("qty", MySqlDbType.Int32);
            MySqlParameter prmBerat = new MySqlParameter("berat", MySqlDbType.Double);
            MySqlParameter prmHarga = new MySqlParameter("harga", MySqlDbType.Int32);
            MySqlParameter prmIdDtTx = new MySqlParameter("iddttx", MySqlDbType.Int32);
            MySqlParameter prmdisitm = new MySqlParameter("discitm", MySqlDbType.Int32);
            MySqlParameter prmbonuscust = new MySqlParameter("bnscust", MySqlDbType.Int32);
            MySqlParameter prmpersen = new MySqlParameter("prsen", MySqlDbType.Byte);

            prmIdtype.Value = Convert.ToInt32(row[1].ToString());
            prmIdUkur.Value = Convert.ToInt32(row[3].ToString());
            prmQty.Value = Convert.ToInt32(row[4].ToString());
            prmBerat.Value = Convert.ToDouble(row[11].ToString());
            prmHarga.Value = Convert.ToInt32(row[5].ToString());
            prmIdDtTx.Value = Convert.ToInt32(row[0].ToString());
            prmdisitm.Value = Convert.ToInt32(row[12].ToString());
            prmbonuscust.Value = Convert.ToInt32(row[13].ToString());
            prmpersen.Value = row[14];

            MySqlCommand command = new MySqlCommand(dtlSql.ToString(), dtConn);
            command.Parameters.Add(prmIdtype);
            command.Parameters.Add(prmIdUkur);
            command.Parameters.Add(prmQty);
            command.Parameters.Add(prmBerat);
            command.Parameters.Add(prmHarga);
            command.Parameters.Add(prmIdDtTx);
            command.Parameters.Add(prmdisitm);
            command.Parameters.Add(prmbonuscust);
            command.Parameters.Add(prmpersen);
            command.ExecuteNonQuery();

            command.Parameters.Clear();
        }

        private void InsertDetil(DataRow row, int TxId)
        {
            StringBuilder dtlSql = new StringBuilder();
            dtlSql.AppendLine("INSERT INTO dt_po (id_po, id_type, id_ukuran, ");
            dtlSql.AppendLine("   qty_po, berat, harga_po, disc_item, bonus_cust, persen)");
            dtlSql.AppendLine("VALUES (@idTx, @idbrg, @idukr, @qty, @berat, ");
            dtlSql.AppendLine("  @harga, @discitm, @bnscust, @prsen)");

            try
            {
                MySqlCommand dtlCommand = new MySqlCommand(dtlSql.ToString(), dtConn);

                MySqlParameter prmIdTx = new MySqlParameter("idTx", MySqlDbType.Int32);
                MySqlParameter prmIdBrg = new MySqlParameter("idbrg", MySqlDbType.Int32);
                MySqlParameter prmQty = new MySqlParameter("qty", MySqlDbType.Int32);
                MySqlParameter prmHrg = new MySqlParameter("harga", MySqlDbType.Double);
                MySqlParameter prmUkuran = new MySqlParameter("idukr", MySqlDbType.Int32);
                MySqlParameter prmberat = new MySqlParameter("berat", MySqlDbType.Double);
                MySqlParameter prmdiscitm = new MySqlParameter("discitm", MySqlDbType.Int32);
                MySqlParameter prmbonuscust = new MySqlParameter("bnscust", MySqlDbType.Int32);
                MySqlParameter prmpersen = new MySqlParameter("prsen", MySqlDbType.Byte);

                prmIdTx.Value = TxId;
                prmIdBrg.Value = Convert.ToInt32(row[1].ToString());
                prmQty.Value = Convert.ToInt32(row[4].ToString());
                prmUkuran.Value = Convert.ToInt32(row[3].ToString());
                prmHrg.Value = Convert.ToInt32(row[5].ToString());
                prmberat.Value = Convert.ToDouble(row[11].ToString());
                prmdiscitm.Value = Convert.ToUInt32(row[12].ToString());
                prmbonuscust.Value = Convert.ToInt32(row[13].ToString());
                prmpersen.Value = row[14];

                dtlCommand.Parameters.Add(prmIdTx);
                dtlCommand.Parameters.Add(prmIdBrg);
                dtlCommand.Parameters.Add(prmUkuran);
                dtlCommand.Parameters.Add(prmQty);
                dtlCommand.Parameters.Add(prmHrg);
                dtlCommand.Parameters.Add(prmberat);
                dtlCommand.Parameters.Add(prmdiscitm);
                dtlCommand.Parameters.Add(prmbonuscust);
                dtlCommand.Parameters.Add(prmpersen);

                dtlCommand.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

    }
}
