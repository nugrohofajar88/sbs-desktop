﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
//using DevExpress.LookAndFeel;
using DevExpress.XtraReports.UI;
//using ListCustomer.Print;

namespace SalesBaja
{
    public partial class frmEditPO : Form
    {
        public int IDTransaksi;
        public byte FormMode;
        private string _userconnected;

        public string Userconnected
        {
            get
            {
                return _userconnected;
            }

            set
            {
                _userconnected = value;
            }
        }

        public frmEditPO()
        {
            InitializeComponent();
        }

        private void SaveEdited()
        {
            string dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
                GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                GlobVar.hdbase;
            MySqlConnection dtConn = new MySqlConnection(dtConnString);
            MySqlCommand tcmd = new MySqlCommand();
            StringBuilder tSql = new StringBuilder();

            tSql.AppendLine("UPDATE t_po SET no_po = '" + tbNoPO.Text + "'");
            tSql.AppendLine("WHERE id_po = " + int.Parse(IDTransaksi.ToString()));

            dtConn.Open();
            try
            {
                tcmd.CommandText = tSql.ToString();
                //tcmd.Parameters.Add(prmIdMst);
                //tcmd.Parameters.Add(prmtgl);
                //tcmd.Parameters.Add(prmnopo);
                //tcmd.Parameters.Add(prmidmkt);
                //tcmd.Parameters.Add(prmidcust);
                tcmd.Connection = dtConn;
                if (tcmd.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Data Master telah terupdate");
                }
                else
                {
                    MessageBox.Show("Data tidak diupdate");
                }

            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }

            dtConn.Close();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (btnSimpan.Text == "Simpan")
            {
                SaveEdited();
            } else
            {
                Close();
            }
        }

        private void PlaceTheData(IDataRecord recrd)
        {
            Double Jumlah = Convert.ToDouble(recrd[8].ToString());
            Double Discpersen = Convert.ToDouble(recrd[9].ToString());
            Double DiscNominal = Math.Ceiling(Jumlah * (Discpersen / 100));
            Double JmlStlhDisc = Jumlah - DiscNominal;
            Double PPN = Convert.ToDouble(recrd[10].ToString());

            string np = recrd.IsDBNull(12) ? "" : recrd[12].ToString();
            tbNoPenawaran.Text = recrd[1].ToString();
            tbCustID.Text = recrd[3].ToString();
            lbNamaCustomer.Text = recrd[6].ToString();
            dtTglPO.Text = recrd[5].ToString();
            tbNoPO.Text = np;
            tbMarketing.Text = recrd[13].ToString();
            tbReff.Text = recrd[14].ToString();

            tbJumlah.Text = Jumlah.ToString("#,##0.00");
            tbDiscPersen.Text = recrd[9].ToString();
            tbDiscNominal.Text = DiscNominal.ToString("#,##0.00");
            tbTotal.Text = JmlStlhDisc.ToString("#,##0.00");
            tbPPN.Text = PPN.ToString("#,##0.00");
            cbPPn.Checked = (PPN != 0);

            tbGrandTotal.Text = (JmlStlhDisc + PPN).ToString("#,##0.00");

            tbKetrangan.Text = recrd[11].ToString();
        }

        public void LoadMasterData()
        {
            string dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
                GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                GlobVar.hdbase;
            MySqlConnection dtConn = new MySqlConnection(dtConnString);

            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT p.id_po, p.No_Penawaran, p.user_marketing, p.Id_Customer, p.status_po, ");
            qry.AppendLine("  p.tanggal_po, c.nama_customer, u.username, p.jumlah, p.disc, p.ppn, ");
            qry.AppendLine("  p.keterangan, p.no_po, p.marketing, p.reff");
            qry.AppendLine("FROM t_po p");
            qry.AppendLine("INNER JOIN m_customer c ON p.Id_Customer = c.id_customer");
            qry.AppendLine("INNER JOIN m_user u ON p.user_marketing = u.id_user");
            qry.AppendLine("WHERE p.id_po = @Key");

            dtConn.Open();
            MySqlCommand edComm = new MySqlCommand(qry.ToString(), dtConn);
            MySqlParameter Kid = new MySqlParameter("Key", MySqlDbType.Int32);
            Kid.Value = IDTransaksi;
            edComm.Parameters.Add(Kid);
            MySqlDataReader rd = edComm.ExecuteReader();

            while (rd.Read())
            {
                PlaceTheData((IDataRecord)rd);
            }
            rd.Close();
            SetFormMode();
            LoadDetilData(dtConn);
            dtConn.Close();
        }

        private void LoadDetilData(MySqlConnection conn)
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT dp.id_po, dp.id_dt_po, dp.id_type, mt.`type`, dp.id_ukuran, ");
            qry.AppendLine("  dp.diminta, dp.qty_po, dp.harga_po, mu.nama_ukuran, mu.panjang, ");
            qry.AppendLine("  mu.lebar, mu.tinggi, mu.faktor_hitung");
            qry.AppendLine("FROM DT_PO dp");
            qry.AppendLine("INNER JOIN m_type mt ON dp.id_type = mt.id_type");
            qry.AppendLine("INNER JOIN m_ukuran mu ON dp.id_ukuran = mu.id_ukuran");
            qry.AppendLine("WHERE dp.id_po = @Key");

            MySqlCommand edComm = new MySqlCommand(qry.ToString(), conn);

            MySqlParameter Kid = new MySqlParameter("Key", MySqlDbType.Int32);
            Kid.Value = IDTransaksi;
            edComm.Parameters.Add(Kid);

            MySqlDataAdapter dtAdapter = new MySqlDataAdapter(edComm);
            DataTable EditableData = new DataTable();
            dtAdapter.Fill(EditableData);

            Double lbr, pjg, tinggi;
            Double berat, faktor;
            Double Jml;
            Int32 Qty, Harga;

            foreach (DataRow row in EditableData.Rows)
            {
                DataGridViewRow rw = new DataGridViewRow();
                rw.CreateCells(dgDetil);

                rw.Cells[1].Value = row[5].ToString();
                rw.Cells[2].Value = "";   // Ukuran diminta
                rw.Cells[4].Value = row[2].ToString();
                rw.Cells[5].Value = row[3].ToString();
                //rw.Cells[5].Value = row[3].ToString();
                rw.Cells[7].Value = row[4].ToString();
                rw.Cells[8].Value = row[10].ToString();
                rw.Cells[9].Value = row[9].ToString();
                lbr = Convert.ToDouble(row[10].ToString());
                pjg = Convert.ToDouble(row[9].ToString());
                tinggi = string.IsNullOrEmpty(row[11].ToString()) ? 0.00 : 
                    Convert.ToDouble(row[11].ToString());
                rw.Cells[10].Value = tinggi.ToString();
                rw.Cells[11].Value = row[12].ToString();
                rw.Cells[12].Value = row[6].ToString();
                faktor = Convert.ToDouble(row[12].ToString());
                Qty = Convert.ToInt32(row[6].ToString());
                berat = HitBerat(lbr, pjg, tinggi, Qty, faktor);
                Harga = Convert.ToInt32(row[7].ToString());
                Jml = Math.Ceiling(berat * Harga);

                rw.Cells[13].Value = berat.ToString("#,##0.00");
                rw.Cells[14].Value = Harga.ToString("#,##0.00");
                rw.Cells[15].Value = Jml.ToString("#,##0.00");

                dgDetil.Rows.Add(rw);
            }
        }

        private void SetFormMode()
        {
            switch (FormMode)
            {
                case 1:
                    {
                        this.Text = "Penawaran - [Edit]";
                        dtTglPO.Enabled = false;
                        tbNoPenawaran.ReadOnly = true;
                        tbCustID.ReadOnly = true;
                        tbMarketing.ReadOnly = true;
                        tbReff.ReadOnly = true;
                        btnCari.Enabled = false;
                        tbKetrangan.ReadOnly = true;
                        btnRowAdd.Enabled = false;
                        btnDel.Enabled = false;
                        dgDetil.Enabled = false;

                        tbJumlah.Enabled = false;
                        tbDiscPersen.Enabled = false;
                        tbDiscNominal.Enabled = false;
                        tbTotal.Enabled = false;
                        cbPPn.Enabled = false;
                        tbPPN.Enabled = false;
                        tbGrandTotal.Enabled = false;
                        btnCetak.Visible = true;
                        btnSimpan.Text = "Simpan";
                        break;
                    }
                case 2:
                    {
                        this.Text = "Penawaran - [View]";
                        dtTglPO.Enabled = false;
                        tbNoPenawaran.ReadOnly = true;
                        tbCustID.ReadOnly = true;
                        tbMarketing.ReadOnly = true;
                        tbReff.ReadOnly = true;
                        btnCari.Enabled = false;
                        tbKetrangan.ReadOnly = true;
                        btnRowAdd.Enabled = false;
                        btnDel.Enabled = false;
                        dgDetil.Enabled = false;
                        tbNoPO.Enabled = false;

                        tbJumlah.Enabled = false;
                        tbDiscPersen.Enabled = false;
                        tbDiscNominal.Enabled = false;
                        tbTotal.Enabled = false;
                        cbPPn.Enabled = false;
                        tbPPN.Enabled = false;
                        tbGrandTotal.Enabled = false;
                        btnSimpan.Text = "Tutup";
                        break;
                    }
            }
        }

        private Double HitBerat(Double Lebar, Double Panjang, Double Tinggi, Int32 Qty, Double Faktor)
        {
            Double Berat = 0.00;

            if (Faktor == 6.2)
            {
                Berat = (Lebar / 100) * (Lebar / 100) * (Panjang / 100) * Faktor;
            }
            else if (Faktor == 8 )
            {
                Berat = (Lebar / 100) * (Tinggi / 100) * (Panjang / 100) * Faktor;
            }

            Berat = Qty * Berat;

            return Berat;
        }

        private void frmEditPO_Load(object sender, EventArgs e)
        {
            toolStatUsername.Text = _userconnected;
        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            //PenawaranPrint PrintPenawaran = new PenawaranPrint();

            //PrintPenawaran.Parameters["ParamID"].Value = IDTransaksi;
            //PrintPenawaran.Parameters["ParamID"].Visible = false;
            //using (ReportPrintTool printTool = new ReportPrintTool(PrintPenawaran))
            //{
            //    printTool.ShowPreviewDialog();
            //}
        }
    }
}
