﻿namespace SalesBaja
{
    partial class frmEditPO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditPO));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbNamaCustomer = new System.Windows.Forms.Label();
            this.btnCari = new System.Windows.Forms.Button();
            this.tbCustID = new System.Windows.Forms.TextBox();
            this.tbReff = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbMarketing = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbNoPenawaran = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbNoPO = new System.Windows.Forms.TextBox();
            this.lbNoPO = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtTglPO = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgDetil = new System.Windows.Forms.DataGridView();
            this.selColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Diminta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UkuranDiminta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cari = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colsGoodType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdDetil = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colGoodsIdUkuran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodsWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodsLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colgoodsHeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Faktor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodsQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Berat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDel = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnRowAdd = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.tbKetrangan = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbPPn = new System.Windows.Forms.CheckBox();
            this.tbGrandTotal = new System.Windows.Forms.TextBox();
            this.tbPPN = new System.Windows.Forms.TextBox();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.tbDiscNominal = new System.Windows.Forms.TextBox();
            this.tbJumlah = new System.Windows.Forms.TextBox();
            this.tbDiscPersen = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCetak = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.statPO = new System.Windows.Forms.StatusStrip();
            this.toolStatuName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStatUsername = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetil)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statPO.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lbNamaCustomer);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.tbCustID);
            this.groupBox1.Controls.Add(this.tbReff);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbMarketing);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbNoPenawaran);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbNoPO);
            this.groupBox1.Controls.Add(this.lbNoPO);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtTglPO);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1353, 122);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Master Transaksi";
            // 
            // lbNamaCustomer
            // 
            this.lbNamaCustomer.AutoSize = true;
            this.lbNamaCustomer.Location = new System.Drawing.Point(246, 61);
            this.lbNamaCustomer.Name = "lbNamaCustomer";
            this.lbNamaCustomer.Size = new System.Drawing.Size(124, 20);
            this.lbNamaCustomer.TabIndex = 18;
            this.lbNamaCustomer.Text = "Nama Customer";
            // 
            // btnCari
            // 
            this.btnCari.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCari.Location = new System.Drawing.Point(198, 57);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(38, 25);
            this.btnCari.TabIndex = 4;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = true;
            // 
            // tbCustID
            // 
            this.tbCustID.Location = new System.Drawing.Point(116, 57);
            this.tbCustID.Name = "tbCustID";
            this.tbCustID.Size = new System.Drawing.Size(75, 26);
            this.tbCustID.TabIndex = 3;
            // 
            // tbReff
            // 
            this.tbReff.Location = new System.Drawing.Point(1079, 86);
            this.tbReff.Name = "tbReff";
            this.tbReff.Size = new System.Drawing.Size(211, 26);
            this.tbReff.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(956, 89);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 20);
            this.label11.TabIndex = 15;
            this.label11.Text = "Reff";
            // 
            // tbMarketing
            // 
            this.tbMarketing.Location = new System.Drawing.Point(1079, 58);
            this.tbMarketing.Name = "tbMarketing";
            this.tbMarketing.Size = new System.Drawing.Size(211, 26);
            this.tbMarketing.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(956, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 20);
            this.label10.TabIndex = 13;
            this.label10.Text = "Marketing";
            // 
            // tbNoPenawaran
            // 
            this.tbNoPenawaran.Location = new System.Drawing.Point(1079, 30);
            this.tbNoPenawaran.Name = "tbNoPenawaran";
            this.tbNoPenawaran.Size = new System.Drawing.Size(211, 26);
            this.tbNoPenawaran.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(956, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "No. Penawaran";
            // 
            // tbNoPO
            // 
            this.tbNoPO.Location = new System.Drawing.Point(116, 86);
            this.tbNoPO.Name = "tbNoPO";
            this.tbNoPO.Size = new System.Drawing.Size(182, 26);
            this.tbNoPO.TabIndex = 6;
            // 
            // lbNoPO
            // 
            this.lbNoPO.AutoSize = true;
            this.lbNoPO.Location = new System.Drawing.Point(44, 89);
            this.lbNoPO.Name = "lbNoPO";
            this.lbNoPO.Size = new System.Drawing.Size(63, 20);
            this.lbNoPO.TabIndex = 6;
            this.lbNoPO.Text = "No. PO ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer";
            // 
            // dtTglPO
            // 
            this.dtTglPO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTglPO.Location = new System.Drawing.Point(116, 28);
            this.dtTglPO.Name = "dtTglPO";
            this.dtTglPO.Size = new System.Drawing.Size(182, 26);
            this.dtTglPO.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tanggal";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dgDetil);
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 128);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1353, 372);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detil Transaksi";
            // 
            // dgDetil
            // 
            this.dgDetil.AllowUserToAddRows = false;
            this.dgDetil.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgDetil.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDetil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetil.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selColumn,
            this.Diminta,
            this.UkuranDiminta,
            this.Cari,
            this.colsGoodType,
            this.colGoodTypeName,
            this.colIdDetil,
            this.colGoodsIdUkuran,
            this.colGoodsWidth,
            this.colGoodsLength,
            this.colgoodsHeight,
            this.Faktor,
            this.colGoodsQty,
            this.Berat,
            this.colGoodPrice,
            this.Total});
            this.dgDetil.Location = new System.Drawing.Point(6, 59);
            this.dgDetil.Name = "dgDetil";
            this.dgDetil.RowHeadersVisible = false;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgDetil.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgDetil.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDetil.Size = new System.Drawing.Size(1341, 306);
            this.dgDetil.TabIndex = 4;
            // 
            // selColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.NullValue = false;
            this.selColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.selColumn.HeaderText = "";
            this.selColumn.Name = "selColumn";
            this.selColumn.Width = 35;
            // 
            // Diminta
            // 
            this.Diminta.HeaderText = "Diminta";
            this.Diminta.Name = "Diminta";
            this.Diminta.Width = 200;
            // 
            // UkuranDiminta
            // 
            this.UkuranDiminta.HeaderText = "Ukuran Diminta";
            this.UkuranDiminta.Name = "UkuranDiminta";
            this.UkuranDiminta.Visible = false;
            // 
            // Cari
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Cari.DefaultCellStyle = dataGridViewCellStyle3;
            this.Cari.DividerWidth = 1;
            this.Cari.HeaderText = "Type";
            this.Cari.Name = "Cari";
            this.Cari.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Cari.Text = "Cari";
            this.Cari.UseColumnTextForButtonValue = true;
            this.Cari.Width = 70;
            // 
            // colsGoodType
            // 
            this.colsGoodType.HeaderText = "IDType";
            this.colsGoodType.Name = "colsGoodType";
            this.colsGoodType.ReadOnly = true;
            this.colsGoodType.Visible = false;
            // 
            // colGoodTypeName
            // 
            this.colGoodTypeName.HeaderText = "Type";
            this.colGoodTypeName.Name = "colGoodTypeName";
            this.colGoodTypeName.ReadOnly = true;
            this.colGoodTypeName.Width = 125;
            // 
            // colIdDetil
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.colIdDetil.DefaultCellStyle = dataGridViewCellStyle4;
            this.colIdDetil.HeaderText = "Ukuran";
            this.colIdDetil.Name = "colIdDetil";
            this.colIdDetil.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colIdDetil.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIdDetil.Text = "Cari";
            this.colIdDetil.UseColumnTextForButtonValue = true;
            this.colIdDetil.Width = 70;
            // 
            // colGoodsIdUkuran
            // 
            this.colGoodsIdUkuran.HeaderText = "IdUkuran";
            this.colGoodsIdUkuran.Name = "colGoodsIdUkuran";
            this.colGoodsIdUkuran.ReadOnly = true;
            this.colGoodsIdUkuran.Visible = false;
            // 
            // colGoodsWidth
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = "0";
            this.colGoodsWidth.DefaultCellStyle = dataGridViewCellStyle5;
            this.colGoodsWidth.HeaderText = "Lebar";
            this.colGoodsWidth.Name = "colGoodsWidth";
            this.colGoodsWidth.ReadOnly = true;
            // 
            // colGoodsLength
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "0";
            this.colGoodsLength.DefaultCellStyle = dataGridViewCellStyle6;
            this.colGoodsLength.HeaderText = "Panjang";
            this.colGoodsLength.Name = "colGoodsLength";
            this.colGoodsLength.ReadOnly = true;
            // 
            // colgoodsHeight
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = "0";
            this.colgoodsHeight.DefaultCellStyle = dataGridViewCellStyle7;
            this.colgoodsHeight.HeaderText = "Tinggi";
            this.colgoodsHeight.Name = "colgoodsHeight";
            this.colgoodsHeight.ReadOnly = true;
            // 
            // Faktor
            // 
            this.Faktor.HeaderText = "Faktor";
            this.Faktor.Name = "Faktor";
            this.Faktor.ReadOnly = true;
            this.Faktor.Visible = false;
            // 
            // colGoodsQty
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = "0";
            this.colGoodsQty.DefaultCellStyle = dataGridViewCellStyle8;
            this.colGoodsQty.HeaderText = "Qty";
            this.colGoodsQty.Name = "colGoodsQty";
            // 
            // Berat
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Berat.DefaultCellStyle = dataGridViewCellStyle9;
            this.Berat.HeaderText = "Berat";
            this.Berat.Name = "Berat";
            this.Berat.ReadOnly = true;
            // 
            // colGoodPrice
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = "0";
            this.colGoodPrice.DefaultCellStyle = dataGridViewCellStyle10;
            this.colGoodPrice.HeaderText = "Harga/Kg";
            this.colGoodPrice.Name = "colGoodPrice";
            this.colGoodPrice.Width = 150;
            // 
            // Total
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            dataGridViewCellStyle11.NullValue = "0";
            this.Total.DefaultCellStyle = dataGridViewCellStyle11;
            this.Total.HeaderText = "Jumlah";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 175;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.btnDel);
            this.panel2.Controls.Add(this.btnRowAdd);
            this.panel2.Location = new System.Drawing.Point(6, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1341, 31);
            this.panel2.TabIndex = 3;
            // 
            // btnDel
            // 
            this.btnDel.FlatAppearance.BorderSize = 0;
            this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.ImageIndex = 0;
            this.btnDel.ImageList = this.imageList1;
            this.btnDel.Location = new System.Drawing.Point(77, 4);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(70, 23);
            this.btnDel.TabIndex = 5;
            this.btnDel.Text = "Delete";
            this.btnDel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDel.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "delete.ico");
            this.imageList1.Images.SetKeyName(1, "1473946589_add.ico");
            // 
            // btnRowAdd
            // 
            this.btnRowAdd.FlatAppearance.BorderSize = 0;
            this.btnRowAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRowAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRowAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRowAdd.ImageIndex = 1;
            this.btnRowAdd.ImageList = this.imageList1;
            this.btnRowAdd.Location = new System.Drawing.Point(5, 4);
            this.btnRowAdd.Name = "btnRowAdd";
            this.btnRowAdd.Size = new System.Drawing.Size(70, 23);
            this.btnRowAdd.TabIndex = 4;
            this.btnRowAdd.Text = "Tambah";
            this.btnRowAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRowAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRowAdd.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.tbKetrangan);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.cbPPn);
            this.panel3.Controls.Add(this.tbGrandTotal);
            this.panel3.Controls.Add(this.tbPPN);
            this.panel3.Controls.Add(this.tbTotal);
            this.panel3.Controls.Add(this.tbDiscNominal);
            this.panel3.Controls.Add(this.tbJumlah);
            this.panel3.Controls.Add(this.tbDiscPersen);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(3, 506);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1353, 164);
            this.panel3.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Keterangan";
            // 
            // tbKetrangan
            // 
            this.tbKetrangan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbKetrangan.Location = new System.Drawing.Point(10, 33);
            this.tbKetrangan.Name = "tbKetrangan";
            this.tbKetrangan.Size = new System.Drawing.Size(477, 124);
            this.tbKetrangan.TabIndex = 13;
            this.tbKetrangan.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1098, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "%";
            // 
            // cbPPn
            // 
            this.cbPPn.AutoSize = true;
            this.cbPPn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPPn.Location = new System.Drawing.Point(1012, 103);
            this.cbPPn.Name = "cbPPn";
            this.cbPPn.Size = new System.Drawing.Size(59, 24);
            this.cbPPn.TabIndex = 11;
            this.cbPPn.Text = "PPN";
            this.cbPPn.UseVisualStyleBackColor = true;
            // 
            // tbGrandTotal
            // 
            this.tbGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGrandTotal.Location = new System.Drawing.Point(1126, 132);
            this.tbGrandTotal.Name = "tbGrandTotal";
            this.tbGrandTotal.Size = new System.Drawing.Size(199, 26);
            this.tbGrandTotal.TabIndex = 10;
            this.tbGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbPPN
            // 
            this.tbPPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPPN.Location = new System.Drawing.Point(1126, 101);
            this.tbPPN.Name = "tbPPN";
            this.tbPPN.Size = new System.Drawing.Size(199, 26);
            this.tbPPN.TabIndex = 9;
            this.tbPPN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbTotal
            // 
            this.tbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotal.Location = new System.Drawing.Point(1126, 69);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.Size = new System.Drawing.Size(199, 26);
            this.tbTotal.TabIndex = 8;
            this.tbTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbDiscNominal
            // 
            this.tbDiscNominal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDiscNominal.Location = new System.Drawing.Point(1126, 39);
            this.tbDiscNominal.Name = "tbDiscNominal";
            this.tbDiscNominal.Size = new System.Drawing.Size(199, 26);
            this.tbDiscNominal.TabIndex = 7;
            this.tbDiscNominal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbJumlah
            // 
            this.tbJumlah.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJumlah.Location = new System.Drawing.Point(1126, 9);
            this.tbJumlah.Name = "tbJumlah";
            this.tbJumlah.Size = new System.Drawing.Size(199, 26);
            this.tbJumlah.TabIndex = 6;
            this.tbJumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbDiscPersen
            // 
            this.tbDiscPersen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDiscPersen.Location = new System.Drawing.Point(1054, 38);
            this.tbDiscPersen.Name = "tbDiscPersen";
            this.tbDiscPersen.Size = new System.Drawing.Size(38, 26);
            this.tbDiscPersen.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1009, 135);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Grand Total";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1009, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Total";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1009, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Disc";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1008, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Jumlah";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnCetak);
            this.panel1.Controls.Add(this.btnSimpan);
            this.panel1.Location = new System.Drawing.Point(3, 675);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1353, 42);
            this.panel1.TabIndex = 7;
            // 
            // btnCetak
            // 
            this.btnCetak.Location = new System.Drawing.Point(1191, 9);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(75, 23);
            this.btnCetak.TabIndex = 1;
            this.btnCetak.Text = "Cetak";
            this.btnCetak.UseVisualStyleBackColor = true;
            this.btnCetak.Click += new System.EventHandler(this.btnCetak_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.Location = new System.Drawing.Point(1268, 9);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 23);
            this.btnSimpan.TabIndex = 0;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // statPO
            // 
            this.statPO.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStatuName,
            this.toolStatUsername});
            this.statPO.Location = new System.Drawing.Point(0, 719);
            this.statPO.Name = "statPO";
            this.statPO.Size = new System.Drawing.Size(1358, 22);
            this.statPO.TabIndex = 8;
            this.statPO.Text = "statusStrip1";
            // 
            // toolStatuName
            // 
            this.toolStatuName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.toolStatuName.Name = "toolStatuName";
            this.toolStatuName.Size = new System.Drawing.Size(39, 17);
            this.toolStatuName.Text = "User :";
            // 
            // toolStatUsername
            // 
            this.toolStatUsername.Name = "toolStatUsername";
            this.toolStatUsername.Size = new System.Drawing.Size(109, 17);
            this.toolStatUsername.Text = "toolStripStatusLabel1";
            // 
            // frmEditPO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1358, 741);
            this.Controls.Add(this.statPO);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEditPO";
            this.Text = "frmEditPO";
            this.Load += new System.EventHandler(this.frmEditPO_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDetil)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.statPO.ResumeLayout(false);
            this.statPO.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbNoPenawaran;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbNoPO;
        private System.Windows.Forms.Label lbNoPO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtTglPO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnRowAdd;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox tbKetrangan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbPPn;
        private System.Windows.Forms.TextBox tbGrandTotal;
        private System.Windows.Forms.TextBox tbPPN;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.TextBox tbDiscNominal;
        private System.Windows.Forms.TextBox tbJumlah;
        private System.Windows.Forms.TextBox tbDiscPersen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCetak;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.StatusStrip statPO;
        private System.Windows.Forms.ToolStripStatusLabel toolStatuName;
        private System.Windows.Forms.ToolStripStatusLabel toolStatUsername;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.DataGridView dgDetil;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Diminta;
        private System.Windows.Forms.DataGridViewTextBoxColumn UkuranDiminta;
        private System.Windows.Forms.DataGridViewButtonColumn Cari;
        private System.Windows.Forms.DataGridViewTextBoxColumn colsGoodType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodTypeName;
        private System.Windows.Forms.DataGridViewButtonColumn colIdDetil;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsIdUkuran;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn colgoodsHeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Faktor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Berat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.TextBox tbMarketing;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbReff;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbNamaCustomer;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.TextBox tbCustID;
    }
}