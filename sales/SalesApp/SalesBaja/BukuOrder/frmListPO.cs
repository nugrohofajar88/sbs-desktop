﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
//using PurchOrder;

namespace SalesBaja
{
    public partial class fr_ListPO : Form
    {
        private static fr_ListPO openForm = null;

        // No need for locking - you'll be doing all this on the UI thread...
        public static fr_ListPO GetInstance()
        {
            if (openForm == null)
            {
                openForm = new fr_ListPO();
                openForm.FormClosed += delegate { openForm = null; };
            }
            return openForm;
        }

        public MySqlConnection dtConn;

        private MySqlDataAdapter dtAdapter;
        private MySqlDataAdapter detilDtAdpater;
        private MySqlCommand sqlCommand;
        private DataTable tbl;
        private DataTable detilTbl;

        private string dtConnString;

        public fr_ListPO()
        {
            InitializeComponent();
            InitDataConnection();
            //FillMasterView();
        }

        private void InitDataConnection()
        {
            dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
                GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                GlobVar.hdbase;
            try
            {
                dtConn = new MySqlConnection();
                dtConn.ConnectionString = dtConnString;
                dtConn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void FillMasterView()
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT p.id_po, p.tanggal_po, p.No_Penawaran, p.id_userinput, ");
            qry.AppendLine("    p.id_marketing, m.nama_marketing, p.id_customer, c.nama_customer, ");
            qry.AppendLine("    p.status_po, p.no_po, u.username, p.po_customer, p.kredit");
            qry.AppendLine("FROM t_po p");
            qry.AppendLine("INNER JOIN m_user u ON p.id_userinput = u.Id_user");
            qry.AppendLine("INNER JOIN m_customer c ON p.Id_customer = c.id_customer");
            qry.AppendLine("INNER JOIN m_marketing m ON m.id_marketing = p.id_marketing");
            //MessageBox.Show(qry.ToString());
            try
            {
                dtAdapter = new MySqlDataAdapter(qry.ToString(), dtConn);
                tbl = new DataTable();
                dtAdapter.Fill(tbl);
                dgViewMastPO.DataSource = tbl;
                SetMasterGridColumn();

                //dtConn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetMasterGridColumn()
        {
            dgViewMastPO.Columns[0].Visible = false;
            dgViewMastPO.Columns[1].HeaderText = "Tanggal Order";
            dgViewMastPO.Columns[1].Width = 70;
            dgViewMastPO.Columns[2].HeaderText = "Nomor Penawaran";
            dgViewMastPO.Columns[2].Width = 105;
            dgViewMastPO.Columns[3].Visible = false;
            dgViewMastPO.Columns[4].Visible = false;
            dgViewMastPO.Columns[5].HeaderText = "Marketing";
            dgViewMastPO.Columns[5].Width = 85;
            dgViewMastPO.Columns[6].Visible = false;
            dgViewMastPO.Columns[7].HeaderText = "Customer";
            dgViewMastPO.Columns[7].Width = 150;
            dgViewMastPO.Columns[8].Visible = false;
            dgViewMastPO.Columns[9].HeaderText = "Nomor Order";
            dgViewMastPO.Columns[10].Visible = false;
            dgViewMastPO.Columns[11].HeaderText = "Nomor PO Customer";
            dgViewMastPO.Columns[12].HeaderText = "Kredit";
            dgViewMastPO.Columns[12].Width = 70;
        }

        private void FillDetilView(int MasterId)
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT d.id_po, d.id_dt_po, d.diminta, d.id_type, t.type, d.id_ukuran, ");
            qry.AppendLine("    u.lebar, u.panjang, u.tinggi, d.qty_po, d.harga_po");
            qry.AppendLine("FROM dt_po d");
            qry.AppendLine("INNER JOIN m_type t ON d.id_type = t.id_type");
            qry.AppendLine("LEFT JOIN m_ukuran u ON d.id_ukuran = u.id_ukuran");
            qry.AppendLine("WHERE id_po = @IdMaster");

            MySqlCommand ViewCommand = new MySqlCommand(qry.ToString(), dtConn);
            MySqlParameter paramId = new MySqlParameter("IdMaster", MasterId);
            ViewCommand.Parameters.Add(paramId);

            detilDtAdpater = new MySqlDataAdapter();
            detilDtAdpater.SelectCommand = ViewCommand;
            detilTbl = new DataTable();
            detilDtAdpater.Fill(detilTbl);
            dgViewDtlPO.DataSource = detilTbl;
            SetDetilGridColumns();
        }

        private void SetDetilGridColumns()
        {
            dgViewDtlPO.Columns[0].Visible = false;
            dgViewDtlPO.Columns[1].Visible = false;
            dgViewDtlPO.Columns[2].Visible = false;
            dgViewDtlPO.Columns[2].HeaderText = "Diminta";
            dgViewDtlPO.Columns[3].Visible = false;
            dgViewDtlPO.Columns[4].HeaderText = "Type";
            dgViewDtlPO.Columns[5].Visible = false;
            dgViewDtlPO.Columns[6].HeaderText = "Panjang";
            dgViewDtlPO.Columns[6].Width = 55;
            dgViewDtlPO.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgViewDtlPO.Columns[7].HeaderText = "Lebar";
            dgViewDtlPO.Columns[7].Width = 55;
            dgViewDtlPO.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgViewDtlPO.Columns[8].HeaderText = "Tinggi";
            dgViewDtlPO.Columns[8].Width = 55;
            dgViewDtlPO.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgViewDtlPO.Columns[9].HeaderText = "Qty";
            dgViewDtlPO.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgViewDtlPO.Columns[9].Width = 50;
            dgViewDtlPO.Columns[10].HeaderText = "Harga";
            dgViewDtlPO.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            // Panggil form entry PO
            fr_t_PO Tambah_PO = new fr_t_PO();
            //Tambah_PO.Userconnected = _userconnected;
            //Tambah_PO.Userid = _userid;
            Tambah_PO.FormMode = 0;
            Tambah_PO.ShowDialog();
            FillMasterView();
            dgViewMastPO.Refresh();
        }

        private void fr_ListPO_Load(object sender, EventArgs e)
        {
            FillMasterView();
        }

        private void dgViewMastPO_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            int IdTrx = Convert.ToInt32(dgViewMastPO.Rows[e.RowIndex].Cells[0].Value);
            FillDetilView(IdTrx);
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            //Menghapus data yang terpilih
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("DELETE FROM t_po");
            qry.AppendLine("WHERE t_po.id_po = @Id");

            int PoID = Convert.ToInt32(dgViewMastPO.Rows[dgViewMastPO.CurrentRow.Index].Cells[0].Value);
            //MessageBox.Show(PoID.ToString());
            MySqlCommand delCmd = new MySqlCommand(qry.ToString(), dtConn);
            MySqlParameter Idprm = new MySqlParameter("Id", PoID);
            delCmd.Parameters.Add(Idprm);
            delCmd.ExecuteNonQuery();

            FillMasterView();
            int IdTrx = 0;
            if (dgViewMastPO.RowCount != 0)
            {
                IdTrx = Convert.ToInt32(dgViewMastPO.CurrentRow.Cells[0].Value);
            }
            FillDetilView(IdTrx);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            // Menampilkan data 
            frmEditPO ViewPO = new frmEditPO();
            //ViewPO.Userconnected = _userconnected;
            //ViewPO.Userid = _userid;
            ViewPO.FormMode = 2;
            ViewPO.IDTransaksi = Convert.ToInt32(dgViewMastPO.Rows[dgViewMastPO.CurrentRow.Index].Cells[0].Value);
            ViewPO.LoadMasterData();
            ViewPO.ShowDialog();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Open frmPO for adding
            fr_t_PO AddPO = fr_t_PO.GetInstance();
            AddPO.frmMode = FormOpenMode.fomNew;
            AddPO.MdiParent = this.MdiParent;
            AddPO.WindowState = FormWindowState.Maximized;
            AddPO.Userconnected = GlobVar.UserActive;
            AddPO.Userid = GlobVar.IdUserActive;
            AddPO.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            // Open frmPO for editing the specified data
            fr_t_PO EditPO = fr_t_PO.GetInstance();
            EditPO.MdiParent = this.MdiParent;
            EditPO.LoadMasterData(Convert.ToInt32(dgViewMastPO.Rows[dgViewMastPO.CurrentRow.Index].Cells[0].Value));
            EditPO.IDTransaksi = Convert.ToInt32(dgViewMastPO.Rows[dgViewMastPO.CurrentRow.Index].Cells[0].Value);
            EditPO.frmMode = FormOpenMode.fomEdit;
            EditPO.Show();
        }
    }
}
