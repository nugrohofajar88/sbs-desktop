﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SalesBaja
{
    public partial class fr_CariPenawaran : Form
    {
        private MySqlConnection conn;
        private string connString;
        private MySqlDataAdapter dtAdapter;
        private MySqlDataAdapter detilDtAdpater;
        private MySqlCommand sqlCommand;
        private DataTable tbl;
        public DataTable detilTbl;

        private String selNoPenawran;
        public string NmCustom;
        public string IdCustom;
        public string NmMarketing;
        public double JmlKotor;
        public int Diskon;
        public double Pajak;

        public fr_CariPenawaran()
        {
            InitializeComponent();
        }

        private static string[] param = new string[]
        {
            "nama_customer",
            "tanggal",
            "marketing"
        };

        public string SelNoPenawaran
        {
            get
            {
                return selNoPenawran;
            }

            set
            {
                selNoPenawran = value;
            }
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            string sqlcmd = "SELECT p.id_penawaran, p.no_penawaran, p.id_customer, " + 
                "c.nama_customer, p.tanggal, p.id_marketing, m.nama_marketing, p.jumlah, " + 
                "p.disc, p.ppn FROM t_penawaran p " + 
                "INNER JOIN m_customer c ON c.ID_CUSTOMER = p.id_customer " + 
                "INNER JOIN m_marketing m ON m.id_marketing = p.id_marketing";
            int seltd = 0;
            StringBuilder prmstrbld = new StringBuilder();

            #region Combobox checking 

            /// <summary>
            /// Bagian ini untuk mengecek apakah control combo box 
            /// dicentang atau tidak? Jika dicentang tambahkan parameter
            /// ke string SQL. 
            /// </summary>

            if (cbNama.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[0] + " LIKE '%" + tbNama.Text + "'");
            }

            if (cbAlamat.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[1] + " LIKE '%" + tbAlamat.Text + "'");
            }

            if (cbTelp.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[2] + " LIKE '%" + tbTelp.Text + "'");
            }

            #endregion

            string prmstr = prmstrbld.ToString();
            string[] prmstrarr = prmstr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (seltd >= 1)
            {
                sqlcmd = sqlcmd + "WHERE ";
                int i = 0;
                do
                {
                    sqlcmd = sqlcmd + prmstrarr[i];
                    if (i < seltd - 1)
                    {
                        sqlcmd = sqlcmd + " AND ";
                    };
                    i++;
                } while (i < seltd);
            }

            SQLGetData(sqlcmd);

            //MessageBox.Show(sqlcmd);
        }

        private void SQLGetData(string sql)
        {
            try
            {
                connString = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase;
                conn = new MySqlConnection(connString);
                dtAdapter = new MySqlDataAdapter(sql, conn);
                tbl = new DataTable();
                dtAdapter.Fill(tbl);
                dgvMaster.DataSource = tbl;
                conn.Open();
                SetMasterGridColumns();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetMasterGridColumns()
        {
            dgvMaster.Columns[0].Visible = false;
            dgvMaster.Columns[1].HeaderText = "No. Penawaran";
            dgvMaster.Columns[2].Visible = false;
            dgvMaster.Columns[3].HeaderText = "Nama Customer";
            dgvMaster.Columns[4].HeaderText = "Tanggal";
            dgvMaster.Columns[5].Visible = false;           //id_marketing
            dgvMaster.Columns[6].HeaderText = "Marketing";
            dgvMaster.Columns[7].HeaderText = "Jumlah";
            dgvMaster.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvMaster.Columns[8].HeaderText = "Disc";
            dgvMaster.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvMaster.Columns[9].HeaderText = "PPN";
            dgvMaster.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void dgvMaster_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            int IdTrx = Convert.ToInt32(dgvMaster.Rows[e.RowIndex].Cells[0].Value);
            FillDetilView(IdTrx);
        }

        private void FillDetilView(int MasterId)
        {
            string sqlcmd = "SELECT d.id_penawaran, d.diminta, d.id_ukdiminta, "+
                "u1.nama_ukuran, u1.faktor_hitung, d.id_type, t.type, d.id_ukuran, " +
                "u2.nama_ukuran, u2.lebar, u2.panjang, u2.tinggi, u2.faktor_hitung, " + 
                "d.qty, d.berat, d.harga FROM dt_penawaran d " +
                "INNER JOIN m_ukuran u1 ON u1.ID_UKURAN = d.id_ukdiminta " +
                "INNER JOIN m_type t ON t.ID_TYPE = d.id_type " +
                "INNER JOIN m_ukuran u2 ON u2.ID_UKURAN = d.id_ukuran " +
                "WHERE id_penawaran = @IdMaster";

            MySqlCommand ViewCommand = new MySqlCommand(sqlcmd, conn);
            MySqlParameter paramId = new MySqlParameter("IdMaster", MasterId);
            ViewCommand.Parameters.Add(paramId);

            detilDtAdpater = new MySqlDataAdapter();
            detilDtAdpater.SelectCommand = ViewCommand;
            detilTbl = new DataTable();
            detilDtAdpater.Fill(detilTbl);
            dgvDetil.DataSource = detilTbl;
            SetDetilGridColumns();
        }

        private void SetDetilGridColumns()
        {
            dgvDetil.Columns[0].Visible = false;
            dgvDetil.Columns[1].HeaderText = "Diminta";
            dgvDetil.Columns[2].Visible = false;
            //dgvDetil.Columns[3].Visible = false;
            dgvDetil.Columns[3].HeaderText = "Ukuran Minta";
            dgvDetil.Columns[4].Visible = false;  //faktor hitung
            dgvDetil.Columns[5].Visible = false;  //id_type
            dgvDetil.Columns[6].HeaderText = "Ditawarkan";
            dgvDetil.Columns[7].Visible = false;  //id_ukuran
            dgvDetil.Columns[8].HeaderText = "Ukuran Ditawarkan";
            dgvDetil.Columns[9].Visible = false;    //lebar
            dgvDetil.Columns[10].Visible = false;   //panjang
            dgvDetil.Columns[11].Visible = false;   //tinggi
            dgvDetil.Columns[12].Visible = false;   //faktor hitung
            dgvDetil.Columns[13].HeaderText = "Qty";
            dgvDetil.Columns[14].HeaderText = "Berat";
            dgvDetil.Columns[15].HeaderText = "Harga";
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            // Isikan data dari form ini ke form pemanggil
            selNoPenawran = dgvMaster.Rows[dgvMaster.CurrentCell.RowIndex].Cells[1].Value.ToString();
            IdCustom = dgvMaster.Rows[dgvMaster.CurrentCell.RowIndex].Cells[2].Value.ToString();
            NmCustom = dgvMaster.Rows[dgvMaster.CurrentCell.RowIndex].Cells[3].Value.ToString();
            NmMarketing = dgvMaster.Rows[dgvMaster.CurrentCell.RowIndex].Cells[6].Value.ToString();
            JmlKotor = double.Parse(dgvMaster.Rows[dgvMaster.CurrentCell.RowIndex].Cells[7].Value.ToString());
            Diskon = int.Parse(dgvMaster.Rows[dgvMaster.CurrentCell.RowIndex].Cells[8].Value.ToString());
            Pajak = double.Parse(dgvMaster.Rows[dgvMaster.CurrentCell.RowIndex].Cells[9].Value.ToString());
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
