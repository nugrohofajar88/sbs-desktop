﻿namespace SalesBaja
{
    partial class fr_t_PO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fr_t_PO));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.seKredit = new DevExpress.XtraEditors.SpinEdit();
            this.tbMarketing = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_PoCustomer = new System.Windows.Forms.TextBox();
            this.btnCariPenawaran = new System.Windows.Forms.Button();
            this.tbReff = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnGeneratNo = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbNoPenawaran = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbNoPO = new System.Windows.Forms.TextBox();
            this.lbNoPO = new System.Windows.Forms.Label();
            this.lbNamaCustomer = new System.Windows.Forms.Label();
            this.btnCari = new System.Windows.Forms.Button();
            this.tbCustID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtTglPO = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDel = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnRowAdd = new System.Windows.Forms.Button();
            this.dgDetil = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCetak = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbKetrangan = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbPPn = new System.Windows.Forms.CheckBox();
            this.tbGrandTotal = new System.Windows.Forms.TextBox();
            this.tbPPN = new System.Windows.Forms.TextBox();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.tbDiscNominal = new System.Windows.Forms.TextBox();
            this.tbJumlah = new System.Windows.Forms.TextBox();
            this.tbDiscPersen = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.selColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Diminta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UkuranDiminta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cari = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colsGoodType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdDetil = new System.Windows.Forms.DataGridViewButtonColumn();
            this.NamaUkuran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodsIdUkuran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodsWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodsLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colgoodsHeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Faktor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodsQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Berat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGoodPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdNewRow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BnsCustom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Persen = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seKredit.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetil)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.seKredit);
            this.groupBox1.Controls.Add(this.tbMarketing);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tb_PoCustomer);
            this.groupBox1.Controls.Add(this.btnCariPenawaran);
            this.groupBox1.Controls.Add(this.tbReff);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.btnGeneratNo);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbNoPenawaran);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbNoPO);
            this.groupBox1.Controls.Add(this.lbNoPO);
            this.groupBox1.Controls.Add(this.lbNamaCustomer);
            this.groupBox1.Controls.Add(this.btnCari);
            this.groupBox1.Controls.Add(this.tbCustID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtTglPO);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1353, 122);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Master Transaksi";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(674, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 20);
            this.label13.TabIndex = 20;
            this.label13.Text = "hari";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // seKredit
            // 
            this.seKredit.EditValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.seKredit.Location = new System.Drawing.Point(609, 58);
            this.seKredit.Name = "seKredit";
            this.seKredit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.seKredit.Properties.Appearance.Options.UseFont = true;
            this.seKredit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.seKredit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seKredit.Size = new System.Drawing.Size(59, 28);
            this.seKredit.TabIndex = 9;
            // 
            // tbMarketing
            // 
            this.tbMarketing.FormattingEnabled = true;
            this.tbMarketing.Location = new System.Drawing.Point(1079, 56);
            this.tbMarketing.Name = "tbMarketing";
            this.tbMarketing.Size = new System.Drawing.Size(211, 28);
            this.tbMarketing.TabIndex = 12;
            this.tbMarketing.SelectedIndexChanged += new System.EventHandler(this.tbMarketing_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(475, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 20);
            this.label12.TabIndex = 17;
            this.label12.Text = "No PO Customer";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tb_PoCustomer
            // 
            this.tb_PoCustomer.Location = new System.Drawing.Point(609, 30);
            this.tb_PoCustomer.Name = "tb_PoCustomer";
            this.tb_PoCustomer.Size = new System.Drawing.Size(254, 26);
            this.tb_PoCustomer.TabIndex = 8;
            // 
            // btnCariPenawaran
            // 
            this.btnCariPenawaran.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCariPenawaran.Location = new System.Drawing.Point(1302, 30);
            this.btnCariPenawaran.Name = "btnCariPenawaran";
            this.btnCariPenawaran.Size = new System.Drawing.Size(41, 23);
            this.btnCariPenawaran.TabIndex = 10;
            this.btnCariPenawaran.Text = "Cari";
            this.btnCariPenawaran.UseVisualStyleBackColor = true;
            this.btnCariPenawaran.Click += new System.EventHandler(this.btnCariPenawaran_Click);
            // 
            // tbReff
            // 
            this.tbReff.Location = new System.Drawing.Point(1079, 86);
            this.tbReff.Name = "tbReff";
            this.tbReff.Size = new System.Drawing.Size(211, 26);
            this.tbReff.TabIndex = 12;
            this.tbReff.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(553, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 20);
            this.label11.TabIndex = 13;
            this.label11.Text = "Kredit";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnGeneratNo
            // 
            this.btnGeneratNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGeneratNo.Location = new System.Drawing.Point(296, 88);
            this.btnGeneratNo.Name = "btnGeneratNo";
            this.btnGeneratNo.Size = new System.Drawing.Size(45, 23);
            this.btnGeneratNo.TabIndex = 7;
            this.btnGeneratNo.Text = "Buat";
            this.btnGeneratNo.UseVisualStyleBackColor = true;
            this.btnGeneratNo.Click += new System.EventHandler(this.btnGeneratNo_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(956, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 20);
            this.label10.TabIndex = 11;
            this.label10.Text = "Marketing";
            // 
            // tbNoPenawaran
            // 
            this.tbNoPenawaran.Location = new System.Drawing.Point(1079, 28);
            this.tbNoPenawaran.Name = "tbNoPenawaran";
            this.tbNoPenawaran.ReadOnly = true;
            this.tbNoPenawaran.Size = new System.Drawing.Size(211, 26);
            this.tbNoPenawaran.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(956, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "No. Penawaran";
            // 
            // tbNoPO
            // 
            this.tbNoPO.Location = new System.Drawing.Point(108, 86);
            this.tbNoPO.Name = "tbNoPO";
            this.tbNoPO.Size = new System.Drawing.Size(182, 26);
            this.tbNoPO.TabIndex = 6;
            // 
            // lbNoPO
            // 
            this.lbNoPO.AutoSize = true;
            this.lbNoPO.Location = new System.Drawing.Point(20, 89);
            this.lbNoPO.Name = "lbNoPO";
            this.lbNoPO.Size = new System.Drawing.Size(77, 20);
            this.lbNoPO.TabIndex = 6;
            this.lbNoPO.Text = "No. Order";
            // 
            // lbNamaCustomer
            // 
            this.lbNamaCustomer.AutoSize = true;
            this.lbNamaCustomer.Location = new System.Drawing.Point(218, 61);
            this.lbNamaCustomer.Name = "lbNamaCustomer";
            this.lbNamaCustomer.Size = new System.Drawing.Size(124, 20);
            this.lbNamaCustomer.TabIndex = 5;
            this.lbNamaCustomer.Text = "Nama Customer";
            // 
            // btnCari
            // 
            this.btnCari.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCari.Location = new System.Drawing.Point(170, 57);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(38, 25);
            this.btnCari.TabIndex = 4;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = true;
            this.btnCari.Click += new System.EventHandler(this.btnCari_Click);
            // 
            // tbCustID
            // 
            this.tbCustID.Location = new System.Drawing.Point(108, 57);
            this.tbCustID.Name = "tbCustID";
            this.tbCustID.Size = new System.Drawing.Size(55, 26);
            this.tbCustID.TabIndex = 3;
            this.tbCustID.Leave += new System.EventHandler(this.tbCustID_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer";
            // 
            // dtTglPO
            // 
            this.dtTglPO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTglPO.Location = new System.Drawing.Point(108, 28);
            this.dtTglPO.Name = "dtTglPO";
            this.dtTglPO.Size = new System.Drawing.Size(182, 26);
            this.dtTglPO.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tanggal";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Controls.Add(this.dgDetil);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 128);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1353, 372);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detil Transaksi";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.btnDel);
            this.panel2.Controls.Add(this.btnRowAdd);
            this.panel2.Location = new System.Drawing.Point(6, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1341, 31);
            this.panel2.TabIndex = 3;
            // 
            // btnDel
            // 
            this.btnDel.FlatAppearance.BorderSize = 0;
            this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.ImageIndex = 0;
            this.btnDel.ImageList = this.imageList1;
            this.btnDel.Location = new System.Drawing.Point(77, 4);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(70, 23);
            this.btnDel.TabIndex = 5;
            this.btnDel.Text = "Delete";
            this.btnDel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "delete.ico");
            this.imageList1.Images.SetKeyName(1, "1473946589_add.ico");
            // 
            // btnRowAdd
            // 
            this.btnRowAdd.FlatAppearance.BorderSize = 0;
            this.btnRowAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRowAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRowAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRowAdd.ImageIndex = 1;
            this.btnRowAdd.ImageList = this.imageList1;
            this.btnRowAdd.Location = new System.Drawing.Point(5, 4);
            this.btnRowAdd.Name = "btnRowAdd";
            this.btnRowAdd.Size = new System.Drawing.Size(70, 23);
            this.btnRowAdd.TabIndex = 4;
            this.btnRowAdd.Text = "Tambah";
            this.btnRowAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRowAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRowAdd.UseVisualStyleBackColor = true;
            this.btnRowAdd.Click += new System.EventHandler(this.btnRowAdd_Click);
            // 
            // dgDetil
            // 
            this.dgDetil.AllowUserToAddRows = false;
            this.dgDetil.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgDetil.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDetil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetil.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selColumn,
            this.Diminta,
            this.UkuranDiminta,
            this.Cari,
            this.colsGoodType,
            this.colGoodTypeName,
            this.colIdDetil,
            this.NamaUkuran,
            this.colGoodsIdUkuran,
            this.colGoodsWidth,
            this.colGoodsLength,
            this.colgoodsHeight,
            this.Faktor,
            this.colGoodsQty,
            this.Berat,
            this.colGoodPrice,
            this.DiscItem,
            this.Total,
            this.IdCol,
            this.IdNewRow,
            this.BnsCustom,
            this.Persen});
            this.dgDetil.Location = new System.Drawing.Point(6, 59);
            this.dgDetil.Name = "dgDetil";
            this.dgDetil.RowHeadersVisible = false;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgDetil.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.dgDetil.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDetil.Size = new System.Drawing.Size(1341, 307);
            this.dgDetil.TabIndex = 2;
            this.dgDetil.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgDetil_CellBeginEdit);
            this.dgDetil.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dgDetil.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDetil_CellEndEdit);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.tbKetrangan);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.cbPPn);
            this.panel3.Controls.Add(this.tbGrandTotal);
            this.panel3.Controls.Add(this.tbPPN);
            this.panel3.Controls.Add(this.tbTotal);
            this.panel3.Controls.Add(this.tbDiscNominal);
            this.panel3.Controls.Add(this.tbJumlah);
            this.panel3.Controls.Add(this.tbDiscPersen);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(3, 507);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1353, 178);
            this.panel3.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCetak);
            this.panel1.Controls.Add(this.btnSimpan);
            this.panel1.Location = new System.Drawing.Point(638, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 45);
            this.panel1.TabIndex = 15;
            // 
            // btnCetak
            // 
            this.btnCetak.Location = new System.Drawing.Point(58, 7);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(75, 30);
            this.btnCetak.TabIndex = 1;
            this.btnCetak.Text = "Cetak";
            this.btnCetak.UseVisualStyleBackColor = true;
            this.btnCetak.Visible = false;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(136, 7);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 30);
            this.btnSimpan.TabIndex = 0;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Keterangan";
            // 
            // tbKetrangan
            // 
            this.tbKetrangan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbKetrangan.Location = new System.Drawing.Point(17, 42);
            this.tbKetrangan.Name = "tbKetrangan";
            this.tbKetrangan.Size = new System.Drawing.Size(500, 110);
            this.tbKetrangan.TabIndex = 13;
            this.tbKetrangan.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1098, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "%";
            // 
            // cbPPn
            // 
            this.cbPPn.AutoSize = true;
            this.cbPPn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPPn.Location = new System.Drawing.Point(1012, 101);
            this.cbPPn.Name = "cbPPn";
            this.cbPPn.Size = new System.Drawing.Size(59, 24);
            this.cbPPn.TabIndex = 10;
            this.cbPPn.Text = "PPN";
            this.cbPPn.UseVisualStyleBackColor = true;
            this.cbPPn.CheckedChanged += new System.EventHandler(this.cbPPn_CheckedChanged);
            // 
            // tbGrandTotal
            // 
            this.tbGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGrandTotal.Location = new System.Drawing.Point(1126, 128);
            this.tbGrandTotal.Name = "tbGrandTotal";
            this.tbGrandTotal.Size = new System.Drawing.Size(199, 26);
            this.tbGrandTotal.TabIndex = 12;
            this.tbGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbPPN
            // 
            this.tbPPN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPPN.Location = new System.Drawing.Point(1126, 99);
            this.tbPPN.Name = "tbPPN";
            this.tbPPN.Size = new System.Drawing.Size(199, 26);
            this.tbPPN.TabIndex = 11;
            this.tbPPN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbTotal
            // 
            this.tbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotal.Location = new System.Drawing.Point(1126, 70);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.Size = new System.Drawing.Size(199, 26);
            this.tbTotal.TabIndex = 9;
            this.tbTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbDiscNominal
            // 
            this.tbDiscNominal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDiscNominal.Location = new System.Drawing.Point(1126, 41);
            this.tbDiscNominal.Name = "tbDiscNominal";
            this.tbDiscNominal.Size = new System.Drawing.Size(199, 26);
            this.tbDiscNominal.TabIndex = 8;
            this.tbDiscNominal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbJumlah
            // 
            this.tbJumlah.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJumlah.Location = new System.Drawing.Point(1126, 12);
            this.tbJumlah.Name = "tbJumlah";
            this.tbJumlah.Size = new System.Drawing.Size(199, 26);
            this.tbJumlah.TabIndex = 6;
            this.tbJumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbDiscPersen
            // 
            this.tbDiscPersen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDiscPersen.Location = new System.Drawing.Point(1054, 40);
            this.tbDiscPersen.Name = "tbDiscPersen";
            this.tbDiscPersen.Size = new System.Drawing.Size(38, 26);
            this.tbDiscPersen.TabIndex = 7;
            this.tbDiscPersen.Text = "0";
            this.tbDiscPersen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbDiscPersen.Leave += new System.EventHandler(this.tbDiscPersen_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1009, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Grand Total";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1009, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Total";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1009, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Disc";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1008, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Jumlah";
            // 
            // selColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.NullValue = false;
            this.selColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.selColumn.HeaderText = "";
            this.selColumn.Name = "selColumn";
            this.selColumn.Width = 35;
            // 
            // Diminta
            // 
            this.Diminta.HeaderText = "Diminta";
            this.Diminta.Name = "Diminta";
            this.Diminta.Visible = false;
            this.Diminta.Width = 200;
            // 
            // UkuranDiminta
            // 
            this.UkuranDiminta.HeaderText = "Ukuran Diminta";
            this.UkuranDiminta.Name = "UkuranDiminta";
            this.UkuranDiminta.Visible = false;
            // 
            // Cari
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Cari.DefaultCellStyle = dataGridViewCellStyle3;
            this.Cari.DividerWidth = 1;
            this.Cari.HeaderText = "Type";
            this.Cari.Name = "Cari";
            this.Cari.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Cari.Text = "Cari";
            this.Cari.UseColumnTextForButtonValue = true;
            this.Cari.Width = 70;
            // 
            // colsGoodType
            // 
            this.colsGoodType.HeaderText = "IDType";
            this.colsGoodType.Name = "colsGoodType";
            this.colsGoodType.ReadOnly = true;
            this.colsGoodType.Visible = false;
            // 
            // colGoodTypeName
            // 
            this.colGoodTypeName.HeaderText = "Type";
            this.colGoodTypeName.Name = "colGoodTypeName";
            this.colGoodTypeName.ReadOnly = true;
            this.colGoodTypeName.Width = 135;
            // 
            // colIdDetil
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.colIdDetil.DefaultCellStyle = dataGridViewCellStyle4;
            this.colIdDetil.HeaderText = "Ukuran";
            this.colIdDetil.Name = "colIdDetil";
            this.colIdDetil.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colIdDetil.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colIdDetil.Text = "Cari";
            this.colIdDetil.UseColumnTextForButtonValue = true;
            this.colIdDetil.Width = 70;
            // 
            // NamaUkuran
            // 
            this.NamaUkuran.HeaderText = "Nama Ukuran";
            this.NamaUkuran.Name = "NamaUkuran";
            this.NamaUkuran.ReadOnly = true;
            this.NamaUkuran.Width = 125;
            // 
            // colGoodsIdUkuran
            // 
            this.colGoodsIdUkuran.HeaderText = "IdUkuran";
            this.colGoodsIdUkuran.Name = "colGoodsIdUkuran";
            this.colGoodsIdUkuran.ReadOnly = true;
            this.colGoodsIdUkuran.Visible = false;
            // 
            // colGoodsWidth
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = "0";
            this.colGoodsWidth.DefaultCellStyle = dataGridViewCellStyle5;
            this.colGoodsWidth.HeaderText = "Lebar";
            this.colGoodsWidth.Name = "colGoodsWidth";
            this.colGoodsWidth.ReadOnly = true;
            this.colGoodsWidth.Visible = false;
            this.colGoodsWidth.Width = 115;
            // 
            // colGoodsLength
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "0";
            this.colGoodsLength.DefaultCellStyle = dataGridViewCellStyle6;
            this.colGoodsLength.HeaderText = "Panjang";
            this.colGoodsLength.Name = "colGoodsLength";
            this.colGoodsLength.ReadOnly = true;
            this.colGoodsLength.Visible = false;
            this.colGoodsLength.Width = 115;
            // 
            // colgoodsHeight
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = "0";
            this.colgoodsHeight.DefaultCellStyle = dataGridViewCellStyle7;
            this.colgoodsHeight.HeaderText = "Tinggi";
            this.colgoodsHeight.Name = "colgoodsHeight";
            this.colgoodsHeight.ReadOnly = true;
            this.colgoodsHeight.Visible = false;
            this.colgoodsHeight.Width = 115;
            // 
            // Faktor
            // 
            this.Faktor.HeaderText = "Faktor";
            this.Faktor.Name = "Faktor";
            this.Faktor.ReadOnly = true;
            this.Faktor.Visible = false;
            // 
            // colGoodsQty
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = "0";
            this.colGoodsQty.DefaultCellStyle = dataGridViewCellStyle8;
            this.colGoodsQty.HeaderText = "Qty";
            this.colGoodsQty.Name = "colGoodsQty";
            this.colGoodsQty.Width = 115;
            // 
            // Berat
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Berat.DefaultCellStyle = dataGridViewCellStyle9;
            this.Berat.HeaderText = "Berat";
            this.Berat.Name = "Berat";
            this.Berat.Width = 115;
            // 
            // colGoodPrice
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = "0";
            this.colGoodPrice.DefaultCellStyle = dataGridViewCellStyle10;
            this.colGoodPrice.HeaderText = "Harga/Kg";
            this.colGoodPrice.Name = "colGoodPrice";
            this.colGoodPrice.Width = 165;
            // 
            // DiscItem
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscItem.DefaultCellStyle = dataGridViewCellStyle11;
            this.DiscItem.HeaderText = "Disc";
            this.DiscItem.Name = "DiscItem";
            // 
            // Total
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = "0";
            this.Total.DefaultCellStyle = dataGridViewCellStyle12;
            this.Total.HeaderText = "Jumlah";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 175;
            // 
            // IdCol
            // 
            this.IdCol.HeaderText = "IdCol";
            this.IdCol.Name = "IdCol";
            this.IdCol.Visible = false;
            // 
            // IdNewRow
            // 
            this.IdNewRow.HeaderText = "IdNewRow";
            this.IdNewRow.Name = "IdNewRow";
            this.IdNewRow.Visible = false;
            // 
            // BnsCustom
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            dataGridViewCellStyle13.NullValue = null;
            this.BnsCustom.DefaultCellStyle = dataGridViewCellStyle13;
            this.BnsCustom.HeaderText = "Bonus Customer";
            this.BnsCustom.Name = "BnsCustom";
            this.BnsCustom.Width = 145;
            // 
            // Persen
            // 
            this.Persen.HeaderText = "Persen";
            this.Persen.Name = "Persen";
            this.Persen.Width = 75;
            // 
            // fr_t_PO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1358, 700);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "fr_t_PO";
            this.Text = "Order Penjualan";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seKredit.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDetil)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.TextBox tbCustID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtTglPO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgDetil;
        private System.Windows.Forms.Label lbNamaCustomer;
        private System.Windows.Forms.TextBox tbNoPO;
        private System.Windows.Forms.Label lbNoPO;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnRowAdd;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbGrandTotal;
        private System.Windows.Forms.TextBox tbPPN;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.TextBox tbDiscNominal;
        private System.Windows.Forms.TextBox tbJumlah;
        private System.Windows.Forms.TextBox tbDiscPersen;
        private System.Windows.Forms.CheckBox cbPPn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox tbKetrangan;
        private System.Windows.Forms.TextBox tbNoPenawaran;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnGeneratNo;
        private System.Windows.Forms.TextBox tbReff;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCetak;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnCariPenawaran;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_PoCustomer;
        private System.Windows.Forms.ComboBox tbMarketing;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.SpinEdit seKredit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Diminta;
        private System.Windows.Forms.DataGridViewTextBoxColumn UkuranDiminta;
        private System.Windows.Forms.DataGridViewButtonColumn Cari;
        private System.Windows.Forms.DataGridViewTextBoxColumn colsGoodType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodTypeName;
        private System.Windows.Forms.DataGridViewButtonColumn colIdDetil;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamaUkuran;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsIdUkuran;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn colgoodsHeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Faktor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodsQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Berat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGoodPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdNewRow;
        private System.Windows.Forms.DataGridViewTextBoxColumn BnsCustom;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Persen;
    }
}

