﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SalesBaja
{
    public partial class fr_listBarang : Form
    {
        private MySqlConnection conn;
        private string connString;
        private MySqlDataAdapter dtAdapter;
        private MySqlCommand sqlCommand;
        private DataTable tbl;

        private string IdSelectedCust;
        public string SelectedID
        {
            get { return IdSelectedCust; }
            set { }
        }

        private string NamaSelectedCust;
        public string SelectedNama
        {
            get { return NamaSelectedCust; }
            set { }
        }

        private string IdType;
        public string SelectedTypeID
        {
            get { return IdType; }
            set { }
        }

        private string TypeNama;
        public string SelectedTypeNama
        {
            get { return TypeNama; }
            set { }
        }

//        private string panjang;
//        private string lebar;
//        private string tinggi;
//        private string diameter;
//        private string idUkuran;

//        public string SelectedPanjang
//        {
//            get { return panjang; }
//            set { }
//        }

//        public string SelectedLebar
//        {
//            get { return lebar; }
//            set { }
//        }

//        public string SelectedTinggi
//        {
//            get { return tinggi; }
//            set { }
//        }

//        public string SelectedDiameter
//        {
//            get { return diameter; }
//            set { }
//        }

//        public string SelectedUkuranId
//        {
//            get { return idUkuran; }
//            set { }
//        }

        private static string[] param = new string[]
        {
            "type"
        };

        public fr_listBarang()
        {
            InitializeComponent();
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            //string sqlcmd = "SELECT s.id_stock as id, b.NAMA_BARANG AS nama, t.ID_TYPE AS typeID, " + 
            //        "t.`TYPE` as type, u.NAMA_UKURAN AS ukuran, u.PANJANG AS length, u.LEBAR as width, u.TINGGI as height, " +
            //        "u.DIAMETER AS dia, u.id_ukuran FROM m_stock s " +
            //        "INNER JOIN m_type t ON s.ID_TYPE = t.ID_TYPE " +
            //        "INNER JOIN m_barang b ON t.ID_BARANG = b.ID_BARANG " +
            //        "LEFT JOIN m_ukuran u on u.ID_UKURAN = s.ID_UKURAN";

            string sqlcmd = "SELECT t.ID_TYPE AS typeID,  t.`TYPE` as type " +
                     "FROM m_type t ";

            int seltd = 0;
            StringBuilder prmstrbld = new StringBuilder();

            #region Combobox checking 

            /// <summary>
            /// Bagian ini untuk mengecek apakah control combo box 
            /// dicentang atau tidak? Jika dicentang tambahkan parameter
            /// ke string SQL. 
            /// </summary>

            if (cbNama.Checked)
            {
                seltd = seltd + 1;
                prmstrbld.AppendLine(param[0] + " LIKE '%" + tbNama.Text + "'");
            }

            //if (cbLength.Checked)
            //{
            //    seltd = seltd + 1;
            //    prmstrbld.AppendLine(param[1] + " LIKE '%" + tbLength.Text + "'");
            //}

            //if (cbWidth.Checked)
            //{
            //    seltd = seltd + 1;
            //    prmstrbld.AppendLine(param[2] + " LIKE '%" + tbWidth.Text + "'");
            //}


            #endregion

            string prmstr = prmstrbld.ToString();
            string[] prmstrarr = prmstr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (seltd >= 1)
            {
                sqlcmd = sqlcmd + "WHERE ";
                int i = 0;
                do
                {
                    sqlcmd = sqlcmd + prmstrarr[i];
                    if (i < seltd - 1)
                    {
                        sqlcmd = sqlcmd + " AND ";
                    };
                    i++;
                } while (i < seltd);
            }

            SQLGetData(sqlcmd);

            //MessageBox.Show(sqlcmd);
        }

        private void SQLGetData(string sql)
        {
            try
            {
                connString = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase;
                conn = new MySqlConnection(connString);
                dtAdapter = new MySqlDataAdapter(sql, conn);
                tbl = new DataTable();
                dtAdapter.Fill(tbl);
                dgViewLstBrg.DataSource = tbl;
                SetGridColumHeader();
                conn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetGridColumHeader()
        {
            // Memberi judul pada kolom di grid
            dgViewLstBrg.Columns[0].HeaderText = "ID Stock";
            dgViewLstBrg.Columns[1].HeaderText = "Nama Barang";
            //dgViewLstBrg.Columns[2].Visible = false;
            //dgViewLstBrg.Columns[3].HeaderText = "Type";
            //dgViewLstBrg.Columns[4].HeaderText = "Ukuran";
            //dgViewLstBrg.Columns[5].HeaderText = "Panjang";
            //dgViewLstBrg.Columns[6].HeaderText = "Lebar";
            //dgViewLstBrg.Columns[7].HeaderText = "Tinggi";
            //dgViewLstBrg.Columns[8].HeaderText = "Diameter";
            //dgViewLstBrg.Columns[9].Visible = false;
        }

        private void dgViewLstBrg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // Mengisi nilai ke dataviewgrid pemanggil
            //NamaSelectedCust = dgViewLstBrg.CurrentRow.Cells["nama"].Value.ToString();
            //IdSelectedCust = dgViewLstBrg.CurrentRow.Cells["id"].Value.ToString();
            IdType = dgViewLstBrg.CurrentRow.Cells["typeID"].Value.ToString();
            TypeNama = dgViewLstBrg.CurrentRow.Cells["type"].Value.ToString();
            //tinggi = dgViewLstBrg.CurrentRow.Cells["height"].Value.ToString();
            //panjang = dgViewLstBrg.CurrentRow.Cells["length"].Value.ToString();
            //lebar = dgViewLstBrg.CurrentRow.Cells["width"].Value.ToString();
            //diameter = dgViewLstBrg.CurrentRow.Cells["dia"].Value.ToString();
            //idUkuran = dgViewLstBrg.CurrentRow.Cells["id_ukuran"].Value.ToString();
            Close();
        }

        private void btnBaru_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Maaf anda tidak berhak untuk menambah barang");
        }
    }
}
