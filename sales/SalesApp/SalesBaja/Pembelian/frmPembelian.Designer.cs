﻿namespace SalesBaja.Pembelian
{
    partial class frmPembelian
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNoResi = new System.Windows.Forms.TextBox();
            this.lbNoResi = new System.Windows.Forms.Label();
            this.dtTglPembelian = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbJenis = new System.Windows.Forms.TextBox();
            this.tbUkuran = new System.Windows.Forms.TextBox();
            this.tbBerat = new System.Windows.Forms.TextBox();
            this.tbHarga = new System.Windows.Forms.TextBox();
            this.tbPPN = new System.Windows.Forms.TextBox();
            this.tbEkspedisi = new System.Windows.Forms.TextBox();
            this.tbHPP = new System.Windows.Forms.TextBox();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.cbSupplier = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // tbNoResi
            // 
            this.tbNoResi.Location = new System.Drawing.Point(119, 100);
            this.tbNoResi.Name = "tbNoResi";
            this.tbNoResi.Size = new System.Drawing.Size(182, 20);
            this.tbNoResi.TabIndex = 9;
            // 
            // lbNoResi
            // 
            this.lbNoResi.AutoSize = true;
            this.lbNoResi.Location = new System.Drawing.Point(31, 103);
            this.lbNoResi.Name = "lbNoResi";
            this.lbNoResi.Size = new System.Drawing.Size(51, 13);
            this.lbNoResi.TabIndex = 10;
            this.lbNoResi.Text = "No.  Resi";
            // 
            // dtTglPembelian
            // 
            this.dtTglPembelian.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTglPembelian.Location = new System.Drawing.Point(119, 42);
            this.dtTglPembelian.Name = "dtTglPembelian";
            this.dtTglPembelian.Size = new System.Drawing.Size(182, 20);
            this.dtTglPembelian.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Tanggal";
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(30, 72);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(45, 13);
            this.labelSupplier.TabIndex = 11;
            this.labelSupplier.Text = "Supplier";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Jenis";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Ukuran";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Berat";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(662, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Harga";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(665, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "PPN";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(665, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Expedisi";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(665, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "HPP";
            // 
            // tbJenis
            // 
            this.tbJenis.Location = new System.Drawing.Point(119, 134);
            this.tbJenis.Name = "tbJenis";
            this.tbJenis.Size = new System.Drawing.Size(100, 20);
            this.tbJenis.TabIndex = 22;
            // 
            // tbUkuran
            // 
            this.tbUkuran.Location = new System.Drawing.Point(119, 161);
            this.tbUkuran.Name = "tbUkuran";
            this.tbUkuran.Size = new System.Drawing.Size(100, 20);
            this.tbUkuran.TabIndex = 23;
            // 
            // tbBerat
            // 
            this.tbBerat.Location = new System.Drawing.Point(119, 188);
            this.tbBerat.Name = "tbBerat";
            this.tbBerat.Size = new System.Drawing.Size(100, 20);
            this.tbBerat.TabIndex = 24;
            // 
            // tbHarga
            // 
            this.tbHarga.Location = new System.Drawing.Point(776, 45);
            this.tbHarga.Name = "tbHarga";
            this.tbHarga.Size = new System.Drawing.Size(100, 20);
            this.tbHarga.TabIndex = 25;
            // 
            // tbPPN
            // 
            this.tbPPN.Location = new System.Drawing.Point(776, 69);
            this.tbPPN.Name = "tbPPN";
            this.tbPPN.Size = new System.Drawing.Size(100, 20);
            this.tbPPN.TabIndex = 26;
            // 
            // tbEkspedisi
            // 
            this.tbEkspedisi.Location = new System.Drawing.Point(776, 103);
            this.tbEkspedisi.Name = "tbEkspedisi";
            this.tbEkspedisi.Size = new System.Drawing.Size(100, 20);
            this.tbEkspedisi.TabIndex = 27;
            // 
            // tbHPP
            // 
            this.tbHPP.Location = new System.Drawing.Point(776, 158);
            this.tbHPP.Name = "tbHPP";
            this.tbHPP.Size = new System.Drawing.Size(100, 20);
            this.tbHPP.TabIndex = 28;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(511, 259);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 30);
            this.btnSimpan.TabIndex = 29;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // cbSupplier
            // 
            this.cbSupplier.FormattingEnabled = true;
            this.cbSupplier.Location = new System.Drawing.Point(119, 73);
            this.cbSupplier.Name = "cbSupplier";
            this.cbSupplier.Size = new System.Drawing.Size(182, 21);
            this.cbSupplier.TabIndex = 30;
            // 
            // frmPembelian
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 528);
            this.Controls.Add(this.cbSupplier);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.tbHPP);
            this.Controls.Add(this.tbEkspedisi);
            this.Controls.Add(this.tbPPN);
            this.Controls.Add(this.tbHarga);
            this.Controls.Add(this.tbBerat);
            this.Controls.Add(this.tbUkuran);
            this.Controls.Add(this.tbJenis);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.tbNoResi);
            this.Controls.Add(this.lbNoResi);
            this.Controls.Add(this.dtTglPembelian);
            this.Controls.Add(this.label1);
            this.Name = "frmPembelian";
            this.Text = "frmPembelian";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbNoResi;
        private System.Windows.Forms.Label lbNoResi;
        private System.Windows.Forms.DateTimePicker dtTglPembelian;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbJenis;
        private System.Windows.Forms.TextBox tbUkuran;
        private System.Windows.Forms.TextBox tbBerat;
        private System.Windows.Forms.TextBox tbHarga;
        private System.Windows.Forms.TextBox tbPPN;
        private System.Windows.Forms.TextBox tbEkspedisi;
        private System.Windows.Forms.TextBox tbHPP;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.ComboBox cbSupplier;
    }
}