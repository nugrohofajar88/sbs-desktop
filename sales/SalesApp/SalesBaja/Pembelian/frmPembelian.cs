﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SalesBaja.Pembelian
{
    public partial class frmPembelian : Form
    {
        public MySqlConnection dtConn;
        private string dtConnString;
        private string _userconnected;
        private string _userfunction;
        private int _userid;

        private MySqlDataAdapter pembelianDataAdapter;
        private DataTable pembelianTable;
        private DataTable supplierTable;

        public string Userconnected
        {
            get
            {
                return _userconnected;
            }

            set
            {
                _userconnected = value;
            }
        }

        public string Userfunction
        {
            get
            {
                return _userfunction;
            }

            set
            {
                _userfunction = value;
            }
        }

        public int Userid
        {
            get
            {
                return _userid;
            }

            set
            {
                _userid = value;
            }
        }

        public frmPembelian()
        {
            InitializeComponent();
            InitDataConnection();
            InitSupplierData();
        }

        private void InitDataConnection()
        {
            dtConnString = "server=" + GlobVar.hAddress + "; uid=" +
                    GlobVar.hUser + "; pwd=" + GlobVar.hPasswd + "; database=" +
                    GlobVar.hdbase;
            try
            {
                dtConn = new MySqlConnection();
                dtConn.ConnectionString = dtConnString;
                //dtConn.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //private void frmPembelian_Load(object sender, EventArgs e)
        //{
        //    // TODO: This line of code loads data into the 'datasbs4DataSet.rkp_pembelian' table. You can move, or remove it, as needed.
        //    this.rkp_pembelianTableAdapter.Fill(this.datasbs4DataSet.rkp_pembelian);
        //    //LoadPembelian();
        //}

        //private void saveToolStripButton_Click(object sender, EventArgs e)
        //{
        //    this.rkp_pembelianTableAdapter.Update(this.datasbs4DataSet.rkp_pembelian);
        //}

        //private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        //{
        //    MessageBox.Show("Data dihapus");
        //}

        //private void LoadPembelian()
        //{
        //    if (dtConn != null && dtConn.State == ConnectionState.Closed)
        //    {
        //        dtConn.Open();
        //    }

        //    string sqlcommand = "SELECT id, tanggal, supplier no_resi, jenis, ukuran, " +
        //        "berat, harga, expedisi, total, hpp, FROM rkp_pembelian";
        //    MySqlDataAdapter MarketingAdapter = new MySqlDataAdapter(sqlcommand, dtConn);
        //    pembelianDataAdapter.Fill(pembelianTable);
        //    gridControl1.DataSource = pembelianDataAdapter;

        //    dtConn.Close();
        //}

        private void InitSupplierData()
        {
            supplierTable = new DataTable();

            if (dtConn != null && dtConn.State == ConnectionState.Closed)
            {
                dtConn.Open();
            }

            string sqlcommand = "SELECT id_supplier, nama_supplier FROM m_supplier";
            MySqlDataAdapter supplierAdapter = new MySqlDataAdapter(sqlcommand, dtConn);
            supplierAdapter.Fill(supplierTable);
            cbSupplier.DataSource = supplierTable;
            cbSupplier.DisplayMember = "nama_supplier";

            dtConn.Close();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            dtConn.Open();
            //if (frmMode == FormOpenMode.fomNew)
            //{
                //Int32 TxID = ShowLastId() + 1;

                //Menyimpan master
                SaveMasterTrx();

                // Menyimpan detil 
                //SaveDtlTrx(TxID);
                //UpdateReg(RegNomor, dtTglPO.Value.Year);

                MessageBox.Show("Data telah tersimpan!");
            dtConn.Close();
            //}
        }

        private void SaveMasterTrx()
        {
            MySqlCommand tcmd = new MySqlCommand();

            StringBuilder tSql = new StringBuilder();

            string tglTrx = dtTglPembelian.Value.ToString("yyyy-MM-dd");

            MySqlParameter prmtgl = new MySqlParameter("tgl", tglTrx);
            MySqlParameter prmidsuppl = new MySqlParameter("idSupplier", MySqlDbType.VarChar);
            MySqlParameter prmResi = new MySqlParameter("noResi", MySqlDbType.VarChar);
            MySqlParameter prmJenis = new MySqlParameter("jenis", MySqlDbType.VarChar);
            MySqlParameter prmUkuran = new MySqlParameter("ukuran", MySqlDbType.VarChar);
            MySqlParameter prmBerat = new MySqlParameter("berat", MySqlDbType.Double);
            MySqlParameter prmHarga = new MySqlParameter("harga", MySqlDbType.Double);
            MySqlParameter prmPPN = new MySqlParameter("ppn", MySqlDbType.Double);
            MySqlParameter prmPengiriman = new MySqlParameter("ekpedisi", MySqlDbType.Double);

            tSql.AppendLine("INSERT INTO rkp_pembelian(tanggal, supplier, no_resi, jenis, ");
            tSql.AppendLine("    ukuran, berat, harga, ppn, expedisi) ");
            tSql.AppendLine("VALUES (@tgl, @idSupplier, @noResi, @jenis, ");
            tSql.AppendLine("    @ukuran, @berat, @harga, @ppn, @ekspedisi)");

            try
            {
                tcmd.CommandText = tSql.ToString();
                prmidsuppl.Value = cbSupplier.Text;
                prmResi.Value = tbNoResi.Text;
                prmJenis.Value = tbJenis.Text;
                prmUkuran.Value = tbUkuran.Text;
                prmBerat.Value = double.Parse(tbBerat.Text);
                prmHarga.Value = double.Parse(tbHarga.Text);
                prmPPN.Value = double.Parse(tbPPN.Text);
                prmPengiriman.Value = double.Parse(tbEkspedisi.Text);
                tcmd.Parameters.Add(prmidsuppl);
                tcmd.Parameters.Add(prmtgl);
                tcmd.Parameters.Add(prmResi);
                tcmd.Parameters.Add(prmJenis);
                tcmd.Parameters.Add(prmUkuran);
                tcmd.Parameters.Add(prmBerat);
                tcmd.Parameters.Add(prmHarga);
                tcmd.Parameters.Add(prmPPN);
                tcmd.Parameters.Add(prmPengiriman);
                tcmd.Connection = dtConn;
                tcmd.ExecuteNonQuery();
                //MessageBox.Show("Data Master telah tersimpan");
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

    }
}
