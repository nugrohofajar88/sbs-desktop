﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;

namespace SalesBaja
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (File.Exists("Opening.ini"))
            {
                string AppDataPath = Application.StartupPath;
                INIFile ini = new INIFile(AppDataPath + "\\Opening.ini");
                GlobVar.hAddress = ini.Read("DATA", "host");
                GlobVar.hUser = ini.Read("DATA", "user");
                GlobVar.hPasswd = ini.Read("DATA", "paswd");
                GlobVar.hdbase = ini.Read("DATA", "dbase");
                //MessageBox.Show("Address: " + GlobVar.hAddress + "; User: " + 
                //    GlobVar.hUser + "; Passwd: " + GlobVar.hPasswd );
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(frm_login.GetInstance());
            //frm_login Masuk = new frm_login();
            //Masuk.ShowDialog();
            //if (Masuk.DialogResult == DialogResult.OK)
            //{
            //    fr_ListPO PO = new fr_ListPO();
            //    PO.Userconnected = Masuk.Userconnected;
            //    PO.Userid = Masuk.Userid;
            //    Application.Run(PO);
            //}
        }
    }
}
