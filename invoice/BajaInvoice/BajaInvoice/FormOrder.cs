﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaInvoice
{
    public partial class FormOrder : Form
    {
        koneksi konek;

        public string tgl_invoice;
        public string no_invoice;
        public string jatuh_tempo;
        public string id_invoice;
        public string id_po;
        public string id_creator;
        public string countInvoice;
        public string countMonth;
        public string countYear;
        public List<String> listCustomer = new List<string>();
        public List<String> listAlamat = new List<string>();
        public List<String> listIdPO = new List<string>();
        public List<String> listNomerPO = new List<string>();
        public List<String> listNoSP = new List<string>();
        public List<String> listSales = new List<string>();
        public List<double> listJumlah = new List<double>();
        public double grandTotal = 0;
        public string grandTotalTerbilang;

        private int mintTotalRecords = 0;
        private int mintPageSize = 0;
        private int mintPageCount = 0;
        private int mintCurrentPage = 1;

        public FormOrder(string id_creator)
        {
            InitializeComponent();
            this.konek = new koneksi();
            this.id_creator = id_creator;

            InitializeData();
        }

        public void InitializeData()
        {
            fillGrid();

            string monthStr = DateTime.Now.ToString("MM");
            int monthInt = 1;
            int.TryParse(monthStr, out monthInt);
            countMonth = ToRoman(monthInt);
            countYear = DateTime.Now.ToString("yy");

            resetAll();
        }

        public void fillGrid()
        {
            this.mintPageSize = 25;
            this.mintTotalRecords = konek.getCountPoNoInvoice();
            this.mintPageCount = this.mintTotalRecords / this.mintPageSize;

            if (this.mintTotalRecords % this.mintPageSize > 0)
                this.mintPageCount++;

            this.mintCurrentPage = 0;

            loadPage();
        }

        public void loadPage()
        {
            konek.fillTargetInvoice(dgv, this.mintCurrentPage, this.mintPageSize);

            this.lblStatus.Text = (this.mintCurrentPage + 1).ToString() + " / " + this.mintPageCount.ToString();
        }

        private void goFirst()
        {
            this.mintCurrentPage = 0;

            loadPage();
        }

        private void goPrevious()
        {
            if (this.mintCurrentPage == this.mintPageSize)
                this.mintCurrentPage = this.mintPageCount - 1;

            this.mintCurrentPage--;

            if (this.mintCurrentPage < 0)
                this.mintCurrentPage = 0;

            loadPage();
        }

        private void goNext()
        {
            this.mintCurrentPage++;

            if (this.mintCurrentPage > (this.mintPageCount - 1))
                this.mintCurrentPage = this.mintPageCount - 1;

            loadPage();
        }

        private void goLast()
        {
            this.mintCurrentPage = this.mintPageCount - 1;

            loadPage();
        }

        public void resetAll()
        {
            grandTotalTerbilang = "";

            listCustomer = new List<string>();
            listAlamat = new List<string>();
            listIdPO = new List<string>();
            listNomerPO = new List<string>();
            listNoSP = new List<string>();
            listSales = new List<string>();
            listJumlah = new List<double>();

            idpo.Text = "";
            pocustomer.Clear();
            nopo.Clear();
            namacustomer.Clear();
            alamatcustomer.Clear();
            sales.Clear();
            nopenawaran.Clear();
            subtotal.Clear();
            discount.Clear();
            ppn.Clear();
            total.Clear();
            jatuhtempo.Clear();
        }

        private string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        private string getNoInvoice()
        {
            string[] nomor_op = nopo.Text.Split('/');
            countInvoice = konek.getCountInvoice();
            no_invoice = nomor_op[0] + "/" + countInvoice + "/INV/SBS/" + countMonth + "/" + countYear;

            return no_invoice;
        }

        private string terbilang(long angka)
        {
            string strterbilang = "";
            // membuat array untuk mengubah 1 - 11 menjadi terbilang
            string[] a = { "", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas" };

            if (angka < 12)
            {
                strterbilang = " " + a[angka];
            }
            else if (angka < 20)
            {
                strterbilang = this.terbilang(angka - 10) + " belas";
            }
            else if (angka < 100)
            {
                strterbilang = this.terbilang(angka / 10) + " puluh" + this.terbilang(angka % 10);
            }
            else if (angka < 200)
            {
                strterbilang = " seratus" + this.terbilang(angka - 100);
            }
            else if (angka < 1000)
            {
                strterbilang = this.terbilang(angka / 100) + " ratus" + this.terbilang(angka % 10);
            }
            else if (angka < 2000)
            {
                strterbilang = " seribu" + this.terbilang(angka - 1000);
            }
            else if (angka < 1000000)
            {
                strterbilang = this.terbilang(angka / 1000) + " ribu" + this.terbilang(angka % 1000);
            }
            else if (angka < 1000000000)
            {
                strterbilang = this.terbilang(angka / 1000000) + " juta" + this.terbilang(angka % 1000000);
            }

            // menghilangkan multiple space
            strterbilang = System.Text.RegularExpressions.Regex.Replace(strterbilang, @"^\s+|\s+$", " ");
            // mengembalikan hasil terbilang
            return strterbilang;
        }

        string[] satuan = new string[10] {"nol", "satu", "dua", "tiga", "empat",
  "lima", "enam", "tujuh", "delapan", "sembilan"};
        string[] belasan = new string[10] {"sepuluh", "sebelas", "dua belas",
  "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas",
  "delapan belas", "sembilan belas"};
        string[] puluhan = new string[10] {"", "", "dua puluh",
  "tiga puluh", "empat puluh", "lima puluh", "enam puluh",
  "tujuh puluh", "delapan puluh", "sembilan puluh"};
        string[] ribuan = new string[5] { "", "ribu", "juta", "milyar", "triliyun" };

        string Terbilang2(Decimal d)
        {
            string strHasil = "";
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = "koma " + Terbilang2(Decimal.Round(frac * 100)) + " rupiah";
            else
                strHasil = "rupiah";

            int nDigit = 0;
            int nPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpBuff = "";
                nDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                nPosisi = (strTemp.Length - i) + 1;
                switch (nPosisi % 3)
                {
                    case 1:
                        bool bAllZeros = false;
                        if (i == 1)
                            tmpBuff = satuan[nDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpBuff = belasan[nDigit] + " ";
                        else if (nDigit > 0)
                            tmpBuff = satuan[nDigit] + " ";
                        else
                        {
                            bAllZeros = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0")
                                    bAllZeros = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0")
                                    bAllZeros = false;
                            tmpBuff = "";
                        }

                        if ((!bAllZeros) && (nPosisi > 1))
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpBuff = "se" + ribuan[(int)Decimal.Round(nPosisi / 3m)] + " ";
                            else
                                tmpBuff = tmpBuff + ribuan[(int)Decimal.Round(nPosisi / 3)] + " ";
                        strHasil = tmpBuff + strHasil;
                        break;
                    case 2:
                        if (nDigit > 0)
                            strHasil = puluhan[nDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (nDigit > 0)
                            if (nDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[nDigit] + " ratus " + strHasil;
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }

            return strHasil;
        }

        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int colIndex = e.ColumnIndex;
            if (colIndex == 1)
            {
                bool state = Convert.ToBoolean(dgv.Rows[rowIndex].Cells[colIndex].Value);
                string strCustomer = dgv.Rows[rowIndex].Cells["nama_customer"].Value.ToString();
                string strAlamatCust = dgv.Rows[rowIndex].Cells["alamat_customer"].Value.ToString();
                string strIdPo = dgv.Rows[rowIndex].Cells["id_po"].Value.ToString();
                string strNomerPo = dgv.Rows[rowIndex].Cells["nomer_po"].Value.ToString();
                string strNoSP = dgv.Rows[rowIndex].Cells["no_sp"].Value.ToString();
                string strSales = dgv.Rows[rowIndex].Cells["sales"].Value.ToString();
                string strJumlah = dgv.Rows[rowIndex].Cells["jumlah"].Value.ToString();
                long longJumlah;
                long.TryParse(strJumlah, out longJumlah);

                if (state)
                {
                    listCustomer.Add(strCustomer);
                    listAlamat.Add(strAlamatCust);
                    listIdPO.Add(strIdPo);
                    if (strNomerPo != null && strNomerPo.Length > 0 && strNomerPo != "-")
                        listNomerPO.Add(strNomerPo);
                    listNoSP.Add(strNoSP);
                    listSales.Add(strSales);
                    listJumlah.Add(longJumlah);

                    Console.WriteLine(string.Join(",", listSales));
                }
                else
                {
                    listCustomer.Remove(strCustomer);
                    listAlamat.Remove(strAlamatCust);
                    listIdPO.Remove(strIdPo);
                    if (strNomerPo != null && strNomerPo.Length > 0 && strNomerPo != "-")
                        listNomerPO.Remove(strNomerPo);
                    listNoSP.Remove(strNoSP);
                    listSales.Remove(strSales);
                    listJumlah.Remove(longJumlah);
                    Console.WriteLine(string.Join(",", listSales));
                }

            }
        }
        
        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgv.Rows[rowIndex];
            if (row.Cells[0].Value == null)
                return;
            String statusSelected = row.Cells[5].Value.ToString();

        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgv.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            dgv.Columns["no"].HeaderText = "No.";
            dgv.Columns["nomer_po"].HeaderText = "No. PO Customer";
            dgv.Columns["no_sp"].HeaderText = "No. Order";
            dgv.Columns["tanggal_po"].HeaderText = "Tanggal Order";
            dgv.Columns["nama_customer"].HeaderText = "Nama Customer";
            dgv.Columns["alamat_customer"].HeaderText = "Alamat Customer";
            dgv.Columns["sales"].HeaderText = "Sales";
            dgv.Columns["jumlah"].HeaderText = "Sub Total";
            dgv.Columns["disc"].HeaderText = "Discount";
            dgv.Columns["ppn"].HeaderText = "PPN";
            dgv.Columns["total"].HeaderText = "Total";

            dgv.Columns["id_po"].Visible = false;
            dgv.Columns["sales"].Visible = false;
            dgv.Columns["no_penawaran"].Visible = false;
            dgv.Columns["alamat_customer"].Visible = false; 
            dgv.Columns["disc"].Visible = true;
            dgv.Columns["disc_total"].Visible = false;
            dgv.Columns["ppn"].Visible = true;
            dgv.Columns["total"].Visible = true;
            dgv.Columns["reff"].Visible = false;
            dgv.Columns["kredit"].Visible = false;
            dgv.Columns["keterangan"].Visible = false;

            dgv.Columns["no"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["no_sp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["tanggal_po"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["jumlah"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["disc"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["ppn"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dgv.Columns["no"].Width = 30;
            dgv.Columns["nomer_po"].Width = 150;
            dgv.Columns["no_sp"].Width = 70;
            dgv.Columns["tanggal_po"].Width = 100;
            dgv.Columns["nama_customer"].Width = 300;
            dgv.Columns["alamat_customer"].Width = 200;
            dgv.Columns["disc"].Width = 70;


        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            goPrevious();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            goFirst();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            goNext();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            goLast();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dgrid = sender as DataGridView;
            if (dgrid != null && dgrid.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dgrid.SelectedRows[0];
                if (row != null)
                {
                    resetAll();

                    int rowIndex = dgv.SelectedRows[0].Index;

                    string strIdPo = row.Cells["id_po"].Value.ToString();
                    string strCustomer = row.Cells["nama_customer"].Value.ToString();
                    string strAlamatCust = row.Cells["alamat_customer"].Value.ToString();
                    string strNomerPo = row.Cells["nomer_po"].Value.ToString();
                    string strNoSP = row.Cells["no_sp"].Value.ToString();
                    string strSales = row.Cells["sales"].Value.ToString();
                    string strJumlah = row.Cells["jumlah"].Value.ToString();
                    string strDisc = row.Cells["disc"].Value.ToString();
                    string strDiscTotal = row.Cells["disc_total"].Value.ToString();
                    string strPpn = row.Cells["ppn"].Value.ToString();
                    string strTotal = row.Cells["total"].Value.ToString();
                    string strNoPenawaran = row.Cells["no_penawaran"].Value.ToString();
                    string strKredit = row.Cells["kredit"].Value.ToString();
                    string strKeterangan = row.Cells["keterangan"].Value.ToString();
                    double longJumlah;
                    //long.TryParse(strJumlah, out longJumlah);
                    longJumlah = Convert.ToDouble(double.Parse(strJumlah, System.Globalization.NumberStyles.Currency).ToString());

                    idpo.Text = strIdPo;
                    pocustomer.Text = strNomerPo;
                    nopo.Text = strNoSP;
                    namacustomer.Text = strCustomer;
                    alamatcustomer.Text = strAlamatCust;
                    sales.Text = strSales;
                    nopenawaran.Text = strNoPenawaran;
                    keterangan.Text = strKeterangan;
                    jatuhtempo.Text = strKredit;
                    subtotal.Text = strJumlah;
                    discount.Text = strDiscTotal;
                    ppn.Text = strPpn;
                    total.Text = strTotal;

                    listCustomer.Add(strCustomer);
                    listAlamat.Add(strAlamatCust);
                    listIdPO.Add(strIdPo);
                    if (strNomerPo != null && strNomerPo.Length > 0 && strNomerPo != "-")
                        listNomerPO.Add(strNomerPo);
                    listNoSP.Add(strNoSP);
                    listSales.Add(strSales);
                    listJumlah.Add(longJumlah);

                    id_po = strIdPo;

                }
            }            
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            tgl_invoice = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            no_invoice = this.getNoInvoice();
            jatuh_tempo = jatuhtempo.Text.Trim();
            if (no_invoice == null || no_invoice.Length == 0)
            {
                MessageBox.Show("Maaf, Nomer Invoice Belum Diisi!");
                return;
            }

            if (jatuh_tempo.Equals(""))
            {
                MessageBox.Show("Maaf, Jatuh Tempo Belum Diisi!");
                return;
            }

            //insert invoice first
            if (listIdPO.Count > 0)
            {
                double totalJumlah = 0;
                foreach (double jum in listJumlah)
                {
                    totalJumlah += jum;
                }
                double totalPPN = totalJumlah / 10;
                grandTotal = totalJumlah + totalPPN;
                grandTotalTerbilang = Terbilang2(Convert.ToDecimal(grandTotal));
                id_invoice = konek.addInvoice(id_creator, tgl_invoice, no_invoice, listCustomer[0], listAlamat[0],
                    string.Join(",", listNomerPO), string.Join(",", listNoSP), string.Join(",", listSales), totalJumlah,
                    totalPPN, grandTotal, grandTotalTerbilang, jatuh_tempo);

                foreach (string idPO in listIdPO)
                {
                    konek.setInvoice(idPO, id_invoice);
                }
                InvoiceReportNew report = new InvoiceReportNew();
                data_perusahaan dp = konek.getDataPerusahaan();


                report.Parameters["parameterIdInvoice"].Value = id_invoice;
                report.Parameters["paramNamaPerusahaan"].Value = dp.nama_perusahaan;
                report.Parameters["paramAlamat"].Value = dp.alamat;
                report.Parameters["paramKota"].Value = dp.kota;
                report.Parameters["paramTelp"].Value = "Telp : " + dp.telp + " | Fax : " + dp.fax;
                report.Parameters["paramFax"].Value = dp.fax;
                report.Parameters["paramNorek"].Value = dp.norek;
                report.Parameters["paramNorekNama"].Value = dp.norek_nama;
                report.Parameters["paramNorekBank"].Value = dp.norek_bank;

                report.Parameters["parameterIdInvoice"].Visible = false;
                report.Parameters["paramNamaPerusahaan"].Visible = false;
                report.Parameters["paramAlamat"].Visible = false;
                report.Parameters["paramKota"].Visible = false;
                report.Parameters["paramTelp"].Visible = false;
                report.Parameters["paramFax"].Visible = false;
                report.Parameters["paramNorek"].Visible = false;
                report.Parameters["paramNorekNama"].Visible = false;
                report.Parameters["paramNorekBank"].Visible = false;

                ReportPrintTool printTool = new ReportPrintTool(report);
                printTool.ShowPreviewDialog();
            }


            resetAll();
            fillGrid();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            tgl_invoice = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            no_invoice = this.getNoInvoice();
            jatuh_tempo = jatuhtempo.Text.Trim();

            if (no_invoice == null || no_invoice.Length == 0)
            {
                MessageBox.Show("Maaf, Nomer Invoice Belum Diisi!");
                return;
            }

            if (jatuh_tempo.Equals(""))
            {
                MessageBox.Show("Maaf, Jatuh Tempo Belum Diisi!");
                return;
            }

            //insert invoice first
            if (listIdPO.Count > 0)
            {
                long totalJumlah = 0;
                foreach (long jum in listJumlah)
                {
                    totalJumlah += jum;
                }
                long totalPPN = 0;
                grandTotal = totalJumlah + totalPPN;
                grandTotalTerbilang = Terbilang2(Convert.ToDecimal(grandTotal));
                id_invoice = konek.addInvoice(id_creator, tgl_invoice, no_invoice, listCustomer[0], listAlamat[0],
                    string.Join(",", listNomerPO), string.Join(",", listNoSP), string.Join(",", listSales), totalJumlah,
                    totalPPN, grandTotal, grandTotalTerbilang, jatuh_tempo);

                foreach (string idPO in listIdPO)
                {
                    konek.setInvoice(idPO, id_invoice);
                }

                InvoiceReportNonPPN report = new InvoiceReportNonPPN();
                data_perusahaan dp = konek.getDataPerusahaan();

                report.Parameters["parameterIdInvoice"].Value = id_invoice;
                report.Parameters["paramNamaPerusahaan"].Value = dp.nama_perusahaan;
                report.Parameters["paramAlamat"].Value = dp.alamat;
                report.Parameters["paramKota"].Value = dp.kota;
                report.Parameters["paramTelp"].Value = "Telp : " + dp.telp + " | Fax : " + dp.fax;
                report.Parameters["paramFax"].Value = dp.fax;
                report.Parameters["paramNorek"].Value = dp.norek;
                report.Parameters["paramNorekNama"].Value = dp.norek_nama;
                report.Parameters["paramNorekBank"].Value = dp.norek_bank;

                report.Parameters["parameterIdInvoice"].Visible = false;
                report.Parameters["paramNamaPerusahaan"].Visible = false;
                report.Parameters["paramAlamat"].Visible = false;
                report.Parameters["paramKota"].Visible = false;
                report.Parameters["paramTelp"].Visible = false;
                report.Parameters["paramFax"].Visible = false;
                report.Parameters["paramNorek"].Visible = false;
                report.Parameters["paramNorekNama"].Visible = false;
                report.Parameters["paramNorekBank"].Visible = false;

                ReportPrintTool printTool = new ReportPrintTool(report);
                printTool.ShowPreviewDialog();
            }

            resetAll();
            fillGrid();
        }
    }
}
