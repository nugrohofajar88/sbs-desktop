﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Drawing;

namespace BajaInvoice
{
    public class koneksi
    {
        public MySqlConnection db;
        public string connetionString = null;
        Dictionary<int, t_po> nePO;
        SettingManager _settingManager = new SettingManager("settings.xml");
        public string jabatanAkses = "";

        public koneksi()
        {
            string server = _settingManager.GetValue("server");
            string database = _settingManager.GetValue("database");
            string username = _settingManager.GetValue("username");
            string password = _settingManager.GetValue("password");
            jabatanAkses = _settingManager.GetValue("jabatan");
            connetionString = "server=" + server + ";database=" + database + ";uid=" + username + ";pwd=" + password + ";Allow User Variables=True;";
            db = new MySqlConnection(connetionString);

        }

        public void fillJabatan(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_jabatan from jabatan ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    cb.Items.Add(nama);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public string getCountInvoice()
        {
            string noSJ = "1";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT coalesce(max(id_invoice)+1, 1) from m_invoice; ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        noSJ = reader.GetString(0).ToString().ToUpper();
                    return noSJ;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return noSJ;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return noSJ;
        }

        public string namaAdmin(string user)
        {
            string namasalah = "xxxxxxxx";
            string nama = user;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_dpn as status from m_user where username='" + user + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if(!reader.IsDBNull(0))
                        nama = reader.GetString(0).ToString().ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public int getidAdmin(string user)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_user as status from m_user where nama_dpn='" + user + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public void fillTargetInvoice(DataGridView dataGridView1, int page = 0, int limit = 5)
        {
            dataGridView1.DataSource = null;
            try
            {
                openkoneksi();

                string Query = "SET @no=0;select (@no:=@no+1)+ " + (page * limit) + " AS no, id_po, tanggal_po,  no_po as no_sp, po_customer as nomer_po, " +
                               "   m_customer.nama_customer, m_customer.alamat as alamat_customer, " +
                               "   nama_marketing AS sales, no_penawaran, FORMAT(jumlah,2) jumlah, CONCAT(CONVERT(disc, CHAR(10)),' %') disc, FORMAT((jumlah * disc / 100), 2) disc_total, " +
                               "   FORMAT(ppn,2) ppn, FORMAT((jumlah - (jumlah * disc / 100) + ppn),2) total, reff, kredit, keterangan " +
                               "from t_po, m_customer, m_marketing " +
                               "where t_po.id_customer = m_customer.id_customer and t_po.id_marketing = m_marketing.id_marketing and t_po.no_invoice is null " + 
                               "order by id_po desc " + 
                               "limit " + (page * limit) + ", " + limit;


                Console.WriteLine("queryQyu = " + Query);

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);

                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public int getCountPoNoInvoice()
        {
            int rows = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT COUNT(*) rows " +
                               "FROM t_po inner join m_customer ON t_po.id_customer = m_customer.id_customer " +
                               "WHERE t_po.no_invoice is NULL ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    rows = int.Parse(reader.GetString(0).ToUpper());
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return rows;
        }

        public int getCountInvoices()
        {
            int rows = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT COUNT(*) rows FROM m_invoice";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    rows = int.Parse(reader.GetString(0).ToUpper());
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return rows;
        }

        public int getidAdmin2(string user)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_user as status from m_user where upper(username) ='" + user.ToUpper() + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public Boolean LoginAdmin(string user, string password)
        {
            user = user.ToUpper();
            password = password.ToUpper();

            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select username, password, upper(nama_jabatan) as status " +
                             " from m_user, Jabatan " +
                             " where m_user.id_jabatan = Jabatan.id_jabatan " +
                             "   and upper(username) = '" + user + "' " +
                             "   and upper(password) = '" + password + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        if (jabatanAkses.ToUpper().Contains(reader.GetString(2)))
                        {
                            return true;
                        }

                        MessageBox.Show("Maaf Jabatan Tidak Sesuai");
                    }
                    else
                    {
                        MessageBox.Show("Maaf username dan password anda tidak ada di Database");
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                MessageBox.Show(ex.Message);
                return false;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return false;

        }

        public void setInvoice(string idPO, string id_invoice)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_po set no_invoice=" + id_invoice + " where id_po=" + idPO + " ; ";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string addInvoice(string id_creator, string tgl_invoice, string no_invoice, 
            string customer, string alamat, string listNoPO, string listNoSP, string listSales,
            double totalJumlah, double totalPPN, double grandTotal, string grandTotalTerbilang, string jatuhTempo = "")
        {
            string hasil = "0";
            try
            {
                string Query = "insert into m_invoice(id_creator,tgl_invoice, no_invoice, nama_customer, alamat_customer, "+
                    " list_po, list_sp, sales, jumlah, ppn, grand_total, grand_total_terbilang, jatuh_tempo) " +
                    " values(" + id_creator + ",'" + tgl_invoice + "','" + no_invoice + "','" + customer + "','" + alamat + "','" + listNoPO + "','" +
                        listNoSP + "','" + listSales + "'," + totalJumlah + "," + totalPPN + "," + grandTotal + ",'" + grandTotalTerbilang + "', " + jatuhTempo + "); " +
                    " select last_insert_id();";

                Console.WriteLine("insert m_sj = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
            return hasil;
        }

        public void openkoneksi()
        {
            db.Open();
        }

        public void closekoneksi()
        {
            db.Close();
        }

        public void display_report(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                string Query = "select tanggal,no_spk,nama_dpn,start_time,finish_time,timestampdiff(hour,start_time,finish_time),status_mesin,nama_mesin from t_spk,m_mesin where t_spk.id_mesin=m_mesin.id_mesin;";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void update()
        {
            try
            {
                //This is my connection string i have assigned the database file address path  
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //This is my update query in which i am taking input from the user through windows forms and update the record.  
                //string Query = "update student.studentinfo set idStudentInfo='" + this.IdTextBox.Text + "',Name='" + this.NameTextBox.Text + "',Father_Name='" + this.FnameTextBox.Text + "',Age='" + this.AgeTextBox.Text + "',Semester='" + this.SemesterTextBox.Text + "' where idStudentInfo='" + this.IdTextBox.Text + "';";
                string Query = "";
                //This is  MySqlConnection here i have created the object and pass my connection string.  


                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                //MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                MessageBox.Show("Data Updated");
                while (MyReader2.Read())
                {
                }
                db.Close();//Connection closed here  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //===================Supplier
        public void addSupplier(string nama, int id, string alamat, string telp, string contact)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_supplier(nama_supplier,input_by,alamat_supplier,input_date_supplier,telp_supplier, contact_person) " +
                    " values('" + nama + "','" + id + "','" + alamat + "','" + formatForMySql + "','" + telp + "','" + contact + "' ) ; ";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void viewSupplier(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                //string Query = "select nama_supplier as NamaSupplier,m_user.nama_dpn as NamaDepan,alamat_supplier as Alamat,input_date_supplier as Tanggal,telp_supplier as Telp " +
                //    "from m_supplier,m_user where m_supplier.input_by=m_user.id_user;";


                string Query = "select nama_supplier as NamaSupplier,alamat_supplier as Alamat,input_date_supplier as Tanggal,telp_supplier as Telp, contact_person as `Contact Person` " +
                    "from m_supplier ";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                Console.WriteLine(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        public void viewInvoice(DataGridView dataGridView1, int page = 0, int limit = 5, string qinv = "", string qpo = "", string qcus = "")
        {
            try
            {
                string paramsStr = "";

                List<string> listParams = new List<string>();
                if (!qinv.Equals(""))
                {
                    listParams.Add("mi.no_invoice LIKE '%" + qinv + "%' ");
                }
                if (!qpo.Equals(""))
                {
                    listParams.Add("mi.list_sp LIKE '%" + qpo + "%' ");
                }
                if (!qcus.Equals(""))
                {
                    listParams.Add("mi.nama_customer LIKE '%" + qcus + "%' ");
                }

                foreach (string p in listParams)
                {
                    if (paramsStr.Equals(""))
                        paramsStr += " where " + p;
                    else
                        paramsStr += " and " + p;
                }

                openkoneksi();
                string Query = "SET @no=0;select (@no:=@no+1)+ " + (page * limit) + " AS no, mi.id_invoice, mi.tgl_invoice, mi.no_invoice, mi.jatuh_tempo, mi.nama_customer, mi.alamat_customer, mi.list_sp, FORMAT(mi.jumlah, 2) jumlah, FORMAT(mi.ppn, 2) ppn, FORMAT(mi.grand_total, 2) grand_total " +
                    " from m_invoice mi " +
                    paramsStr +
                    " order by mi.id_invoice " +
                    " limit " + (page * limit) + ", " + limit + ";";

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        //===================Piutang
        public int addPiutang(string id_creator, string id_invoice, string faktur_pajak, string tanda_terima,
            string estimasi_jt, string keterangan)
        {
            int id = 0;

            try
            {
                string Query = "insert into m_piutang(id_creator,id_invoice, faktur_pajak, tanda_terima, estimasi_jt, " +
                    " keterangan) " +
                    " values(" + id_creator + "," + id_invoice + ",'" + faktur_pajak + "','" + tanda_terima + "','" + estimasi_jt + "','" +
                        keterangan + "'); " +
                    " select last_insert_id();";

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();
                while (MyReader2.Read())
                {
                    id = MyReader2.GetInt32(0);
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }

            return id;
        }

        public int updatePiutang(string id_creator, string id_piutang, string faktur_pajak, string tanda_terima,
            string estimasi_jt, string keterangan)
        {
            int result = 0;

            try
            {
                string Query = "update m_piutang set id_creator=" + id_creator +
                    ", faktur_pajak='" + faktur_pajak + "', tanda_terima='" + tanda_terima +
                    "', estimasi_jt='" + estimasi_jt + "', keterangan='" + keterangan + "' " +
                    "where id_piutang = " + id_piutang;
                
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                openkoneksi();
                result = MyCommand2.ExecuteNonQuery();
                
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }

            return result;
        }
        public List<int> viewPiutang(DataGridView dataGridView1)
        {
            List<int> row_month = new List<int>();

            try
            {
                openkoneksi();
                string Query = "SELECT mi.id_invoice " +
                                "    , mi.tgl_invoice " +
                                "	,DATE_FORMAT(mi.tgl_invoice, '%d') AS tgl_inv " +
                                "    , DATE_FORMAT(mi.tgl_invoice, '%M') AS bln_inv " +
                                "     , DATE_FORMAT(mi.tgl_invoice, '%Y') AS thn_inv " +
                                "      , mi.no_invoice " +
                                "	,SUBSTR(mi.no_invoice, 1, (LOCATE('/', mi.no_invoice) - 1)) AS no_inv " +
                                "       , mi.nama_customer " +
                                "	,mi.jumlah AS nominal " +
                                "	,mi.ppn " +
                                "	,mi.grand_total AS total " +
                                "	,mp.estimasi_jt AS estimasi_jt " +
                                "	,mp.tanda_terima AS tanda_terima " +
                                "	,GROUP_CONCAT(DISTINCT dpb.jenis_bayar) AS pembayaran " +
                                "    , SUM(dpb.nominal)AS nominal_bayar " +
                                "    , CASE WHEN SUM(dpb.nominal) >= mi.jumlah THEN 'LUNAS' END AS `status` " +
                                "	, CASE WHEN SUM(dpb.nominal) >= mi.jumlah THEN MAX(dpb.tgl_tagih) END AS `status` " +
                                "	, mp.keterangan AS keterangan " +
                                "FROM m_invoice mi " +
                                "LEFT JOIN m_piutang mp ON mp.id_invoice = mi.id_invoice " +
                                "LEFT JOIN dt_piutang_bayar dpb ON dpb.id_piutang = mp.id_piutang " +
                                "GROUP BY mi.id_invoice, mi.tgl_invoice, mi.no_invoice";
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);

                string bulan = "";
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();

                foreach (DataRow row in dTable.Rows)
                {
                    int rowIndex = dataGridView1.Rows.Add();
                    var r = dataGridView1.Rows[rowIndex];

                    if (!bulan.Equals(row["bln_inv"] + " " + row["thn_inv"]))
                    {
                        //    r.Cells["id_invoice"].Value = "";
                        //    r.Cells["tgl_inv"].Value = row["bln_inv"];
                        //    row_month.Add(rowIndex);

                        //    rowIndex = dataGridView1.Rows.Add();
                        //    r = dataGridView1.Rows[rowIndex];
                        bulan = row["bln_inv"] + " " + row["thn_inv"];
                    }
                    else
                        bulan = "";

                r.Cells["id_invoice"].Value = row["id_invoice"];
                    r.Cells["bulan"].Value = bulan;
                    r.Cells["tgl_inv"].Value = row["tgl_inv"];
                    r.Cells["no_inv"].Value = row["no_invoice"];
                    r.Cells["nama_customer"].Value = row["nama_customer"];
                    r.Cells["jumlah"].Value = (!row["nominal"].Equals("")) ? string.Format("{0:N0}", row["nominal"]) : row["nominal"];
                    r.Cells["ppn"].Value = (!row["ppn"].Equals("")) ? string.Format("{0:N0}", row["ppn"]) : row["ppn"];
                    r.Cells["grand_total"].Value = (!row["total"].Equals("")) ? string.Format("{0:N0}", row["total"]) : row["total"];
                    r.Cells["tanda_terima"].Value = row["tanda_terima"];
                    r.Cells["pembayaran"].Value = row["pembayaran"];
                    r.Cells["nominal"].Value = (!row["nominal_bayar"].Equals("")) ? string.Format("{0:N0}", row["nominal_bayar"]) : row["nominal_bayar"];
                    r.Cells["status"].Value = row["status"];
                    r.Cells["keterangan"].Value = row["keterangan"];

                    bulan = row["bln_inv"] + " " + row["thn_inv"]; //row["bln_inv"].ToString();
                }  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dataGridView1.Columns["id_invoice"].Visible = false;

                if (db != null)
                {
                    db.Close();
                }
            }

            return row_month;
        }

        public DataTable detailPiutang(int id_invoice)
        {
            DataTable dt = null;

            try
            {
                openkoneksi();
                string Query = "SELECT mi.id_invoice " +
                            "    , mp.id_piutang " +
                            "	,mi.tgl_invoice " +
                            "	,DATE_FORMAT(mi.tgl_invoice, '%d') AS tgl_inv " +
                            "    , DATE_FORMAT(mi.tgl_invoice, '%M') AS bln_inv " +
                            "     , DATE_FORMAT(mi.tgl_invoice, '%Y') AS thn_inv " +
                            "      , mi.no_invoice " +
                            "	,SUBSTR(mi.no_invoice, 1, (LOCATE('/', mi.no_invoice) - 1)) AS no_inv " +
                            "       , mi.nama_customer " +
                            "	,mi.jumlah AS nominal " +
                            "	,mi.ppn " +
                            "	,mi.grand_total AS total " +
                            "	,mp.faktur_pajak AS faktur_pajak " +
                            "	,mp.estimasi_jt AS estimasi_jt " +
                            "	,mp.tanda_terima AS tanda_terima " +
                            "	,mp.keterangan AS keterangan " +
                            "	,dpb.tgl_tagih AS tgl_tagih " +
                            "	,dpb.jenis_bayar AS pembayaran " +
                            "	,dpb.no_bg AS no_bg " +
                            "	,dpb.tgl_bg AS tgl_bg " +
                            "	,dpb.nominal AS nominal_bayar " +
                            "FROM m_invoice mi " +
                            "LEFT JOIN m_piutang mp ON mp.id_invoice = mi.id_invoice " +
                            "LEFT JOIN dt_piutang_bayar dpb ON dpb.id_piutang = mp.id_piutang " +
                            "WHERE mi.id_invoice = " + id_invoice.ToString();

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                dt = new DataTable();
                MyAdapter.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
            return dt;
        }

        public int addPiutangBayar(string id_creator, string id_piutang, string tgl_tagih, string jenis_bayar,
            string no_bg, string tgl_bg, string nominal)
        {
            int result = 0;

            try
            {
                string Query = "insert into dt_piutang_bayar values(" + id_creator +
                    ", " + id_piutang + ", '" + tgl_tagih +
                    "', '" + jenis_bayar + "', '" + no_bg + "', '" + tgl_bg + "', " + nominal + ")";
                Console.WriteLine(Query);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                openkoneksi();
                result = MyCommand2.ExecuteNonQuery();

                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }

            return result;
        }

        public int deletePiutangBayar(string id_piutang)
        {
            int result = 0;

            try
            {
                string Query = "delete from dt_piutang_bayar where id_piutang = " + id_piutang;

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                openkoneksi();
                result = MyCommand2.ExecuteNonQuery();

                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }

            return result;
        }

        //==================User
        public void addUser(string admin, string id_jabatan, string username, string password, string nama_dpn, string nama_blk, int telp)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_user(user_inputby,id_jabatan,username,password,nama_dpn,nama_blk,input_date,telp_user) values('" + getidAdmin(admin) + "','" + getIndexJabatan(id_jabatan) + "','" + username + "','" + password + "','" + nama_dpn + "','" + nama_blk + "','" + formatForMySql + "','" + telp + "');";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void viewUser(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                string Query = "select nama_dpn as NamaDepan,nama_blk as NamaBelakang,telp_user as Telp,username as Username,password as Password,jabatan.nama_jabatan as NamaJabatan" +
                    " from m_user,Jabatan where m_user.id_jabatan=Jabatan.id_jabatan;";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public int getIndexJabatan(string jabatan)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_jabatan as jabatan from Jabatan where nama_jabatan='" + jabatan + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
                if (index == 0)
                {
                    index = getNewJabatan(jabatan);
                }
            }

            return index;

        }

        public int getNewJabatan(string jabatanStr)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into jabatan(nama_jabatan) values('" + jabatanStr + "');";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return getIndexJabatan(jabatanStr);
        }

        //==================Customer
        public void addCustomer(int admin, string nama, string alamat, string ket, string telp, string contact)
        {

            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_customer(customer_inputby,nama_customer,alamat,ketr,input_date_customer,telp_cust,contact_person) " +
                    " values('" + admin + "','" + nama + "','" + alamat + "','" + ket + "','" + formatForMySql + "','" + telp + "','" + contact + "' );";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void viewCustomer(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                //string Query = "select nama_customer as NamaCustomer,Alamat as Alamat,ketr as Keterangan,input_date_customer as Tanggal,telp_cust as Telp,m_user.nama_dpn as InputBy" +
                //    " from m_customer,m_user where m_customer.customer_inputby=m_user.id_user;";

                string Query = "select nama_customer as NamaCustomer,Alamat as Alamat,ketr as Keterangan,input_date_customer as Tanggal,telp_cust as Telp, contact_person" +
                    " from m_customer ";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        //===========Stock
        public int getIndexBarang(string namaBarang)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_barang from m_barang where nama_barang='" + namaBarang + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return index;

        }

        public int getIndexTypeBarang(string tpBarang)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_type from m_type where type='" + tpBarang + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return index;

        }

        public int getIndexUkuran(string ukuran)
        {
            int index = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_ukuran from m_ukuran where nama_ukuran='" + ukuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    index = int.Parse(reader.GetString(0).ToUpper());
                    return index;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return index;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return index;

        }



        public void addType(string typeBesi)
        {
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_type(type) values('" + typeBesi + "');";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void addUkuran(string nmUkuran, string pjg, string lbr, string tinggi)
        {
            string faktor_hitung = "8";
            if (tinggi == null || tinggi.Length == 0 || (tinggi.Length > 0 && Double.Parse(tinggi) == 0))
            {
                tinggi = "NULL";
                faktor_hitung = "6.2";
            }
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into m_ukuran(nama_ukuran,panjang,lebar,tinggi,faktor_hitung) values('" + nmUkuran + "'," + pjg + "," +
                    lbr + "," + tinggi + ",'" + faktor_hitung + "' );";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void addStock(string type, string pjg, string lbr, string tinggi, string qty, string kg, bool piltype)
        {

            try
            {
                string Query;
                string nmUkuran;
                if (tinggi != null && tinggi.Length > 0)
                    nmUkuran = lbr + " x " + pjg + " x " + tinggi;
                else
                    nmUkuran = lbr + " x " + pjg;
                int idType = 0;
                int idUkuran = 0;

                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");

                if (piltype == true)
                {
                    idType = getIndexTypeBarang(type);
                    idUkuran = getIndexUkuran(nmUkuran);
                    if (idUkuran <= 0)
                    {
                        addUkuran(nmUkuran, pjg, lbr, tinggi);
                        idUkuran = getIndexUkuran(nmUkuran);
                    }

                    Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg) values('" + idType + "','" + idUkuran + "','" + qty + "','" + kg + "') " +
                        " ON DUPLICATE KEY UPDATE qty_stock= " + qty + " ;";
                }
                else
                {

                    addType(type);
                    idType = getIndexTypeBarang(type);
                    addUkuran(nmUkuran, pjg, lbr, tinggi);
                    idUkuran = getIndexUkuran(nmUkuran);

                    Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg) values('" + idType + "','" + idUkuran + "','" + qty + "','" + kg + "');";
                }

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void addStockOnly(string idType, string idUkuran, string qty, string kg)
        {

            try
            {
                string Query;

                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");

                Query = "insert into m_stock(id_type,id_ukuran,qty_stock,kg) values(" + idType + "," + idUkuran + "," + qty + "," + kg + ") " +
                     " ON DUPLICATE KEY UPDATE qty_stock= " + qty + " ;";


                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void selectNamaBarang(ComboBox cb)
        {

            try
            {
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select type from m_type ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    cb.Items.Add(nama);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void selectTypeBarang(ComboBox cb, int id)
        {

            try
            {
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select type from m_type where id_barang='" + id + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    cb.Items.Add(nama);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void viewStock(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                string Query = "select type as Type,nama_ukuran as Ukuran, qty_stock as QTY,kg as KG, harga_stock as Harga" +
                    " from m_stock,m_type,m_ukuran where m_stock.id_type=m_type.id_type and m_stock.id_ukuran=m_ukuran.id_ukuran ;";
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        //====================Stok Awal


        public void selectBrgType(ComboBox cb, string no_po)
        {
            cb.Items.Clear();
            try
            {
                string tipe = "";
                int id_type = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT type, dpo.id_type " +
                            "from `m_type`, " +
                            "( " +
                            "  SELECT DISTINCT id_type FROM `dt_po`,`t_po` WHERE no_po = '" + no_po + "' " +
                            " and dt_po.id_po = t_po.id_po " +
                            ") dpo " +
                            "WHERE m_type.id_type = dpo.id_type ";

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    tipe = reader.GetString(0).ToString().ToUpper();
                    id_type = reader.GetInt32(1);
                    type itemType = new type();
                    itemType.id_type = id_type;
                    itemType.tipe = tipe;
                    //cb.Items.Add(namatipe);
                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void selectAllBrgType(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                string tipe = "";
                int id_type = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT type, id_type " +
                            "from `m_type` ";

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    tipe = reader.GetString(0).ToString().ToUpper();
                    id_type = reader.GetInt32(1);
                    type itemType = new type();
                    itemType.id_type = id_type;
                    itemType.tipe = tipe;
                    //cb.Items.Add(namatipe);
                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }

        public void selectAllAvailableBrgType(ComboBox cb)
        {
            cb.Items.Clear();
            try
            {
                string tipe = "";
                int id_type = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT type, m_type.id_type " +
                            "from `m_type`, " +
                            "( " +
                            "  select distinct id_type " +
                            "  from m_stock " +
                            "  where qty_stock > 0 " +
                            ")stock " +
                            "WHERE m_type.id_type = stock.id_type"
                            ;

                //Console.WriteLine("query select type = "+sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    tipe = reader.GetString(0).ToString().ToUpper();
                    id_type = reader.GetInt32(1);
                    type itemType = new type();
                    itemType.id_type = id_type;
                    itemType.tipe = tipe;
                    //cb.Items.Add(namatipe);
                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }


        }


        public void getAvailableUkuran(ComboBox cb /*cbUkuran*/, string id_type_pilih)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p = 0, l = 0, t = 0;
                string nama = "", idStock = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();

                string sql = "SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, m_stock.qty_stock, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi, " +
                            "       m_stock.id_stock " +
                            "FROM m_stock, m_ukuran " +
                            "WHERE m_stock.id_type = " + id_type_pilih + " " +
                            "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            "AND qty_stock > 0 " +
                            "ORDER BY lebar, panjang, tinggi ";

                Console.WriteLine("query ukuranRekom = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    idStock = reader.GetString(6).ToString();

                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    itemUkuran.id_stock = idStock;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getAllUkuran(ComboBox cb /*cbUkuran*/)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p = 0, l = 0, t = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();

                string sql = "SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, 0, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi " +
                            "FROM m_ukuran " +
                            "ORDER BY lebar, panjang, tinggi ";

                //Console.WriteLine("query ukuranRekom = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        public void getnamaUkuran(ComboBox cb /*cbUkuran*/, string id_type, string no_po, string id_ukuran, string id_type_pilih)
        {
            cb.Items.Clear();
            try
            {
                int ukuran_id = 0, max_qty = 0, p = 0, l = 0, t = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                //string sql = "SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, m_stock.qty_stock, " +
                //             "       m_ukuran.panjang, m_ukuran.lebar, m_ukuran.tinggi " +
                //             "FROM m_stock, m_ukuran, " +
                //             "( " +
                //             " select m_type.id_type, m_type.type, panjang as p, lebar as l, tinggi as t " +
                //             " from dt_po, m_ukuran, m_type, t_po " +
                //             " where dt_po.id_ukuran = m_ukuran.id_ukuran " +
                //             "   AND dt_po.id_type = m_type.id_type " +
                //             "   AND dt_po.id_po = t_po.id_po " +
                //             "     AND t_po.no_po = '" + no_po + "' " +
                //             "     AND m_type.id_type = " + id_type + " " +
                //             "     AND dt_po.id_ukuran = " + id_ukuran + " " +
                //             ") ord " +
                //             "WHERE m_stock.id_type = ord.id_type " +
                //             "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                //             "AND panjang >= ord.p AND lebar>= ord.l AND tinggi >= ord.t " +
                //             "AND qty_stock > 0 ";

                string sql = "SELECT m_ukuran.id_ukuran, m_ukuran.nama_ukuran, m_stock.qty_stock, " +
                            "       m_ukuran.panjang, m_ukuran.lebar,  COALESCE(m_ukuran.tinggi,0) as tinggi " +
                            "FROM m_stock, m_ukuran, " +
                            "( " +
                            " select m_type.id_type, m_type.type, panjang as p, lebar as l, tinggi as t " +
                            " from dt_po, m_ukuran, m_type, t_po " +
                            " where dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            "   AND dt_po.id_type = m_type.id_type " +
                            "   AND dt_po.id_po = t_po.id_po " +
                            "     AND t_po.no_po = '" + no_po + "' " +
                            "     AND m_type.id_type = " + id_type + " " +
                            "     AND dt_po.id_ukuran = " + id_ukuran + " " +
                            ") ord " +
                            "WHERE m_stock.id_type = " + id_type_pilih + " " +
                            "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            "AND panjang >= ord.p AND lebar>= ord.l AND  COALESCE(tinggi, 0 ) >=  COALESCE(ord.t, 0 ) " +
                            "AND qty_stock > 0 ";

                Console.WriteLine("query ukuranRekom = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    ukuran_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    max_qty = reader.GetInt32(2);
                    p = reader.GetInt32(3);
                    l = reader.GetInt32(4);
                    t = reader.GetInt32(5);
                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = ukuran_id;
                    itemUkuran.nama_ukuran = nama;
                    itemUkuran.max_qty = max_qty;
                    itemUkuran.panjang = p;
                    itemUkuran.lebar = l;
                    itemUkuran.tinggi = t;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getTypeRekomendasi(ComboBox cb /*cbType */, string id_type, string no_po, string id_ukuran)
        {
            cb.Items.Clear();
            try
            {
                int type_id = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();


                string sql = "SELECT distinct m_stock.id_type, m_type.type " +
                            "FROM m_stock, m_ukuran, m_type, " +
                            "( " +
                            " select m_type.id_type, m_type.type, panjang as p, lebar as l, COALESCE(tinggi,0) as t " +
                            " from dt_po, m_ukuran, m_type, t_po " +
                            " where dt_po.id_ukuran = m_ukuran.id_ukuran " +
                            "   AND dt_po.id_type = m_type.id_type " +
                            "   AND dt_po.id_po = t_po.id_po " +
                            "     AND t_po.no_po = '" + no_po + "' " +
                            "     AND m_type.id_type = " + id_type + " " +
                            "     AND dt_po.id_ukuran = " + id_ukuran + " " +
                            ") ord " +
                            "WHERE m_stock.id_type = m_type.id_type " +
                            "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            "AND panjang >= ord.p AND lebar>= ord.l AND  COALESCE(tinggi, 0 ) >=  COALESCE(ord.t, 0 ) " +
                            "AND qty_stock > 0 ";

                Console.WriteLine("Query typeRekom = " + sql);
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    type_id = reader.GetInt32(0);
                    nama = reader.GetString(1).ToString().ToUpper();
                    type itemType = new type();
                    itemType.id_type = type_id;
                    itemType.tipe = nama;

                    cb.Items.Add(itemType);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getQtyHarga(TextBox lblQty, TextBox lblKg, int id_ukuran, int id_type)
        {
            try
            {
                int qty = 0;
                int harga = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select qty_stock, harga_stock from m_stock where id_ukuran = '" + id_ukuran + "' and id_type='" + id_type + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    qty = reader.GetInt32(0);
                    harga = reader.GetInt32(1);
                    lblQty.Text = qty.ToString();
                    lblKg.Text = harga.ToString();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getnamaUkuranOrder(ComboBox cb, string id, string no_po)
        {
            cb.Items.Clear();
            try
            {
                int id_ukuran = 0;
                string nama = "";
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT nama_ukuran, dpo.id_ukuran " +
                            "from `m_ukuran`, " +
                            "( " +
                            "  SELECT id_ukuran FROM `dt_po`,`t_po` WHERE no_po = '" + no_po + "' " +
                            "  and id_type = " + id + " and dt_po.id_po = t_po.id_po " +
                            ") dpo " +
                            "WHERE m_ukuran.id_ukuran = dpo.id_ukuran ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    id_ukuran = reader.GetInt32(1);
                    ukuran itemUkuran = new ukuran();
                    itemUkuran.id_ukuran = id_ukuran;
                    itemUkuran.nama_ukuran = nama;
                    cb.Items.Add(itemUkuran);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getQtyHargaOrder(Label lblQty, Label lblKg, int id_ukuran, int id_type)
        {
            try
            {
                int qty = 0;
                int harga = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select qty_po, harga_po from dt_po where id_ukuran = '" + id_ukuran + "' and id_type='" + id_type + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    qty = reader.GetInt32(0);
                    harga = reader.GetInt32(1);
                    lblQty.Text = qty.ToString();
                    lblKg.Text = harga.ToString();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void getQtyKgOrder(TextBox lblQty, TextBox lblKg, int id_ukuran, int id_type)
        {
            try
            {
                int qty = 0;
                double p = 0, l = 0, t = 0;
                double kg = 0;
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select qty_po, panjang, lebar,  COALESCE(tinggi, 0 ) as tinggi " +
                             "from dt_po, m_ukuran where dt_po.id_ukuran = " + id_ukuran + " " +
                             "and dt_po.id_ukuran = m_ukuran.id_ukuran " +
                             "and dt_po.id_type = " + id_type + " ";

                Console.WriteLine("query kg = " + sql);

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    qty = reader.GetInt32(0);
                    p = reader.GetDouble(1);
                    l = reader.GetDouble(2);
                    t = reader.GetDouble(3);
                    if (t > 0)
                    {
                        kg = (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;
                    }
                    else
                    {
                        kg = (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
                    }
                    lblQty.Text = qty.ToString();
                    lblKg.Text = kg.ToString();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        //=============INSERT STOCK

        public int getId_po(string no_po)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_po  from t_po where no_po='" + no_po + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public void addt_spk(int id_mesin, int inputby, int id_po, string no_spk, int status_spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into t_spk(id_mesin,admin_inputby,id_po,no_spk,input_time,status_spk) " +
                    "values('" + id_mesin + "','" + inputby + "','" + id_po + "','" + no_spk + "','" + formatForMySql + "','" + status_spk + "');";

                Console.WriteLine("insert t_spk = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string addt_spk(string inputby, string id_po, string no_spk, int status_spk)
        {
            string hasil = "0";
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into t_spk(admin_inputby,id_po,no_spk,input_time,status_spk) " +
                    " values(" + inputby + "," + id_po + ",'" + no_spk + "','" + formatForMySql + "','" + status_spk + "'); " +
                    " select last_insert_id();";

                Console.WriteLine("insert t_spk = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
            return hasil;
        }

        public bool cekUkuran(string namaUkuran)
        {
            bool count = false;
            int jml = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_ukuran from m_ukuran where nama_ukuran='" + namaUkuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    jml = jml + 1;
                }

                if (jml > 0)
                    count = true;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return count;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return count;
        }

        public string getBentukUkuran(string namaUkuran)
        {
            string nama = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select type_bentuk from m_ukuran where nama_ukuran = '" + namaUkuran + "' ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToString().ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public int getId_Spk(string no_spk)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_spk  from t_spk where no_spk='" + no_spk + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public int getIdStock(int id_type, int id_ukuran)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_stock  from m_stock where id_type='" + id_type + "' and id_ukuran='" + id_ukuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                }

                if (nama == 0)
                {
                    reader.Close();
                    dbcmd = db.CreateCommand();
                    sql = "insert into m_stock (id_type, id_ukuran, qty_stock, kg) values ('" + id_type + "', '" + id_ukuran + "', 0, 0 );  " +
                        "  select last_insert_id(); "; ;
                    dbcmd.CommandText = sql;
                    reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        nama = int.Parse(reader.GetString(0).ToUpper());

                    }
                }

                return nama;


            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public ukuran getUkuran(string l, string p, string t)
        {
            int namasalah = 00;
            int nama = 0;
            ukuran hasil = new ukuran();

            string faktor_hitung = "8";
            if (t == null || t.Length == 0 || (t.Length > 0 && Double.Parse(t) == 0))
            {
                t = "NULL";
                faktor_hitung = "6.2";
            }

            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_ukuran, nama_ukuran, lebar, panjang, tinggi, faktor_hitung " +
                            " from m_ukuran " +
                            " where lebar=" + l + " and panjang=" + p + " and tinggi=" + t + " ";
                if (t == "NULL")
                    sql = "select id_ukuran, nama_ukuran, lebar, panjang, tinggi, faktor_hitung " +
                            " from m_ukuran " +
                            " where lebar=" + l + " and panjang=" + p + " and tinggi IS NULL ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    hasil.id_ukuran = nama;
                    hasil.nama_ukuran = reader.GetString(1);
                    hasil.lebar = reader.GetInt32(2);
                    hasil.panjang = reader.GetInt32(3);
                    if (!reader.IsDBNull(4))
                        hasil.tinggi = reader.GetInt32(4);
                }

                if (nama == 0)
                {
                    reader.Close();

                    string nama_ukuran = grupUkuran(l, p, t);

                    dbcmd = db.CreateCommand();
                    sql = "insert into m_ukuran (nama_ukuran, lebar, panjang, tinggi, faktor_hitung) " +
                        "  values ('" + nama_ukuran + "', " + l + " , " + p + " , " + t + " , " + faktor_hitung + "  );  " +
                        "  select last_insert_id(); ";
                    dbcmd.CommandText = sql;
                    reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        nama = int.Parse(reader.GetString(0).ToUpper());
                        hasil.id_ukuran = nama;
                        hasil.nama_ukuran = nama_ukuran;
                        hasil.lebar = int.Parse(l);
                        hasil.panjang = int.Parse(p);
                        if (t != "NULL")
                            hasil.tinggi = int.Parse(t);
                    }
                }

                return hasil;


            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return hasil;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public string grupUkuran(string l, string p, string t)
        {
            string gabung;
            if (t.Length > 0 && !t.Equals("NULL"))
                gabung = l + " x " + p + " x " + t;
            else
                gabung = l + " x " + p;

            return gabung;
        }

        public bool cekStock(int id_type, int id_ukuran)
        {
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select count(*) from m_stock where id_type=" + id_type + " and id_ukuran=" + id_ukuran + "";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return false;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return (nama > 0);
        }

        public void adddt_spk(int id_spk, int stock_awal, int stock_order, int stock_akhir, string qty_awal, string qty_order, string qty_akhir,
            int status, string kg_awal, string kg_order, string kg_akhir)
        {
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                string Query = "insert into dt_spk(id_spk,stock_awal,stock_order,stock_akhir,qty_awal,qty_order,qty_akhir, status_pengerjaan, " +
                               "                   kg_awal, kg_order, kg_akhir) " +
                    "values('" + id_spk + "','" + stock_awal + "','" + stock_order + "','" + stock_akhir + "','" + qty_awal + "','" + qty_order + "', " +
                    "'" + qty_akhir + "','" + status + "', " + kg_awal + ", " + kg_order + ", " + kg_akhir + " ); ";

                Console.WriteLine("Query DT_SPK = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string adddt_spk(int id_spk, int stock_awal, int stock_order, int stock_akhir, string qty_awal, string qty_order, string qty_akhir,
           int status, string kg_awal, string kg_order, string kg_akhir, string idmesin, string prequisite)
        {
            string strStockAkhir = "";
            if (stock_akhir == -1)
            {
                strStockAkhir = "NULL";
                qty_akhir = "NULL";
                kg_akhir = "NULL";
            }
            else
                strStockAkhir = stock_akhir.ToString();

            string hasil = "";
            try
            {
                DateTime now = DateTime.Now;
                string Query = " insert into dt_spk(id_spk,stock_awal,stock_order,stock_akhir,qty_awal,qty_order,qty_akhir, status_pengerjaan, " +
                               "                   kg_awal, kg_order, kg_akhir, id_mesin, prequesite_dt_spk) " +
                    "values( " + id_spk + "," + stock_awal + "," + stock_order + "," + strStockAkhir + ",'" + qty_awal + "','" + qty_order + "', " +
                    qty_akhir + ",'" + status + "', " + kg_awal + ", " + kg_order + ", " + kg_akhir + ", " + idmesin + " , " + prequisite + "  ); " +
                    " select last_insert_id();";

                Console.WriteLine("Query DT_SPK = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return hasil;
        }

        public int getId_Customer(string no_po)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_customer  from t_po where no_po='" + no_po + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public string getNameCustomer(string no_po)
        {
            string namasalah = "";
            string nama = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_customer from m_customer, " +
                    "( " +
                    " select id_customer from t_po where no_po = '" + no_po + "' " +
                    ") tpo " +
                    "where m_customer.id_customer = tpo.id_customer ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = reader.GetString(0).ToUpper();
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public void updatePO(string no_po, int status)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_po set status_po='" + 1 + "' where no_po='" + no_po + "';";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public int getId_dt_po(int id_po, int idUkuran)
        {
            int namasalah = 00;
            int nama = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_dt_po  from dt_po where id_po='" + id_po + "' and id_ukuran='" + idUkuran + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        // Report

        public void viewReport(DataGridView dataGridView1)
        {
            //tgl.spk.oprt.start.finish            
            try
            {
                openkoneksi();
                string sql = "SELECT " +
                                "DATE_FORMAT(spk.input_time, '%d/%m/%Y') AS Tanggal, " +
                                "spk.no_spk as SPK, " +
                                "DATE_FORMAT(spk.start_time, '%H:%i') AS Start, " +
                                "DATE_FORMAT(spk.finish_time, '%H:%i') AS Finish, " +
                                "TIMESTAMPDIFF(MINUTE, spk.start_time, spk.finish_time) AS Waktu, " +
                                "case spk.status_spk " +
                                "    when 0 then 'Antrian' " +
                                "    when 1 then 'Progress' " +
                                "    else 'Finish' " +
                                "end AS Status " +
                            "FROM `t_spk` spk; ";

                Console.WriteLine("QUERY DGV == " + sql);
                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                Console.WriteLine(dTable);
                dataGridView1.DataSource = dTable;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

        }

        public void listOrder(DataGridView dataGridView1, string no_po)
        {
            try
            {
                openkoneksi();

                string Query = "select type, dt_po.id_type,  lebar, panjang, COALESCE(tinggi,0) as tinggi, qty_po " +
                               "from m_type, dt_po, t_po, m_ukuran " +
                    "where dt_po.id_po = t_po.id_po and t_po.no_po = '" + no_po + "' " +
                    "and dt_po.id_type = m_type.id_type " +
                    "and dt_po.id_ukuran = m_ukuran.id_ukuran ";

                Console.WriteLine("queryQyu = " + Query);

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);

                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void listRekomendasi(DataGridView dataGridView1, string type, string p, string l, string t)
        {
            if (p.Length == 0)
                p = int.MaxValue.ToString();
            if (l.Length == 0)
                l = int.MaxValue.ToString();
            if (t.Length == 0)
                t = int.MaxValue.ToString();
            try
            {
                openkoneksi();

                //string Query = "SELECT TYPE , panjang, lebar, COALESCE(tinggi,0) as tinggi, qty_stock " +
                //                "FROM m_type, m_stock, m_ukuran " +
                //                "WHERE m_stock.id_type = m_type.id_type " +
                //                "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                //                "AND type = '" + type + "' and panjang >= " + p + " and lebar>= " + l + " " +
                //                "and tinggi >= " + t + " and qty_stock > 0 ";

                string Query = "SELECT TYPE ,  lebar, panjang, COALESCE(tinggi,0) as tinggi, qty_stock " +
                                "FROM m_type, m_stock, m_ukuran " +
                                "WHERE m_stock.id_type = m_type.id_type " +
                                "AND m_stock.id_ukuran = m_ukuran.id_ukuran " +
                                "AND panjang >= " + p + " and lebar>= " + l + " " +
                                "and  COALESCE( tinggi, 0 ) >= COALESCE(" + t + ",0) and qty_stock > 0 ";

                Console.WriteLine("queryQyu = " + Query);

                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);

                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }

        public void decreseStock(int id_stock, string qty)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "UPDATE `m_stock` SET `qty_stock` = `qty_stock` - " + qty + " WHERE `id_stock` = " + id_stock;

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void setFinishSPK(int id_spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "UPDATE `t_spk` SET status_spk = 2 WHERE id_spk = " + id_spk;

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public data_perusahaan getDataPerusahaan()
        {
            data_perusahaan dp = null;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select * from data_perusahaan ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    dp = new data_perusahaan();
                    dp.nama_perusahaan = reader.GetString(0);
                    dp.alamat = reader.GetString(1);
                    dp.kota = reader.GetString(2);
                    dp.telp = reader.GetString(3);
                    dp.fax = reader.GetString(4);
                    dp.norek = reader.GetString(5);
                    dp.norek_nama = reader.GetString(6);
                    dp.norek_bank = reader.GetString(7);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return dp;
        }

    }

    public class t_spk
    {
        public string input_time { set; get; }
        public int no_spk { set; get; }
        public string start_time { set; get; }
        public string finish_time { set; get; }
        public int status { set; get; }
        public int mesin { set; get; }
    }

    public class t_po
    {
        public int id_po { set; get; }
        public int user_marketing { get; set; }
        public int id_customer { get; set; }
        public DateTime tanggal { get; set; }
        public string status { get; set; }
    }

    public class stock
    {
        public string nama_barang { get; set; }
        public double panjang { get; set; }
        public double lebar { get; set; }
        public double tinggi { get; set; }
        public double diameter { get; set; }
        public string bentuk_barang { get; set; }
        public int qty { get; set; }
        public double harga { get; set; }
    }

    public class supplier
    {
        public string nama_supplier { get; set; }
        public string alamat_supplier { get; set; }
        public string input_by { get; set; }
        public DateTime input_date { get; set; }
        public int telp { get; set; }

    }

    public class type
    {
        public int id_type { get; set; }
        public string tipe { get; set; }

        public override string ToString()
        {
            return tipe;
        }

    }

    public class ukuran
    {
        public int id_ukuran { get; set; }
        public string nama_ukuran { get; set; }
        public int max_qty { get; set; }
        public int panjang { get; set; }
        public int lebar { get; set; }
        public int tinggi { get; set; }

        public string id_stock { get; set; }

        public override string ToString()
        {
            return nama_ukuran;
        }
    }

    public class detailUkuran
    {
        public string tipe { get; set; }
        public string panjang { get; set; }
        public string lebar { get; set; }
        public string tinggi { get; set; }
        public int qty { get; set; }
    }

    public class data_perusahaan
    {
        public string nama_perusahaan { get; set; }
        public string alamat { get; set; }
        public string kota { get; set; }
        public string telp { get; set; }
        public string fax { get; set; }
        public string norek { get; set; }
        public string norek_nama { get; set; }
        public string norek_bank { get; set; }
    }
}
