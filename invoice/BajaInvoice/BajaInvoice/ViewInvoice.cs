﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BajaInvoice
{
    public partial class ViewInvoice : Form
    {
        koneksi konek;

        public bool isFirstTime = true;
        public string id_invoice = "";

        public string q_invoice;
        public string q_po;
        public string q_customer;

        private int mintTotalRecords = 0;
        private int mintPageSize = 0;
        private int mintPageCount = 0;
        private int mintCurrentPage = 1;

        public ViewInvoice()
        {
            InitializeComponent();
            this.konek = new koneksi();

            fillGrid();
        }

        public void fillGrid()
        {
            this.mintPageSize = 50;
            this.mintTotalRecords = konek.getCountInvoices();
            this.mintPageCount = this.mintTotalRecords / this.mintPageSize;

            if (this.mintTotalRecords % this.mintPageSize > 0)
                this.mintPageCount++;

            this.mintCurrentPage = 0;

            loadPage();
        }

        public void loadPage()
        {
            this.q_invoice = qInvoice.Text.Trim();
            this.q_po = qPo.Text.Trim();
            this.q_customer = qCustomer.Text.Trim();

            konek.viewInvoice(dgvInvoice, this.mintCurrentPage, this.mintPageSize, this.q_invoice, this.q_po, q_customer);

            this.lblStatus.Text = (this.mintCurrentPage + 1).ToString() + " / " + this.mintPageCount.ToString();
        }

        private void goFirst()
        {
            this.mintCurrentPage = 0;

            loadPage();
        }

        private void goPrevious()
        {
            if (this.mintCurrentPage == this.mintPageSize)
                this.mintCurrentPage = this.mintPageCount - 1;

            this.mintCurrentPage--;

            if (this.mintCurrentPage < 0)
                this.mintCurrentPage = 0;

            loadPage();
        }

        private void goNext()
        {
            this.mintCurrentPage++;

            if (this.mintCurrentPage > (this.mintPageCount - 1))
                this.mintCurrentPage = this.mintPageCount - 1;

            loadPage();
        }

        private void goLast()
        {
            this.mintCurrentPage = this.mintPageCount - 1;

            loadPage();
        }

        private void dgvInvoice_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;
            for (int i = 0; i < dgvInvoice.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvInvoice[i, e.RowIndex].Style.BackColor = Color.Yellow;
            }

        }

        private void dgvInvoice_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (isFirstTime)
                return;
            for (int i = 0; i < dgvInvoice.Rows[e.RowIndex].Cells.Count; i++)
            {
                dgvInvoice[i, e.RowIndex].Style.BackColor = Color.Empty;
            }
        }
        
        private void dgvInvoice_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1)
                return;
            DataGridViewRow row = dgvInvoice.Rows[rowIndex];
            id_invoice = row.Cells["id_invoice"].Value.ToString();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(id_invoice))
                return;

            InvoiceReportNew report = new InvoiceReportNew();
            data_perusahaan dp = konek.getDataPerusahaan();

            report.Parameters["parameterIdInvoice"].Value = id_invoice;
            report.Parameters["paramNamaPerusahaan"].Value = dp.nama_perusahaan;
            report.Parameters["paramAlamat"].Value = dp.alamat;
            report.Parameters["paramKota"].Value = dp.kota;
            report.Parameters["paramTelp"].Value = "Telp : " + dp.telp + " | Fax : " + dp.fax;
            report.Parameters["paramFax"].Value = dp.fax;
            report.Parameters["paramNorek"].Value = dp.norek;
            report.Parameters["paramNorekNama"].Value = dp.norek_nama;
            report.Parameters["paramNorekBank"].Value = dp.norek_bank;

            report.Parameters["parameterIdInvoice"].Visible = false;
            report.Parameters["paramNamaPerusahaan"].Visible = false;
            report.Parameters["paramAlamat"].Visible = false;
            report.Parameters["paramKota"].Visible = false;
            report.Parameters["paramTelp"].Visible = false;
            report.Parameters["paramFax"].Visible = false;
            report.Parameters["paramNorek"].Visible = false;
            report.Parameters["paramNorekNama"].Visible = false;
            report.Parameters["paramNorekBank"].Visible = false;

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(id_invoice))
                return;

            InvoiceReportNew report = new InvoiceReportNew();
            data_perusahaan dp = konek.getDataPerusahaan();

            report.Parameters["parameterIdInvoice"].Value = id_invoice;
            report.Parameters["paramNamaPerusahaan"].Value = dp.nama_perusahaan;
            report.Parameters["paramAlamat"].Value = dp.alamat;
            report.Parameters["paramKota"].Value = dp.kota;
            report.Parameters["paramTelp"].Value = "Telp : " + dp.telp + " | Fax : " + dp.fax;
            report.Parameters["paramFax"].Value = dp.fax;
            report.Parameters["paramNorek"].Value = dp.norek;
            report.Parameters["paramNorekNama"].Value = dp.norek_nama;
            report.Parameters["paramNorekBank"].Value = dp.norek_bank;

            report.Parameters["parameterIdInvoice"].Visible = false;
            report.Parameters["paramNamaPerusahaan"].Visible = false;
            report.Parameters["paramAlamat"].Visible = false;
            report.Parameters["paramKota"].Visible = false;
            report.Parameters["paramTelp"].Visible = false;
            report.Parameters["paramFax"].Visible = false;
            report.Parameters["paramNorek"].Visible = false;
            report.Parameters["paramNorekNama"].Visible = false;
            report.Parameters["paramNorekBank"].Visible = false;

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            goNext();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            goLast();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            goPrevious();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            goFirst();
        }

        private void dgvInvoice_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            dgvInvoice.Columns["no"].HeaderText = "No.";
            dgvInvoice.Columns["id_invoice"].HeaderText = "ID Invoice";
            dgvInvoice.Columns["tgl_invoice"].HeaderText = "Tanggal Invoice";
            dgvInvoice.Columns["no_invoice"].HeaderText = "No. Invoice";
            dgvInvoice.Columns["nama_customer"].HeaderText = "Nama Customer";
            dgvInvoice.Columns["alamat_customer"].HeaderText = "Alamat Customer";
            dgvInvoice.Columns["list_sp"].HeaderText = "No. Order";
            dgvInvoice.Columns["jumlah"].HeaderText = "Sub Total";
            dgvInvoice.Columns["ppn"].HeaderText = "PPN";
            dgvInvoice.Columns["grand_total"].HeaderText = "Total";
            dgvInvoice.Columns["jatuh_tempo"].HeaderText = "Kredit"; 

            dgvInvoice.Columns["id_invoice"].Visible = false;

            dgvInvoice.Columns["no"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvInvoice.Columns["tgl_invoice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvInvoice.Columns["no_invoice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvInvoice.Columns["jatuh_tempo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvInvoice.Columns["list_sp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvInvoice.Columns["jumlah"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInvoice.Columns["ppn"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInvoice.Columns["grand_total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dgvInvoice.Columns["no"].Width = 30;
            dgvInvoice.Columns["tgl_invoice"].Width = 70;
            dgvInvoice.Columns["no_invoice"].Width = 150;
            dgvInvoice.Columns["jatuh_tempo"].Width = 50;
            dgvInvoice.Columns["list_sp"].Width = 70;
            dgvInvoice.Columns["nama_customer"].Width = 220;
            dgvInvoice.Columns["alamat_customer"].Width = 400;
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            loadPage();
        }
    }
}
