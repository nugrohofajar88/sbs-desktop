﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaInvoice
{
    public partial class PiutangEdit : Form
    {
        koneksi konek;

        private DateTimePicker cellDateTimePicker;
        private ComboBox cellComboBox;
        private string id_creator;

        public PiutangEdit(string id_creator, int id_inovice)
        {
            InitializeComponent();
            konek = new koneksi();

            initializeData(id_inovice);
            this.id_creator = id_creator;
            this.cellDateTimePicker = new DateTimePicker();
            this.cellDateTimePicker.Format = DateTimePickerFormat.Custom;
            this.cellDateTimePicker.CustomFormat = "dd-MM-yyyy";
            this.cellDateTimePicker.ValueChanged += new EventHandler(cellDateTimePickerValueChanged);
            this.cellDateTimePicker.Visible = false;
            this.dgv_pembayaran.Controls.Add(cellDateTimePicker);

            var option = new List<JenisBayar>();
            option.Add(new JenisBayar{ Name="BANK", Value="BANK"});
            option.Add(new JenisBayar { Name = "CEK", Value = "CEK" });
            option.Add(new JenisBayar { Name = "BG", Value = "BG" });
            option.Add(new JenisBayar { Name = "CASH", Value = "CASH" });
            this.cellComboBox = new ComboBox();
            this.cellComboBox.DataSource = option;
            this.cellComboBox.DisplayMember = "Name";
            this.cellComboBox.ValueMember = "Value";
            this.cellComboBox.SelectedValueChanged += new EventHandler(cellComboBoxValueChanged);
            this.cellComboBox.Visible = false;
            this.dgv_pembayaran.Controls.Add(cellComboBox);

        }

        private void initializeData(int id_invoice)
        {
            this.dgv_pembayaran.Columns.Add("tgl_tagih", "Tgl. Tagih");
            this.dgv_pembayaran.Columns.Add("jenis_bayar", "Jenis Bayar");
            this.dgv_pembayaran.Columns.Add("no_bg", "No. BG");
            this.dgv_pembayaran.Columns.Add("tgl_bg", "NO Tgl. BG");
            this.dgv_pembayaran.Columns.Add("nominal_bayar", "Nominal");

            DataTable dt = konek.detailPiutang(id_invoice);
            foreach (DataRow r in dt.Rows)
            {
                this.id_inv.Text = id_invoice.ToString();
                this.id_piutang.Text = r["id_piutang"].ToString();
                if (!r["tgl_invoice"].ToString().Trim().Equals(""))
                {
                    string dtp = (r["tgl_invoice"].ToString().Trim().Split(' '))[0];
                    string[] dtp_arr = dtp.Split('/');
                    int day = Int32.Parse(dtp_arr[1].TrimStart('0'));
                    int month = Int32.Parse(dtp_arr[0].TrimStart('0'));
                    int year = Int32.Parse(dtp_arr[2]);
                    
                    this.dt_tgl_invoice.Value = new DateTime(year, month, day);
                }
                                
                this.tb_no_invoice.Text = r["no_invoice"].ToString();
                this.tb_nama_cus.Text = r["nama_customer"].ToString();
                this.tb_tot_piutang.Text = string.Format("{0:N0}", r["total"]).ToString();
                this.tb_faktur_pajak.Text = r["faktur_pajak"].ToString();
                this.tb_tanda_terima.Text = r["tanda_terima"].ToString();
                if (!r["estimasi_jt"].ToString().Trim().Equals(""))
                {
                    string dtp = (r["estimasi_jt"].ToString().Trim().Split(' '))[0];
                    string[] dtp_arr = dtp.Split('/');
                    int day = Int32.Parse(dtp_arr[1].TrimStart('0'));
                    int month = Int32.Parse(dtp_arr[0].TrimStart('0'));
                    int year = Int32.Parse(dtp_arr[2]);
                    
                    this.dt_jt.Value = new DateTime(year, month, day);
                }
                this.tb_keterangan.Text = r["keterangan"].ToString();
                
                /*
                 * Kolom detil pembayaran
                 * 1.Tgl.tagih -> tgl_tagih
                 * 2.Jenis bayar -> pembayaran
                 * 3.No. BG -> no_bg
                 * 4.Tgl. BG -> tgl_bg
                 * 5.Nominal -> nominal_bayar
                 */

                this.dgv_pembayaran.Rows.Add(r["tgl_tagih"].ToString(), r["pembayaran"].ToString(), r["no_bg"].ToString(),
                    r["tgl_bg"].ToString(), string.Format("{0:N0}", r["nominal_bayar"]));

            }
        }

        void cellDateTimePickerValueChanged(object sender, EventArgs e)
        {
            string temp = cellDateTimePicker.Value.Date.ToShortDateString();
            string[] temp_arr = temp.Split('/');
            dgv_pembayaran.CurrentCell.Value = temp_arr[1].PadLeft(2, '0') + "-" + temp_arr[0].PadLeft(2, '0') + "-" + temp_arr[2];
            cellDateTimePicker.Visible = false;
        }

        void cellComboBoxValueChanged(object sender, EventArgs e)
        {
            dgv_pembayaran.CurrentCell.Value = cellComboBox.SelectedValue;
            cellComboBox.Visible = false;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            bool error = false;

            string id_invoice = this.id_inv.Text;
            string id_piutang = this.id_piutang.Text;
            string tgl_invoice = this.dt_tgl_invoice.Text;
            string[] temp_arr = tgl_invoice.Split('-');
            tgl_invoice = temp_arr[2].PadLeft(2, '0') + "-" + temp_arr[1].PadLeft(2, '0') + "-" + temp_arr[0];
            
            string no_invoice = this.tb_no_invoice.Text;
            string nama_customer = this.tb_nama_cus.Text;
            string total = this.tb_tot_piutang.Text;

            string faktur_pajak = this.tb_faktur_pajak.Text;
            string tanda_terima = this.tb_tanda_terima.Text;
            string estimasi_jt = this.dt_jt.Text;
            string[] temp_arr2 = estimasi_jt.Split('-');
            estimasi_jt = temp_arr[2].PadLeft(2, '0') + "-" + temp_arr[1].PadLeft(2, '0') + "-" + temp_arr[0];
            string keterangan = this.tb_keterangan.Text;
            
            if (!error)
            {
                //add piutang
                if (id_piutang.Equals(""))
                {
                    int new_id = konek.addPiutang(this.id_creator, id_invoice, faktur_pajak, tanda_terima, estimasi_jt, keterangan);
                    id_piutang = new_id.ToString();
                }
                //update piutang
                else
                {
                    int result = konek.updatePiutang(this.id_creator,id_piutang, faktur_pajak, tanda_terima, estimasi_jt, keterangan);
                }

                if (dgv_pembayaran.Rows.Count > 1)
                {
                    if (!id_piutang.Equals(""))
                        konek.deletePiutangBayar(id_piutang);

                    dgv_pembayaran.BeginEdit(false);

                    for (int i = 0; i < dgv_pembayaran.Rows.Count - 1; i++)
                    {
                        DataGridViewRow row = dgv_pembayaran.Rows[i];
                        string tgl_tagih = (row.Cells["tgl_tagih"].Value == null) ? "" : row.Cells["tgl_tagih"].Value.ToString();
                        string jenis_bayar = row.Cells["jenis_bayar"].Value.ToString();
                        string no_bg = (row.Cells["no_bg"].Value == null) ? "" : row.Cells["no_bg"].Value.ToString();
                        string tgl_bg = (row.Cells["tgl_bg"].Value == null) ? "" : row.Cells["tgl_bg"].Value.ToString();
                        string nominal_bayar = row.Cells["nominal_bayar"].Value.ToString();

                        if (!tgl_tagih.Trim().Equals(""))
                        {
                            string[] arr = tgl_tagih.Split('-');
                            tgl_tagih = arr[2] + '-' + arr[1].PadLeft(2, '0') + '-' + arr[0].PadLeft(2, '0');
                        }

                        if (!tgl_bg.Trim().Equals(""))
                        {
                            string[] arr = tgl_bg.Split('-');
                            tgl_bg = arr[2] + '-' + arr[1].PadLeft(2, '0') + '-' + arr[0].PadLeft(2, '0');
                        }

                        if (!jenis_bayar.Trim().Equals("") && !nominal_bayar.Trim().Equals(""))
                        {
                            konek.addPiutangBayar(this.id_creator, id_piutang, tgl_tagih, jenis_bayar, no_bg, tgl_bg, nominal_bayar.Replace(",", ""));
                        }
                    }

                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Periksa kembali data.");
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_pembayaran_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dgv_pembayaran.Columns[e.ColumnIndex].Name == "tgl_tagih" ||
                this.dgv_pembayaran.Columns[e.ColumnIndex].Name == "tgl_bg" ||
                this.dgv_pembayaran.Columns[e.ColumnIndex].Name == "jenis_bayar")
            {
                this.dgv_pembayaran.Columns[e.ColumnIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            else if (this.dgv_pembayaran.Columns[e.ColumnIndex].Name == "nominal_bayar")
            {
                this.dgv_pembayaran.Columns[e.ColumnIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        }
        
        private void dgv_pembayaran_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 || e.ColumnIndex == 3)
            {
                if (dgv_pembayaran.CurrentCell.Value != null && !dgv_pembayaran.CurrentCell.Value.ToString().Trim().Equals(""))
                {
                    string dt = (dgv_pembayaran.CurrentCell.Value.ToString().Trim().Split(' '))[0];
                    string[] dt_arr = dt.Split('-');
                    int day = Int32.Parse(dt_arr[0].TrimStart('0'));
                    int month = Int32.Parse(dt_arr[1].TrimStart('0'));
                    int year = Int32.Parse(dt_arr[2]);

                    cellDateTimePicker.Value = new DateTime(year, month, day);

                }

                Rectangle tempRect = dgv_pembayaran.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);
                cellDateTimePicker.Location = tempRect.Location;
                cellDateTimePicker.Width = tempRect.Width;
                cellDateTimePicker.Visible = true;
            }
            else if (e.ColumnIndex == 1)
            {
                if (dgv_pembayaran.CurrentCell.Value != null && !dgv_pembayaran.CurrentCell.Value.ToString().Trim().Equals(""))
                {
                    cellComboBox.Text = dgv_pembayaran.CurrentCell.Value.ToString().ToUpper();
                }

                Rectangle tempRect = dgv_pembayaran.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);
                cellComboBox.Location = tempRect.Location;
                cellComboBox.Width = tempRect.Width;
                cellComboBox.Visible = true;
            }
            else
            {
                if (e.ColumnIndex == 4 && dgv_pembayaran.CurrentCell.Value != null && !dgv_pembayaran.CurrentCell.Value.ToString().Trim().Equals(""))
                {
                    dgv_pembayaran.CurrentCell.Value = dgv_pembayaran.CurrentCell.Value.ToString().Replace(",", "");
                }
                dgv_pembayaran.BeginEdit(true);
            }
        }

        private void dgv_pembayaran_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                foreach (DataGridViewRow row in dgv_pembayaran.Rows)
                {
                    if (row.Cells[4].Value != null && !row.Cells[4].Value.ToString().Equals(""))
                    {
                        string temp = row.Cells[4].Value.ToString();

                        row.Cells[4].Value = string.Format("{0:N0}", Convert.ToDecimal(temp));
                    }
                }
            }
        }
    }

    public class JenisBayar
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
