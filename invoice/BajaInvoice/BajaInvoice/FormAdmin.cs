﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;

namespace BajaInvoice
{
    public partial class FormAdmin : Form
    {
        koneksi konek;

        // simpan dta
        public string admin;

        //close form
        public FormLogin _formlogin;
        public ViewInvoice _viewInvoice;
        public ViewPiutang _viewPiutang;
        
        public string id_creator;
                       
        public FormAdmin(String username)
        {
            InitializeComponent();
            konek = new koneksi();


            this.admin = username;
            id_creator = konek.getidAdmin2(username).ToString();
        }

        private void FormAdmin_Load_1(object sender, EventArgs e)
        {
            stUserId.Text = admin;
            tsTanggal.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm");

            DocumentManager manager = new DocumentManager();
            manager.MdiParent = this;
            manager.View.Appearance.BackColor = Color.Gray;
            manager.View = new TabbedView();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            _viewInvoice = new ViewInvoice();
            konek.viewInvoice(_viewInvoice.dgvInvoice);
            _viewInvoice.Show();
            _viewInvoice.isFirstTime = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void piutangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _viewPiutang = new ViewPiutang(id_creator);
            List<int> months = konek.viewPiutang(_viewPiutang.dgv);

            _viewPiutang.Show();
            _viewPiutang.isFirstTime = false;
        }

        private void daftarInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType() == typeof(ViewInvoice))
                {
                    form.Activate();
                    return;
                }
            }

            _viewInvoice = new ViewInvoice();
            _viewInvoice.MdiParent = this;
            _viewInvoice.isFirstTime = true;

            _viewInvoice.Show();            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Diagnostics.Process.Start(Application.ExecutablePath);
        }

        private void FormAdmin_Shown(object sender, EventArgs e)
        {
            FormOrder fo = new FormOrder(id_creator);
            fo.MdiParent = this;
            fo.Show();
        }
    }
}
