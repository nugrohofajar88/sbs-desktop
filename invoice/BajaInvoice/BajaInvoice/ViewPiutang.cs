﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaInvoice
{
    public partial class ViewPiutang : Form
    {
        koneksi konek;

        public bool isFirstTime = true;
        public string id_invoice = "";
        public string id_creator = "";

        public ViewPiutang(string id_creator)
        {
            InitializeComponent();
            konek = new koneksi();
            this.id_creator = id_creator;

            this.dgv.Columns.Add("id_invoice", "ID INV");
            this.dgv.Columns.Add("bulan", "Periode");
            this.dgv.Columns.Add("tgl_inv","Tgl. Inv");
            this.dgv.Columns.Add("no_inv", "No. Invoice");            
            this.dgv.Columns.Add("nama_customer", "Nama Customer");
            this.dgv.Columns.Add("jumlah", "Jumlah");
            this.dgv.Columns.Add("ppn", "PPN");
            this.dgv.Columns.Add("grand_total", "Total");
            this.dgv.Columns.Add("tanda_terima", "Tanda Terima");
            this.dgv.Columns.Add("pembayaran", "Pembayaran");
            this.dgv.Columns.Add("nominal", "Nominal");
            this.dgv.Columns.Add("status", "Status");
            this.dgv.Columns.Add("keterangan", "Keterangan");

            this.dgv.Columns["bulan"].Width = 75;
            this.dgv.Columns["tgl_inv"].Width = 50;
            this.dgv.Columns["no_inv"].Width = 100;
            this.dgv.Columns["nama_customer"].Width = 200;
            this.dgv.Columns["jumlah"].Width = 100;
            this.dgv.Columns["ppn"].Width = 100;
            this.dgv.Columns["grand_total"].Width = 100;
            this.dgv.Columns["tanda_terima"].Width = 100;
            this.dgv.Columns["pembayaran"].Width = 100;
            this.dgv.Columns["nominal"].Width = 100;
            this.dgv.Columns["status"].Width = 50;
            this.dgv.Columns["keterangan"].Width = 200;

            this.dgv.Columns["jumlah"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["ppn"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["grand_total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["nominal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            for (int i = 0; i < this.dgv.ColumnCount; i++) {
                this.dgv.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                if (i == 9 || i == 10) {
                    this.dgv.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
                }

            }

            this.dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            //this.dgv.ColumnHeadersHeight = this.dgv.ColumnHeadersHeight * 2;
            
            //this.dgv.CellPainting += new DataGridViewCellPaintingEventHandler(dgv_CellPainting);
            //this.dgv.Paint += new PaintEventHandler(dgv_Paint);
        }

        void dgv_Paint(object sender, PaintEventArgs e)
        {
            string[] monthes = { "PEMBAYARAN"};
            for (int i = 9; i < 11;)
            {
                Rectangle r1 = this.dgv.GetCellDisplayRectangle(i, -1, true); //get the columnb header cell
                r1.X += 1;
                r1.Y += 1;
                r1.Width = r1.Width * 2 - 2;
                r1.Height = r1.Height / 2 - 2;
                e.Graphics.FillRectangle(new SolidBrush(this.dgv.ColumnHeadersDefaultCellStyle.BackColor), r1);
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString(monthes[0], this.dgv.ColumnHeadersDefaultCellStyle.Font, new SolidBrush(this.dgv.ColumnHeadersDefaultCellStyle.ForeColor), r1, format);
                i += 2;
            }
        }

        void dgv_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex > -1)
            {
                e.PaintBackground(e.CellBounds, false);

                Rectangle r2 = e.CellBounds;
                r2.Y += e.CellBounds.Height / 2;
                r2.Height = e.CellBounds.Height / 2;
                e.PaintContent(r2);
                e.Handled = true;
            }
        }

        private void dgv_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dgv.SelectedRows[0];
                int id_invoice = Int32.Parse(row.Cells["id_invoice"].Value.ToString());

                PiutangEdit pe = new PiutangEdit(this.id_creator, id_invoice);
                var result = pe.ShowDialog();

                konek.viewPiutang(dgv);
            }
        }

        private void btExport_Click(object sender, EventArgs e)
        {
            ReportPiutangViewer view = new ReportPiutangViewer();
            view.Show();
        }
    }
}
