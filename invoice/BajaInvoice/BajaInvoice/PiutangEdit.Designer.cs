﻿namespace BajaInvoice
{
    partial class PiutangEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_no_invoice = new System.Windows.Forms.TextBox();
            this.tb_nama_cus = new System.Windows.Forms.TextBox();
            this.tb_tot_piutang = new System.Windows.Forms.TextBox();
            this.tb_tanda_terima = new System.Windows.Forms.TextBox();
            this.tb_keterangan = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_pembayaran = new System.Windows.Forms.DataGridView();
            this.id_inv = new System.Windows.Forms.Label();
            this.id_piutang = new System.Windows.Forms.Label();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.tb_faktur_pajak = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dt_jt = new System.Windows.Forms.DateTimePicker();
            this.dt_tgl_invoice = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_pembayaran)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tgl. Invoice";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "No. Invoice";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nama Customer";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total Piutang";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(288, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tanda Terima";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(288, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Estimasi Jatuh Tempo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(288, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Keterangan";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = ":";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(119, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(118, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(119, 119);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(420, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 15);
            this.label12.TabIndex = 13;
            this.label12.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(421, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 15);
            this.label13.TabIndex = 12;
            this.label13.Text = ":";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(421, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(10, 15);
            this.label14.TabIndex = 11;
            this.label14.Text = ":";
            // 
            // tb_no_invoice
            // 
            this.tb_no_invoice.Location = new System.Drawing.Point(138, 55);
            this.tb_no_invoice.Name = "tb_no_invoice";
            this.tb_no_invoice.Size = new System.Drawing.Size(144, 21);
            this.tb_no_invoice.TabIndex = 15;
            // 
            // tb_nama_cus
            // 
            this.tb_nama_cus.Location = new System.Drawing.Point(138, 87);
            this.tb_nama_cus.Name = "tb_nama_cus";
            this.tb_nama_cus.Size = new System.Drawing.Size(144, 21);
            this.tb_nama_cus.TabIndex = 16;
            // 
            // tb_tot_piutang
            // 
            this.tb_tot_piutang.Location = new System.Drawing.Point(138, 117);
            this.tb_tot_piutang.Name = "tb_tot_piutang";
            this.tb_tot_piutang.Size = new System.Drawing.Size(144, 21);
            this.tb_tot_piutang.TabIndex = 17;
            // 
            // tb_tanda_terima
            // 
            this.tb_tanda_terima.Location = new System.Drawing.Point(440, 55);
            this.tb_tanda_terima.Name = "tb_tanda_terima";
            this.tb_tanda_terima.Size = new System.Drawing.Size(144, 21);
            this.tb_tanda_terima.TabIndex = 18;
            // 
            // tb_keterangan
            // 
            this.tb_keterangan.Location = new System.Drawing.Point(441, 120);
            this.tb_keterangan.Name = "tb_keterangan";
            this.tb_keterangan.Size = new System.Drawing.Size(144, 21);
            this.tb_keterangan.TabIndex = 19;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_pembayaran);
            this.groupBox1.Location = new System.Drawing.Point(18, 159);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(567, 148);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pembayaran : ";
            // 
            // dgv_pembayaran
            // 
            this.dgv_pembayaran.AllowUserToResizeRows = false;
            this.dgv_pembayaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_pembayaran.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_pembayaran.Location = new System.Drawing.Point(3, 17);
            this.dgv_pembayaran.MultiSelect = false;
            this.dgv_pembayaran.Name = "dgv_pembayaran";
            this.dgv_pembayaran.Size = new System.Drawing.Size(561, 128);
            this.dgv_pembayaran.TabIndex = 0;
            this.dgv_pembayaran.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_pembayaran_CellClick);
            this.dgv_pembayaran.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_pembayaran_CellFormatting);
            this.dgv_pembayaran.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_pembayaran_CellLeave);
            // 
            // id_inv
            // 
            this.id_inv.AutoSize = true;
            this.id_inv.Location = new System.Drawing.Point(135, 4);
            this.id_inv.Name = "id_inv";
            this.id_inv.Size = new System.Drawing.Size(39, 15);
            this.id_inv.TabIndex = 22;
            this.id_inv.Text = "id_inv";
            this.id_inv.Visible = false;
            // 
            // id_piutang
            // 
            this.id_piutang.AutoSize = true;
            this.id_piutang.Location = new System.Drawing.Point(180, 4);
            this.id_piutang.Name = "id_piutang";
            this.id_piutang.Size = new System.Drawing.Size(65, 15);
            this.id_piutang.TabIndex = 23;
            this.id_piutang.Text = "id_piutang";
            this.id_piutang.Visible = false;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(429, 322);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 23);
            this.btnSimpan.TabIndex = 24;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.Location = new System.Drawing.Point(510, 322);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(75, 23);
            this.btnBatal.TabIndex = 25;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // tb_faktur_pajak
            // 
            this.tb_faktur_pajak.Location = new System.Drawing.Point(441, 22);
            this.tb_faktur_pajak.Name = "tb_faktur_pajak";
            this.tb_faktur_pajak.Size = new System.Drawing.Size(144, 21);
            this.tb_faktur_pajak.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(422, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(10, 15);
            this.label15.TabIndex = 27;
            this.label15.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(289, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 15);
            this.label16.TabIndex = 26;
            this.label16.Text = "Faktur Pajak";
            // 
            // dt_jt
            // 
            this.dt_jt.CustomFormat = "dd-MM-yyyy";
            this.dt_jt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_jt.Location = new System.Drawing.Point(441, 91);
            this.dt_jt.Name = "dt_jt";
            this.dt_jt.Size = new System.Drawing.Size(146, 21);
            this.dt_jt.TabIndex = 29;
            // 
            // dt_tgl_invoice
            // 
            this.dt_tgl_invoice.CustomFormat = "dd-MM-yyyy";
            this.dt_tgl_invoice.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_tgl_invoice.Location = new System.Drawing.Point(138, 25);
            this.dt_tgl_invoice.Name = "dt_tgl_invoice";
            this.dt_tgl_invoice.Size = new System.Drawing.Size(146, 21);
            this.dt_tgl_invoice.TabIndex = 30;
            // 
            // PiutangEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 366);
            this.Controls.Add(this.dt_tgl_invoice);
            this.Controls.Add(this.dt_jt);
            this.Controls.Add(this.tb_faktur_pajak);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.id_piutang);
            this.Controls.Add(this.id_inv);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tb_keterangan);
            this.Controls.Add(this.tb_tanda_terima);
            this.Controls.Add(this.tb_tot_piutang);
            this.Controls.Add(this.tb_nama_cus);
            this.Controls.Add(this.tb_no_invoice);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "PiutangEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detail Piutang";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_pembayaran)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_no_invoice;
        private System.Windows.Forms.TextBox tb_nama_cus;
        private System.Windows.Forms.TextBox tb_tot_piutang;
        private System.Windows.Forms.TextBox tb_tanda_terima;
        private System.Windows.Forms.TextBox tb_keterangan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label id_inv;
        private System.Windows.Forms.Label id_piutang;
        public System.Windows.Forms.DataGridView dgv_pembayaran;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.TextBox tb_faktur_pajak;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dt_jt;
        private System.Windows.Forms.DateTimePicker dt_tgl_invoice;
    }
}