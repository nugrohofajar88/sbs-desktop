﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaOperator
{
    public partial class detailSpk : UserControl
    {
        public string id_dt_spk = "6";
        public string id_mesin_operation { get; set; }

        private dt_spk dataHeader;
        private koneksi konek;

        public detailSpk(string idDtSpk, string idMesinOperation)
        {
            InitializeComponent();
            konek = new koneksi();
            id_dt_spk = idDtSpk;
            id_mesin_operation = idMesinOperation;
            reloadData();

        }

        private void reloadData()
        {
            //load data header
            dataHeader = konek.getDataDtSpk(id_dt_spk);
            tbType.Text = dataHeader.type_stok_awal;
            tbUkuranAwal.Text = dataHeader.ukuran_awal;
            tbQtyAwal.Text = dataHeader.qty_awal;
            tbKgAwal.Text = dataHeader.kg_awal;
            tbUkuranOrder.Text = dataHeader.ukuran_order;
            tbQtyOrder.Text = dataHeader.qty_order;
            tbKgOrder.Text = dataHeader.kg_order;
            tbKet.Text = dataHeader.keterangan;

            //load langkah potong
            konek.getLangkahPotong(id_dt_spk, lbox,lboxWork);

            //load plan stok akhir
            konek.getPlanStokAkhir(id_dt_spk, lbox2);

            //load first real stok akhir
            WorkStockAkhir newWork = new WorkStockAkhir();
            newWork.id_dt_pembelian = dataHeader.id_dt_pembelian;
            newWork.id_type = dataHeader.id_type;
            newWork.id_mesin_operation = id_mesin_operation;
            newWork.id_dt_spk = id_dt_spk;
            newWork.harga = dataHeader.harga;
            newWork.btnPlus.Click += BtnPlus_Click;
            lboxWork2.Controls.Add(newWork);
        }

        private void BtnPlus_Click(object sender, EventArgs e)
        {
            WorkStockAkhir newWork = new WorkStockAkhir();
            newWork.id_dt_pembelian = dataHeader.id_dt_pembelian;
            newWork.id_type = dataHeader.id_type;
            newWork.id_mesin_operation = id_mesin_operation;
            newWork.id_dt_spk = id_dt_spk;
            newWork.btnPlus.Click += BtnPlus_Click;
            lboxWork2.Controls.Add(newWork);
        }

        public void saveToDB()
        {
            //save work langkah potong
            for(int i=0; i<lboxWork.Controls.Count; i++)
            {
                WorkLangkahPotong item = (WorkLangkahPotong) lboxWork.Controls[i];
                item.saveToDB();
            }

            //save work stock akhir
            for(int i=0; i<lboxWork2.Controls.Count; i++)
            {
                WorkStockAkhir item = (WorkStockAkhir)lboxWork2.Controls[i];
                item.saveToDB();
            }


        }
    }
}
