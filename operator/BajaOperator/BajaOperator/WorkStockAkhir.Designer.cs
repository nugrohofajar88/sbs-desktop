﻿namespace BajaOperator
{
    partial class WorkStockAkhir
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kgA = new System.Windows.Forms.TextBox();
            this.qtyA = new System.Windows.Forms.NumericUpDown();
            this.t = new System.Windows.Forms.TextBox();
            this.l = new System.Windows.Forms.TextBox();
            this.p = new System.Windows.Forms.TextBox();
            this.btnPlus = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.qtyA)).BeginInit();
            this.SuspendLayout();
            // 
            // kgA
            // 
            this.kgA.Location = new System.Drawing.Point(185, 1);
            this.kgA.Multiline = true;
            this.kgA.Name = "kgA";
            this.kgA.Size = new System.Drawing.Size(52, 32);
            this.kgA.TabIndex = 11;
            this.kgA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // qtyA
            // 
            this.qtyA.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyA.Location = new System.Drawing.Point(139, 3);
            this.qtyA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qtyA.Name = "qtyA";
            this.qtyA.Size = new System.Drawing.Size(44, 24);
            this.qtyA.TabIndex = 10;
            this.qtyA.TextChanged += new System.EventHandler(this.qty_ValueChanged);
            this.qtyA.Enter += new System.EventHandler(this.qtyA_Enter);
            // 
            // t
            // 
            this.t.Location = new System.Drawing.Point(93, 1);
            this.t.Multiline = true;
            this.t.Name = "t";
            this.t.Size = new System.Drawing.Size(45, 32);
            this.t.TabIndex = 9;
            this.t.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // l
            // 
            this.l.Location = new System.Drawing.Point(-1, 1);
            this.l.Multiline = true;
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(45, 32);
            this.l.TabIndex = 7;
            this.l.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // p
            // 
            this.p.Location = new System.Drawing.Point(46, 1);
            this.p.Multiline = true;
            this.p.Name = "p";
            this.p.Size = new System.Drawing.Size(45, 32);
            this.p.TabIndex = 8;
            this.p.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlus.Location = new System.Drawing.Point(240, 0);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(30, 30);
            this.btnPlus.TabIndex = 12;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            // 
            // WorkStockAkhir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.kgA);
            this.Controls.Add(this.qtyA);
            this.Controls.Add(this.t);
            this.Controls.Add(this.l);
            this.Controls.Add(this.p);
            this.Controls.Add(this.btnPlus);
            this.Name = "WorkStockAkhir";
            this.Size = new System.Drawing.Size(271, 32);
            ((System.ComponentModel.ISupportInitialize)(this.qtyA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox kgA;
        public System.Windows.Forms.NumericUpDown qtyA;
        public System.Windows.Forms.TextBox t;
        public System.Windows.Forms.TextBox l;
        public System.Windows.Forms.TextBox p;
        public System.Windows.Forms.Button btnPlus;
    }
}
