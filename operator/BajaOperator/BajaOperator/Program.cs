﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaOperator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new FormOperator());

            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("en-US");

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;


            Random rand = new Random();
            Console.Write("test     {0}     ", rand.NextDouble());

            FormLogin _formLogin = new FormLogin();

            if (_formLogin.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new FormOperator(_formLogin.username, _formLogin.iduser));
                
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
