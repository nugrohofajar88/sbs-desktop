﻿namespace BajaOperator
{
    partial class WorkLangkahPotong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qtyO = new System.Windows.Forms.NumericUpDown();
            this.to = new System.Windows.Forms.TextBox();
            this.lo = new System.Windows.Forms.TextBox();
            this.po = new System.Windows.Forms.TextBox();
            this.kgO = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).BeginInit();
            this.SuspendLayout();
            // 
            // qtyO
            // 
            this.qtyO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyO.Location = new System.Drawing.Point(140, 6);
            this.qtyO.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.qtyO.Name = "qtyO";
            this.qtyO.Size = new System.Drawing.Size(43, 24);
            this.qtyO.TabIndex = 10;
            this.qtyO.TextChanged += new System.EventHandler(this.qty_ValueChanged);
            // 
            // to
            // 
            this.to.Location = new System.Drawing.Point(95, 3);
            this.to.Multiline = true;
            this.to.Name = "to";
            this.to.ReadOnly = true;
            this.to.Size = new System.Drawing.Size(43, 32);
            this.to.TabIndex = 9;
            this.to.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lo
            // 
            this.lo.Location = new System.Drawing.Point(3, 3);
            this.lo.Multiline = true;
            this.lo.Name = "lo";
            this.lo.ReadOnly = true;
            this.lo.Size = new System.Drawing.Size(43, 32);
            this.lo.TabIndex = 7;
            this.lo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // po
            // 
            this.po.Location = new System.Drawing.Point(49, 3);
            this.po.Multiline = true;
            this.po.Name = "po";
            this.po.ReadOnly = true;
            this.po.Size = new System.Drawing.Size(43, 32);
            this.po.TabIndex = 8;
            this.po.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // kgO
            // 
            this.kgO.Location = new System.Drawing.Point(185, 2);
            this.kgO.Multiline = true;
            this.kgO.Name = "kgO";
            this.kgO.ReadOnly = true;
            this.kgO.Size = new System.Drawing.Size(43, 32);
            this.kgO.TabIndex = 12;
            this.kgO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // WorkLangkahPotong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.kgO);
            this.Controls.Add(this.qtyO);
            this.Controls.Add(this.to);
            this.Controls.Add(this.lo);
            this.Controls.Add(this.po);
            this.Name = "WorkLangkahPotong";
            this.Size = new System.Drawing.Size(230, 38);
            ((System.ComponentModel.ISupportInitialize)(this.qtyO)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.NumericUpDown qtyO;
        public System.Windows.Forms.TextBox to;
        public System.Windows.Forms.TextBox lo;
        public System.Windows.Forms.TextBox po;
        public System.Windows.Forms.TextBox kgO;
    }
}
