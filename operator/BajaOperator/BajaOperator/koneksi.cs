﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;

namespace BajaOperator
{
    public class koneksi
    {
        public MySqlConnection db;
        public string connetionString = null;
        public string jabatanLogin = "";
        SettingManager _settingManager = new SettingManager("settings.xml");
        public string mesinId = "";

        public koneksi()
        {
            string server = _settingManager.GetValue("server");
            string database = _settingManager.GetValue("database");
            string username = _settingManager.GetValue("username");
            string password = _settingManager.GetValue("password");
            mesinId = _settingManager.GetValue("mesin");
            jabatanLogin = _settingManager.GetValue("jabatan").ToUpper();
            connetionString = "server=" + server + ";database=" + database + ";uid=" + username + ";pwd=" + password + ";";
                db = new MySqlConnection(connetionString);

         }
        
        public Boolean LoginOperator(string user, string password)
        {
            user = user.ToUpper();
            password = password.ToUpper();

            try
            {
                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select username,password, nama_jabatan as status from m_user, Jabatan where m_user.id_jabatan = Jabatan.id_jabatan";
                Console.WriteLine("sql login = "+sql);
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if ((reader.GetString(0).ToString().ToUpper() == user) && (reader.GetString(1).ToString().ToUpper() == password) && (jabatanLogin.Contains(reader.GetString(2).ToUpper())))
                    {
                        return true;
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return false;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return false;

        }
        
        public string namaOperator(TextBox tbUser)
        {
            string user = tbUser.Text;
            string namasalah = "xxxxxxxx";
            string nama = user;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select nama_dpn, id_user as status from m_user where username='" + user + "'";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if(!reader.IsDBNull(0))
                        nama = reader.GetString(0).ToString().ToUpper();
                    tbUser.Tag = reader.GetInt32(1);
                    return nama;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return namasalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return nama;
        }

        public void  openkoneksi()
        {           
            db.Open();
        }

        public void closekoneksi()
        {
            db.Close();
        }

        public void display_report(DataGridView dataGridView1)
        {
            try
            {
                openkoneksi();
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //Display query  
                string Query = "select tanggal,no_spk,nama_dpn,start_time,finish_time,timestampdiff(hour,start_time,finish_time),status_mesin,nama_mesin from t_spk,m_mesin where t_spk.id_mesin=m_mesin.id_mesin;";
                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                //  MyConn2.Open();  
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                dataGridView1.DataSource = dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                                                   // MyConn2.Close();  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void update()
        {
            try
            {
                //This is my connection string i have assigned the database file address path  
                //string MyConnection2 = "datasource=localhost;port=3307;username=root;password=root";
                //This is my update query in which i am taking input from the user through windows forms and update the record.  
                //string Query = "update student.studentinfo set idStudentInfo='" + this.IdTextBox.Text + "',Name='" + this.NameTextBox.Text + "',Father_Name='" + this.FnameTextBox.Text + "',Age='" + this.AgeTextBox.Text + "',Semester='" + this.SemesterTextBox.Text + "' where idStudentInfo='" + this.IdTextBox.Text + "';";
                string Query = "";
                //This is  MySqlConnection here i have created the object and pass my connection string.  


                //MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                //MyConn2.Open();
                MyReader2 = MyCommand2.ExecuteReader();
                MessageBox.Show("Data Updated");
                while (MyReader2.Read())
                {
                }
                db.Close();//Connection closed here  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void viewJob(List<string> listIdDT, Hashtable hashTab, DataGridView dataGridView1, string id_spk)
        {
            //tgl.spk.oprt.start.finish            
            try
            {
                openkoneksi();
                string sql = "SELECT st_awal.type, st_awal.nama_ukuran as ukuran_awal, dt.qty_awal as quota_awal, " +
                             "    st_order.nama_ukuran as ukuran_order, dt.qty_order as quota_order, " +
                             "    st_akhir.nama_ukuran as ukuran_akhir, dt.qty_akhir as quota_akhir, st_akhir.id_stock, dt.id_dt_spk " +
                             "FROM " +
                             "  `dt_spk` dt, " +
                             "  ( " +
                             "    SELECT stock.id_stock, type.type, ukuran.nama_ukuran " +
                             "    FROM `m_stock` stock, " +
                             "         `m_type`  type, " +
                             "         `m_ukuran` ukuran " +
                             "    WHERE stock.id_type = type.id_type " +
                             "         and stock.id_ukuran = ukuran.id_ukuran " +
                             "  ) st_awal, " +
                             "  ( " +
                             "    SELECT stock.id_stock, type.type, ukuran.nama_ukuran " +
                             "    FROM `m_stock` stock, " +
                             "         `m_type`  type, " +
                             "         `m_ukuran` ukuran " +
                             "    WHERE stock.id_type = type.id_type " +
                             "         and stock.id_ukuran = ukuran.id_ukuran " +
                             "  ) st_order, " +
                             "  ( " +
                             "    SELECT stock.id_stock, type.type, ukuran.nama_ukuran " +
                             "    FROM `m_stock` stock, " +
                             "         `m_type`  type, " +
                             "         `m_ukuran` ukuran " +
                             "    WHERE stock.id_type = type.id_type " +
                             "         and stock.id_ukuran = ukuran.id_ukuran " +
                             "  ) st_akhir " +
                             "WHERE dt.id_spk = "+id_spk+" " +
                             "      and dt.id_mesin = " + mesinId + " " +
                             "      and dt.status_pengerjaan = 0 " +     
                             "      and dt.stock_awal = st_awal.id_stock " +
                             "      and dt.stock_order = st_order.id_stock " +
                             "      and dt.stock_akhir = st_akhir.id_stock ";

                //Console.WriteLine("Query Panjang = " + sql);

                MySqlCommand MyCommand2 = new MySqlCommand(sql, db);
                //For offline connection we weill use  MySqlDataAdapter class.  
                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);

                //filling hash table :
                foreach (DataRow row in dTable.Rows)
                {
                    hashTab.Add(row[7], row[6]);
                    listIdDT.Add(row[8].ToString());
                }

                Console.WriteLine(dTable);
                dataGridView1.DataSource = dTable;
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[8].Visible = false;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

        }

        public int countUnfinish(string idspk)
        {
            int hasil = 0;
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select count(*) from dt_spk where id_spk='" + idspk + "' and status_pengerjaan < 1";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    hasil = reader.GetInt32(0);
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public void updateStart(string time,string spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_spk set start_time= (" +
                        " select DATE_FORMAT(min(start),'%Y-%m-%d %H:%i') " +
                        " from t_mesinoperation " +
                        " where id_spk = " + spk + " " +
                        " ) where id_spk='" + spk + "';";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void updateMesinOperationDtSPK(string joinedID, string idMesinOperation)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update dt_spk set id_mesin_operation=" + idMesinOperation + " where id_dt_spk in (" + joinedID + ");";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void updateStatusPengerjaanDtSPK(string joinedID)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update dt_spk set status_pengerjaan=1 where id_dt_spk in (" + joinedID + ");";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void updatePrequisiteDtSPK(string joinedID)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update dt_spk set status_pengerjaan=0 where PREQUESITE_DT_SPK in (" + joinedID + ");";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void updateStop(string time, string spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_spk set finish_time='" + time + "', status_spk = 2 where id_spk='" + spk + "';";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void updateToFinished (string spk)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_spk set status_spk = 2 where id_spk='" + spk + "';";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //perlu cek jika digunakan prequisite, maka tidak ditambah ke stok
        public void updateStockAkhir(Hashtable hashTab)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "UPDATE `m_stock` " +
                               "SET qty_stock = ( " +
                               "                case ";
                foreach (DictionaryEntry entry in hashTab)
                {
                    Query = Query + string.Format("when id_stock = {0} then qty_stock + {1} ", entry.Key.ToString(), entry.Value.ToString());
                    //Console.WriteLine("{0}, {1}", entry.Key, entry.Value);
                }

                Query = Query + "                end) " +
                                "WHERE id_stock in ( ";

                int i = 0;
                foreach (Object key in hashTab.Keys)
                {
                    if (i == hashTab.Count - 1)
                        Query = Query + key.ToString();
                    else
                        Query = Query + key.ToString() + ", ";
                    i++;
                }
                Query = Query + ") ";

                //Console.WriteLine("Query update stAkhir = "+Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string insertOperation(string id_spk, string id_user, string timeStart)
        {
            string hasil = "NULL";
            try
            {
                DateTime now = DateTime.Now;
                string Query = "insert into t_mesinoperation(operation_number, id_user, id_spk, start) "+
                    " SELECT COALESCE( MAX( operation_number ) , 0 ) + 1,  " + id_user + ", " + id_spk + ", '" + timeStart + "' "+
                    " from t_mesinoperation WHERE  id_spk =" + id_spk + " ; " +
                    " select last_insert_id(); ";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetString(0);
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return hasil;
        }

        public string insertOperationNew(string id_dt_spk, string id_user, string timeStart)
        {
            string hasil = "NULL";

            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_mesin_operation from t_mesinoperation where id_dt_spk=" + id_dt_spk + " ";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    hasil = reader.GetString(0);
                }
                db.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }

            if (!hasil.Equals("NULL"))
            {
                try
                {
                    string Query = "update t_mesinoperation set start='" + timeStart + "' , id_user='"+id_user+"' "+
                        "where id_mesin_operation=" + hasil + " ;";

                    //This is command class which will handle the query and connection object.  
                    MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                    MySqlDataReader MyReader2;
                    openkoneksi();
                    MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                                                                //MessageBox.Show("Save Data Ukuran");
                    while (MyReader2.Read())
                    {
                    }
                    db.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                return hasil;
            }
                

            try
            {
                DateTime now = DateTime.Now;
                string Query = "insert into t_mesinoperation(id_user, id_dt_spk, start) " +
                    " VALUES( " + id_user + ", " + id_dt_spk + ", '" + timeStart + "' ); " +
                    " select last_insert_id(); ";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetString(0);
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return hasil;
        }

        public string getIdMesinOperation(string id_user, string id_spk)
        {
            string mesinsalah = "xxxxxxxx";
            string mesin = "";
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_mesin_operation from t_mesinoperation where id_user = " + id_user + " and id_spk = "+id_spk+";";
                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    mesin = reader.GetString(0).ToString().ToUpper();
                    return mesin;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return mesinsalah;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return mesin;
        }


        public dataUpdateStock hitungSisaStockOrder(string id_dt_spk)
        {

            dataUpdateStock hasil = new dataUpdateStock();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql= "SELECT induk.qty_order - sum(qty_awal), induk.stock_order "+
                            "from dt_spk, "+
                            "( "+
                            "  select id_dt_spk, qty_order, stock_order "+
                            "  from dt_spk " +
                            "  where id_dt_spk = " + id_dt_spk + " " +
                            ") induk " +
                            "where dt_spk.prequesite_dt_spk = induk.id_dt_spk " +
                            "and dt_spk.stock_awal = induk.stock_order ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if(!reader.IsDBNull(0))
                    {
                        hasil.increment_stok = reader.GetInt32(0);
                        hasil.id_stok = reader.GetString(1);
                    }
                    return hasil;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return hasil;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public dataUpdateStock hitungSisaStockAkhir(string id_dt_spk)
        {

            dataUpdateStock hasil = new dataUpdateStock();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT induk.qty_akhir - sum(qty_awal), induk.stock_akhir "+
                            "from dt_spk, " +
                            "( " +
                            "  select id_dt_spk, qty_akhir, stock_akhir " +
                            "  from dt_spk " +
                            "  where id_dt_spk = " + id_dt_spk + " " +
                            ") induk " +
                            "where dt_spk.prequesite_dt_spk = induk.id_dt_spk " +
                            "and dt_spk.stock_awal = induk.stock_akhir ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        hasil.increment_stok = reader.GetInt32(0);
                        hasil.id_stok = reader.GetString(1);
                    }
                    return hasil;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return hasil;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }


        public dt_spk getDataDtSpk(string id_dt_spk)
        {
            dt_spk hasil = new dt_spk();

            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select m_type.type, m_ukuran.nama_ukuran, qty_awal, dt.kg_awal,  " +
                            "   ukuran_order, dt.qty_order, dt.kg_order, dt.keterangan, m_type.id_type, m_stock.id_dt_pembelian, m_stock.harga_stock as harga " +
                            "from dt_spk dt, m_type, m_ukuran, m_stock, " +
                            " ( " +
                            "   SELECT id_dt_spk, nama_ukuran as ukuran_order " +
                            "   from dt_spk, m_ukuran " +
                            "   where dt_spk.stock_order = m_ukuran.id_ukuran " +
                            "   and dt_spk.id_dt_spk = " + id_dt_spk + " " +
                            ") q_order " +
                            "where dt.stock_awal = m_stock.id_stock " +
                            "    and m_stock.id_type = m_type.id_type " +
                            "    and m_stock.id_ukuran = m_ukuran.id_ukuran " +
                            "    and dt.id_dt_spk = q_order.id_dt_spk; ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        hasil.type_stok_awal = reader.GetString(0);
                        hasil.ukuran_awal = reader.GetString(1);
                        hasil.qty_awal = reader.GetString(2);
                        hasil.kg_awal = reader.GetString(3);
                        hasil.ukuran_order = reader.GetString(4);
                        hasil.qty_order = reader.GetString(5);
                        hasil.kg_order = reader.GetString(6);
                        if (!reader.IsDBNull(7))
                            hasil.keterangan = reader.GetString(7);
                        else
                            hasil.keterangan = "NULL";
                        hasil.id_type = reader.GetString(8);
                        if (!reader.IsDBNull(9))
                            hasil.id_dt_pembelian = reader.GetString(9);
                        else
                            hasil.id_dt_pembelian = "NULL";
                        if (!reader.IsDBNull(10))
                            hasil.harga = reader.GetString(10);
                        else
                            hasil.harga = "NULL";

                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public ukuran getUkuran(string l, string p, string t)
        {
            int namasalah = 00;
            int nama = 0;
            ukuran hasil = new ukuran();

            string faktor_hitung = "8";
            if (t == null || t.Length == 0 || (t.Length > 0 && Double.Parse(t) == 0))
            {
                t = "NULL";
                faktor_hitung = "6.2";
            }

            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "select id_ukuran, nama_ukuran, lebar, panjang, tinggi, faktor_hitung " +
                            " from m_ukuran " +
                            " where lebar=" + l + " and panjang=" + p + " and tinggi=" + t + " ";
                if (t == "NULL")
                    sql = "select id_ukuran, nama_ukuran, lebar, panjang, tinggi, faktor_hitung " +
                            " from m_ukuran " +
                            " where lebar=" + l + " and panjang=" + p + " and tinggi IS NULL ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    nama = int.Parse(reader.GetString(0).ToUpper());
                    hasil.id_ukuran = nama;
                    hasil.nama_ukuran = reader.GetString(1);
                    hasil.lebar = reader.GetInt32(2);
                    hasil.panjang = reader.GetInt32(3);
                    if (!reader.IsDBNull(4))
                        hasil.tinggi = reader.GetInt32(4);
                }

                if (nama == 0)
                {
                    reader.Close();

                    string nama_ukuran = grupUkuran(l, p, t);

                    dbcmd = db.CreateCommand();
                    sql = "insert into m_ukuran (nama_ukuran, lebar, panjang, tinggi, faktor_hitung) " +
                        "  values ('" + nama_ukuran + "', " + l + " , " + p + " , " + t + " , " + faktor_hitung + "  );  " +
                        "  select last_insert_id(); ";
                    dbcmd.CommandText = sql;
                    reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        nama = int.Parse(reader.GetString(0).ToUpper());
                        hasil.id_ukuran = nama;
                        hasil.nama_ukuran = nama_ukuran;
                        hasil.lebar = int.Parse(l);
                        hasil.panjang = int.Parse(p);
                        if (t != "NULL")
                            hasil.tinggi = int.Parse(t);
                    }
                }

                return hasil;


            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
                return hasil;

            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }

            return hasil;
        }

        public string grupUkuran(string l, string p, string t)
        {
            string gabung;
            if (t.Length > 0 && !t.Equals("NULL"))
                gabung = l + " x " + p + " x " + t;
            else
                gabung = l + " x " + p;

            return gabung;
        }


        public void getLangkahPotong(string id_dt_spk, FlowLayoutPanel lbox, FlowLayoutPanel lboxWork)
        {
            lbox.Controls.Clear();
            lboxWork.Controls.Clear();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_langkah_potong, lebar, panjang, tinggi, qty  " +
                            " FROM dt_spk_langkah_potong " +
                            " where id_dt_spk = "+id_dt_spk+" ;  ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        LangkahPotong newLangkah = new LangkahPotong();
                        WorkLangkahPotong newWork = new WorkLangkahPotong();

                        newLangkah.lo.Text = reader.GetString(1);
                        newWork.lo.Text = newLangkah.lo.Text;

                        newLangkah.po.Text = reader.GetString(2);
                        newWork.po.Text = newLangkah.po.Text;

                        if (reader[3].Equals(System.DBNull.Value))
                        {
                            newLangkah.to.Text = "";
                        }
                        else
                        {
                            newLangkah.to.Text = reader.GetString(3);
                            newWork.to.Text = newLangkah.to.Text;
                        }

                        newLangkah.qtyO.Text = reader.GetString(4);
                        newWork.qtyO.Text = "0";

                        newWork.id_langkah_potong = reader.GetString(0);

                        lbox.Controls.Add(newLangkah);
                        lboxWork.Controls.Add(newWork);

                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        public void getPlanStokAkhir(string id_dt_spk, FlowLayoutPanel lbox)
        {
            lbox.Controls.Clear();
            try
            {

                openkoneksi();
                MySqlCommand dbcmd = db.CreateCommand();
                string sql = "SELECT id_plan_stok_akhir, lebar, panjang, tinggi, qty, kg  " +
                            " FROM dt_spk_plan_stok_akhir " +
                            " where id_dt_spk = " + id_dt_spk + " ;  ";

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        StockAkhir newStokAkhir = new StockAkhir();
                        newStokAkhir.l.Text = reader.GetString(1);
                        newStokAkhir.p.Text = reader.GetString(2);
                        if (reader[3].Equals(System.DBNull.Value))
                        {
                            newStokAkhir.t.Text = "";
                        }
                        else
                        {
                            newStokAkhir.t.Text = reader.GetString(3);
                        }
                        newStokAkhir.qtyA.Text = reader.GetString(4);
                        newStokAkhir.kgA.Text = reader.GetString(5);

                        lbox.Controls.Add(newStokAkhir);
                    }
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (db != null)
                {
                    db.Close();
                }
            }
        }


        public void updateRealLangkahPotong(string id_langkah_potong, string qtyVal, string kgVal)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update dt_spk_langkah_potong set qty_real = " + qtyVal +", "+
                               "                                 kg_real = " + kgVal + "  "+ 
                               " where id_langkah_potong = " + id_langkah_potong + " ;";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void insertRealStokAkhir(string id_dt_spk, string id_mesin_operation, string l, string p, string t, string qty, string kg)
        {
            string hasil = "0";
            string Query;
            try
            {
                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");
                if (t == "")
                {
                    Query = "insert into dt_spk_real_stok_akhir (id_dt_spk,id_operation,lebar,panjang,qty,kg) " +
                        " values(" + id_dt_spk + "," + id_mesin_operation + "," + l + "," + p + "," + qty + "," + kg + "); " +
                        " select last_insert_id();";
                }
                else
                {
                    Query = "insert into dt_spk_real_stok_akhir (id_dt_spk,id_operation,lebar,panjang,tinggi,qty,kg) " +
                        " values(" + id_dt_spk + "," + id_mesin_operation + "," + l + "," + p + "," + t + "," + qty + "," + kg + "); " +
                        " select last_insert_id();";
                }

                Console.WriteLine("insert t_spk = " + Query);

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                    hasil = MyReader2.GetInt32(0).ToString();
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.Close();
            }
        }

        public void addStockOnly(string idType, string idUkuran, string idDtPembelian, string qty, string kg)
        {

            try
            {
                string Query;

                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");

                Query = "insert into m_stock(id_type,id_ukuran,id_dt_pembelian,qty_stock,kg) values(" +
                     idType + "," + idUkuran + "," + idDtPembelian + "," + qty + "," + kg + ") " +
                     " ON DUPLICATE KEY UPDATE qty_stock = qty_stock + " + qty + " , kg =  kg + " + kg + " ;";


                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void addStockOnly(string idType, string idUkuran, string idDtPembelian, string harga, string qty, string kg)
        {

            try
            {
                string Query;

                DateTime now = DateTime.Now;
                string formatForMySql = now.ToString("yyyy-MM-dd HH:mm");

                Query = "insert into m_stock(id_type,id_ukuran,id_dt_pembelian,harga,qty_stock,kg) values(" +
                     idType + "," + idUkuran + "," + idDtPembelian + "," + harga + "," + qty + "," + kg + ") " +
                     " ON DUPLICATE KEY UPDATE qty_stock = qty_stock + " + qty + " , harga="+ harga +" , kg =  kg + " + kg + " ;";


                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void increaseStock(string id_stok, int increment_stok)
        {
            try
            { 
                DateTime now = DateTime.Now; 
                string Query = "update m_stock set qty_stock = qty_stock + " + increment_stok + " where id_stock = " + id_stok + " ;";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void updateMesinOperationStop(string id_mesin_operation, string time)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_mesinoperation set finish='" + time + "' where id_mesin_operation=" + id_mesin_operation + ";";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        public void updateMesinOperationStart(string id_mesin_operation, string time)
        {
            try
            {
                DateTime now = DateTime.Now;
                string Query = "update t_mesinoperation set start='" + time + "' where id_mesin_operation=" + id_mesin_operation + ";";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand2 = new MySqlCommand(Query, db);
                MySqlDataReader MyReader2;
                openkoneksi();
                MyReader2 = MyCommand2.ExecuteReader();     // Here our query will be executed and data saved into the database.  
                //MessageBox.Show("Save Data Ukuran");
                while (MyReader2.Read())
                {
                }
                db.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


    }

    public class ukuran
    {
        public int id_ukuran { get; set; }
        public string nama_ukuran { get; set; }
        public int max_qty { get; set; }
        public int panjang { get; set; }
        public int lebar { get; set; }
        public int tinggi { get; set; }

        public string id_stock { get; set; }

        public override string ToString()
        {
            return nama_ukuran;
        }
    }

    public class dt_spk
    {
        public string type_stok_awal { get; set; }
        public string ukuran_awal { get; set; }
        public string qty_awal { get; set; }
        public string kg_awal { get; set; }

        public string ukuran_order { get; set; }
        public string qty_order { get; set; }
        public string kg_order { get; set; }

        public string keterangan { get; set; }
        public string id_type { get; set; }
        public string id_dt_pembelian { get; set; }
        public string harga { get; set; }


    }

    public class dt_spk_langkah_potong
    {
        public string l_potong { get; set; }
        public string p_potong { get; set; }
        public string t_potong { get; set; }
        public string qty_potong { get; set; }
        public string kg_potong { get; set; }

    }

    class t_po
    {
        public int id_po { set; get; }
        public int user_marketing { get; set; }
        public int id_customer { get; set; }
        public DateTime tanggal { get; set; }
        public string status { get; set; }
    }

    class stock
    {
        public string nama_barang { get; set; }
        public double panjang { get; set; }
        public double lebar { get; set; }
        public double tinggi { get; set; }
        public double diameter { get; set; }
        public string bentuk_barang { get; set; }
        public int qty { get; set; }
        public double harga { get; set; }
    }

    class supplier
    {
        public string nama_supplier { get; set; }
        public string alamat_supplier { get; set; }
        public string input_by { get; set; }
        public DateTime input_date { get; set; }
        public int telp { get; set; }

    }

    public class dataUpdateStock
    {
        public string id_stok { set; get; }
        public int increment_stok { set; get; }
    }


}
