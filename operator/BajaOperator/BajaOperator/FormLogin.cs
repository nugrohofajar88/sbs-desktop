﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaOperator
{
    public partial class FormLogin : System.Windows.Forms.Form
    {
        FormOperator _formOperator;
        koneksi konek;
        public String username = "";
        public string iduser = "";
        public FormLogin()
        {
            InitializeComponent();
            konek = new koneksi();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (konek.LoginOperator(tbusername.Text, tbpassword.Text) == true)
            {
                this.username = konek.namaOperator(tbusername);
                this.iduser = tbusername.Tag.ToString();
                this.Close();
                DialogResult = DialogResult.OK;

            }
            else
            {
                MessageBox.Show("Username atau password salah");
            }
        }
    }
}
