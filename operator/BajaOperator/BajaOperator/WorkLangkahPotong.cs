﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaOperator
{
    public partial class WorkLangkahPotong : UserControl
    {
        public string id_langkah_potong { get; set; }
        koneksi konek;

        public WorkLangkahPotong()
        {
            InitializeComponent();
            konek = new koneksi();
        }

        private void qty_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numQty = (NumericUpDown)sender;
            double p,l,t,kgVal = 0;
            double.TryParse(po.Text, out p);
            double.TryParse(lo.Text, out l);
            double.TryParse(to.Text, out t);

            kgVal = countKg(p,l,t, numQty.Value);
            kgO.Text = kgVal.ToString();
        }

        private double countKg(double p, double l, double t, decimal qtyVal)
        {
            int qty = (int)qtyVal;
            if (t > 0)
            {
                return (p / 100) * (l / 100) * (t / 100) * 8.0f * qty;
            }
            else
            {
                return (l / 100) * (l / 100) * (p / 100) * 6.2f * qty;
            }
        }

        public void saveToDB()
        {
            konek.updateRealLangkahPotong(id_langkah_potong, qtyO.Value.ToString(), kgO.Text);
        }
    }
}
