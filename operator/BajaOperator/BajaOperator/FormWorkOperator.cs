﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BajaOperator
{
    public partial class FormWorkOperator : Form
    {
        string id_spk = "";
        string id_user = "";
        koneksi konek;
        Stopwatch stopwatch;
        string mIdMesinOperation = "";
        Hashtable dataStockAkhir;
        List<string> listIdDt;

        public FormWorkOperator(string idSpk, string iduser)
        {
            InitializeComponent();
            konek = new koneksi();
            id_spk = idSpk;
            id_user = iduser;
            stopwatch = new Stopwatch();
            dataStockAkhir = new Hashtable();
            listIdDt = new List<string>();

           
        }
        
        private void FormWorkOperator_Load(object sender, EventArgs e)
        {
            viewStatus();
        }
        
        public void viewStatus()
        {
            konek.viewJob(listIdDt, dataStockAkhir, dataGridView1, id_spk);
            //foreach () { }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnFinish.Enabled = true;
            string timeStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

            mIdMesinOperation = konek.insertOperation(id_spk, id_user, timeStart);
            konek.updateMesinOperationDtSPK(string.Join(",", listIdDt), mIdMesinOperation);
            konek.updateStart(timeStart, id_spk);

            konek.updateMesinOperationStart(mIdMesinOperation, timeStart);
            stopwatch.Start();
           
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            string timeStop = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            stopwatch.Stop();
            string joinedID = string.Join(",", listIdDt);
            konek.updateStatusPengerjaanDtSPK(joinedID);
            konek.updatePrequisiteDtSPK(joinedID);

            konek.updateStop(timeStop, id_spk);
            konek.updateMesinOperationStop(mIdMesinOperation, timeStop);

            foreach(string idPrequisite in listIdDt)
            {
                //hitung sisa stokOrder, dan tambahkan ke stok
                dataUpdateStock updateOrder = new dataUpdateStock();
                updateOrder = konek.hitungSisaStockOrder(idPrequisite);
                if (updateOrder.increment_stok > 0)
                    konek.increaseStock(updateOrder.id_stok, updateOrder.increment_stok);

                //hitung sisa stokAkhir, dan tambahkan ke stok
                dataUpdateStock updateAkhir = new dataUpdateStock();
                updateAkhir = konek.hitungSisaStockAkhir(idPrequisite);
                if (updateOrder.increment_stok > 0)
                    konek.increaseStock(updateAkhir.id_stok, updateAkhir.increment_stok);
            }

            //cek and update status t_spk whether all is finished
            int countUnfinished = konek.countUnfinish(id_spk);
            if (countUnfinished == 0)
                konek.updateToFinished(id_spk);
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            label2.Text = string.Format("{0:hh\\:mm\\:ss}", stopwatch.Elapsed);
        }
    }
}
