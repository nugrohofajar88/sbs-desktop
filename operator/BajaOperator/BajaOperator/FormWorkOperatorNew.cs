﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BajaOperator.FormOperator;

namespace BajaOperator
{
    public partial class FormWorkOperatorNew : Form
    {

        string id_dt_spk = "6";
        string id_user = "";
        string id_spk = "";
        koneksi konek;
        Stopwatch stopwatch;
        string mIdMesinOperation = "";
        detailSpk currentDetailSPK;
        
        public FormWorkOperatorNew(dataDtSpk data, string iduser)
        {
            InitializeComponent();
            konek = new koneksi();

            id_spk = data.id_spk;
            id_dt_spk = data.id_dt_spk;
            id_user = iduser;
            
            stopwatch = new Stopwatch();

            string timeStart = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            mIdMesinOperation = konek.insertOperationNew(id_dt_spk, id_user, timeStart);
            Console.WriteLine("idmesin operation = " + mIdMesinOperation);
            stopwatch.Start();

            currentDetailSPK = new detailSpk(id_dt_spk, mIdMesinOperation);
            currentDetailSPK.id_mesin_operation = mIdMesinOperation;
            lbox.Controls.Add(currentDetailSPK);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label18.Text = string.Format("{0:hh\\:mm\\:ss}", stopwatch.Elapsed);
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            currentDetailSPK.saveToDB();

            string timeStop = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            stopwatch.Stop();
            konek.updateStatusPengerjaanDtSPK(id_dt_spk);

            konek.updateStop(timeStop, id_spk);
            konek.updateMesinOperationStop(mIdMesinOperation, timeStop);


            Close();
        }
    }
}
