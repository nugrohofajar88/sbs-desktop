﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace BajaOperator
{
    public partial class FormOperator : System.Windows.Forms.Form
    {
        private string username;
        private string iduser;
        koneksi konek;

        public FormOperator()
        {
            InitializeComponent();
        }

        public FormOperator(string username, string iduser)
        {
            InitializeComponent();
            this.username = username;
            this.iduser = iduser;
            konek = new koneksi();
            label1.Text += " Mesin : " + konek.mesinId;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbox.Controls.Clear();
            reloadData();
        }

        private void reloadData()
        {
            try
            {
                konek.openkoneksi();
                MySqlCommand dbcmd = konek.db.CreateCommand();
                //string sql = "select id_spk, no_spk, status_spk from t_spk where status_spk ='0' and id_mesin = " + konek.mesinId + "  ";
                string sql = "SELECT dt.id_spk, CONCAT_WS('_',  no_spk, dt.id_dt_spk ) as name, dt.id_dt_spk, dt.keterangan, m_ukuran.nama_ukuran " +
                            " from dt_spk dt, t_spk, dt_po, m_ukuran " +
                            " where status_pengerjaan = 0 " +
                            "  and id_mesin = " + konek.mesinId + " " +
                            "  and t_spk.id_spk = dt.id_spk and t_spk.id_po = dt_po.id_po and dt_po.id_ukuran = m_ukuran.id_ukuran order by id_dt_spk asc ; ";
               

                dbcmd.CommandText = sql;
                MySqlDataReader reader = dbcmd.ExecuteReader();
                int locatX = 38;
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        Button btn = new Button();
                        btn.Location = new Point(23, locatX);
                        btn.Height = 50;
                        btn.Width = 1270;
                        btn.ForeColor = Color.White;

                        dataDtSpk newTag = new dataDtSpk();
                        newTag.id_spk = reader.GetString(0);
                        newTag.id_dt_spk = reader.GetString(2);
                        btn.Tag = newTag;

                        btn.Text = reader.GetString(1).ToUpper() + " - " + reader.GetString(4).ToUpper();
                        btn.Name = "btn" + reader.GetString(1).ToUpper();
                        btn.BackColor = Color.Blue;
                        btn.Click += Btn_Click;


                        locatX = locatX + 40;
                        lbox.Controls.Add(btn);

                        //Console.WriteLine("halo" + reader.GetInt32(2));
                    }

                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (konek.db != null)
                {
                    konek.db.Close();
                }
            }
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            dataDtSpk selectedDtSpk = (dataDtSpk)((Button)sender).Tag;
            FormWorkOperatorNew workForm = new FormWorkOperatorNew(selectedDtSpk, iduser);
            workForm.FormClosed += WorkForm_FormClosed;
            workForm.Show();
            timer1.Enabled = false;

        }

        private void WorkForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            lbox.Controls.Clear();
            reloadData();
            timer1.Enabled = true;
        }

        private void FormOperator_Load(object sender, EventArgs e)
        {
            tbnamalogin.Text = username;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Diagnostics.Process.Start(Application.ExecutablePath);
        }


        public class dataDtSpk
        {
            public string id_spk { get; set; }
            public string id_dt_spk { get; set; }
        }
    }

       
    }
